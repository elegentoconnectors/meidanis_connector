<?php

include "commonM2.php";

class main extends m2
{
    public function run() {

        $xmlarray=simplexml_load_file($this->jsondefaultdatafile);
        if (!$xmlarray) {
            $this->logError('Not Valid XML document. file: ' .$this->jsondefaultdatafile);
            return 0;
        }

        $last_export['lastTimeRun']=(string)$xmlarray->last_time_export; //get last time export is made
        $last_export['lastOrderId'] = (string)$xmlarray->last_order_id_export; //get last order id
        $last_export['hasChanged'] = 0;

        $orders = $this->getOrders($last_export['lastTimeRun']); //get also the last exported order, in case there was a new order in the same second with the latest export

        $this->aasort($orders, "updated_at"); //sort based on date
        $firstorder = reset($orders); //get first order

        $last_export = $this->hasOrderChanged($orders,$last_export); //get last date time
        if ((string)$firstorder['entity_id'] === (string)$xmlarray->last_order_id_export  && $firstorder['updated_at'] === (string)$xmlarray->last_time_export) {array_shift($orders);} //remove first record if it was the same as the last order
        $xmlarray->last_time_export = 	$last_export['lastTimeRun'];//Those values will be used ONLY if save is succesfull
        $xmlarray->last_order_id_export = $last_export['lastOrderId'];

        foreach ($orders as $korders => $vorders) {

            if (file_exists($this->filestop)) {$this->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop

//            if ($vorders['status'] != 'pending' && $vorders['status'] !='processing' && $vorders['status'] !='skroutz_pending' && $vorders['status'] !='complete') continue;

            echo("....");
            $xmlarray->last_order_id_export = $vorders['order_id']; //these values will be saved ONLY if export was successful
            $xmlarray->last_increment_id_export = $vorders['increment_id'];
            $xmlarray->last_time_export = $vorders['updated_at'];

            $totalweetax = 0;


            if (is_int($korders)) { //orders are always use number for each order
                //$k='order'; //change number to text
                $orderinfo=$this->getOrder($vorders['entity_id']);

//                if($vorders['increment_id'] != '000000045'){
//                    continue;
//                }

                $this->debug ("Exporting Order: " .$vorders['increment_id'] ."updated_at = " .$vorders['updated_at'], true);
                $items=$orderinfo['items']; //get items from order
                unset($orderinfo['items']); //remove from order var

                $order = $this->order_getOrderAttributes($orderinfo); //remove unwanted variables
                $customer = null;
                
                if ($order['customer_id']) { //if order is from register load customer
                    $customer = $this->getCustomer($order['customer_id']);
                }

                if ($customer)
                    $order = array_merge($order,$this->getCustomerAttributes($customer)); //we want only the first customer
                else { //guest
                    //   $q = "select * from am_customerattr_guest where order_id = '{$vorders['order_id']}';";
                    //  $guestattr = $proxy->call($sessionId, 'innoapi_api.sqlrawread', array($q));
                    //   if ($guestattr)
                    //      $order = array_merge($order,$guestattr[0]); //we want only the first set
                }
                $parents = array();
                $i=1; //reset item counter
                $order['items'] = array();

                foreach ($items as $item =>$v) //get each item array to $v
                {
                    if ($v['product_type'] == 'configurable') {$parents[$v['item_id']]=$item; continue;} //skip configurable products


                    $productinfo = $this->getProductBySku($v['sku']);
                    if (!$productinfo) {$this->logError("Item {$v['sku']} does not exist in order {$order['increment_id']}"); return 0; } //exit now. will try to run again, next time.. //if product no longer exists ignore it

                    $v = $this->order_getitems($items,$item,$parents,$customer,$productinfo); //remove all unwanted variables
                    $totalweetax += $v['wee_tax'];
                    $order['items'][] = array_merge($v, array('increment_id' => $order['increment_id']) , array('recID' => $i));;

                    $i++;
                }

                //$xmlDocorder->total_wee_tax = $totalweetax;
                $order['total_wee_tax'] = $totalweetax;

                if (count($items)) { //if there are products in order save to db

                    echo("New Order found.. " .$vorders['increment_id'] ." Exporting now..<br>\n");
                   $jsonOrderData = json_encode($order);
                    //$jsonOrderData = str_replace('"', '\'', $jsonOrderData);
                    //$jsonOrderData = str_replace('}"', '}\'', $jsonOrderData);
                   $jsonSaved = $this->saveOrdertoDB($jsonOrderData, $order['increment_id']);

                    if ($jsonSaved) {
                        $xmlarray->asXML($this->jsondefaultdatafile); //success. Update values on order config
                    }
                    else {
                        $this->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                        return 0; //exit now. will try to run again, next time..
                    }
                }
                else {
                    $xmlarray->asXML($this->jsondefaultdatafile); //success (although no order for rdc). Update values on order config
                }

                $this->heartbeat();

                /*//print_r($order);
                //$xmlDoc = array_to_xml($orderoutput, new SimpleXMLElement("<order></order>"), $orders);
                $dom = dom_import_simplexml($xmlDoc)->ownerDocument;
                $dom->formatOutput = true;
                $domsaved = $dom->save($ordersXMLFolder .$vorders['increment_id']); //write to tmp file
                if ($domsaved) {
                    $renamed = rename($ordersXMLFolder .$vorders['increment_id'], $ordersXMLFolder .gmdate('ymd_H-i-s_', time()) .$vorders['increment_id'] .'order' .'.xml'); //rename to .xml
                    if ($renamed) {
                        $xmlarray->last_order_id_export = $vorders['order_id'];
                        $xmlarray->last_increment_id_export = $vorders['increment_id'];
                        $xmlarray->asXML($orderConfigFile);
                    }
                    else
                    logError("Export was completed but could not be saved to XML file. The order will be exported again in the next run.");
                }
                else
                logError("Export was completed but could not be saved as XML temporary file.  The order will be exported again in the next run.");

            }	*/
            }
        }
        return true;
    }

    public function hasOrderChanged($orders, $last_export)
    {
        $newest = strtotime($last_export['lastTimeRun']); //get UNIX time stamp
        $newest_orderid = $last_export['lastOrderId']; //get last order id
        $hasChanged = 0;

        foreach ($orders as $order) {
            $updated_at = strtotime($order['updated_at']);
            $order_id = $order['order_id'];

            if ($updated_at > $newest) { //if order exported is newer
                $newest = $updated_at;
                $newest_orderid = $order_id;
                $hasChanged = 1;
            } elseif ($updated_at == $newest) { //if last update is the same with current order
                if ($newest_orderid != $order_id) { //check if customer id has changed
                    $newest_orderid = $order_id;
                    $hasChanged = 1;
                }
            }
        }

        $last_export['lastTimeRun'] = date('Y-m-d H:i:s', $newest);
        $last_export['lastOrderId'] = $newest_orderid;
        $last_export['hasChanged'] = $hasChanged;
        return $last_export;
    }

    public function order_getitems($items,$item,$parents, $customer ,$productinfo = null){

        $var=$items[$item];
        $parent_id = @$parents[$items[$item]['parent_item_id']];

        $attributes = array();
        foreach ($productinfo['custom_attributes'] as $attribute) {
            $attributes[$attribute['attribute_code']] = $attribute['value'];
        }

        $minsaleqty = isset($attributes['packages']) ? floatval($attributes['packages']) : 1;

        $customerGroupId = $customer['group_id'];
        $vatPercentage = $var['tax_percent'];
        $vatToDivideWith = 1 + ($vatPercentage / 100);
        foreach($productinfo['tier_prices'] as $tierPrice){
            if($tierPrice['customer_group_id'] == $customerGroupId){
                $tierPercentage = $tierPrice['extension_attributes']['percentage_value'];
                $tierPriceAmount = (($var['base_original_price'] / $vatToDivideWith) * $var['qty_ordered']);
                $tierPriceAmount = number_format($tierPriceAmount * ($tierPercentage / 100), 4, '.','');
                break;
            }
        }



        $out = array();

        $out['item_sku'] = $var['sku'];
        $out['item_barcode'] = $attributes['barcode'];
        $out['item_name'] = $var['name'];
        $out['item_qty_ordered'] = $var['qty_ordered']*$minsaleqty;
        $out['item_id'] = $var['item_id'];


        if ($productinfo) {
            $out['item_erpid'] = @$attributes['erpid'];
            $out['item_original_price'] = $productinfo['price']/$minsaleqty; //original price without catalog, cart rules and special price
           // if(isset($tierPercentage)){ //if b2b customer then base amount is the original price without tax
                $out['item_original_price_without_tax'] = number_format(($productinfo['price']/$minsaleqty) / $vatToDivideWith, 4, '.','');
          //  }
//            else{
//                $out['item_original_price_without_tax'] = (isset($attributes['special_price']) && $attributes['special_price'] != 0) ? number_format((floatval($attributes['special_price'])/$minsaleqty) / $vatToDivideWith, 4, '.','') : number_format(($productinfo['price']/$minsaleqty) / $vatToDivideWith, 4, '.','');
//            }
            $out['special_price']  = $attributes['special_price']/$minsaleqty;
            $out['special_price_without_tax'] = number_format(($attributes['special_price']/$minsaleqty) / $vatToDivideWith, 4, '.','');
            $out['item_discount_zone_prc'] = isset($tierPercentage) ? $tierPercentage : 0;
            //$out['item_discount_zone_amt'] = isset($tierPriceAmount) ? $tierPriceAmount : 0;
            $out['row_discount_zone_amt'] =  isset($tierPriceAmount) ? $tierPriceAmount : 0;
            //  $out['item_manufacturer'] = $productinfo['manufacturer'];
        }

        $out['fpt'] = (isset($attributes['fpt']) && !empty($attributes['fpt'])) ? $attributes['fpt'][0]['value'] * $var['qty_ordered'] : '0';

        if ($parent_id !== null) {
            $out['item_price'] = $items[$parent_id]['price_incl_tax']/$minsaleqty; //no discounts from rules here (only special price)
            $out['item_tax_percent'] = $items[$parent_id]['tax_percent'];
            $out['item_discount_amount'] = $items[$parent_id]['discount_amount']/$minsaleqty;
            $out['item_discount_percent'] = $items[$parent_id]['discount_percent'];
            $out['item_row_total'] = number_format($items[$parent_id]['row_total_incl_tax'], 4, '.','');
        } else {
            $out['item_price'] = $var['price_incl_tax']/$minsaleqty; //no discounts from rules here (only special price)
            $out['item_tax_percent'] = $var['tax_percent'];
            $out['item_discount_amount'] = $var['discount_amount']/$minsaleqty;
            $out['item_discount_percent'] = $var['discount_percent'];
            $out['item_row_total'] = number_format($var['row_total_incl_tax'], 4, '.','');
        }

        //TASK-15743 change way of calculating tax amount due to config change of TASK-15520. Due to that change, tax amount field provided by Magento's API is now calculated on pre-coupon-discount price and not after. This causes issues at ERP system order values.
        $taxAmount = number_format(($out['item_row_total'] - $out['fpt']) - (($out['item_row_total'] - $out['fpt']) / $vatToDivideWith),4, '.','');

        $out['item_tax_amount'] = ($taxAmount/$out['item_qty_ordered']);
        $out['item_row_tax_amount'] = number_format($taxAmount, 4, '.','');

        $out['item_discount_amount_without_tax'] = number_format($out['item_discount_amount'] / ($vatToDivideWith * $out['item_qty_ordered']), 4, '.','');
        $out['row_discount_amount_without_tax'] = number_format((($var['discount_amount'])   / $vatToDivideWith), 4, '.','');

        $out['wee_tax'] = $attributes['weetaxfixed']/$minsaleqty;
        $out['fpt'] = (isset($attributes['fpt']) && !empty($attributes['fpt'])) ? $attributes['fpt'][0]['value'] * $var['qty_ordered'] : '0';
        $out['net_price_for_erp'] = number_format($out['item_row_total'] - $out['fpt'] - $out['item_row_tax_amount'],4, '.','');
        //$out['item_discount_amount'] = $out['item_original_price'] - $out['item_price'];
        //$out['item_discount_percent'] = ($out['item_discount_amount']/$out['item_original_price'])*100;

        if($attributes['special_price'] && ($out['special_price_without_tax'] < ($out['item_original_price_without_tax'] - (floatval($out['row_discount_zone_amt']) / ($var['qty_ordered'] * $minsaleqty))))){
            $out['base_amount'] = $out['special_price_without_tax'];
            $out['item_discount_zone_prc'] = 0;
            $out['row_discount_zone_amt'] = 0;
        }
        else{
            $out['base_amount'] = $out['item_original_price_without_tax'];
        }

        if(isset($attributes['max_discount'])){
            $out['max_discount'] = $attributes['max_discount'];
        }
        return $out;
    }


    public function order_getOrderAttributes($var){
        $out = array();
        $out['customer_id'] = $var['customer_id'];
        $out['updated_at'] = $var['updated_at'];
        $out['created_at'] = $var['created_at'];
        $out['increment_id'] = $var['increment_id'];
        $out['order_id'] = $var['entity_id'];
        $out['status'] = $var['status'];
        $out['grand_total'] = number_format($var['grand_total'], 2, ".", "");
        $out['tax_amount'] = $var['tax_amount'];
        $out['subtotal'] = $var['subtotal'];
        $out['discount_amount'] = $var['discount_amount'];
        $out['shipping_amount'] = $var['shipping_incl_tax'];
        $out['shipping_method'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['method'];
        $out['shipping_description'] = $var['shipping_description'];
        $out['payment_method'] = $var['payment']['method'];
        $payment_description = '';
        foreach ($var['extension_attributes']['payment_additional_info'] as $info){
            if ($info['key'] == 'method_title'){
                $payment_description = $info['value'];
            }
        }
        $out['payment_description'] = $payment_description;
        $out['cod_fee'] = $var['grand_total'] - $var['shipping_incl_tax'];
        $out['cod_fee'] = floatval(number_format($out['cod_fee'], 4, ".", "")) - floatval(number_format($var['subtotal_incl_tax'], 4, ".", ""));
        if($var['discount_amount']){
            $out['cod_fee'] = floatval(number_format($out['cod_fee'], 4, ".", "")) - floatval(number_format($var['discount_amount'],4, ".", ""));
        }
        if ($out['cod_fee'] < 0) {$out['cod_fee'] = 0;}



        $out['billing_postcode'] = $var['billing_address']['postcode'];
        $out['billing_lastname'] = $var['billing_address']['lastname'];
        $out['billing_street'] = implode(", ",$var['billing_address']['street']);
        $out['billing_city'] = $var['billing_address']['city'];
        $out['billing_country_id'] = $var['billing_address']['country_id'];
        $out['billing_telephone'] = $var['billing_address']['telephone'];
        $out['billing_fax'] = $var['billing_address']['fax'];
        $out['billing_firstname'] = $var['billing_address']['firstname'];
        $out['billing_region'] = $var['billing_address']['region'];
        $out['billing_region_code'] = $var['billing_address']['region_code'];
        $out['billing_region_id'] = $var['billing_address']['region_id'];
        $out['shipping_postcode'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['postcode'];
        $out['shipping_lastname'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname'];
        $out['shipping_street'] = implode(", ",$var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['street']);
        $out['shipping_city'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['city'];
        $out['shipping_telephone'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['telephone'];
        $out['shipping_fax'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['fax'];
        $out['shipping_firstname'] = $var['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'];
        $out['customer_lastname'] = $var['customer_lastname'];
        $out['customer_firstname'] = $var['customer_firstname'];
        $out['customer_email'] = $var['customer_email'];
        $out['coupon_code'] = $var['coupon_code'];
        $out['customer_group_id'] = $var['customer_group_id'];

        foreach ($var['extension_attributes']['amasty_order_attributes'] as $amorderattr) {
            $out['am_' .$amorderattr['attribute_code']] = $amorderattr['value'];
        }

        $out['custom_option'] = "";
        foreach ($var['extension_attributes']['custom_options'] as $customoptions){
            foreach($customoptions['items'] as $custom_option) {
                $out['custom_option'] .= $custom_option['option_label'] . ":" . $custom_option['option_value'] . " ";
            }
        }

        /*if (isset($var['extension_attributes']['vat_number']) && $var['extension_attributes']['vat_number']) {
            $out['am_isinvoice'] = 40; //force invoice
            $out['vat_number'] = $var['extension_attributes']['vat_number'];
            $out['tax_address'] = $var['extension_attributes']['tax_address'] ?: "-";
            $out['company_name'] = $var['extension_attributes']['company_name'] ?: "-";
            $out['company_address'] = $var['extension_attributes']['company_address'] ?: "-";
            $out['company_occupation'] = $var['extension_attributes']['company_occupation'] ?: "-";
        }*/

        @$out['gift_message'] = $var['gift_message'];
        //@$out['comments'] = $var['order_comment'];
        $out['comments'] = $out['am_comments'];
        $previous = '';
        foreach ($var['status_histories'] as $history) {
            if ($history['comment']) {
                if ($previous != $history['comment']) {
                    if(stripos($history['comment'], 'Κωδικός συναλλαγής') !== false){
                        $transcactionId = explode('Κωδικός συναλλαγής: ', $history['comment'])[1];
                        $out['transaction_id'] = $transcactionId;
                    }
                    elseif(stripos($history['comment'], 'ID Συναλλαγής') !== false){
                        $transcactionId = explode('ID Συναλλαγής: ', $history['comment'])[1];
                        $out['transaction_id'] = $transcactionId;
                    }
                }
                $previous = $history['comment'];
            }
        }


        return $out;
    }
    public function getCustomerAttributes($var) {
        $out = array();
        $out['customer_id'] = $var['id'];
        $out['customer_created_at'] = $var['created_at'];
        $out['customer_updated_at'] = $var['updated_at'];
        //$out['customer_increment_id'] = $var['increment_id'];
        $out['customer_store_id'] = $var['store_id'];
        $out['customer_website_id'] = $var['website_id'];
        $out['customer_email'] = $var['email'];
        $out['customer_firstname'] = $var['firstname'];
        $out['customer_lastname'] = $var['lastname'];
        $out['customer_group_id'] = $var['group_id'];
        $out['customer_default_billing'] = $var['default_billing'];
        $out['customer_default_shipping'] = $var['default_shipping'];
        // @$out['isinvoice'] = $var['isinvoice'];
        // @$out['companyname'] = $var['companyname'];
        // @$out['companyactivity'] = $var['companyactivity'];
        // @$out['vatid'] = $var['vatid'];

        foreach($var['custom_attributes'] as $attr){
            if($attr['attribute_code'] == 'customer_payterm_code'){
                $out['customer_payterm_code'] = $attr['value'];
            }
        }

        return $out;
    }

}
//##############################################3
error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);


//$main = new m2();
$main = new main();
//$main = $main;

$main->table = "connector_orders"; //###
$main->entity = 'OrdersExport'; //###
$main->blockingentities = array("OrdersImport");


echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

//$main->sets = $main->config['set'];
//$main->languages = $main->config['languages'];


//########################################################
if ($main->canRun()) { //check if previous connection is running
    $main->heartbeat(); //start heartbeat

    if($main->config && $main->init_db()) {
        //file_put_contents($fileruntime, "operation started at " .gmdate(DATE_RFC822, time()));
        //$main->items = $main->getReproccessedXmlData(); //get reproccesed data up to 3 times //read first the reprocesede
        //$main->items = array_merge($main->items,$main->getUnproccessedXmlData()); //merge new files
        if ($main->config) {
            if ($main->init_connector())
                $main->run();
        }
    }
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail

   // unlink($main->fileruntime);
    $main->allowRun();
    $main->close_connector();
}
else        {
    $main->logError( "previous synchronization is running.");
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail
    $main->close_connector();
    return 0;
}
echo "End {$main->entity} \n\r";
//########################################################


