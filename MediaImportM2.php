<?php
include "commonM2.php";
class main extends m2 {

    protected $media_folder = '/media/';

    public function getUnproccessedFiles($media_folder) {
        $process = array();
        $files = array_diff(scandir($media_folder), array('..', '.', 'uploaded', 'non_existent_products'));
        foreach ($files as $file) {
            $fileIsBeingUploaded = $this->checkIfFileIsBeingUploaded($file);
            if($fileIsBeingUploaded){
                $this->logError('files are uploading to connector\'s server right now. exiting. ');
                return false; //files are being uploaded - stop image import
            }
            $fileToLower = strtolower($file);
            $data = explode(".jpg", $fileToLower); //remove extension from file
            $data = explode("_", $data[0]);
            $erpCode = $data[0];
            $sku = $erpCode;
            $position = isset($data[1]) ? $data[1] : '1';
            $filename = $media_folder .$file;
            //if (count($data) >= 2){
                $process[] = array("filename" => $filename, "orig_filename"=> $file, "sku" => $sku, "position" => $position, "title" => $position);
           // }
        }
        return $process;
    }

    function moveMedia($uploaded, $file) {
        return rename($file['filename'], $uploaded .$file['orig_filename']);
    }

    public function checkIfFileIsBeingUploaded($filename){
        $filepath = dirname(__FILE__) .$this->media_folder. $filename;
        $filetime = filemtime($filepath);
        $currentTime = time();
        //$lastModifiedDatetime = date("d M Y H:i:s", $filetime);
        if ($currentTime - $filetime > 3) { //check if timestamp of last modification of file is different enough from current timestamp - this means file has completely uploaded to server
            return false;
        }
        return true;
    }

    public function mediaImport($imagesData){
        foreach($imagesData as $imageData){
            $this->heartbeat(); //IMPORTANT in order to avoid duplicate runs
            $sku = $imageData['sku'];
            $searchCriteriaArray = array();
            $searchCriteriaArray[] = ['sku', 'eq', $sku];
            $result = $this->get('products/',$searchCriteriaArray); //search if product exists
            if($result['total_count'] > 0){
                $name = $this->makeCodeFromString($result['items'][0]['name']); //name to use for image filename
                $images = $this->getProductMediaBySku($sku);
                $this->findAndDeleteDuplicates($images, $sku); //double check and delete duplicate images if exists
                $image = $this->makeImageData($imageData,$sku, $name);
                $imageexists = $this->filterImageByPosition($images, $imageData['position']);
                $uploaded = false;
                $equalBase64 = $this->checkIfBase64IsEqual($imageexists, $image); //check if image is indeed identical
                if ($imageexists && !$equalBase64) {
                    $deleted = $this->deleteProductMediaBySku($sku, $imageexists['id']);//add the updated one
                    if($deleted && !$this->imageExists($sku, $imageData['position'])){ //double check if image with position X does not exist after delete
                        $uploaded = $this->addProductMediaBySku($sku, $image);
                    } else {
                        $this->logError("failed delete for {$imageData['filename']} ");
                    }
                }
                elseif(!$imageexists) {
                    $uploaded = $this->addProductMediaBySku($sku, $image);
                }
                elseif($equalBase64){   //existing image - move it to uploaded at once
                    $this->logError('file with name: ' .$imageData['filename'] .' is already uploaded. ');
                    $uploaded = true;
                }
                if(boolval($uploaded)){
                    $productImages = $this->getProductMediaBySku($sku);
                    $imageexists = $this->filterImageByPosition($productImages, $imageData['position']);
                    $existingImageJsonData = json_encode($imageexists);
                    $importedImageJsonData = json_encode($image);
                    //$this->debug("Imported Image FileName = {$image['filename']} | Imported image data: $importedImageJsonData | Existing image data final check before moving to uploaded : $existingImageJsonData");
                    $equalBase64 = $this->checkIfBase64IsEqual($imageexists, $image);
                    if($imageexists && $equalBase64){ //double check if image has been successfully uploaded
                        $this->moveMedia(dirname(__FILE__) .$this->media_folder ."uploaded/", $imageData);
                    }
                }
            }
            else{
                //$this->logError('error occured: product with sku ' .$sku .' does not exist. Skipping...');
                continue;
            }
        }
        return true;
    }

    public function run() {
        $files = $this->getUnproccessedFiles(dirname(__FILE__) .$this->media_folder);
        if($files !== false && count($files)){
            $this->mediaImport($files);
        }
    }


}

error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);

$main = new main();
$main->entity = 'ProductMediaImport';
$main->blockingentities = array("ProductImport","ProductStockImport","connector_prices","connector_media");

echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

if ($main->canRun()) { //check if previous connection is running
    $main->heartbeat(); //start heartbeat

    if($main->config) {
        if ($main->init_connector()){
            $main->run();
        }
    }
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail

    $main->allowRun();
    $main->close_connector();
}

else{
    $main->logError( "previous synchronization is running.");
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail
    $main->close_connector();
    return 0;
}
echo "End {$main->entity} \n\r";