<?php

include "commonM2.php";

class main extends m2 {

	public function run() {

		foreach ($this->items as $item) {

			$this->item = $item;

			//read one by one all files
			$this->debug("Parsing record id: ". $item['id'] ."<br>\n");
			if (file_exists($this->filestop)) {$this->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            $returnValue = $this->LoadData();
			if ($returnValue) //if created-updated
			{
                if($returnValue === true){
                    $returnValue = 1;
                }
				$this->setXmlDataStatus($item['id'] ,$returnValue); //set as processed succesfully
			}
			else
			{
				//logError("Error Parsing file: " .$file);
				if ($item['status'] == self::STATUSUNPROSSESSED) {
					//if this was the first time we process data retry on next run
					$this->setXmlDataStatus($item['id'], self::STATUSRETRY);
					$this->setXmlDataRetries($item['id'], "1"); //first try
				}
				else { //status 2, RETRY
					if ($item['status'] == self::STATUSRETRY && $item['retries'] < $this->retries) {
						$item['retries']++; //increase retries
						$this->setXmlDataStatus($item['id'], self::STATUSRETRY);
						$this->setXmlDataRetries($item['id'], $item['retries']); //
					} else {
						$this->setXmlDataStatus($item['id'], self::STATUSERROR);
					}
				}
			}

            $this->heartbeat();

			if (!is_null($this->errors)) {
				$this->setXmlDataError($item['id'], $this->errors);
				$this->senderrormail();
			}

		}
		return true;
	}

	public function LoadData() { //reads the $productsxml file and creates-updates products
		try {


            $jsonData = $this->item['data'];
            $value = json_decode($jsonData, true);

            if ($this->areDataChangedFromLastUpdate($this->item, array('erp_upd_date'), false) === false)
            {
                return 5; //ignore if data are not changed and set status = 5
            }

			$return_val = 1;

            $flag = 1; //reset flag
            $errors = null; //delete error log


            $orderinfo = $this->getOrderByIncrementId($value['orderId']);

            if ($orderinfo) {
                $orderStatusBeforeActions = $orderinfo['status'];
                $magentoShippingMethod = $orderinfo['extension_attributes']['shipping_assignments'][0]['shipping']['method'];
                $isPickupFromStore = stripos($magentoShippingMethod, 'pickupfromstore');

                $this->debug("order status importing: order = {$value['orderId']}; {$this->table} id=" .$this->item['id'] ."<br />");
                try {
                    $data = $this->remapStatusData($value, $orderinfo);
                    if(isset($data['comment'])){
                        $commentToAdd = $data['comment'];
                        unset($data['comment']);
                    }

                    if(!empty($data)){

                        $orderCommentData = array();
                        $orderCommentData['statusHistory'] = array();
                        $orderCommentData['statusHistory']['is_visible_on_front'] = 0;
                        $orderCommentData['statusHistory']['parent_id'] = 0;

                        if (stripos($data['type_id'], "shipped") !== false) {
                            if (isset($data['invoiceitems'])) {
                                $data['items'] = $data['invoiceitems'];
                                unset($data['invoiceitems']);
                                $flag &= boolval($this->addOrderInvoice($data['order_id'], $data));
                            }

                            if (isset($data['shippeditems'])) {
                                $data['items'] = $data['shippeditems'];
                                unset($data['shippeditems']);
                                //$data['notify'] = true;
                                $flag &= boolval($this->addOrderShipping($data['order_id'], $data));
                            }

                            $orderCommentData['statusHistory']['comment'] = 'Order completed';
                            $orderCommentData['statusHistory']['status'] = 'complete';
                            $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                            if($commentAdded === false){
                                $flag = 0;
                            }

                            if(isset($commentToAdd) && $orderStatusBeforeActions != 'complete' && $flag && $value['DELIVARY_STATUS'] != 'NoEmail'){
                                $orderCommentData['statusHistory']['comment'] = $commentToAdd;
                                $orderCommentData['statusHistory']['status'] = 'complete';
                                $orderCommentData['statusHistory']['is_visible_on_front'] = 0;
                                $orderCommentData['statusHistory']['parent_id'] = 0;
                                $orderCommentData['statusHistory']['is_customer_notified'] = 1;
                                $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                                if($commentAdded === false){
                                    $flag = 0;
                                }
                            }
                        }
                        elseif($data['type_id'] == "partially_shipped"){
                            if (($isPickupFromStore === false) && isset($data['invoiceitems'])) {
                                $data['items'] = $data['invoiceitems'];
                                unset($data['invoiceitems']);
                                $flag &= boolval($this->addOrderInvoice($data['order_id'], $data));
                            }

                            if (($isPickupFromStore === false) && isset($data['shippeditems'])) {
                                $data['items'] = $data['shippeditems'];
                                unset($data['shippeditems']);
                                //$data['notify'] = true;
                                $flag &= boolval($this->addOrderShipping($data['order_id'], $data));
                            }

                            $orderCommentData['statusHistory']['comment'] = 'Order partially shipped';
                            $orderCommentData['statusHistory']['status'] = 'partially_shipped';
                            $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                            if($commentAdded === false){
                                $flag = 0;
                            }
                        }
                        elseif ($data['type_id'] == "pending_shipment") {

                            if (($isPickupFromStore === false) && isset($data['invoiceitems'])) {
                                $data['items'] = $data['invoiceitems'];
                                unset($data['invoiceitems']);
                                $flag &= boolval($this->addOrderInvoice($data['order_id'], $data));
                            }
                            $orderCommentData['statusHistory']['comment'] = 'Order pending shipment';
                            $orderCommentData['statusHistory']['status'] = 'pending_shipment';
                            $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                            if($commentAdded === false){
                                $flag = 0;
                            }

                        }
                        elseif ($data['type_id'] == "partially_collected") {
                            if (($isPickupFromStore === false) && isset($data['invoiceitems'])) {
                                $data['items'] = $data['invoiceitems'];
                                unset($data['invoiceitems']);
                                $flag &= boolval($this->addOrderInvoice($data['order_id'], $data));
                            }
                            $orderCommentData['statusHistory']['comment'] = 'Order partially collected';
                            $orderCommentData['statusHistory']['status'] = 'partially_collected';
                            $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                            if($commentAdded === false){
                                $flag = 0;
                            }
                        }

                        elseif ($data['type_id'] == "pending_collection") {
                            $orderCommentData['statusHistory']['comment'] = 'Order pending collection';
                            $orderCommentData['statusHistory']['status'] = 'pending_collection';
                            $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                            if($commentAdded === false){
                                $flag = 0;
                            }
                        }

                        elseif ($data['type_id'] == "cancel") {
                            $flag &= boolval($this->cancelOrder($data['order_id']));
                            if(isset($commentToAdd) && $flag && $value['DELIVARY_STATUS'] != 'NoEmail'){
                                $orderCommentData['statusHistory']['comment'] = $commentToAdd;
                                $orderCommentData['statusHistory']['status'] = 'canceled';
                                $orderCommentData['statusHistory']['is_visible_on_front'] = 0;
                                $orderCommentData['statusHistory']['parent_id'] = 0;
                                $orderCommentData['statusHistory']['is_customer_notified'] = 1;
                                $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                                if($commentAdded === false){
                                    $flag = 0;
                                }
                            }
                            if(isset($data['skus_for_stock_update']) && !empty($data['skus_for_stock_update'])){
                                $this->forceStockUpdate($data['skus_for_stock_update']);
                            }
                        }
                        elseif ($data['type_id'] == "credit") {
                            if (isset($data['credititems'])) {
                                $data['items'] = $data['credititems'];
                                unset($data['credititems']);
                                $flag &= boolval($this->creditOrder($data['order_id'], $data));

                                if(isset($commentToAdd) && $flag && $value['DELIVARY_STATUS'] != 'NoEmail'){
                                    $orderCommentData['statusHistory']['comment'] = $commentToAdd;
                                    $orderCommentData['statusHistory']['status'] = 'closed';
                                    $orderCommentData['statusHistory']['is_visible_on_front'] = 0;
                                    $orderCommentData['statusHistory']['parent_id'] = 0;
                                    $orderCommentData['statusHistory']['is_customer_notified'] = 1;
                                    $commentAdded = boolval($this->addOrderComment($orderinfo['entity_id'], $orderCommentData));
                                    if($commentAdded === false){
                                        $flag = 0;
                                    }
                                }
                            }
                            if(isset($data['skus_for_stock_update']) && !empty($data['skus_for_stock_update'])){
                                $this->forceStockUpdate($data['skus_for_stock_update']);
                            }
                        }
                    }

                } catch (Exception $fault ) {
                        $this->logError($fault);
                        $flag = 0;
                }
            }
            else{
                return 0; //something went wrong if order was not found at magento
            }

            if ($flag) {
                $this->debug("Updating: ORDER STATUS = {$value['orderId']} => {$data['type_id']}; was succesful<br />");
            }

            $return_val &=  $flag; //flag for checking for errors


			return $return_val;
		} catch( SoapFault $fault ) {
			$this->logError($fault);
			return 0;
		}
	}

    public function getSLMPickupDate(){
        date_default_timezone_set('Europe/Athens');
        $date = date('H:i');
        if($date < '14:00'){
            return date('Y-m-d');
        }
        else{
            $datetime = new DateTime('tomorrow');
            return $datetime->format('Y-m-d');
        }
    }

	public function remapStatusData($data, $orderinfo){
	    $out = array();
        $orderStatusBeforeActions = $orderinfo['status'];
//        if ($data['status'] == 1) { //FOR CHECK, do nothing
//            return $out;
//        }


        $hasinvoiceditems = false;

	    foreach ($orderinfo['items'] as $item) {
            $invoiceqty = $item['qty_ordered'] - $item['qty_invoiced'];

            if ($item['qty_invoiced'] > 0) $hasinvoiceditems = true; //if at least one item has invoiced

	        if ($invoiceqty > 0) {
                $out['invoiceitems'][] = array('order_item_id' => $item['item_id'], 'qty' => $invoiceqty);
            }

            $shippedqty = $item['qty_ordered'] - $item['qty_shipped'];
            if ($shippedqty > 0) {
                $out['shippeditems'][] = array('order_item_id' => $item['item_id'], 'qty' => $shippedqty);
            }
            $creditqty = $item['qty_ordered'] - $item['qty_refunded'];
            if ($creditqty > 0) {
                $out['credititems'][] = array('order_item_id' => $item['item_id'], 'qty' => $creditqty);
            }
        }

	    $out['notify'] = false;
//	    if (isset($data['comment']) && $data['comment']) {
//            $out['appendComment'] = 'true';
//            $out['comment'] = array('comment' => $data['comment'], 'is_visible_on_front' => 0);
//        }

	    if ($data['VOUCHER_NO'] && trim($data['VOUCHER_NO']) != '') {
            $title = '';
	        if(isset($data['SHIPPING_CODE'])){
	            if($data['SHIPPING_CODE'] == '331'){
	                $title = 'Speedex';
                }
	            elseif($data['SHIPPING_CODE'] == '337'){
                    $title = 'Courier Center';
                }
            }
            $out['tracks'][] = array(
                'track_number'=> $data['VOUCHER_NO'],
                'title' => $title,
                'carrier_code' => isset($data['carrierName']) ? $data['carrierName'] : 'custom');
        }
        elseif($data['SHIPPING_CODE'] == 335){ //335 means SLM
            $pickupDate = $this->getSLMPickupDate();
            $out['tracks'][] = array(
                'track_number'=> 'generate',
                'title' => '{"pickup_notes":"","pickup_date":"'.$pickupDate .'","pickup_window":"afternoon"}',
                'carrier_code' => 'skroutzpoint'
            );
        }

	    if($data['ORDER_STATUS'] == 5 && trim($data['VOUCHER_NO']) == '' && ($data['COURIER_STATUS'] == "00" || $data['COURIER_STATUS'] == "00.0") && $data['SHIPPING_CODE'] != 335){
	        return array(); //wait for voucher number
        }

	    if($data['ORDER_STATUS'] == 5 && trim($data['VOUCHER_NO']) == '' && (!isset($data['TRANSPORTER']) || trim($data['TRANSPORTER'] == "") || trim($data['TRANSPORTER'] == "#")) && trim($data['DELIVARY_STATUS']) != "PickUpFromBranch" && $data['SHIPPING_CODE'] != 335){
            $out['type_id'] = "shipped_meidanis";
            $out['comment'] = "Γεια σου 👋
            
            Η παραγγελία σου {$orderinfo['increment_id']} απεστάλη! Θα παραδοθεί με δικά μας μέσα σε σύντομο χρονικό διάστημα.
            
            Ευχαριστούμε για την προτίμησή σου 🙂
            ";
        }
	    elseif($data['ORDER_STATUS'] == 5 && trim($data['VOUCHER_NO']) == '' && isset($data['TRANSPORTER']) && trim($data['TRANSPORTER']) != "" && trim($data['DELIVARY_STATUS']) != "PickUpFromBranch" && $data['SHIPPING_CODE'] != 335){
	        $transporterInfo = explode("#", $data['TRANSPORTER']);
	        $transporterName = $transporterInfo[0];
	        $transporterTel = $transporterInfo[1];
            $out['type_id'] = "shipped_transporter";
            $out['comment'] = "Γεια σου 👋
            
            Η παραγγελία σου {$orderinfo['increment_id']} απεστάλη! Θα παραδοθεί με τη μεταφορική εταιρία $transporterName.
            
            Τηλέφωνο επικοινωνίας μεταφορικής εταιρίας: $transporterTel
            
            Ευχαριστούμε για την προτίμησή σου 🙂
            ";
        }

        $out['order_id'] = $orderinfo['entity_id'];


	    if(!isset($out['comment'])){
            if($data['ORDER_STATUS'] == 1){
                $out['type_id'] = "pending_collection";
            }
            elseif($data['ORDER_STATUS'] == 2){
                $out['type_id'] = "partially_collected";
            }
            elseif($data['ORDER_STATUS'] == 3){
                $out['type_id'] = "pending_shipment";
            }
            elseif($data['ORDER_STATUS'] == 4){
                $out['type_id'] = "partially_shipped";
            }
            elseif($data['ORDER_STATUS'] == 5){
                $out['type_id'] = "shipped";
            }
            elseif($data['ORDER_STATUS'] == 99){
                $out['skus_for_stock_update'] = $this->getOrderItemsForStockForceUpdate($orderinfo);
                if ($hasinvoiceditems) {
                    $out['type_id'] = "credit";
                    if($orderStatusBeforeActions == 'processing'){
                        $out['comment'] = "Γεια σου 👋
            
                         Θα θέλαμε να σε ενημερώσουμε ότι έχουμε προχωρήσει στην ακύρωση της παραγγελίας.
            
                         Επίσης θα γίνει επιστροφή του ποσού, θα εμφανιστεί στον λογαριασμό σου τις επόμενες 3-7 εργάσιμες.
            
                         Ευελπιστούμε την επόμενη φορά να σε εξυπηρετήσουμε πιο αποτελεσματικά.
                         
                         Στη διάθεσή σου για ό,τι άλλο χρειαστείς.";
                    }
                    elseif($orderStatusBeforeActions == 'pending_shipment' || $orderStatusBeforeActions == 'partially_collected'){
                        $out['comment'] = "Γεια σου 👋
            
                         Θα θέλαμε να σε ενημερώσουμε ότι έχουμε προχωρήσει στην ακύρωση της παραγγελίας.
            
                         Ευελπιστούμε την επόμενη φορά να σε εξυπηρετήσουμε πιο αποτελεσματικά.
                         
                         Στη διάθεσή σου για ό,τι άλλο χρειαστείς.";
                    }
                } else {
                    $out['type_id'] = "cancel";
                    if($orderStatusBeforeActions == 'pending' || $orderStatusBeforeActions == 'pending_shipment'){
                        $out['comment'] = "Γεια σου 👋
            
                         Θα θέλαμε να σε ενημερώσουμε ότι έχουμε προχωρήσει στην ακύρωση της παραγγελίας.
            
                         Ευελπιστούμε την επόμενη φορά να σε εξυπηρετήσουμε πιο αποτελεσματικά.
                         
                         Στη διάθεσή σου για ό,τι άλλο χρειαστείς.";
                    }
                }
            }
        }


	    return $out;

    }

    public function getOrderItemsForStockForceUpdate($order){
	    $skus = [];
        foreach ($order['items'] as $item) {
            $skus[] = $item['sku'];
        }
        return $skus;
    }
}
//##############################################3
error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);


//$main = new m2();
$main = new main();
//$main = $main;

$main->table = "connector_orders_status"; //###
$main->entity = 'ProductOrderImport'; //###
$main->blockingentities = array();


echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

//$main->sets = $main->config['set'];
//$main->languages = $main->config['languages'];


//########################################################
if ($main->canRun()) { //check if previous connection is running
	$main->heartbeat(); //start heartbeat

	if($main->config && $main->init_db()) {
		//file_put_contents($fileruntime, "operation started at " .gmdate(DATE_RFC822, time()));
        $main->items = $main->getReproccessedJsonData(); //get reproccesed data up to 3 times //read first the reprocesede
        $main->items = array_merge($main->items,$main->getUnproccessedJsonData()); //merge new files
		if ($main->config && count($main->items)) {
			if ($main->init_connector())
				$main->run();
		}
	}
	if ($main->errors)
		$main->senderrormail(); //sent if any errors mail

	//unlink($main->fileruntime);
    $main->allowRun();
	$main->close_connector();
}
else        {
	$main->logError( "previous synchronization is running.");
	if ($main->errors)
		$main->senderrormail(); //sent if any errors mail
	$main->close_connector();
	return 0;
}
echo "End {$main->entity} \n\r";
//########################################################


//######################################################################################



