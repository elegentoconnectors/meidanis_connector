<?php

include "commonM2.php";

class main extends m2 {

    public function mapColorTemperature($colorTemp){
        $thermoLeuko = 14645;
        $fusikoLeuko = 22907;
        $psuxroLeuko = 14647;

        if(is_numeric($colorTemp)){
            if($colorTemp >= 2000 && $colorTemp <= 3500){
                return $thermoLeuko;
            }
            elseif($colorTemp >= 3700 && $colorTemp <= 4200){
                return $fusikoLeuko;
            }
            elseif($colorTemp >= 5000 && $colorTemp <= 6500){
                return $psuxroLeuko;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }

    public function formatPackageDesc($erpPackageDesc, $packageQty){
        $mappingArray = array(
            'Μέτ' => 'm',
            'Τεμ' => 'τμχ',
            'Κιλ' => 'k'
        );

        return isset($mappingArray[$erpPackageDesc]) ? " $packageQty{$mappingArray[$erpPackageDesc]}" : '';
    }

    public function formatCustomAtrributes($custom_attributes) {
        $data = array();
        foreach ($custom_attributes as $code=>$value) {
            $data[] = array('attribute_code'=>$code, 'value'=>$value);
        }
        return (object)$data;
    }
    public function formatProductLinks($product_links) {
        $data = array();
        foreach ($product_links as $type=>$items) {
            foreach ($items as $item) {
                $data[] = array('link_type' => $type, 'linked_product_type'=>'simple', 'sku' => $item['sku'], 'linked_product_sku' => $item['linked_product_sku'], 'position' => $item['position']);
            }
        }
        return (object)$data;
    }
    public function getCategoryIdFromConfig($erpcategoryid) {
        if (isset($this->config['erpcategories']["s".$erpcategoryid])) {
            return $this->config['erpcategories']["s".$erpcategoryid];
        } else {
            return false;
        }
    }

	public function addCustomDropdownOptions($dropdowns, $allData = null) {

        $flag = true;
        foreach ($dropdowns as $key=>$dropdown) {

            $dropdown['value'] = trim($dropdown['value']);
            $dropdown['value'] = str_replace("\n", "", $dropdown['value']);

            if(isset($translationsToInsert)){
                unset($translationsToInsert);
            }

            if($dropdown['realcode'] == 'xrwma_fwtos' && isset($allData['color_temperature'])){
                continue;
            }

            if($dropdown['realcode'] == "attribute_set" || $dropdown['realcode'] == "attributeSet" || $dropdown['realcode'] == 'erpCategories' || $dropdown['realcode'] == 'relatedProducts') continue; //ignore attribute_set. arrtibuteset is not dropdown
            if (!$dropdown['value'] || !$dropdown['id']) continue; //ignore attribute if not set


            $attrcode =  $dropdown['realcode']; //get magento code from erp
            //$attrcode = $this->config['attributes']["s".$dropdown['realcode']]; //get magento code from xml config
            $s_id = $attrcode ."_" .$dropdown['id'];



            if ($this->isOptionChangedAtERP($s_id,$dropdown['value']) === false) continue; //if option did not change continue

           // $options = $this->getAttributeOptions($attrcode);
            $erpoptionid=$this->getOptionID($s_id, $attrcode); //convert s1 id to magento option_id
            //$mageoptionid = $options[$dropdown['value']]; //search if value (label) already exists
           // $mageoptionid = in_array(strtolower($dropdown['value']), $this->indexeddata[$attrcode]) ? strval(array_search(strtolower($dropdown['value']), $this->indexeddata[$attrcode])) : false;



            $loweredIndexedArray = array_map(function($var) {
                return mb_strtolower($var, mb_detect_encoding($var));
            }, $this->indexeddata[$dropdown['realcode']]);
            $mageoptionid = in_array(mb_strtolower($dropdown['value'], 'UTF-8'),  $loweredIndexedArray) ? strval(array_search(mb_strtolower($dropdown['value'], 'UTF-8'), $loweredIndexedArray)) : false;



            if(isset($dropdown['translations'])){
                $translationsToInsert = array();
                $translations = $dropdown['translations'];
                foreach($translations as $storeCode => $translation){
                    $translationsToInsert[$storeCode] = $translation;
                }
            }

            if (boolval($mageoptionid)) { //option label already exists in magento
                if (!$erpoptionid) { //but erp id does not exist with mapping
                    $flag &= (bool)$this->addOptionID($attrcode, $mageoptionid, $s_id,$dropdown['value']); //save the id to map_options table for future reference
                } elseif ($mageoptionid != $erpoptionid) {
                    $optionVal = $dropdown['value'];
                    $this->logError("MAJOR ERROR!!! check option_map table. s_id = $s_id , optionVal = $optionVal , attrID = $attrcode , ");
                    return 0; //stop now
                }
                //return $mageoptionid;
            } else { //label does not exist in magento
                if ($erpoptionid) { //no option found but we know the s_id. that means that the LABEL ONLY changed. update
                    if(isset($translationsToInsert)){
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],$mageoptionid,$translationsToInsert);
                        //$newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$optioncreted] = $dropdown['value'];
                    }
                    else{
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],$mageoptionid);
                      //  $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$optioncreted] = $dropdown['value'];
                    }
                    $flag &= (bool)$optioncreted;
                    //return $erpoptionid;
                } else { //no erpid and no mageid. Create the option
                    if(isset($translationsToInsert)){
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],null,$translationsToInsert);
                       // $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$optioncreted] = $dropdown['value'];
                    }
                    else{
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value']);
                       // $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$optioncreted] = $dropdown['value'];
                    }
                    if((bool)$optioncreted !== false){
                        $flag = 1;
                    }
                    //$flag &= $optioncreted;

                    //TODO read translations

                    if ($flag) { //mage api does not return the created option id. so check again
                        //$options = $this->getAttributeOptions($attrcode);
                        $mageoptionid = strval(array_search($dropdown['value'], $this->indexeddata[$attrcode]));
                        if ($mageoptionid) {
                            $flag &= (bool)$this->addOptionID($attrcode, $mageoptionid, $s_id,$dropdown['value']); //save the id to map_options table for future reference
                            //return $mageoptionid;
                        }
                    }

                }

            }
        }
        return $flag;

    }

    public function addCustomMultipleselectOptions($multiselect) {
	    $flag = true;
	    foreach ($multiselect as $dropdown) {
	       $flag &= $this->addCustomDropdownOptions($dropdown); //each multiselect is like dropdown
        }
        return $flag;
    }

    public function checkRequired($ProductData) {

        $flag = 1;

        //TODO use object instead of assoc
        $ProductData = json_decode(json_encode($ProductData),true);

        $flag &= is_string($ProductData ['sku']);
        $i=is_string($ProductData ['sku']);
        if (!is_string($ProductData ['sku'])) $this->logError("required field 'sku' missing");

        //$flag &= is_string($ProductData ['erpid']);
        //if (!is_string($ProductData ['erpid'])) logError("required field 'erpid' missing sku=" .$ProductData ['sku']);

        $flag &= is_string($ProductData ['name']);

        if (!is_string($ProductData ['name'])) $this->logError("required field 'name' missing sku=" .$ProductData ['sku']);


        //$flag &= is_string($ProductData ['description']);
        //if (!is_string($ProductData ['description'])) logError("required field 'description' missing sku=" .$ProductData ['sku']);


        //$flag &= is_string($ProductData ['short_description']);
        //if (!is_string($ProductData ['short_description'])) logError("required field 'short_description' missing sku=" .$ProductData ['sku']);


        $flag &= is_numeric($ProductData ['status']);
        if (!is_numeric($ProductData ['status'])) $this->logError("required field 'status' missing sku=" .$ProductData ['sku']);

        //$flag &= strlen(urlencode($ProductData ['url_key']));


        //$flag &= is_numeric($ProductData ['weight']);
//        if (!is_numeric($ProductData ['weight']['value'])) $this->logError("required field 'weight' missing sku=" .$ProductData ['sku']['value']);


       // $flag &= is_numeric($ProductData ['tax_class_id']);
        // if (!is_numeric($ProductData ['tax_class_id'])) $this->logError("required field 'tax_class_id' missing sku=" .$ProductData ['sku']);

//        foreach ($ProductData ['categories'] as $category) {
//            if ($category) {
//                $catexists = $this->CategoryExists($category);
//                $flag &= $catexists;
//                if (!$catexists) $this->logError("Category with id = $category does not exist. SKU=" .$ProductData ['sku']);
//            }
//        }
        if(isset($ProductData ['price'])){
            $flag &= is_numeric($ProductData ['price']);
            if (!is_numeric($ProductData ['price'])) $this->logError("required field 'price' missing sku=" .$ProductData ['sku']);
        }


        //$flag &= is_numeric($ProductData ['special_price']);
        //if (!is_numeric($ProductData ['special_price'])) logError("required field 'special_price' missing sku=" .$ProductData ['sku']);


        //$flag &= is_int($ProductData ['qty']);


        //$flag &= is_int($ProductData ['is_in_stock']);
        return $flag;

    }
	
    /**
     * @param $data
     * @param bool $productexist
     * @return StdClass
     * @throws Exception
     */
    public function remapProductVars($data, $productexist, $existingProduct = false) {
        
        global $languages;
        $languages = array();
       // $defaults = $this->getJsonDefaultData(); //load default data as object
        // $data = array_merge($defaults,$data);
    

        
        $product = new StdClass();
        $extension_attributes = new stdClass();
        $custom_attributes = new stdClass();
        $product_links = new stdClass();


        //$minsaleqty = empty($data['minSaleQty']) ? 1 : $data['minSaleQty'];

        $product->sku = $data['sku'];
        //if($productexist === false){ //we DO NOT want to update an existing product's name with the name which comes from the erp neither it's translations
            $product->name = trim($data['item_web_description']) != "" ? $data['item_web_description'] : $data['name'];
            if(isset($data['package']) && isset($data['measurement']) && floatval($data['package']) > 1){
                $product->name = $product->name .$this->formatPackageDesc($data['measurement'], $data['package']);
            }

            if(isset($data['measurement'])){
                $custom_attributes->measurement_unit = $data['measurement'];
            }
//            if(isset($data['name']['translations'])){
//                foreach ($data['name']['translations'] as $storeCode => $storeValue){
//                    if(!isset($languages[$storeCode])){
//                        $languages[$storeCode] = array();
//                    }
//                    $languages[$storeCode]['name'] = $storeValue; // $language array will be used later to import product translations
//                }
//            }
       // }
//        if($data['type_id']['value'] == 'simple'){
//            $filterSizes = $this->getProductFilterSize($data['dropdown']['ssize']['value']);
//            if(!empty($filterSizes)){
//                $filterSizes = implode(',', $filterSizes);
//                $custom_attributes->fsize = $filterSizes;
//            }
//        }

        //$categoriesToInsert = array_keys($this->categoriesMapping, trim($data['erp_tree_code']));
        $erpTreeCode = trim($data['erp_tree_code']);
        $categoriesToInsert = (isset($this->categoriesMapping[$erpTreeCode])) ? $this->categoriesMapping[$erpTreeCode] : array();
        $catPosition = 0;
        if($categoriesToInsert){
            foreach ($categoriesToInsert as $catId){
                $catPosition++;
                $extension_attributes->category_links[] = array("position"=> $catPosition,'category_id' =>$catId); //assigns category
            }
            if($productexist && isset($existingProduct['extension_attributes']['category_links'])){
                foreach($existingProduct['extension_attributes']['category_links'] as $magentoCat){
                    if(in_array($magentoCat['category_id'], $this->categoriesWithNoErpId)){
                        $catPosition++;
                        $extension_attributes->category_links[] = array("position"=> $catPosition,'category_id' => intval($magentoCat['category_id'])); //assigns category
                    }
                }
            }
            $custom_attributes->category_ids = null; //delete other categories
        }



        if(!$productexist){
            $product->created_at = date('Y/m/d H:i:s');
        }
//        if(isset($data['washingExtra'])){
//            if (!is_array($data['washingExtra']['value'])){ //if they give us empty string, xml2array function converts it to empty array
//                $custom_attributes->washing_extra = $data['washingExtra']['value'];
//                if(isset($data['washingExtra']['translations'])){
//                    foreach ($data['washingExtra']['translations'] as $storeCode => $storeValue){
//                        if(!isset($languages[$storeCode])){
//                            $languages[$storeCode] = array();
//                        }
//                        $languages[$storeCode]['washing_extra'] = $storeValue; // $language array will be used later to import product translations
//                    }
//                }
//            }
//
//        }

//        if(isset($data['parentcode'])){
//            if(!is_array($data['parentcode']['value'])){ //if they give us empty string, xml2array function converts it to empty array
//                $custom_attributes->parent_code = $data['parentcode']['value'];
//            }
//        }

        if(isset($data['package']) && floatval($data['package']) > 1){
            $minimumPackages = floatval($data['package']);
        }
        else{
            $minimumPackages = 1;
        }

        if(isset($data['package']) && isset($data['measurement'])){
            $custom_attributes->packages = $minimumPackages;
        }
        elseif(!isset($data['package']) || !$data['package']){
            $custom_attributes->packages = 1;
        }

        if(isset($data['barcode'])){
            $custom_attributes->barcode = $data['barcode'];
        }

        if(isset($data['erp_id'])){
            $custom_attributes->erp_id = $data['erp_id'];
        }

        if(isset($data['manufacturer_color'])){
            $custom_attributes->manufacturer_color = $data['manufacturer_color'];
        }

        if(isset($data['mpn'])){
            $custom_attributes->mpn = $data['mpn'];
        }

        if(isset($data['max_discount'])){
            $custom_attributes->max_discount = $data['max_discount'];
        }

        if(isset($data['isxys_metasximatisti'])){
            $custom_attributes->isxys_metasximatisti = $data['isxys_metasximatisti'];
        }

        if(isset($data['lumen'])){
            $custom_attributes->lumen = $data['lumen'];
        }

        if(isset($data['color_temperature'])){
            $custom_attributes->color_temperature = $data['color_temperature'];
            $custom_attributes->xrwma_fwtos = $this->mapColorTemperature($data['color_temperature']);
        }

        if(isset($data['energeiakh_klash'])){
            $custom_attributes->energeiakh_klash = $data['energeiakh_klash'];
        }

        if(isset($data['vathmos_steganothtas'])){
            $custom_attributes->vathmos_steganothtas = $data['vathmos_steganothtas'];
        }

        if(isset($data['onomastiki_isxis_watt_m'])){
            $custom_attributes->onomastiki_isxis_watt_m = $data['onomastiki_isxis_watt_m'];
        }

        if(isset($data['onomastiki_entasi'])){
            $custom_attributes->onomastiki_entasi = $data['onomastiki_entasi'];
        }

        if(isset($data['seires'])){
            $custom_attributes->seires = $data['seires'];
        }

        if(isset($data['sthles'])){
            $custom_attributes->sthles = $data['sthles'];
        }

        if(isset($data['grammes'])){
            $custom_attributes->grammes = $data['grammes'];
        }

        if(isset($data['onomastiki_isxis'])){
            $custom_attributes->onomastiki_isxis = $data['onomastiki_isxis'];
        }

        if(isset($data['eggyhsh'])){
            $custom_attributes->eggyhsh = $data['eggyhsh'];
        }

        if(isset($data['stoixeia'])){
            $custom_attributes->stoixeia = $data['stoixeia'];
        }

        if(isset($data['vash_topothethshs'])){
            $custom_attributes->vash_topothethshs = $data['vash_topothethshs'];
        }

        if(isset($data['kalwdio'])){
            $custom_attributes->kalwdio = $data['kalwdio'];
        }

        if(isset($data['typos_lampthrwn_thermansis'])){
            $custom_attributes->typos_lampthrwn_thermansis = $data['typos_lampthrwn_thermansis'];
        }

        if(isset($data['klimakes_thermanshs'])){
            $custom_attributes->klimakes_thermanshs = $data['klimakes_thermanshs'];
        }

        if(isset($data['kleidaria'])){
            $custom_attributes->kleidaria = $data['kleidaria'];
        }

        if(isset($data['kalwdiwsi'])){
            $custom_attributes->kalwdiwsi = $data['kalwdiwsi'];
        }

        if(isset($data['kit'])){
            $custom_attributes->kit = $data['kit'];
        }

        if(isset($data['arithmos_diamerismatwn'])){
            $custom_attributes->arithmos_diamerismatwn = $data['arithmos_diamerismatwn'];
        }

        if(isset($data['yliko_kataskeyis_mpoytonieras'])){
            $custom_attributes->yliko_kataskeyis_mpoytonieras = $data['yliko_kataskeyis_mpoytonieras'];
        }

        if(isset($data['fwtizomenh_othonh'])){
            $custom_attributes->fwtizomenh_othonh = $data['fwtizomenh_othonh'];
        }

        if(isset($data['megistos_arithmos_krousewn'])){
            $custom_attributes->megistos_arithmos_krousewn = $data['megistos_arithmos_krousewn'];
        }

        if(isset($data['euros_tasis'])){
            $custom_attributes->euros_tasis = $data['euros_tasis'];
        }

        if(isset($data['ischus_fotoboltaikou'])){
            $custom_attributes->ischus_fotoboltaikou = $data['ischus_fotoboltaikou'];
        }

        if(isset($data['diastaseis_fotoboltaikou'])){
            $custom_attributes->diastaseis_fotoboltaikou = $data['diastaseis_fotoboltaikou'];
        }

        if(isset($data['ekswterikh_diametros'])){
            $custom_attributes->ekswterikh_diametros = $data['ekswterikh_diametros'];
        }

        if(isset($data['eswterikh_diametros'])){
            $custom_attributes->eswterikh_diametros = $data['eswterikh_diametros'];
        }

        if(isset($data['antoxh_sth_ghransh'])){
            $custom_attributes->antoxh_sth_ghransh = $data['antoxh_sth_ghransh'];
        }

        if(isset($data['antimikroviakh_prostasia'])){
            $custom_attributes->antimikroviakh_prostasia = $data['antimikroviakh_prostasia'];
        }

        if(isset($data['den_diadidei_floga'])){
            $custom_attributes->den_diadidei_floga = $data['den_diadidei_floga'];
        }

        if(isset($data['meiwmenes_trives'])){
            $custom_attributes->meiwmenes_trives = $data['meiwmenes_trives'];
        }

        if(isset($data['lead_time']) && $data['lead_time']){
            $custom_attributes->lead_time = $data['lead_time'];
        }
        else{
            $custom_attributes->lead_time = 4;
        }

        if(isset($data['mhkos'])){
            $custom_attributes->mhkos = $data['mhkos'];
        }

        if(isset($data['width'])){
            $custom_attributes->width = $data['width'];
        }

        if(isset($data['ypsos'])){
            $custom_attributes->ypsos = $data['ypsos'];
        }

        if(isset($data['diameter'])){
            $custom_attributes->diameter = $data['diameter'];
        }

        if(isset($data['fpt'])){
            $fpt = array();
            if($data['fpt']){
                $fpt[0]['country'] = 'GR';
                $fpt[0]['value'] = $data['fpt'] * $minimumPackages;
                $fpt[0]['website_id'] = '0';
            }
            $custom_attributes->fpt = $fpt;
        }

        if(!$productexist && isset($data['tier_price'])){
            $tier_prices = array();
            foreach ($data['tier_price'] as $tPrice) {
                if($tPrice['price'] <= 0){
                    continue;
                }
                $tier_price = new stdClass();
                $tier_price->customer_group_id = $tPrice['cust_group'];
                $tier_price->qty = 1;
                $tier_price->extension_attributes->website_id = 0;
                $tier_price->extension_attributes->percentage_value = $tPrice['price'];
                $tier_prices[] = $tier_price;
            }
        }

        //$custom_attributes->color = $data['color']['name']['value'];
        //$custom_attributes->size = $data['size']['name']['value'];
        //if ($minsaleqty > 1) $product->name = $product->name ." ($minsaleqty {$data['dropdown']['Mtrunit4']})";
        $product->weight = isset($data['weight']) ? $data['weight'] : 1;
        //$custom_attributes->skroutz_xml = $data['skroutz'] ? "24" : "317";
        //$custom_attributes->best_price_xml = $data['bestprice'] ? "25" : "318";
        //$custom_attributes->totos_xml = $data['totos'] ? "26" : "403";
        //$product->price = $data['price'];
        //$product->price = $data['price03']*$minsaleqty; //timi xwris FPA kai xwris wee_tax
//        if(isset($data['startingPrice'])){
//            $product->price = $data['startingPrice']['value'];
//            if ($data['startingPrice']['value'] > $data['finalPrice']['value']) {
//                $custom_attributes->special_price = $data['finalPrice']['value'];
//            } else {
//                $custom_attributes->special_price = $product->price; //set both prices the same as workouround for bug https://github.com/magento/magento2/issues/8862
//            }
//        }

//        if(isset($data['producttype']) && $data['producttype']['value']){
//            $custom_attributes->producttype = $data['producttype']['value'];
//        }


//        $tier_prices = array();
//        if ($data['price01']) { //xondriki
//            $tier_price = new stdClass();
//            $tier_price->customer_group_id = 2; //wholesale group
//            $tier_price->qty = 1;
//            $tier_price->extension_attributes->website_id = 0;
//            $tier_price->value = $data['price01']*$minsaleqty;
//            $tier_prices[] = $tier_price;
//        }

//        if ($data['price02']) {
//            $tier_price = new stdClass();
//            $tier_price->customer_group_id = 3; //retailer group
//            $tier_price->qty = 1;
//            $tier_price->extension_attributes->website_id = 0;
//            $tier_price->value = $data['price02']*$minsaleqty;
//            $tier_prices[] = $tier_price;
//        }

        //$custom_attributes->timikatastimatos = ($data['price04']*$minsaleqty + $data['wee_tax']*$minsaleqty)*1.24; //timi katastimatos
        //$product->timixondrikis = $data['price01'];
        //$product->timiilektrologou = $data['price02'];
        $product->type_id = 'simple';//$data['type_id']['value'];
        if(!$productexist){
            $product->price = $data['price'] * $minimumPackages;
            if(isset($data['special_price_to_insert']) && $data['special_price_to_insert']){
                $custom_attributes->special_price = $data['special_price_to_insert'] * $minimumPackages;
            }
            else{
                if(isset($data['final_retail']) && $data['final_retail'] < $data['price']){
                    $custom_attributes->special_price = $data['final_retail'] * $minimumPackages;
                }
                else{
                    $custom_attributes->special_price = $data['price'] * $minimumPackages;
                }
            }
        }
        $custom_attributes->gift_message_available = 2; //default value must be set
//        if ($data['manufacturer_color']){
//            if(!is_array($data['manufacturer_color']['value'])){
//                $custom_attributes->manufacturer_color = $data['manufacturer_color']['value'];
//            }
//        }
        //$custom_attributes->erpid = $data['custom_erpId']['value'];
        //if($data['description']) $custom_attributes->description = str_replace("</BODY></HTML>","",str_replace("<HTML><HEAD></HEAD><BODY>","",$data['description']['value']));
        //$custom_attributes->short_description = $data['short_description'] ?: null;
        //if ($minsaleqty > 1) $custom_attributes->short_description = $custom_attributes->short_description ." ($minsaleqty TMX)";
        //$custom_attributes->mpn = $data['mpn'];
        //if ($data['sku_1']) $custom_attributes->barcode = $data['sku_1'];
		//$custom_attributes->weetaxfixed = $data['wee_tax'];
//        if ($data['wee_tax'] > 0) {
//            $custom_attributes->weeetax = array(0 => array('website_id' => "0", 'country' => "GR", "value" => $data['wee_tax']*$minsaleqty));
//        }
//        if (isset($data['temaxia_sti_nsyskeysia'])) $custom_attributes->temaxia_sti_nsyskeysia = $data['temaxia_sti_nsyskeysia'];

        //$custom_attributes->minsaleqty = $minsaleqty;

//        if (isset($data['nextArrival']) && !empty($data['nextArrival'])) $custom_attributes->backinstockdate = $data['nextArrival'];


//        if (isset($data['vat'])) {
//            $vats = array('1410' => 2); //1420-24%
//            $custom_attributes->tax_class_id = $vats[$data['vat']];
//        }

        $product->attributeSetId = 4;//$this->attributesets[$data['dropdown']['sattributeSet']['value']];

//        if ($data['type_id']['value'] == 'configurable' && $data['childskus']) {
//            $childs = explode(";",$data['childskus']);
//            foreach ($childs as $child) {
//                $childdata = $this->getProductBySku($child);
//                if ($childdata['id']) {
//                    $configurableProductLinks[] = $childdata['id'];
//                } else {
//                    $this->logError("childsku=$child is not created yet. Sku={$data['sku']}");
//                    return false;
//                }
//            }
//            if (count($configurableProductLinks)) {
//                $extension_attributes->configurableProductLinks = $configurableProductLinks;
//            }
//        }
//        $normalize = array(
//            'ά' => 'α', 'Ά' => 'Α',
//            'έ' => 'ε', 'Έ' => 'Ε',
//            'ή' => 'η', 'Ή' => 'Η',
//            'ί' => 'ι', 'Ί' => 'Ι',
//            'ό' => 'ο', 'Ό' => 'Ο',
//            'ύ' => 'υ', 'Ύ' => 'Υ',
//            'ώ' => 'ω', 'Ώ' => 'Ω'
//        );


//        if ($data['type_id']['value'] == 'configurable') {
//            $extension_attributes->configurableProductOptions = array(array("attributeId"=>$this->config['size'],"label"=>"Size",'id'=>0,"position"=>0,"isUseDefault"=>true,"values" => array(array("valueIndex"=>0))));
//        }
        $fetchedDropdowns = array();
		foreach ($data['dropdown'] as $key => $dropdown) {

            //$dropdown['realcode'] = $this->config['attributes']["s".$dropdown['realcode']]; //get magento code from xml config

            if($dropdown['realcode'] == 'xrwma_fwtos' && isset($data['color_temperature'])){
                $fetchedDropdowns[] = $dropdown['realcode']; //TASK-15643 do not empty xrwma_fwtos. It is filled depending on color_temperature value
                continue;
            }

		    if ($dropdown['realcode'] && $dropdown['realcode'] != "attribute_set" && $dropdown['realcode'] != "attributeSet") {
                $escaped_value = str_replace("\n", "", $dropdown['value']);
                $fetchedDropdowns[] = $dropdown['realcode'];
		      //  $optionid = $this->getOptionID($dropdown['realcode']."_".$dropdown['id'],$dropdown['realcode']);
                $loweredIndexedArray = array_map(function($var) {
                    return mb_strtolower($var, mb_detect_encoding($var));
                }, $this->indexeddata[$dropdown['realcode']]);
                $optionid = in_array(mb_strtolower($escaped_value, 'UTF-8'),  $loweredIndexedArray) ? strval(array_search(mb_strtolower($escaped_value, 'UTF-8'), $loweredIndexedArray)) : false;
		        if ($optionid) {
                    $custom_attributes->{$dropdown['realcode']} = $optionid;
                } else {
		            $this->logError('MAJOR ERROR sku=' .$data['sku'] ."option=" .$dropdown['realcode']."_".$dropdown['id'] ." does not exist. The option should exist before assign.");
		            return false;
                }
            }
        }
		if(!in_array('xrwma_fwtos', $fetchedDropdowns) && isset($data['color_temperature'])){
            $fetchedDropdowns[] = 'xrwma_fwtos'; //TASK-15643 do not empty xrwma_fwtos. It is filled depending on color_temperature value
        }
		unset($data['dropdown']);
        $categoryposition = 0;
        $relatedposition = 0;
        $fetchedMultiselects = array();
        foreach ($data['multiselect'] as $multiselect) {
            foreach ($multiselect as $dropdown) {
                if ($dropdown['realcode']) {
                    $fetchedMultiselects[] = $dropdown['realcode'];
//                    if ($dropdown['realcode']== 'erpCategories') {
//                        $categoryid = $this->getCategoryIdFromConfig($dropdown['id']);
//                        if ($categoryid) {
//                            //$data['categories'][] = $categoryid;
//                            $extension_attributes->category_links[] = array("position"=> $categoryposition,'category_id' =>$categoryid); //ADDS (concatenates) category
//                            $categoryposition++;
//                            $custom_attributes->category_ids = null; //delete other categories
//                        }
//                    }
//                    if ($dropdown['realcode']== 'relatedProducts') {
//                        $product_links->related[] = array('linked_product_sku'=>$dropdown['label'], "sku"=>$product->sku, "position"=>$relatedposition);
//                        $relatedposition++;
//                    }

                    $optionid = $this->getOptionID($dropdown['realcode']."_".$dropdown['id'],$dropdown['realcode']);
                    if ($optionid) {
                        $custom_attributes->{$dropdown['realcode']} = $optionid;
                    } else {
                        $this->logError('MAJOR ERROR sku=' .$data['sku'] ."option=" .$dropdown['realcode']."_".$dropdown['id'] ." does not exist. The option should exist before assign.");
                        return false;
                    }
                }
            }
        }
        unset($data['multiselect']);

        
//        if ($data['status']['value'] == 'enable' || $data['status']['value'] == '1') $product->status = 1;
//        elseif ($data['status']['value'] == 'disable' || $data['status']['value'] == '0') $product->status = 2; //magento status = 2 is disabled
        if ($data['is_web'] == 1){
            $product->status = 1;
        }
        else{
            $product->status = 2; //magento status = 2 is disabled
        }


        if ($data['is_web_active'] == 1){
            $product->visibility = 4;
        }
        else{
            $product->visibility = 1;
        }

        if($data['price'] == 0){
            $product->status = 2; //magento status = 2 is disabled
        }



            
        if ($productexist === false) //new product make greeklish url
            $custom_attributes->url_key = $data['url_key'] ?: $this->makeCodeFromString($product->name ."-" .$product->sku); //remove white spaces - make greeklish - format correctly
        
      //  if ($productexist === false) { //new product
            //$product->weight = $data['weight'] ? (float)$data['weight'] : 1;
            //$custom_attributes->tax_class_id =is_numeric($data['taxclass_id']) ? (int)$data['taxclass_id'] : 2;
            if($productexist === false){
                $extension_attributes->stock_item->qty = $data['qty'] ? (int)($data['qty'] / $minimumPackages) : 0;

//                if($data['type_id']['value'] == 'configurable'){
//                    $extension_attributes->stock_item->is_in_stock = true;
//                }
                if (intval($data['qty']) > 0){
                    $extension_attributes->stock_item->is_in_stock = true;
                    $custom_attributes->elgstockstatus = 12; //In Stock
                }
                else{
                    $custom_attributes->elgstockstatus = 13; //Available in 3-10 days
                    $extension_attributes->stock_item->is_in_stock = false;
                }
                $extension_attributes->stock_item->is_qty_decimal = false;
            }

          //  $extension_attributes->stock_item->stock_id = 1;

      //  }
        
        if ($data['news_from_date']) {
            $date = new DateTime($data['news_from_date']['value']);
            $custom_attributes->news_from_date =$date->format('Y-m-d H:i:s');
        }
        if ($data['news_to_date']) {
            $date = new DateTime($data['news_to_date']['value']);
            $custom_attributes->news_to_date =$date->format('Y-m-d H:i:s');
        }
        if ($data['special_from_date']) {
            $date = new DateTime($data['special_from_date']['value']);
            $custom_attributes->special_from_date =$date->format('Y-m-d H:i:s');
        }
        if ($data['special_to_date']) {
            $date = new DateTime($data['special_to_date']['value']);
            $custom_attributes->special_to_date =$date->format('Y-m-d H:i:s');
        }
        
        if ($data['websites']) $extension_attributes->websites = array($data['websites']['value']);
        else $extension_attributes->website_ids = array("1");

        //if url key not specified use sku
        //if ($productexist === 0 || $data['url_key']) //new product
         //   $product->url_key = $data['url_key'] ? $data['url_key'] : $this->greeklish($product->name ."-" .$product->sku); //also converts to greeklish

//        if (isset($data['BALANCE'])) {
//            if ($data['BALANCE'] > 0) {
//                $extension_attributes->stockItem->isInStock = true;
//                $extension_attributes->stockItem->qty = $data['BALANCE'];
//            } else {
//                $extension_attributes->stockItem->isInStock = false;
//                $extension_attributes->stockItem->qty = $data['BALANCE'];
//            }
//        }

//        if ($data['katargimeno'] == 1) { //
//            $extension_attributes->stock_item->use_config_backorders="0";
//            $extension_attributes->stock_item->backorders="0";
//        } else {
            $extension_attributes->stock_item->use_config_backorders="0";
            $extension_attributes->stock_item->backorders="2"; //allow orders bellow 0 and notify customer
            $extension_attributes->stock_item->isInStock = true;

        if($data['is_web'] == 1 && $data['is_web_active'] == 1){
            if($data['purchase_block_status'] == 1 || (($data['purchase_block_status'] == 0) && $data['webavailtype'] != 'Available')){ //no backorders
                $extension_attributes->stock_item->use_config_backorders="0";
                $extension_attributes->stock_item->backorders="0";
            }
            if($data['sales_block_status'] == 1){   //block sales
                $product->visibility = 1;
            }
        }
       //}

        if($existingProduct){
            foreach ($existingProduct['existingCustomAttributes'] as $customAttribute){
                if(in_array($customAttribute, $this->customAttributes) && !isset($data[$customAttribute]) && !in_array($customAttribute, $fetchedDropdowns) && !in_array($customAttribute, $fetchedMultiselects)){
                    $custom_attributes->{$customAttribute} = null; //erase attribute value
                }
            }
        }

        $product->custom_attributes = $this->formatCustomAtrributes($custom_attributes);
        if($productexist && isset($existingProduct['extension_attributes']) && isset($existingProduct['extension_attributes']['product_attachments'])){
            $extension_attributes->product_attachments = $existingProduct['extension_attributes']['product_attachments'];
        }
        $product->extension_attributes = $extension_attributes;
        if(isset($tier_prices) && !$productexist){
            $product->tier_prices = $tier_prices;
        }
        $product->product_links = $this->formatProductLinks($product_links);

        return $product; //need to be array
    }
    public function run() {
        $this->getAllAttributeOptions();
        $this->getStoreViews();
        $this->getAttributeSets();
        $this->getCategoriesMapping();
        $this->getErpCustomAttributes();
        foreach ($this->items as $item) {
            
            $this->item = $item;

            //read one by one all files
            $this->debug("Parsing record id: ". $item['id'] ."<br>\n");
            if (file_exists($this->filestop)) {$this->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            $returnValue = $this->LoadData();
            if ($returnValue) { //if created-updated
                if($returnValue === true){
                    $returnValue = 1;
                }
                $this->setXmlDataStatus($item['id'] ,$returnValue); //set as processed
            }
            else {
                //logError("Error Parsing file: " .$file);
                if ($item['status'] == self::STATUSUNPROSSESSED) {
                    //if this was the first time we process data retry on next run
                    $this->setXmlDataStatus($item['id'], self::STATUSRETRY);
                    $this->setXmlDataRetries($item['id'], "1"); //first try
                }
                else { //status 2, RETRY
                    if ($item['status'] == self::STATUSRETRY && $item['retries'] < $this->retries) {
                        $item['retries']++; //increase retries
                        $this->setXmlDataStatus($item['id'], self::STATUSRETRY);
                        $this->setXmlDataRetries($item['id'], $item['retries']); //
                    } else {
                        $this->setXmlDataStatus($item['id'], self::STATUSERROR);
                    }
                }
            }

            $this->heartbeat();

            if (!is_null($this->errors)) {
                $this->setXmlDataError($item['id'], $this->errors);
                $this->senderrormail();
            }
                
        }
        return true;
    }

    public function LoadData() { //reads the $productsxml file and creates-updates products
    try {
        
        global $languages;

        $this->item = $this->getLatestDuplicateAndDiscardOthers($this->item); //set status 4 on rows with same entity id and status 0 or 2

        $jsonData = $this->item['data'];
        $value = json_decode($jsonData, true);
        if (!$value)
        {
            $this->logError('Not Valid JSON document');
            return 0;
        }

        if ($this->areDataChangedFromLastUpdate($this->item, array('erp_upd_date')) === false)
        {
            return 5; //ignore if data are not changed and set status = 5
        }
//        $objDOM = new DOMDocument();
//        $objDOM->loadXML($xml); //make sure path is correct
//
//
//            $product = $objDOM->getElementsByTagName("product");

            // for each product tag, parse the document and get values    
            $return_val = 1;
            //foreach( $jsonArray as $value )
            //{
                $flag = 1; //reset flag
                $errors = null; //delete error log
    
    
//                $details = $value->getElementsByTagName("sku"); //get sku from XML
//                $sku= $details->item(0)->nodeValue;
                $sku = $value['sku'];
                
//                $details = $value->getElementsByTagName("erpId"); //get sku from XML
//                $erpid= $details->item(0)->nodeValue;
                $erpid = $value['erp_id'];


                
                // $details = $value->getElementsByTagName("attributeSet"); //get typecategory from XML
                // $attribute_set= $details->item(0)->nodeValue;
              

                //$this->getAttributeSetsMapings();
                

    

    
                //ob_end_flush();
    
//                $sxml=simplexml_import_dom($value); //convert node to simple xml
//                $xmlarray=$this->xml2array($sxml); //convert XML to array

                $this->debug("importing: sku= $sku; {$this->table} id=" .$this->item['id'] ."<br />");

                if(isset($value['dropdown']['sattributeSet'])){
                    $attribute_set = $value['dropdown']['sattributeSet']['value'];
                    if (!$attribute_set || array_key_exists($attribute_set, $this->attributesets) === false){
                        $this->logError("CRITICAL ERROR. typecategory = $attribute_set does not match. sku = $sku; $this->table id=" .$this->item['id'] ."<br />");
                        //senderrormail();
                        return 0;
                    }
                }

                
                $productInfo=$this->getProductBySku($sku); //if product exists get info

                $livesku = $productInfo ? $productInfo['sku'] : false; //get sku from live product //get sku from live product
                $productInfo['existingCustomAttributes'] = array();
                if($livesku){
                    foreach ($productInfo['custom_attributes'] as $customAttribute) {
                        $productInfo['existingCustomAttributes'][] = $customAttribute['attribute_code'];
                    }
                }



                if(!$livesku && $value['is_web'] != 1){
                    $this->logError('Product has is_web = 0 and does not already exist in magento. Skip it.');
                    return 1; //if product is new and status is disabled ignore product
                }

                if(!$livesku && $value['price'] == 0){
                    $this->logError('Product has zero price and does not already exist in magento. Skip it.');
                    return 1; //do nothing if product comes with zero price and does not already exists
                }
                /* if ($xmlarray['erpCategories']) {
                 debug("Upadting Categories: sku= $sku; $table id=" .$file['id'] ."<br />");
                    $erpCategories = parseCategories($xmlarray['erpCategories']);
                    foreach ($erpCategories as $erpCategory) {
                        if ($erpCategory['parenterpid']) //ignore root categories from ERP
                            $flag &= updateOrCreateCategories($erpCategory['erpid'], $erpCategory['parenterpid'], $erpCategory['name']);
                    }
                } */
                
//                if ($livesku === false && $xmlarray['status']['value'] == 0){
//                    $this->logError('Product has status = 0 and does not already exist in magento. Skip it.');
//                    return 0; //if product is new and status is disabled ignore product
//                }

				$dropdowns = $value['dropdown']; //dropdowns from erp
                $this->addCustomDropdownOptions($dropdowns, $value);
                $multiselect = $value['multiselect']; //multiselects from erp
                $this->addCustomMultipleselectOptions($multiselect);

                //if (count($images) == count($xmlarray['photos'])) unset($xmlarray['photos']);

                $productData = $this->remapProductVars($value, $livesku, $productInfo);

//                $languages = $productData['languages'];
//                unset($productData['languages']);

                //$productData = makeMultiSelectArrays($productData);
                //$productData = addOptions($productData);
                //$productData = addCustomDropdownOptions($productData);
                if (boolval($productData) == 0){
                    $this->logError("CRITICAL ERROR. Could not create options for sku = $sku; $this->table id=" .$this->item['id'] ."<br />");
                    //senderrormail();
                    return 0;
                }
    
                if ($livesku) {
                    //if product exists use only the values contained in XML to update
                        
                    //$productData['sku']=$sku; //use this sku (if sku is numeric this sku has an m as first character)
                    if ($productData->type_id === 'simple') {
                        $flag &= $this->updateOrCreateProduct ($sku, $productData);
                    }
                    elseif ($productData->type_id === 'configurable'){
                        //$productData = $this->getConfigurableData($productData); //make configurable data
                        //unset($productData->configurableProductOptions); //attribute link must be created only once. Erase new links
                        $flag &= $this->updateOrCreateProduct($sku, $productData);
                    }
                    else {
                        $this->logError("Type_id = " .$productData->type_id ." not supported yet. Contact Administrator");
                        $flag = 0;
                    }
                    if ($flag && $languages)
                        $flag &= $this->updateProductTranslations($productData); //update translations (name, desc, short_desc)
                    $required = 1; //flag is always 1 because product allready exists
                    if ($flag)
                    $this->debug("Updating: sku= $sku; was succesful<br />");
                }
                else //if sku is not found
                {
                    if ($productData->type_id === 'simple') {
                        $required = $this->checkRequired($productData);
                        if ($required)
                        {
                            $flag &= $this->updateOrCreateProduct($sku, $productData);
                            if ($flag)
                                $this->debug("New product created succesful. product_id= $sku;<br />");
                        }
                    }
                    elseif ($productData->type_id === 'configurable'){
                        //$productData['configurable_skus'] = getConfigurableSkus($productData['associated_products']);
                        //$productData = $this->getConfigurableData($productData); //make configurable data
                        $required = $this->checkRequired($productData);
                        if ($required)
                        {
                            // Create new config product
                         $flag &= $this->updateOrCreateProduct($sku, $productData);
                         if ($flag)
                             $this->debug("New Configurable product created succesful. product_id= $sku;<br />");
                        }
                    }
                    else {
                        $this->logError("Type_id = " .$productData->type_id ." not supported yet. Contact Administrator");
                        return 0;
                    }

                    //if ($flag && $languages)
                        //$flag &= $this->UpdateProductTranslations($productData); //update translations (name, desc, short_desc)
                }

               // $this->setStockBySku($sku, $xmlarray['qty']); //hack because after 2.3 update from product not working

//                if ($flag && $xmlarray['photos']) {
//                    $images = $this->getProductMediaBySku($sku);
//                    $photos = $this->xml2array(is_object($xmlarray['photos']['photo'][0]) ? $xmlarray['photos']['photo'] : $xmlarray['photos']); //crazy hack for xml to array strange behaviour
//                    foreach ($photos as $photo) {
//                        $image = $this->makeImageData($photo);
//                        $imageexists = $this->filterImageByPosition($images, $photo['ordering']);
//                        if ($imageexists) {
//                            //TODO update image if exist
//                        } else {
//                            $flag &= $this->addProductMediaBySku($sku, $image);
//                        }
//                    }
//                }
                
                //$flag &= addRelatedProducts($sku, $productData);
            //  $flag &=addUpsellProducts($sku, $productData);
           // if ($productData['associated_products'] && $productData['type_id'] == "simple") {
           //     $flag &= attachProductToConfigurable($productData['associated_products'], $productData['sku']);  
            //} elseif ($productData['productcategory_id'] == '18' && $productData['simple']) {
            //    $flag &= addRingCustomOptions($sku, $productData['ringsize']);
            //}
           // $flag &= addLinkProducts($sku, $productData['related_products'], 'up_sell');
           // $flag &= addLinkProducts($sku, $productData['more_colors'], 'related');
           // $flag &= addLinkProducts($sku, $productData['cross_sell'], 'cross_sell');
    
                $return_val &= $required & $flag; //flag for checking for errors
    
                //if (!($required & $flag)) //if error occured, save to db. Saves every error separate
                //saveProductError2DB($sku, $product_email, $sxml->asXML(), $errors); //save to db as xml
    
                //if (!is_null($errors))
                //senderrormail();
            //}
        return $return_val;
    } catch( SoapFault $fault ) {
        $this->logError($fault);
        return 0;
        }
    }
}
//##############################################3
error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);


//$main = new m2();
$main = new main();
//$main = $main;

$main->table = "connector_products"; //###
$main->entity = 'ProductImport'; //###
$main->blockingentities = array("ProductStockImport","connector_prices","connector_media");


echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

//$main->sets = $main->config['set'];
//$main->languages = $main->config['languages'];


//########################################################
if ($main->canRun()) { //check if previous connection is running
    $main->heartbeat(); //start heartbeat
    
    if($main->config && $main->init_db()) {
        //file_put_contents($fileruntime, "operation started at " .gmdate(DATE_RFC822, time()));
        $main->items = $main->getReproccessedJsonData(); //get reproccesed data up to 3 times //read first the reprocessed
        $main->items = array_merge($main->items,$main->getUnproccessedJsonData()); //merge new files
        if ($main->config && count($main->items)) {
            if ($main->init_connector())
                $main->run();
        }
    }
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail
        
    //unlink($main->fileruntime);
    $main->allowRun();
    $main->close_connector();
}
else        {
   // $main->logError( "previous synchronization is running.");
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail
    $main->close_connector();
    return 0;
}
echo "End {$main->entity} \n\r";
//########################################################


//######################################################################################



