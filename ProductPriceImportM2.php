<?php

include "commonM2.php";

class main extends m2 {

    public function formatCustomAtrributes($custom_attributes) {
        $data = array();
        foreach ($custom_attributes as $code=>$value) {
            $data[] = array('attribute_code'=>$code, 'value'=>$value);
        }
        return (object)$data;
    }
    public function formatProductLinks($product_links) {
        $data = array();
        foreach ($product_links as $type=>$items) {
            foreach ($items as $item) {
                $data[] = array('link_type' => $type, 'linked_product_type'=>'simple', 'sku' => $item['sku'], 'linked_product_sku' => $item['linked_product_sku'], 'position' => $item['position']);
            }
        }
        return (object)$data;
    }
    public function getCategoryIdFromConfig($erpcategoryid) {
        if (isset($this->config['erpcategories']["s".$erpcategoryid])) {
            return $this->config['erpcategories']["s".$erpcategoryid];
        } else {
            return false;
        }
    }

    public function addCustomDropdownOptions($dropdowns) {

        $flag = true;
        foreach ($dropdowns as $key=>$dropdown) {

            $dropdown['value'] = trim($dropdown['value']);

            if(isset($translationsToInsert)){
                unset($translationsToInsert);
            }

            if($dropdown['realcode'] == "attribute_set" || $dropdown['realcode'] == "attributeSet" || $dropdown['realcode'] == 'erpCategories' || $dropdown['realcode'] == 'relatedProducts') continue; //ignore attribute_set. arrtibuteset is not dropdown
            if (!$dropdown['value'] || !$dropdown['id']) continue; //ignore attribute if not set


            $attrcode =  $dropdown['realcode']; //get magento code from erp
            //$attrcode = $this->config['attributes']["s".$dropdown['realcode']]; //get magento code from xml config
            $s_id = $attrcode ."_" .$dropdown['id'];

            if ($this->isOptionChangedAtERP($s_id,$dropdown['value']) === false) continue; //if option did not change continue

            // $options = $this->getAttributeOptions($attrcode);
            $erpoptionid=$this->getOptionID($s_id, $attrcode); //convert s1 id to magento option_id
            //$mageoptionid = $options[$dropdown['value']]; //search if value (label) already exists
            $mageoptionid = in_array($dropdown['value'], $this->indexeddata[$attrcode]) ? strval(array_search($dropdown['value'], $this->indexeddata[$attrcode])) : false;

            if(isset($dropdown['translations'])){
                $translationsToInsert = array();
                $translations = $dropdown['translations'];
                foreach($translations as $storeCode => $translation){
                    $translationsToInsert[$storeCode] = $translation;
                }
            }

            if ($mageoptionid) { //option label already exists in magento
                if (!$erpoptionid) { //but erp id does not exist with mapping
                    $flag &= (bool)$this->addOptionID($attrcode, $mageoptionid, $s_id,$dropdown['value']); //save the id to map_options table for future reference
                } elseif ($mageoptionid != $erpoptionid) {
                    $optionVal = $dropdown['value'];
                    $this->logError("MAJOR ERROR!!! check option_map table. s_id = $s_id , optionVal = $optionVal , attrID = $attrcode , ");
                    return 0; //stop now
                }
                //return $mageoptionid;
            } else { //label does not exist in magento
                if ($erpoptionid) { //no option found but we know the s_id. that means that the LABEL ONLY changed. update
                    if(isset($translationsToInsert)){
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],$mageoptionid,$translationsToInsert);
                        $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$newlyCreatedOptionId] = $dropdown['value'];
                    }
                    else{
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],$mageoptionid);
                        $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$newlyCreatedOptionId] = $dropdown['value'];
                    }
                    $flag &= (bool)$optioncreted;
                    //return $erpoptionid;
                } else { //no erpid and no mageid. Create the option
                    if(isset($translationsToInsert)){
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value'],null,$translationsToInsert);
                        $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$newlyCreatedOptionId] = $dropdown['value'];
                    }
                    else{
                        $optioncreted = $this->setAttributeOption($attrcode,$dropdown['value']);
                        $newlyCreatedOptionId = explode("_", $optioncreted)[1]; //setAttributeOption returns string at the form of 'id_xxxx'
                        $this->indexeddata[$attrcode][$newlyCreatedOptionId] = $dropdown['value'];
                    }
                    if((bool)$optioncreted !== 0){
                        $flag = 1;
                    }
                    //$flag &= $optioncreted;

                    //TODO read translations

                    if ($flag) { //mage api does not return the created option id. so check again
                        //$options = $this->getAttributeOptions($attrcode);
                        $mageoptionid = strval(array_search($dropdown['value'], $this->indexeddata[$attrcode]));
                        if ($mageoptionid) {
                            $flag &= (bool)$this->addOptionID($attrcode, $mageoptionid, $s_id,$dropdown['value']); //save the id to map_options table for future reference
                            //return $mageoptionid;
                        }
                    }

                }

            }
        }
        return $flag;

    }

    public function addCustomMultipleselectOptions($multiselect) {
        $flag = true;
        foreach ($multiselect as $dropdown) {
            $flag &= $this->addCustomDropdownOptions($dropdown); //each multiselect is like dropdown
        }
        return $flag;
    }

    public function checkRequired($ProductData) {

        $flag = 1;

        //TODO use object instead of assoc
        $ProductData = json_decode(json_encode($ProductData),true);

        $flag &= is_string($ProductData ['sku']);
        $i=is_string($ProductData ['sku']);
        if (!is_string($ProductData ['sku'])) $this->logError("required field 'sku' missing");

        //$flag &= is_string($ProductData ['erpid']);
        //if (!is_string($ProductData ['erpid'])) logError("required field 'erpid' missing sku=" .$ProductData ['sku']);

        $flag &= is_string($ProductData ['name']);

        if (!is_string($ProductData ['name'])) $this->logError("required field 'name' missing sku=" .$ProductData ['sku']);


        //$flag &= is_string($ProductData ['description']);
        //if (!is_string($ProductData ['description'])) logError("required field 'description' missing sku=" .$ProductData ['sku']);


        //$flag &= is_string($ProductData ['short_description']);
        //if (!is_string($ProductData ['short_description'])) logError("required field 'short_description' missing sku=" .$ProductData ['sku']);


        $flag &= is_numeric($ProductData ['status']);
        if (!is_numeric($ProductData ['status'])) $this->logError("required field 'status' missing sku=" .$ProductData ['sku']);

        //$flag &= strlen(urlencode($ProductData ['url_key']));


        //$flag &= is_numeric($ProductData ['weight']);
//        if (!is_numeric($ProductData ['weight']['value'])) $this->logError("required field 'weight' missing sku=" .$ProductData ['sku']['value']);


        // $flag &= is_numeric($ProductData ['tax_class_id']);
        // if (!is_numeric($ProductData ['tax_class_id'])) $this->logError("required field 'tax_class_id' missing sku=" .$ProductData ['sku']);

//        foreach ($ProductData ['categories'] as $category) {
//            if ($category) {
//                $catexists = $this->CategoryExists($category);
//                $flag &= $catexists;
//                if (!$catexists) $this->logError("Category with id = $category does not exist. SKU=" .$ProductData ['sku']);
//            }
//        }
        if(isset($ProductData ['price'])){
            $flag &= is_numeric($ProductData ['price']);
            if (!is_numeric($ProductData ['price'])) $this->logError("required field 'price' missing sku=" .$ProductData ['sku']);
        }


        //$flag &= is_numeric($ProductData ['special_price']);
        //if (!is_numeric($ProductData ['special_price'])) logError("required field 'special_price' missing sku=" .$ProductData ['sku']);


        //$flag &= is_int($ProductData ['qty']);


        //$flag &= is_int($ProductData ['is_in_stock']);
        return $flag;

    }

    /**
     * @param $data
     * @param bool $productexist
     * @return StdClass
     * @throws Exception
     */
    public function remapProductVars($data, $productexist = false) {

        global $languages;
        $languages = array();




        $product = new StdClass();
        $custom_attributes = new stdClass();



        $product->sku = $data['sku'];

        $product->type_id = 'simple';

        if(isset($data['package']) && $data['package'] > 1){
            $minimumPackages = floatval($data['package']);
        }
        else{
            $minimumPackages = 1;
        }


        if(isset($data['fpt'])){
            $fpt = array();
            if($data['fpt']){
                $fpt[0]['country'] = 'GR';
                $fpt[0]['value'] = $data['fpt'] * $minimumPackages;
                $fpt[0]['website_id'] = '0';
            }
            $custom_attributes->fpt = $fpt;
        }

        if(isset($data['tier_price'])){
            $tier_prices = array();
            foreach ($data['tier_price'] as $tPrice) {
                if($tPrice['price'] <= 0){
                    continue;
                }
                $tier_price = new stdClass();
                $tier_price->customer_group_id = $tPrice['cust_group'];
                $tier_price->qty = 1;
                $tier_price->extension_attributes->website_id = 0;
                $tier_price->extension_attributes->percentage_value = $tPrice['price'];
                $tier_prices[] = $tier_price;
            }
        }



        $product->price = $data['price'] * $minimumPackages;
        if(isset($data['special_price_to_insert']) && $data['special_price_to_insert']){
            $custom_attributes->special_price = $data['special_price_to_insert'] * $minimumPackages;
        }
        else{
            if(isset($data['final_retail']) && $data['final_retail'] < $data['price']){
                $custom_attributes->special_price = $data['final_retail'] * $minimumPackages;
            }
            else{
                $custom_attributes->special_price = $data['price'] * $minimumPackages;
            }
        }



//        if ($data['special_from_date']) {
//            $date = new DateTime($data['special_from_date']['value']);
//            $custom_attributes->special_from_date =$date->format('Y-m-d H:i:s');
//        }
//        if ($data['special_to_date']) {
//            $date = new DateTime($data['special_to_date']['value']);
//            $custom_attributes->special_to_date =$date->format('Y-m-d H:i:s');
//        }

        if(isset($data['max_discount'])){
            $custom_attributes->max_discount = $data['max_discount'];
        }




        $product->custom_attributes = $this->formatCustomAtrributes($custom_attributes);
        if(isset($tier_prices)){
            $product->tier_prices = $tier_prices;
        }
        return $product; //need to be array
    }
    public function run() {
        $this->getAllAttributeOptions();
        $this->getStoreViews();
        $this->getAttributeSets();
        $this->getCategoriesMapping();
        foreach ($this->items as $item) {

            $this->item = $item;

            //read one by one all files
            $this->debug("Parsing record id: ". $item['id'] ."<br>\n");
            if (file_exists($this->filestop)) {$this->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            if ($this->LoadData()) { //if created-updated
                $this->setXmlDataStatus($item['id'] ,self::STATUSSUCCESFULL); //set as processed succesfully
            }  else {
                //logError("Error Parsing file: " .$file);
                if ($item['status'] == self::STATUSUNPROSSESSED) {
                    //if this was the first time we process data retry on next run
                    $this->setXmlDataStatus($item['id'], self::STATUSRETRY);
                    $this->setXmlDataRetries($item['id'], "1"); //first try
                }
                else { //status 2, RETRY
                    if ($item['status'] == self::STATUSRETRY && $item['retries'] < $this->retries) {
                        $item['retries']++; //increase retries
                        $this->setXmlDataStatus($item['id'], self::STATUSRETRY);
                        $this->setXmlDataRetries($item['id'], $item['retries']); //
                    } else {
                        $this->setXmlDataStatus($item['id'], self::STATUSERROR);
                    }
                }
            }

            $this->heartbeat();

            if (!is_null($this->errors)) {
                $this->setXmlDataError($item['id'], $this->errors);
                $this->senderrormail();
            }

        }
        return true;
    }

    public function LoadData() { //reads the $productsxml file and creates-updates products
        try {


            $jsonData = $this->item['data'];
            $value = json_decode($jsonData, true);
            if (!$value)
            {
                $this->logError('Not Valid JSON document');
                return 0;
            }

            $return_val = 1;

            $flag = 1; //reset flag
            $errors = null; //delete error log
            $sku = $value['sku'];

            if(isset($value['price']) && $value['price'] == 0){
                return 1; //ignore products which come with zero price
            }



            $this->debug("importing price: sku= $sku; {$this->table} id=" .$this->item['id'] ."<br />");



            $productInfo=$this->getProductBySku($sku); //if product exists get info

            $livesku = $productInfo ? $productInfo['sku'] : false; //get sku from live product //get sku from live product

            if(!$livesku){
                return 1; //do nothing if product does not exist
            }

            $productData = $this->remapProductVars($value, $livesku);


            if ($livesku) {
                //if product exists use only the values contained in XML to update

                //$productData['sku']=$sku; //use this sku (if sku is numeric this sku has an m as first character)
                if ($productData->type_id === 'simple') {
                    $flag &= $this->updateOrCreateProduct ($sku, $productData);
                }
                elseif ($productData->type_id === 'configurable'){
                    $flag &= $this->updateOrCreateProduct($sku, $productData);
                }
                else {
                    $this->logError("Type_id = " .$productData->type_id ." not supported yet. Contact Administrator");
                    $flag = 0;
                }

                if ($flag)
                    $this->debug("Updating price: sku= $sku; was succesful<br />");
            }

            $return_val &= $flag; //flag for checking for errors
            return $return_val;
        } catch( SoapFault $fault ) {
            $this->logError($fault);
            return 0;
        }
    }
}
//##############################################3
error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);


//$main = new m2();
$main = new main();
//$main = $main;

$main->table = "connector_prices"; //###
$main->entity = 'ProductPriceImport'; //###
$main->blockingentities = array("ProductImport");


echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

//$main->sets = $main->config['set'];
//$main->languages = $main->config['languages'];


//########################################################
if ($main->canRun()) { //check if previous connection is running
    $main->heartbeat(); //start heartbeat

    if($main->config && $main->init_db()) {
        //file_put_contents($fileruntime, "operation started at " .gmdate(DATE_RFC822, time()));
        $main->items = $main->getReproccessedJsonData(); //get reproccesed data up to 3 times //read first the reprocessed
        $main->items = array_merge($main->items,$main->getUnproccessedJsonData()); //merge new files
        if ($main->config && count($main->items)) {
            if ($main->init_connector())
                $main->run();
        }
    }
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail

    //unlink($main->fileruntime);
    $main->allowRun();
    $main->close_connector();
}
else        {
    // $main->logError( "previous synchronization is running.");
    if ($main->errors)
        $main->senderrormail(); //sent if any errors mail
    $main->close_connector();
    return 0;
}
echo "End {$main->entity} \n\r";
//########################################################


//######################################################################################



