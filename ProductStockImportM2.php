<?php

include "commonM2.php";

class main extends m2 {




	public function run() {

		foreach ($this->items as $item) {

			$this->item = $item;

			//read one by one all files
			$this->debug("Parsing record id: ". $item['id'] ."<br>\n");
			if (file_exists($this->filestop)) {$this->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
			if ($this->LoadData()) //if created-updated

			{
				$this->setXmlDataStatus($item['id'] ,self::STATUSSUCCESFULL); //set as processed succesfully
			}
			else
			{
				//logError("Error Parsing file: " .$file);
				if ($item['status'] == self::STATUSUNPROSSESSED) {
					//if this was the first time we process data retry on next run
					$this->setXmlDataStatus($item['id'], self::STATUSRETRY);
					$this->setXmlDataRetries($item['id'], "1"); //first try
				}
				else { //status 2, RETRY
					if ($item['status'] == self::STATUSRETRY && $item['retries'] < $this->retries) {
						$item['retries']++; //increase retries
						$this->setXmlDataStatus($item['id'], self::STATUSRETRY);
						$this->setXmlDataRetries($item['id'], $item['retries']); //
					} else {
						$this->setXmlDataStatus($item['id'], self::STATUSERROR);
					}
				}
			}

            $this->heartbeat();

			if (!is_null($this->errors)) {
				$this->setXmlDataError($item['id'], $this->errors);
				$this->senderrormail();
			}

		}
		return true;
	}

	public function LoadData() { //reads the $productsxml file and creates-updates products
		try {



			$jsonData = $this->item['data'];
			$value = json_decode($jsonData, true);

			$return_val = 1;
            $flag = 1; //reset flag
            $errors = null; //delete error log


            $sku= $value['sku'];

            $qty= $value['qty'];


        //	$this->getStoreViews();
            $this->debug("stock importing: sku= $sku; {$this->table} id=" .$this->item['id'] ."<br />");




            try {
                //$flag = UpdateQTYfast($sku,$qty); //use api instead
                $flag &= boolval(intval($this->setStockBySku($sku,$qty)));

            } catch( SoapFault $fault ) {
                $this->logError($fault);
                $flag = 0;
            }
            if ($flag)
                $this->debug("Updating: sku= $sku; was succesfull<br />");

            $return_val &=  $flag; //flag for checking for errors

            //if (!($required & $flag)) //if error occured, save to db. Saves every error separate
            //saveProductError2DB($sku, $product_email, $sxml->asXML(), $errors); //save to db as xml

            //if (!is_null($errors))
            //senderrormail();

			return $return_val;
		} catch( SoapFault $fault ) {
			$this->logError($fault);
			return 0;
		}
	}
}
//##############################################3
error_reporting(E_ERROR | E_PARSE);
ob_implicit_flush(TRUE);


//$main = new m2();
$main = new main();
//$main = $main;

$main->table = "connector_stock"; //###
$main->entity = 'ProductStockImport'; //###
$main->blockingentities = array("ProductImport","connector_prices","connector_media");


echo "Start {$main->entity} ... \n\r";

$main->initConfig(); //create dynamic config variables
$main->debug ("{$main->entity} sync started..<br>\n", true);
$main->readConf("confs/conf.xml"); //read magento and db config

//$main->sets = $main->config['set'];
//$main->languages = $main->config['languages'];


//########################################################
if ($main->canRun()) { //check if previous connection is running
	$main->heartbeat(); //start heartbeat

	if($main->config && $main->init_db()) {
		//file_put_contents($fileruntime, "operation started at " .gmdate(DATE_RFC822, time()));
		$main->items = $main->getReproccessedJsonData(); //get reproccesed data up to 3 times //read first the reprocesede
		$main->items = array_merge($main->items,$main->getUnproccessedJsonData()); //merge new files
		if ($main->config && count($main->items)) {
			if ($main->init_connector())
				$main->run();
		}
	}
	if ($main->errors)
		$main->senderrormail(); //sent if any errors mail

	//unlink($main->fileruntime);
    $main->allowRun();
	$main->close_connector();
}
else        {
	$main->logError( "previous synchronization is running.");
	if ($main->errors)
		$main->senderrormail(); //sent if any errors mail
	$main->close_connector();
	return 0;
}
echo "End {$main->entity} \n\r";
//########################################################


//######################################################################################



