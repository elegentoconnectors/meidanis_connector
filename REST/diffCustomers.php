<?php
error_reporting(E_ERROR | E_PARSE);

echo "Start Customers Diff from Feed XML... \n\r";
include "s1Rest_common.php";
$s1rest = new s1rest();
$s1rest->datatable = "connector_customers_data";
$s1rest->entity = "CustomersDiff";
$s1rest->blockingentities = array();
$s1rest->sitename = "meidanis.gr";

$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

$fileruntime = "running/ERP2MCustomersDiff.txt";


if ($s1rest->canRun()) {
    $s1rest->heartbeat();

    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db'] );

    if ($s1rest->sqllink) {

        $jdata = $s1rest->getCustomersDataFromErpDb();
        $total= count($jdata);

        if($total > 0){
            $tempTable = $s1rest->createTempTableCopy();
            if(!$tempTable){
                $s1rest->logError("Connector could not create temp table.");
                $s1rest->emergencyExit();
            }

            $tempTableData = $s1rest->getIndexedTempTableData($tempTable);

            $total= count($jdata);

        }

        $i= 0;
        foreach ($jdata as $customerdata) {

            $i++;

            $customer = $s1rest->filterCustomers($customerdata);
//            $customer['name'] = 'test';

            echo "Exporting {$customer['TIN']} $i of $total\n\r";

            $ischanged = $s1rest->isRowChanged($tempTableData,'TIN', $customer);

            if ($ischanged === 0) { //new row
                $jsonsaved = $s1rest->saveDatatoDB(json_encode($customer), $customer['TIN'], $tempTable);

            }
            elseif ($ischanged === true){
                $jsonsaved = $s1rest->updateDatatoDB($tempTable, json_encode($customer), 'entity_id', $customer['TIN']);
            }
            else{

                $jsonsaved = 1;
            }


            if ($jsonsaved) {

            } else {
                $dbError = true;
                $s1rest->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                break; // exit now. will try to run again, next time..
            }

            $s1rest->heartbeat();
        }
        if($total > 0 && !isset($dbError)){
            $copied = $s1rest->copyTempToOrig();
        }
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    $s1rest->allowRun();
} else {
//$s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Product Diff from Feed XML... \n\r";




