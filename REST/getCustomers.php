<?php
error_reporting(E_ERROR | E_PARSE);

echo "Start Customer Import from ERP... \n\r";

include "s1Rest_common.php";
$s1rest = new s1rest();
$s1rest->entity = "CustomersImport";
$s1rest->sitename = "meidanis.gr";
$s1rest->datatable = "connector_customers_import";

$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

$configFile = "confs/ERP2M{$s1rest->entity}.conf";
$fileruntime = "running/ERP2M{$s1rest->entity}.txt";

if ($s1rest->canRun()) {
    $s1rest->heartbeat();
    
    $xmlconf=simplexml_load_file($configFile);
    if (!$xmlconf) {
        $s1rest ->logError('Not Valid XML config. file: ' .$configFile);
        return 0;
    }
    $last_export['lastTimeRun']=(string)$xmlconf->last_time_export; //get last time export is made
    
    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db'] );

    if ($s1rest->sqllink && $last_export) {
        //TODO make method to get last updated
        $upddate = $last_export['lastTimeRun'];
        
        //$jdata = $s1rest->getItemsBalance(array("UPDDATE"=>$upddate));
        $jdata = $s1rest->getErpCustomers($upddate);
        $s1rest->aasort($jdata, "ELG_UPD_DATE"); //sort based on update date
        $total= count($jdata);
        $i= 0;  
        foreach ($jdata as $customerdata) {
            if (file_exists($s1rest->filestop)) {$s1rest->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            $i++;
            echo "Exporting {$customerdata['EMAIL']} $i of $total\n\r";

            $milliSeconds = explode('.', $customerdata['ELG_UPD_DATE']);
            if(isset($milliSeconds[1])){
                $milliSeconds = $milliSeconds[1];
            }
            else{
                $milliSeconds = 0;
            }
            $lastupdate = date("Y-m-d H:i:s", strtotime($customerdata['ELG_UPD_DATE'])) .".$milliSeconds";
            $customerdata['customer_group_id'] = isset($s1rest->customerGroups[$customerdata['GXDSZNID']]) ? $s1rest->customerGroups[$customerdata['GXDSZNID']]['magento_id'] : '106'; //B2B General (id 106) by default
            if(stripos($customerdata['EMAIL'], '@') === false){ //if email field does not contain '@' symbol, then consider email as non valid and replace it with a '.' char
                $customerdata['EMAIL'] = '.';
            }
            $customerdata['EMAIL'] = preg_replace( "/\r|\n/", "", $customerdata['EMAIL']);
            $customerdata['EMAIL'] = str_replace( "'", ".", $customerdata['EMAIL']);
           // $xmlDoc = new SimpleXMLElement("<products></products>");
           // $xmlProducts = $s1rest->array_to_xml($product, $xmlDoc->addChild("product"));
            //$xmlstring = str_replace('\'', '"', $xmlDoc->asXML());
            $jsonCustomerData = json_encode($customerdata);
            $jsonSaved = $s1rest->saveDatatoDB($jsonCustomerData, $customerdata['EMAIL']);
            
            
            
            if ($jsonSaved) {
                $xmlconf->last_time_export = $lastupdate;
                $xmlconf->asXML($configFile); //success. Update values on export config
            } else {
                $s1rest->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                break; // exit now. will try to run again, next time..
            }

            $s1rest->heartbeat();
        }
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    //unlink($fileruntime);
    $s1rest->allowRun();
    // $s1rest->mysql_close();
} else {
$s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Customer Import from ERP... \n\r";





