<?php
error_reporting(E_ERROR | E_PARSE);

echo "Start Status Import from ERP... \n\r";

include "s1Rest_common.php";
$s1rest = new s1rest();
// $s1rest->service_url = 'https://01100299350811.oncloud.gr/s1services';
// $s1rest->appid = "1199";
$s1rest->datatable = "connector_orders_status";
$s1rest->entity = "OrderStatusImport";
$s1rest->sitename = "meidanis.gr";

$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

$configFile = "confs/ERP2M{$s1rest->entity}.conf";
$fileruntime = "running/ERP2M{$s1rest->entity}.txt";

if ($s1rest->canRun()) {
    $s1rest->heartbeat();
    
    $xmlconf=simplexml_load_file($configFile);
    if (!$xmlconf) {
        $this->logError('Not Valid XML config. file: ' .$configFile);
        return 0;
    }
    $lastDailyExport = $xmlconf->last_daily_export;
    $now = date('Y-m-d H:i:s');

    $lastDailyExportDateTime = new DateTime($lastDailyExport);
    $lastDailyExportDate = $lastDailyExportDateTime->format('Y-m-d H:i:s');
    $lastDailyExportDate = new DateTime($lastDailyExportDate);

    $nowDateTime = new DateTime($now);
    $nowDate = $nowDateTime->format('Y-m-d H:i:s');
    $nowDate = new DateTime($nowDate);


    $diff = $lastDailyExportDate->diff($nowDate)->format("%r%a");
    $dailyRun = false;
    $todayNoon = new DateTime(date('Y-m-d 13:00:00'));
    if(intval($diff) >= 1 && $nowDate >= $todayNoon){ //do not run before noon
        $dailyRun = true;
        $last_export['lastTimeRun'] = $lastDailyExportDate->format('Y-m-d 13:00:00');
    }

    else{
        $last_export['lastTimeRun']=(string)$xmlconf->last_time_export; //get last time export is made
    }
    
    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db'] );

    if ($s1rest->sqllink && $last_export) {

        $upddate = $last_export['lastTimeRun'];

        $jdata = $s1rest->getOrderStatusFromErp($upddate);
        $s1rest->aasort($jdata, "ELG_UPD_DATE"); //sort based on update date
        $total= count($jdata);
        $i= 0;  
        foreach ($jdata as $data) {
            $i++;
            echo "Exporting {$data['WEB_ORDER_NUMBER']} $i of $total\n\r";
           // $product = $s1rest->remapStock($data);
           // $lastupdate = $product['lastupdate'];

            $milliSeconds = explode('.', $data['ELG_UPD_DATE']);
            if(isset($milliSeconds[1])){
                $milliSeconds = $milliSeconds[1];
            }
            else{
                $milliSeconds = 0;
            }
            $lastupdate = date("Y-m-d H:i:s", strtotime($data['ELG_UPD_DATE'])) .".$milliSeconds";

            $order = $s1rest->remapOrderStatus($data);

            $jsonOrderData = json_encode($order);
            $jsonSaved = $s1rest->saveDatatoDB($jsonOrderData, $data['WEB_ORDER_NUMBER']);
            
            
            
            if ($jsonSaved) {
                $xmlconf->last_time_export = $lastupdate;
                $xmlconf->asXML($configFile); //success. Update values on export config
            } else {
                $s1rest->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                break; // exit now. will try to run again, next time..
            }

            $s1rest->heartbeat();
        }
        if($dailyRun){
            $xmlconf->last_daily_export = $now;
            $xmlconf->asXML($configFile); //success. Update values on export config
        }
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    //unlink($fileruntime);
    $s1rest->allowRun();
    // $s1rest->mysql_close();
} else {
$s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Order Status Import from ERP... \n\r";





