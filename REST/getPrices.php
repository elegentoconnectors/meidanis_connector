<?php
error_reporting(E_ERROR | E_PARSE);

echo "Start Price Import from ERP... \n\r";
include "s1Rest_common.php";
$s1rest = new s1rest();
// $s1rest->service_url = 'https://01100299350811.oncloud.gr/s1services';
// $s1rest->appid = "1199";
$s1rest->datatable = "connector_prices";
$s1rest->entity = "PriceImport";
$s1rest->blockingentities = array("ProductStockImport", "ProductImport");
$s1rest->sitename = "meidanis.gr";

$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

$configFile = "confs/ERP2MPriceImport.conf";
$fileruntime = "running/ERP2MPriceImport.txt";




if ($s1rest->canRun()) {
    $s1rest->heartbeat();

    $xmlconf=simplexml_load_file($configFile);
    if (!$xmlconf) {
        $s1rest->logError('Not Valid XML config. file: ' .$configFile);
        return 0;
    }
    $last_export['lastTimeRun']=(string)$xmlconf->last_time_export; //get last time export is made
    $last_export['lastSpecialPriceFromTo'] = (string)$xmlconf->last_time_special_from_to;


    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db'] );
    //$clientid = $s1rest->loginAndAuthenticate($s1rest->erpuser['user'],$s1rest->erpuser['pass']);

    if ($s1rest->sqllink && $last_export) {
        $now = $s1rest->getCurrentTimestampFromErp();
        if(isset($now[0])){
            $now = $now[0]['now'];
        }
        //TODO make method to get last updated
        $productsToUpdate = array();
        $upddate = $last_export['lastTimeRun'];
        $sp_price_from_to_upddate = $last_export['lastSpecialPriceFromTo'];

        // $jdata = $s1rest->getItems(array("UPDDATE"=>"20170118 22:40:48", "CODE"=> "1MBR002549;1MBR002552" ));
        //  $jdata = $s1rest->getItems(array('CODE'=>"1SRI003650"));
        $jdata = $s1rest->getItemPricesFromErpDb($upddate);
        $dataForSpecialPriceFrom = $s1rest->getItemPricesBySpecialFromErpDb($sp_price_from_to_upddate, $now);
        $dataForSpecialPriceTo = $s1rest->getItemPricesBySpecialToErpDb($sp_price_from_to_upddate, $now);
        $groupPrices = $s1rest->getAllItemCustomerGroupPrices();


        $s1rest->aasort($jdata, "ELG_UPD_DATE_PRICE"); //sort based on update date
        $s1rest->aasort($dataForSpecialPriceFrom, "ELG_SP_PRICE_FROM"); //sort based on update date
        $s1rest->aasort($dataForSpecialPriceTo, "ELG_SP_PRICE_TO"); //sort based on update date

        foreach ($jdata as $firstData){ //first loop through products that were fetched due to UPD_DATE_PRICE
            $productsToUpdate[$firstData['ELG_SKU']] = $firstData;
            $productsToUpdate[$firstData['ELG_SKU']]['special_price_to_insert'] = $s1rest->getSpecialPrice($firstData, $now);
        }

        foreach ($dataForSpecialPriceFrom as $secondData){ //then loop products that have a new special price based on SP_PRICE_FROM
            if(!isset($productsToUpdate[$secondData['ELG_SKU']])){
                $productsToUpdate[$secondData['ELG_SKU']] = $secondData;
                $productsToUpdate[$secondData['ELG_SKU']]['special_price_to_insert'] = $s1rest->getSpecialPrice($secondData, $now);
                $productsToUpdate[$secondData['ELG_SKU']]['ELG_UPD_DATE_PRICE'] = null; //null means ignore upddate and do not save at xml below
            }
            else{
                continue;
            }
        }

        foreach ($dataForSpecialPriceTo as $thirdData){ //then loop products that have an expired special price based on SP_PRICE_TO
            if(!isset($productsToUpdate[$thirdData['ELG_SKU']])){
                $productsToUpdate[$thirdData['ELG_SKU']] = $thirdData;
                $productsToUpdate[$thirdData['ELG_SKU']]['special_price_to_insert'] = $s1rest->getSpecialPrice($thirdData, $now);
                $productsToUpdate[$thirdData['ELG_SKU']]['ELG_UPD_DATE_PRICE'] = null; //null means ignore upddate and do not save at xml below
            }
            else{
                continue;
            }
        }




        //$jdata = $s1rest->splitProducts($jdata);
        $s1rest->aasort($productsToUpdate, "ELG_UPD_DATE_PRICE"); //sort based on update date
        $total= count($productsToUpdate);
        $i= 0;
        foreach ($productsToUpdate as $productdata) {
            if (file_exists($s1rest->filestop)) {$s1rest->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            $i++;
//            if($i < 18459){
//                var_dump($i);
//                continue;
//            }
            if($productdata['ELG_UPD_DATE_PRICE'] !== null){
                $milliSeconds = explode('.', $productdata['ELG_UPD_DATE_PRICE']);
                if(isset($milliSeconds[1])){
                    $milliSeconds = $milliSeconds[1];
                }
                else{
                    $milliSeconds = 0;
                }
                $lastupdate = date("Y-m-d H:i:s", strtotime($productdata['ELG_UPD_DATE_PRICE'])) .".$milliSeconds";
            }
            else{
                $lastupdate = null;
            }



            $productdata = $s1rest->filterProduct($productdata);

            $product = $s1rest->remapProduct($productdata,$groupPrices);


            echo "Exporting {$product['sku']} $i of $total\n\r";

            $jsonProductData = json_encode($product);

            $jsonSaved = $s1rest->saveDatatoDB($jsonProductData, $product['sku']);



            if ($jsonSaved) {
                if($lastupdate !== null){
                    $xmlconf->last_time_export = $lastupdate; //add 1 sec to try on next run //$last_export['lastTimeRun']; //Those values will be used ONLY if save is succesfull
                    $xmlconf->asXML($configFile); //success. Update values on export config
                }
            } else {
                $s1rest->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                break; // exit now. will try to run again, next time..
            }

            $s1rest->heartbeat();
        }

        $xmlconf->last_time_special_from_to = $now;
        $xmlconf->asXML($configFile); //success. Update values on export config
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    //unlink($s1rest->fileruntime);
    $s1rest->allowRun();
    // $s1rest->mysql_close();
} else {
//$s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Import from ERP... \n\r";




