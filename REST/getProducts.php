<?php
error_reporting(E_ERROR | E_PARSE);

echo "Start Import from ERP... \n\r";
include "s1Rest_common.php";
$s1rest = new s1rest();
// $s1rest->service_url = 'https://01100299350811.oncloud.gr/s1services';
// $s1rest->appid = "1199";
$s1rest->datatable = "connector_products";
$s1rest->entity = "ProductImport";
$s1rest->blockingentities = array("ProductStockImport");
$s1rest->sitename = "meidanis.gr";

$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

$configFile = "confs/ERP2MProductImport.conf";
$fileruntime = "running/ERP2MProductImport.txt";




if ($s1rest->canRun()) {
    $s1rest->heartbeat();
    
    $xmlconf=simplexml_load_file($configFile);
    if (!$xmlconf) {
        $s1rest->logError('Not Valid XML config. file: ' .$configFile);
        return 0;
    }

    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db']);

    $lastDailyExport = $xmlconf->last_daily_export;
    $now = date('Y-m-d H:i:s');

    $lastDailyExportDateTime = new DateTime($lastDailyExport);
    $lastDailyExportDate = $lastDailyExportDateTime->format('Y-m-d');
    $lastDailyExportDate = new DateTime($lastDailyExportDate);

    $nowDateTime = new DateTime($now);
    $nowDate = $nowDateTime->format('Y-m-d');
    $nowDate = new DateTime($nowDate);


    $diff = $lastDailyExportDate->diff($nowDate)->format("%r%a");
    $dailyRun = false;
    if(intval($diff) >= 1){
        $dailyRun = true;
        $last_export['lastTimeRun'] = $lastDailyExportDate->format('Y-m-d 00:00:00');
    }

    else{
        $last_export['lastTimeRun']=(string)$xmlconf->last_time_export; //get last time export is made
    }



    //$clientid = $s1rest->loginAndAuthenticate($s1rest->erpuser['user'],$s1rest->erpuser['pass']);
    
    if ($s1rest->sqllink && $last_export) {
        //TODO make method to get last updated
        $now = $s1rest->getCurrentTimestampFromErp(); //GET ERP TIME
        if(isset($now[0])){
            $now = $now[0]['now'];
        }
        $upddate = $last_export['lastTimeRun'];
        
       // $jdata = $s1rest->getItems(array("UPDDATE"=>"20170118 22:40:48", "CODE"=> "1MBR002549;1MBR002552" ));
     //  $jdata = $s1rest->getItems(array('CODE'=>"1SRI003650"));
        $jdata = $s1rest->getItemsFromErpDb($upddate);
        $groupPrices = $s1rest->getAllItemCustomerGroupPrices();
//        foreach ($jdata[0] as $ind => $dat){
//            var_dump(($ind));
//            var_dump(mb_detect_encoding($ind));
//        }

        //$jdata = array();
//        $i=0;
//        while($i<3){
//            $jdata[] = $jdataAll[$i];
//            $i++;
//        }
        //$jdata[] = $jdataAll[0];

        $s1rest->aasort($jdata, "ELG_UPD_DATE"); //sort based on update date


        //$jdata = $s1rest->splitProducts($jdata);
        $productArrayWithMaxUpddate = $s1rest->getMaxUpddates($jdata);

       $total= count($jdata);

        $s1rest->notifyAdminAboutProductQueueIfNeeded($total);

        $i= 0;     
        foreach ($jdata as $index => $productdata) {
            if (file_exists($s1rest->filestop)) {$s1rest->logError("Force Stop Detected. Exiting Now."); return 1;} //force stop
            $i++;
//            if($i < 18459){
//                var_dump($i);
//                continue;
//            }
            if(isset($productArrayWithMaxUpddate[$productdata['ELG_SKU']]) && $productArrayWithMaxUpddate[$productdata['ELG_SKU']]['index'] != $index){
                continue; //skip duplicate products with older upddate
            }
            $milliSeconds = explode('.', $productdata['ELG_UPD_DATE']);
            if(isset($milliSeconds[1])){
                $milliSeconds = $milliSeconds[1];
            }
            else{
                $milliSeconds = 0;
            }
            $lastupdate = date("Y-m-d H:i:s", strtotime($productdata['ELG_UPD_DATE'])) .".$milliSeconds";
            $productdata['special_price_to_insert'] = $s1rest->getSpecialPrice($productdata, $now);
            $productdata = $s1rest->filterProduct($productdata);
            if($productdata['FREE_QTY'] == ""){
                $productdata['FREE_QTY'] = 0;
            }
            //$productrunid = $s1rest->runId; //keep runId from products
            //$jstock = $s1rest->getErpItemsTotalBalance($productdata['ELG_ITEM_ID']);
            //$jstock['0']['quantity'] = 1;
            //$productdata['FREE_QTY'] = ($jstock != 0) ? $jstock[0]['FREE_QTY'] : 0;
            //$s1rest->runId = $productrunid; //restore runId if destroyed from getStock()


            //$photos = $s1rest->getRdcPhotosByParam(array('erpId' => $productdata['erpId']));

            //$productdata['photos'] = $photos;
            $product = $s1rest->remapProduct($productdata,$groupPrices);


            echo "Exporting {$product['sku']} $i of $total\n\r";
            $jsonProductData = json_encode($product);
//            $xmlDoc = new SimpleXMLElement("<products></products>");
//            $xmlProducts = $s1rest->array_to_xml($product, $xmlDoc->addChild("product"));
            //$xmlstring = str_replace('\'', '"', $xmlDoc->asXML());
            $jsonSaved = $s1rest->saveDatatoDB($jsonProductData, $product['sku']);

           /* if ($productdata['RINGITEM_CODE'] == $productdata['CODE']) { //if product has connection with itself, it is configurable, so we need to create a configurable product with the same values

                $confproduct = $product;

                echo "Exporting CONFIGURABLE {$product['sku']} $i of $total\n\r";
                $confproduct['type_id']= 'configurable';

                $confproduct['associated_products'] = implode(";", $s1rest->getAssociatedProducts($productdata['MTRL']));
                $confproduct['sku'] = "CF-" .$product['sku']; //configurable sku

                $xmlDoc = new SimpleXMLElement("<products></products>");
                $xmlProducts = $s1rest->array_to_xml($confproduct, $xmlDoc->addChild("product"));
                $xmlstring = str_replace('\'', '"', $xmlDoc->asXML());
                $s1rest->saveDatatoDB($xmlstring, $confproduct['sku']);
            }*/

            /* if ($productdata['RINGITEM_CODE'] == $productdata['CODE']) { //if product has connection with itself, it is configurable, so we need to create a configurable product with the same values
                
                $confproduct = $product;
            
                echo "Exporting CONFIGURABLE {$product['sku']} $i of $total\n\r";
                $confproduct['type_id']= 'configurable';
                
                $confproduct['associated_products'] = implode(";", $s1rest->getAssociatedProducts($productdata['MTRL']));
                $confproduct['sku'] = "CF-" .$product['sku']; //configurable sku
        
                $xmlDoc = new SimpleXMLElement("<products></products>");
                $xmlProducts = $s1rest->array_to_xml($confproduct, $xmlDoc->addChild("product"));
                $xmlstring = str_replace('\'', '"', $xmlDoc->asXML());
                $s1rest->saveDatatoDB($xmlstring, $confproduct['sku']);
            } */
            
            
            
            if ($jsonSaved) {
                $xmlconf->last_time_export = $lastupdate;
                $xmlconf->asXML($configFile); //success. Update values on export config
                //$s1rest->setProductAsUpdated($productdata['erpId']['value']); //set product as updated to ERP
            } else {
                $s1rest->logError("Export was completed but could not be saved as XML to db. Export will repeat again. No action needed.");
                break; // exit now. will try to run again, next time..
            }

            $s1rest->heartbeat();
        }
        if($dailyRun){
            $xmlconf->last_daily_export = $now;
            $xmlconf->asXML($configFile); //success. Update values on export config
        }
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    //unlink($s1rest->fileruntime);
    $s1rest->allowRun();
    // $s1rest->mysql_close();
} else {
//$s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Import from ERP... \n\r";




