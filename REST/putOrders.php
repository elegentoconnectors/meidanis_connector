<?php
error_reporting(E_ERROR | E_PARSE);

class m2e {
    public $params = null;
    public $saldoc = array();
    public $itemlines = array();
    public $vatnal = array();
    public $mtrdoc = array();
    public $expanal = array();
    public $trdr = null;
    public $customerdata = array();
    public $customerparams = array();
    public $cusextra = array();
    public $appname = 'matfashion';
    public $finstate = '100'; //code for NOT CHECKED
    public $salesman = 16;
    public $shipingEXPN = '104';
    public $billingEXPN = '105';
    public $TRDCATEGORY = 3000;
    public $TRDCATEGORYINVOICE = 3000;
    public $invoiceid = '31';
    public $invoicecode = "am_isinvoice";
    public $vatcode = 'am_vatid';
    public $invoiceCustomerCode = '97.99.*';
    public $nonInvoiceCustomerCode = '97.00.*';
    public $vatauthoritycode = 'am_doy';
    public $companyactivitycode = 'am_companyactivity';
    public $companynamecode = 'am_companyname';
    public $companyadresscode = 'am_companyaddress';
    public $companyPostcode = 'am_postalcode';
    public $companyCitycode = 'am_companycity';
    public $seiratimologio = "9004"; //timologio
    public $seiralianikis = "9003"; //lianiki
    public $erpOrderId;

    // public function makeSaldoc($series, $date, $orderid, $trdr, $comments, $comments1, $paymentmethod, $shipment, $payment){
    //     //         $this->saldoc = array('SERIES'=>$series, "NUM01"=>$orderid ,'TRNDATE'=>$date, 'TRDR'=>$trdr, 'COMMENTS'=>$comments, 'COMMENTS1'=>$comments1, 'VARCHAR02'=> $paymentmethod, 'SHIPMENT'=>$shipment);
    //     if (count($comments) == 0) $comments = '';
    //     if (count($comments1) == 0) $comments1 = '';
    //     $this->saldoc = array('SERIES'=>$series, "FINCODE"=>$orderid ,'TRNDATE'=>$date, 'TRDR'=>$trdr, 'COMMENTS'=>$comments, 'COMMENTS1'=>$comments1, 'VARCHAR02'=> $paymentmethod, 'SHIPMENT'=> $shipment, 'PAYMENT'=> $payment);
    //}
    public function makeSaldoc($series, $date, $orderid, $trdr, $comments, $comments1, $paymentmethod, $shipment, $payment, $sumamnt, $discount, $voucherPrice){
        //$date, $orderid, $trdr, $comments, $comments1, $paymentmethod, $shipment, $payment){
        //         $this->saldoc = array('SERIES'=>$series, "NUM01"=>$orderid ,'TRNDATE'=>$date, 'TRDR'=>$trdr, 'COMMENTS'=>$comments, 'COMMENTS1'=>$comments1, 'VARCHAR02'=> $paymentmethod, 'SHIPMENT'=>$shipment);
        if (count($comments) == 0) $comments = '';
        if (count($comments1) == 0) $comments1 = '';
        $this->saldoc = array('SERIES'=>$series, 'TRNDATE'=>$date, 'TRDR'=>$trdr, "FINSTATES"=>$this->finstate, 'SHIPMENT'=> $shipment, 'PAYMENT'=> $payment, 'REMARKS'=> $comments, "CCCWEBID"=>$orderid , "CCCGIFTWRAP" => 0, "SUMAMNT"=>floatval(number_format($sumamnt, 2)), "DISC1VAL" => floatval(number_format($discount, 2)), "CCCVoucherPrice" => (($voucherPrice != null) ? floatval(number_format($voucherPrice, 2)) : null) , "CCCCOMMENTS" => $comments);
    }
    public function addItemline($sku, $qty, $price, $specialprice = 0, $num, $comments = null, $orderid){
        $line = array('CCCWEBID'=>$orderid,'SRCHCODE'=>$sku, 'QTY1'=>$qty, 'PRICE'=>$price, 'DISC1VAL' => 0);
        if ($specialprice > 0) $line['DISC1VAL'] = floatval(number_format($specialprice, 4));
        $this->itemlines[] = $line;
    }
    public function makeVatanal($total, $vat, $vatclass){
        $this->vatnal = array('VAT'=>$vatclass, 'SUBVAL'=>$total-$vat, 'VATVAL'=>$vat);
    }
    public function makeMtrdoc($address, $zip, $city, $vatid, $name ,$phone) {
        $this->mtrdoc = array('SHIPPINGADDR'=>$address, 'SHPZIP'=>$zip, 'SHPCITY'=>$city, 'SHPDISTRICT' => $city);
    }
    public function makeExpanal($shipingcost, $cod) {

        $this->expanal[] = array('EXPN'=>$this->shipingEXPN, 'EXPVAL'=>number_format(($shipingcost /1.24),2 )); // Μεταφορικά Πωλήσεων
        $this->expanal[] = array('EXPN'=>$this->billingEXPN, 'EXPVAL'=>number_format(($cod /1.24), 2)); //Έξοδα Αντικαταβολής
        //TODO add $this->expanal[] 106 -> Έξοδα αντικαταβολής Κύπρος
    }
    public function makeOrderParams() {

        $params = array("service"=>"setData", "OBJECT"=>"SALDOC");
        if($this->finstate == '202'){ //cancel order in erp
            $params["FORM"] = "eSHOP"; //need this to cancel order in erp
            $params["KEY"] = $this->erpOrderId;
        }
        $params['DATA']['SALDOC'] = array($this->saldoc);
        $params['DATA']['ITELINES'] = $this->itemlines;
        $params['DATA']['EXPANAL'] = $this->expanal;
        //$params['DATA']['VATANAL'] = array($this->vatnal);
        $params['DATA']['MTRDOC'] = array($this->mtrdoc);
        $this->params = $params;
    }
    public function  makeUpdateFinstate() {
        $params = array("service"=>"setData", "OBJECT"=>"SALDOC");
        $params['DATA']['SALDOC'] = array('CCCFINSTATES'=>$this->finstate);
        $this->params = $params;
    }

    public function makeCustomerData($siteCustomerId,$code, $email, $firstname, $lastname, $phone1, $phone2, $address, $city, $zip, $vatid, $doy, $companyactivity , $isinvoice, $trdr, $companyname) {
        // $phone2 = '';
        //$phone1 = '';
        //if (strpos($phone, '69') === 0) {$phone2=$phone;}
        //else $phone1=$phone;

        //$code = $customerid ? $customerid : "*";

        $this->trdr = $trdr ? $trdr : null; //add trdr if exist

        //if (strtolower($email) == "tel@" .$this->appname .".gr" && $phone1) $email = $phone1 ."@" .$this->appname .".gr"; //change with {mobile}@hairwaybeauty.gr

        $this->customerdata = array("CODE"=> $code, "CCCWEBID" => $siteCustomerId,"EMAIL"=>$email, "AFM"=> $vatid, "CITY"=>$city, "ADDRESS"=> $address, "ZIP"=>$zip, "PHONE01"=> $phone1, "PHONE02" => $phone2, "IRSDATA"=>$doy, "JOBTYPETRD"=>$companyactivity, "TRDCATEGORY"=> $this->TRDCATEGORY);
        $this->customerdata["CCCFIRSTNAME"] = $firstname;
        $this->customerdata["CCCLASTNAME"] = $lastname;
        $this->customerdata["DISTRICT"] = $city;


        if (!$trdr) $this->customerdata["NAME"] = $lastname ." " .$firstname; //if customer is new create his name. if customer exist ignore
        $this->customerdata = array_filter( $this->customerdata, 'strlen');
        if ($isinvoice == $this->invoiceid) {
            $this->customerdata['TRDCATEGORY'] = $this->TRDCATEGORYINVOICE;
            $this->customerdata['NAME'] = $companyname;
        }

        // $this->cusextra = array('DATE04' => date('Y-m-d H:m:s'), 'DATE05' => date("Y-m-d H:m:s", strtotime("+5 years")), "CCCFXBOOL01" => $acceptim, "CCCFXBOOL02" => $acceptpersonal, "CCCFXBOOL03"=>$acceptsms,"CCCFXBOOL04"=>$acceptstore, "CCCFXBOOL05"=>$acceptmail, "CCCFXBOOL06"=>$accept, "CCCFXBOOL07"=>$acceptphone);
    }
    public function makeCustomerParams() {
        $params = array("service"=>"setData", "OBJECT"=>"CUSTOMER", "KEY"=> $this->trdr);
        $params['DATA']['CUSTOMER'] = array($this->customerdata);
        if (count($this->cusextra)) $params['DATA']['CUSEXTRA'] = array($this->cusextra);
        $this->customerparams = $params;
    }
}

echo "Start Order Export from Magento to ERP... \n\r";

include "s1Rest_common.php";
$s1rest = new s1rest();
// $s1rest->service_url = 'https://01100299350811.oncloud.gr/s1services';
// $s1rest->appid = "1199";
$s1rest->datatable = "connector_orders";
$s1rest->entity = "OrderExport";
$s1rest->sitename = "meidanis.gr";


$s1rest->debugfile = "debug/Debug{$s1rest->entity}.txt";
$s1rest->errorlogfile = "errors/{$s1rest->entity }Errors.log";
$s1rest->mailsubject = " e2s Error - {$s1rest->entity}";
$s1rest->fileruntime = 'running/' .$s1rest->entity .'Running.txt';

//$configFile = "confs/ERP2MStockImport.conf";
$fileruntime = "running/M2ERP{$s1rest->entity}.txt";

if ($s1rest->canRun()) {
    $s1rest->heartbeat();

    $s1rest->init_db($s1rest->dbconf['host'],$s1rest->dbconf['user'] ,$s1rest->dbconf['pass'] ,$s1rest->dbconf['db'] );

    if($s1rest->sqllink) {
        $files = $s1rest->getReproccessedJsonData($s1rest->retries);
        $files = array_merge($files,$s1rest->getUnproccessedJsonData()); //get reproccesed data up to 3 times
        if (count($files)) {
            OrderExport();
        }
    } else {
        $s1rest->logError("Connector could not be initiated. Contact admin.");
    }
    //unlink($fileruntime);
    $s1rest->allowRun();
    // $s1rest->mysql_close();
} else {
    $s1rest->logError("previous synchronization is running.");
}

$s1rest->close();
echo "End Order Export from Magento to ERP... \n\r";


function OrderExport()
{   global $s1rest;
    $files = $s1rest->getReproccessedJsonData($s1rest->retries);
    $files = array_merge($files,$s1rest->getUnproccessedJsonData()); //get reproccesed data up to 3 times
    if (count($files)) {
        $i=0;
        foreach ($files as $file) {
            //read one by one all files
            $s1rest->debug("Parsing record id: ". $file['id'] ."<br>\n");


            if (LoadOrderData($file)) //if order created-updated move xml to history
            {
                $s1rest->setXmlDataStatus($file['id'] ,1); //set as processed succesfully
            }
            else
            {
                if (!is_null($s1rest->latesterror)) {
                    $s1rest->setXmlDataError($file['id'], $s1rest->latesterror);
                    $s1rest->latesterror = null;
                }

                //logError("Error Parsing file: " .$file);
                if ($file['status'] == 0 ) {
                    //if this was the first time we process data retry on next run
                    $file['status'] = 2; //status 2 means rerty
                    $s1rest->setXmlDataStatus( $file['id'], $file['status']);
                }
                else if ($file['status'] > 1) {
                    //if status is 2 or up (process again) mark it as fault
                    $file['status']++; //every value above 2 is fault counter
                    $s1rest->setXmlDataStatus( $file['id'], $file['status']);
                    //      if ($file['status'] > $config['retries'])
                    //     $s1rest->logError("unrecoverable error on $table id=" .$file['id'] ." retries =" .$config['retries']);
                }
            }

            $s1rest->heartbeat();

            if (!is_null($s1rest->latesterror)) {
                $s1rest->setXmlDataError($file['id'], $s1rest->latesterror);
                $s1rest->latesterror = null;
            }
        }
    }

    $s1rest->debug("Order Import ended..<br>\n");
    return 1;
}

function clearItems($order){
    foreach($order['items'] as $key => $item){

        // Skip specific skus that contain the 'not-erp' code
        if (mb_stripos($item['item_sku'], 'not-erp') !== false) {
            unset($order['items'][$key]);
        }
    }

    return $order['items'];
}

function LoadOrderData($file) {
    global $s1rest;
    $m2e = new m2e();
    try {
        $newVatCustomer = false;
        $jsonData = $file['data'];

        $order = json_decode($jsonData, true);

        if (!$order)
        {
            $s1rest->logError('Not Valid JSON document');
            return 0;
        }
        $order['items'] = clearItems($order);
        $return_val = 1;

        $flag = 1; //reset flag
        $s1rest->latesterror = null;

        $orderid = $order['increment_id'];
        $erporder = $s1rest->getErpOrder($orderid);
        if($erporder){
//            if(isset($erporder[0]['erpId'])){
//                $m2e->erpOrderId = $erporder[0]['erpId'];  //save erp id in order to send it if it needs to be canceled at erp
//            }
            $s1rest->checkAndUpdateSkroutzVoucher($order);
            return 1;
        }
        
        $status = $order['status'];
        if ($status != 'pending' && $status !='processing' && $status !='skroutz_pending') return 1; //skip orders that are not ok
        //if ($erporder && $erporder[0]['CCCFINSTATES'] != '5') continue; //if order is imported and was not pending_payment, ignore
        if ($erporder === false) {return 0;} //false is when query fails, try again
//        elseif (isset($erporder[0]['erpId']) && $status != 'canceled' && $status != 'closed') return 1; //if order is imported and is not canceled or closed from magento, ignore
//        elseif(($status == 'canceled' || $status == 'closed') && $erporder['okforcancel'] == '0'){
//            return 1; //if order is canceled or closed from eshop but erp says it cannot be canceled through eshop, ignore
//        }
//        elseif(($status == 'canceled' || $status == 'closed') && !isset($erporder[0]['erpId'])){   //order needs to be canceled but does NOT exist in erp
//            $s1rest->logError("Order #" .$orderid .' needs to be canceled but does NOT exist in erp. Skipping...');
//            return 1;
//        }
        $isinvoice = $order[$m2e->invoicecode];
        $isSkroutz = $s1rest->isSkroutzOrder($order);


        //TODO use isInvoice() function everywhere
        if(isInvoice($order ,$m2e)){
            $mapped_nomos = mapZipToNomos($order['am_companyzipcode']);
        }


        $prefectureCode = isInvoice($order ,$m2e) ? mapRegionCodeToErpCode($mapped_nomos) : mapRegionCodeToErpCode($order['billing_region_code']);
        $taxCode = isset($order['am_doy']) ? mapTaxOfficeToCode($order['am_doy']) : false;
        $country = '';
        if($order['billing_country_id'] == 'GR'){
            $country = 'Ελλάδα';
        }
        elseif($order['billing_country_id'] == 'CY'){
            $country = 'Κύπρος';
        }



        //task #11862
        //created 3 mapping arrays
        //We will send 2 extra fields in the ERP containing category name and category number only for those specific kategories
        //based on the mapping arrays, otherwise we return ""
        $hlektrologosCode = "02";
        $kataskeuasthsCode = "06";
        $ksenodoxeioCode = "12";

        $hlektrologosCategoryMappingArray = array("ΗΛΕΚΤΡΟΛΟΓΟΣ",
            "45310000 ΚΑΛΩΔΙΩΣΕΙΣ ΚΑΙ ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣ",
            "45311100 ΕΡΓΑΣΙΕΣ ΗΛΕΚΤΡΙΚΩΝ ΚΑΛΩΔΙΩΣΕΩΝ ΚΑΙ ΕΞ",
            "45311092 ΥΠΗΡΕΣΙΕΣ ΕΠΙΣΚΕΥΗΣ ΗΛΕΚΤΡΙΚΩΝ ΚΑΛΩΔΙΩΣΕ",
            "45311091 ΥΠΗΡΕΣΙΕΣ ΕΠΙΣΚΕΥΗΣ ΗΛΕΚΤΡΙΚΩΝ ΚΑΛΩΔΙΩΣΕ",
            "51151025 ΑΝΤΙΠΡΟΣΩΠΕΙΑ Η ΜΕΣΙΤΕΙΑ ΠΩΛΗΣΗΣ ΗΛΕΚΤ",
            "EMΠOPIO HΛ/YΛIKOY",
            "EMΠ.HΛ/KOY YΛIKOY",
            "HΛEKTPOΛOΓOΣ",
            "HΛEK/KA-ΔOMIKA EP",
            "HΛ/KEΣ EΓK/ΣEΙΣ",
            "HΛEK/MHXANOΛOΓIKA",
            "HΛEKTPIKA EIΔH",
            "HΛEKTPOMHX/KA EPΓ",
            "HΛEK/KA EPΓA",
            "HΛEKTPONIKA",
            "HΛEK/ΓOΣ-MHX/ΓOΣ",
            "HΛ.EΓKATAΣTAΣEIΣ",
            "HΛ.KΛIM.ΦΩT.EΓKAT",
            "MHXANOΛOΓOΣ-HΛEKT",
            "MHXANOΛOΓOΣ",
            "XΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΑΛΛΩΝ ΜΕΡΩΝ ΗΛΕΚΤΡΟΝΙΚΩΝ ΛΥΧΝΙΩΝ",
            "ΑΛΛΕΣ ΕΝΣΥΡΜΑΤΕΣ (ΚΑΛΩΔΙΑΚΕΣ) ΔΙΑΔΙΚΤΥΑΚΕΣ ΥΠΗΡΕΣ",
            "ΑΠΟΘΗΚΗ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ",
            "ΑΠΟΘΗΚΗ ΗΛΕΚ/ΚΟΥ ΥΛΙΚΟΥ-ΕΜΠΟΡΙΟ ΚΛΙΜΑΤΙΣΤΙΚΩΝ",
            "ΒΙΟΤ.ΗΛΕΚ.ΕΙΔΩΝ",
            "ΒΙΟΜΗΧΑΝΙΚΟ ΗΛΕΚ/ΚΟ & ΗΛΕΚΤΡΟΝΙΚΟ ΥΛΙΚΟ",
            "ΒΙΟΤΕΧΝΙΑ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΠΙΝΑΚΩΝ",
            "ΒΙΟΜΗΧΑΝΙΑ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ",
            "ΕΓΚΑΤΑΣΤΑΣΕΙΣ- ΗΛΕΚΤΡΟΜΗΧΑΝΟΛΟΓΙΚΩΝ ΕΡΓΩΝ",
            "ΕΙΔΙΚΟ ΗΛΕΚ/ΚΟ ΥΛΙΚΟ ΒΙΟΜΗΧΑΝΙΑΣ",
            "ΕΙΣΑΓΩΓΗ & ΕΜΠΟΡΙΑ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ",
            "ΕΡΓΑΣΙΕΣ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΕΓΚΑΤΑΣΤΑΣΕΩΝ",
            "ΗΛΕΚΤΡΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΡΓΑΣΙΕΣ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΝΙΚΩΝ ΥΠΟΛΟΓΙΣΤΩΝ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ",
            "ΕΜΠΟΡΙΑ & ΕΠΙΣΚΕΥΗ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ & ΗΛΕΚΤΡΟΝΙΚΟΥ ΥΛΙΚΟΥΥ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΝΙΚΟΥ ΥΛΙΚΟΥ",
            "ΗΛΕΚΤΡΟΜΗΧΑΝΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΒΙΟΜΗΧΑΝΙΚΩΝ ΠΡΟΙΟΝΤΩΝ",
            "ΗΛΕΚ/ΚΟ ΥΛΙΚΟ ΚΑΙ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΡΟΝΙΚΑ ΕΙΔΗ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΝΙΚΩΝ ΕΙΔΩΝ",
            "ΗΛΕΚΤΡΙΚΕΣ ΚΑΤΑΣΚΕΥΕΣ",
            "ΗΛΕΚΤΡΟΤΕΧΝΙΚΑ ΕΡΓΑ ΚΑΙ ΠΡΟΙΟΝΤΑ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΟ ΥΛΙΚΟ",
            "ΗΛΕΚΤΡΟΜΗΧΑΝΙΚΑ ΕΡΓΑ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΟΣ ΕΞΟΠΛΙΣΜΟΣ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ- ΜΗΧΑΝΙΚΟΣ",
            "ΗΛΕΚΤΡΙΚΕΣ ΕΓΚΑΤΑΣΑΣΕΙΣ-ΕΜΠΟΡΙΟ ΗΛΕΚ/ΚΩΝ ΕΙΔΩΝ",
            "ΕΜΠΟΡΙΑ & ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΙΚΩΝ ΠΙΝΑΚΩΝ",
            "ΗΛΕΚΤΡΙΚΑ ΕΙΔΗ & ΑΝΤΑΛΛΑΚΤΙΚΑ ΗΛ.ΕΙΔΩΝ",
            "ΗΛ/ΓΟΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΑ - ΕΙΔΗ ΔΩΡΩΝ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ Τ.Ε.",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ -ΜΗΧΑΝ./ ΟΙΚΟΔΟΜΙΚ. ΕΠΙΧΕΙΡ.",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ-ΜΗΧΑΝΟΛΟΓΟΣ",
            "ΗΛΕΚΤΡΙΚ. ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛ/ΚΕΣ ΕΡΓ.ΕΓΚ/ΣΕ",
            "ΗΛ/ΚΕΣ ΕΓΚΑΤ.",
            "ΗΛΕΚΤΡΙΚΑ - ΥΔΡΑΥΛΙΚΑ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ/ΗΛΕΚΤΡΟΝΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΟΥ ΡΕΥΜΑΤΟΣ",
            "ΗΛΕΚΤΡΟΜΗΧΑΝΟΛΟΓΙΚΑ ΕΡΓΑ",
            "ΗΛ.ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΡΙΚΕΣ ΕΡΓΑΣΙΕΣ",
            "ΗΛΕΚΤΡΙΚΑ ΕΙΔΗ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΑ ΕΡΓΑ - ΥΔΡΑΥΛΙΚΑ",
            "ΗΛΕΚΤΡΙΚ.ΠΙΝΑΚΕΣ",
            "ΗΛΕΚΤΡΟΝΙΚΑ",
            "ΗΛΕΚΤΡ/ΚΑ ΕΙΔΗ",
            "ΗΛΕΚ/ΓΟΣ-ΜΗΧ/ΓΟΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ-ΣΥΣΤΗΜΑΤΑ ΣΥΝΑΓΕΡΜΟΥ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΜΗΧΑΝΟΛΟΓΟΣ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΕΓΚΑΤΑΣΤΑΤΗΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΑ ΕΡΓΑ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ & ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ",
            "ΗΛΕΚΤΡΟΛ-ΗΛΕΚΤΡΟΝ ΥΛΙΚΟ-Η/Υ ΕΠΙΣΚΕΥΕΣ & ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΕΜΠ.ΗΛΕΚΤΡ.ΕΙΔΝ",
            "ΗΛΕΚΤΡΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ-ΑΥΤΟΜΑΤΙΣΜΟΙ",
            "ΗΛΕΚΤΡΟΝΙΚΕΣ ΕΠΙΣΚΕΥΕΣ",
            "ΕΜΠΟΡΙΑ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ-ΛΙΠΑΝΤΙΚΩΝ-ΑΝΤ/ΚΩΝ ΑΥΤΟΚΙΝΗΤΩΝ",
            "ΗΛΕΚΤΡΟΝΙΚΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΗΛΕΚΤΡ.-ΜΗΧΑΝΟΛ.& ΤΕΧΝ.ΕΡΓΩΝ ΑΝΩΝ.ΕΜΠΟΡ.ΕΤ.",
            "ΗΛΕΚΤΡΟΝΙΚΟΣ",
            "ΕΜΠΟΡΙΑ ΗΛΕΚ/ΚΟΥ ΥΛΙΚΟΥ Κ ΗΛΕΚΤΡΟΝΙΚΟΥ ΥΛΙΚΟΥ",
            "ΗΛ/ΓΟΣ ΜΗΧΑΝ/ΚΟΣ",
            "ΗΛΕΚΤΡΟΝΙΚΟ ΕΡΓ.ΕΜΠΟΡΙΟ ΣΥΣ/ΩΝ & Η/Υ",
            "ΗΛΕΚΤΡΟΝ.-ΗΛΕΚΤΡΟΛ. ΕΦΑΡΜΟΓΕΣ",
            "ΕΜΠΟΡΙΑ ΗΛΕΚ.ΕΙΔΩ",
            "ΗΛΕΚΤΡΟΜΗΧΑΝΟΛΟΓΙΚΟ ΕΜΠΟΡΙΟ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ-ΕΡΓΑ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ - ΗΛΕΚΤΡΟΝΙΚΟΣ",
            "ΕΜΠΟΡΙΟ ΘΕΡΜΑΝΤΙΚΩΝ ΣΩΜΑΤΩΝ",
            "ΗΛΕΚΤΡΟΝΙΚΟΣ-ΗΛΕΚΤΡΟΛΟΓΙΚΟΣ ΕΞΟΠΛΙΣΜΟΣ",
            "ΗΛ/ΚΟ ΥΛΙΚΟ",
            "ΕΡΓΑΣΙΕΣ ΗΛΕΚΤΡ.ΕΓΚ/ΣΕΩΝ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΩΝ ΠΙΝΑΚΩΝ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ & ΗΛΕΚΤΡΟΝΙΚΟΥ ΥΛΙΚΟΥ",
            "ΗΛΕΚΤΡ. ΕΓΚΑΤ/ΣΕΙΣ - ΚΑΥΣΤΗΡΕΣ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΜΗΧΑΝΟΛΟΓΟΣ ΤΕ",
            "ΕΠΙΣΚΕΥΕΣ - ΕΜΠΟΡΙΑ ΗΛΕΚΤΡΙΚΩΝ & ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ",
            "ΗΛ. ΕΓΚΑΤ/ΣΕΙΣ - ΟΙΚΟΔ. ΕΠΙΧΕΙΡ.",
            "ΗΛΕΚΤΡΟΒΙΟΜΗΧΑΝΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΣΕΙΣ & ΤΕΧΝΙΚΕΣ ΕΦΑΡΜΟΓΕΣ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ- ΜΗΧΑΝ/ΚΕΣ ΚΑΤΑΣΚΕΥΕΣ",
            "ΗΛΕΚΤΡΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΗΣ",
            "ΗΛΕΚΤΡΙΚΑ-ΕΜΠΟΡΙΟ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ & ΤΕΧ/ΓΙΑΣ ΥΠΟΛΟΓΙΣΤΩΝ",
            "ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΝΙΚΩΝ ΚΑΙ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΕΙΔΩΝ",
            "ΗΛΕΚΡΤΙΚΟΙ ΠΙΝΑΚΕΣ - ΑΥΤΟΜΑΤΙΣΜΟΙ",
            "Η/Υ ΕΞΟΠΛ.ΠΛΗΡΟΦΟ",
            "ΗΛΕΚΤΡΙΚΑ-ΗΛΕΚΤΡΟΝΙΚΑ",
            "ΗΛΕΚΤΡ/ΚΕΣ -ΗΛΕΚΤΡΟΝ/ΚΕΣ ΕΓΚΑΤ/ΣΕΙΣ ΕΜΠΟΡΙΟ",
            "ΗΛΕΚΤΡΟΠΑΡΟΧΙΚΑ ΣΥΣΤΗΜΑΤΑ",
            "ΗΛΕΚ/ΚΑ ΤΕΧΝΙΚΑ ΕΡΓΑ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ ΑΥΤΟΚΙΝΗΤΩΝ",
            "ΗΛΕΚΤΡΟΛΟΓΙΚΕΣ ΥΔΡΑΥΛΙΚΕΣ ΥΠΗΡΕΣΙΕΣ",
            "ΗΛΕΚΤΡΟΛΟΓΟΣ- ΜΗΧΑΝΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ ΕΔΕ",
            "ΗΛΕΤΡΟΝΙΚΑ ΣΥΣΤΗΜΑΤΑ",
            "ΗΛΕΚ/ΜΗΧΑ/ΚΑ ΕΡΓΑ",
            "ΕΜΠΟΡΙΟ ΗΛ/ΥΛΙΚΟΥ",
            "ΕΜΠΟΡΙΟ ΗΛ/ΚΟΥ ΥΛ.& ΠΑΡΟΧΗ",
            "ΕΠΙΣΚΕΥΗ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ",
            "ΕΜΠΟΡΙΟ - ΗΛΕΚΤΡΟΜΗΧΑΝΟΛΟΓΙΚΑ ΕΡΓΑ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΤΙΡΙΩΝ ΓΙΑ ΚΑΤΟΙΚΙΕΣ ΚΑ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΝΙΚΩΝ ΟΛΟΚΛΗΡΩΜΕΝΩΝ ΚΥΚΛΩΜΑΤΩΝ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΙΚΩΝ ΠΙΝΑΚΩΝ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΙΚΩΝ ΠΥΚΝΩΤΩΝ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΝΙΚΩΝ ΣΥΣΤΗΜΑΤΩΝ ΑΝΙΧΝΕΥΣΗΣ, ΕΛΕΓ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΦΩΤΙΣΤΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΔΙΑΓΝΩΣΤΙΚΩΝ ΣΥΣΚΕΥΩΝ ΠΟΥ ΧΡΗΣΙΜ",
            "ΚΑΤΑΣΚΕΥΗ ΠΙΝΑΚΩΝ ΚΑΙ ΑΛΛΩΝ ΔΙΑΤΑΞΕΩΝ, ΕΞΟΠΛΙΣΜΕΝ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΝΙΚΩΝ ΕΞΑΡΤΗΜΑΤΩΝ",
            "ΚΑΤΑΣΚΕΥΗ ΠΙΝΑΚΩΝ ΚΑΙ ΑΛΛΩΝ ΔΙΑΤΑΞΕΩΝ, ΕΞΟΠΛΙΣΜΕΝΩ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΝΙΚΩΝ ΥΠΟΛΟΓΙΣΤΩΝ ΚΑΙ ΠΕΡΙΦΕΡΕΙΑ",
            "ΚΑΤΑΣΚΕΥΗ ΠΙΝΑΚΩΝ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΙΚΩΝ ΣΥΣΚΕΥΩΝ ΓΙΑ ΤΗΝ ΕΝΣΥΡΜΑΤΗ ΤΗ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΙΚΩΝ ΣΥΣΚΕΥΩΝ ΓΙΑ ΤΗΝ ΕΝΣΥΡΜΑΤΗ Τ",
            "ΚΑΤΑΣΚΕΥΗ ΗΛΕΚΤΡΟΝΙΚΩΝ ΣΥΣΤΗΜΑΤΩΝ ΑΝΙΧΝΕΥΣΗΣ, ΕΛΕ",
            "ΚΑΤΑΣΚΕΥΗ ΜΕΡΩΝ ΑΛΛΟΥ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ· ΗΛΕΚ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ ΥΨΗΛΗΣ ΤΑΣΕ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΩΝ ΣΤΟΙΧΕΙΩΝ ΚΑΙ ΗΛΕΚΤΡΙ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ ΦΩΤΙΣΜΟΥ Η",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΛΟΓΙΚΟΥ ΥΛΙΚΟΥ ΥΨΗΛΗΣ ΤΑΣ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΩΝ ΠΙΝΑΚΩΝ ΚΑΙ ΑΛΛΩΝ ΔΙΑ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΙΚΩΝ ΠΙΝΑΚΩΝ ΚΑΙ ΑΛΛΩΝ ΔΙΑΤ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΚΙΝΗΤΗΡΩΝ, ΗΛΕΚΤΡΟΓΕΝΝΗΤΡ",
            "ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ ΗΛΕΚΤΡΟΚΙΝΗΤΗΡΩΝ, ΗΛΕΚΤΡΟΓΕΝΝΗΤΡΙ",
            "ΥΠΗΡΕΣΙΕΣ ΜΕΛΕΤΩΝ ΗΛΕΚΤΡΟΛΟΓΟΥ ΜΗΧΑΝΙΚΟΥ ΓΕΝΙΚΑ",
            "ΥΠΗΡΕΣΙΕΣ ΕΠΙΣΚΕΥΗΣ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΕΓΚΑΤΑΣΤΑΣΕΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΕΚΠΟΝΗΣΗΣ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΜΕΛΕΤΩΝ ΚΤΙΡΙΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΣΧΕΔΙΩΝ ΤΕΧΝΟΛΟΓΟΥ ΗΛΕΚΤΡΟΛΟΓΟΥ ΜΗΧΑΝΙΚΟΥ",
            "ΥΠΗΡΕΣΙΕΣ ΕΚΠΟΝΗΣΗΣ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΜΕΛΕΤΩΝ ΚΤΙΡΙΩ");
        $kataskeuasthsCategoryMappingArray = array("ΥΠΗΡΕΣΙΕΣ ΕΚΠΟΝΗΣΗΣ ΤΕΧΝΙΚΩΝ ΜΕΛΕΤΩΝ Γ",
            "74203400 ΥΠΗΡΕΣΙΕΣ ΕΚΠΟΝΗΣΗΣ ΤΕΧΝΙΚΩΝ ΜΕΛΕΤΩΝ Γ",
            "74203300 ΥΠΗΡΕΣΙΕΣ ΕΚΠΟΝΗΣΗΣ ΤΕΧΝΙΚΩΝ ΜΕΛΕΤΩΝ Γ",
            "CONSTRUCTION COMPANY",
            "EPΓOΛ.ΔHMOΣ.EPΓΩN",
            "KATAΣK/KH ETAIPIA",
            "KATAΣKEYAΣTIKH",
            "KATAΣ/KH ETAIPEIA",
            "TEXNIKH ETAIPEIA",
            "OIKOΔOMIKEΣ EΠIX.",
            "TEXNIKH-EMΠOPIKH",
            "TEXNIKO ΓPAΦEIO",
            "OIKOΔOM.EPΓAΣIEΣ",
            "MHXANIKOΣ",
            "TEXNIKH KATAΣ/KH",
            "OIKOΔOM.EΠIX/ΣEIΣ",
            "YΠEP.AΣΦAΛEIAΣ",
            "ΑΛΛΕΣ ΕΡΓΑΣΙΕΣ ΕΓΚΑΤΑΣΤΑΣΕΩΝ Π.Δ.Κ.Α.",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΕΣ ΠΟΛΙΤΙΚΟΥ ΜΗΧΑΝΙΚΟΥ Π.Δ.Κ.Α.",
            "ΑΛΛΕΣ ΕΞΕΙΔΙΚΕΥΜΕΝΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΔΡΑΣΤΗΡΙΟΤΗΤ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ, ΠΟΥ ΔΕΝ ΚΑΤΟΝΟΜΑΖΟ",
            "ΑΛΛΕΣ ΕΞΕΙΔΙΚΕΥΜΕΝΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΔΡΑΣΤΗΡΙΟΤΗΤΕ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ, ΠΟΥ ΔΕΝ ΚΑΤΟΝΟΜΑΖ",
            "ΑΛΛΕΣ ΕΠΑΓΓΕΛΜΑΤΙΚΕΣ, ΕΠΙΣΤΗΜΟΝΙΚΕΣ ΚΑΙ ΤΕΧΝΙΚΕΣ Δ",
            "ΑΛΛΕΣ ΕΠΑΓΓΕΛΜΑΤΙΚΕΣ, ΤΕΧΝΙΚΕΣ ΚΑΙ ΕΠΙΧΕΙΡΗΜΑΤΙΚΕΣ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΑΛΛΕΣ ΕΡΓΑΣΙΕΣ ΟΛΟΚΛΗΡΩΣΗΣ (ΑΠΟΠΕΡΑΤΩΣΗΣ) ΚΤΙΡΙΩΝ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΕΣ ΠΟΛΙΤΙΚΟΥ ΜΗΧΑΝΙΚΟΥ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΟΛΟΚΛΗΡΩΣΗΣ ΚΑΙ ΤΕ",
            "ΑΛΛΕΣ ΕΠΑΓΓΕΛΜΑΤΙΚΕΣ, ΕΠΙΣΤΗΜΟΝΙΚΕΣ ΚΑΙ ΤΕΧΝΙΚΕΣ",
            "ΑΛΛΕΣ ΕΡΓΑΣΙΕΣ ΔΙΑΣΤΡΩΣΗΣ ΚΑΙ ΕΠΕΝΔΥΣΗΣ ΔΑΠΕΔΩΝ, Ε",
            "ΑΛΛΕΣ ΕΡΓΑΣΙΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΩΝ ΕΓΚΑΤΑΣΤΑΣΕΩΝ",
            "ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΟΛΟΚΛΗΡΩΣΗΣ ΚΑΙ ΤΕΛ",
            "ΑΛΛΕΣ ΕΡΓΑΣΙΕΣ ΔΙΑΣΤΡΩΣΗΣ ΚΑΙ ΕΠΕΝΔΥΣΗΣ ΔΑΠΕΔΩΝ,",
            "ΑΛΛΕΣ ΕΠΑΓΓΕΛΜΑΤΙΚΕΣ, ΤΕΧΝΙΚΕΣ ΚΑΙ ΕΠΙΧΕΙΡΗΜΑΤΙΚΕ",
            "ΑΛΛΕΣ ΥΠΗΡΕΣΙΕΣ ΠΑΡΟΧΗΣ ΕΠΙΣΤΗΜΟΝΙΚΩΝ ΚΑΙ ΤΕΧΝΙΚΩ",
            "ΑΛΛΕΣ ΥΠΗΡΕΣΙΕΣ ΠΑΡΟΧΗΣ ΕΠΙΣΤΗΜΟΝΙΚΩΝ ΚΑΙ ΤΕΧΝΙΚΩΝ",
            "ΑΝΑΠΤΥΞΙΑΚΗ-ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ",
            "ΑΝΕΓΕΡΣΗ & ΕΚΜΕΤΑΛ.ΑΚΙΝΗΤΩΝ",
            "αναπτυξη ακινητων",
            "ΑΝΩΝΥΜΗ ΤΕΧΝΙΚΗ ΕΜΠΟΡΙΚΗ ΕΤΑΙΡΙΑ",
            "ΒΙΟΜΗΧΑΝΙΚΗ ΕΜΠΟΡΙΚΗ ΚΑΙ ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΓΕΝΙΚΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΟΙΚΙΣΤΙΚΑ ΚΤ",
            "ΓΕΝΙΚΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΟΙΚΙΣΤΙΚΑ ΚΤΙ",
            "ΓΕΝΙΚΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΜΗ ΟΙΚΙΣΤΙΚΑ",
            "ΓΕΝ. ΚΑΤ/ΚΕΣ ΕΡΓΑΣΙΕΣ",
            "ΓΕΩΡΓΙΚΑ ΕΦΟΔΙΑ-ΤΕΧΝ.ΚΑΤΑΣΚΕΥΕΣ",
            "ΓΕΝΙΚΕΣ ΚΑΤΑΣΚ/ΚΕΣ ΕΡΓΑΣΙΕΣ",
            "ΓΕΝΙΚΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΕΡΓΟΣΤΑΣΙΑ Ε",
            "ΔΟΜΗΣΗ ΕΣΩΤ.ΧΩΡΟΥ",
            "ΕΜΠΟΡΙΚΗ ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΕΜΠΟΡΙΚΗ ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΕΝΕΡΓΕΙΑΚΗ ΕΤΑΙΡΕΙΑ",
            "ΕΜΠΟΡΙΚΗ-ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΕΡΓΟΛΗΠΤΗΣ Δ.Ε.",
            "ΕΜΠΟΡΙΟ ΚΑΤΑΣΚΕΥΗ",
            "ΕΡΓΟΛΗΠΤΙΚΗ ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΕΡΓΟΛΑΒΟΣ ΟΙΚΟΔΟΜΩΝ",
            "ΕΜΠΟΡΙΟ-ΜΕΛΕΤΕΣ",
            "ΕΡΓΟΛΑΒΟΣ ΟΙΚΟΔΟΜΗ",
            "ΕΜΠΟΡΙΑ-ΚΑΤΑΣΚΕΥΗ",
            "ΕΡΓΟΛΑΒΟΣ",
            "ΕΝΕΡΓΕΙΑΚΑ ΕΡΓΑ",
            "ΕΡΓΟΛΑΒΟΣ ΔΗΜΟΣΙΩΝ ΕΡΓΩΝ",
            "ΕΡΓΟΛΑΒΙΕΣ",
            "ΕΡΓΟΛΑΒΟΣ ΤΕΧΝΙΚΩΝ ΕΡΓΩΝ",
            "ΕΝΕΡΓΕΙΑΚΕΣ ΕΦΑΡΜΟΓΕΣ-ΑΠΕ",
            "ΕΤΑΙΡΙΑ ΚΑΤΑΣΚΕΥΩΝ -ΟΙΚΟΔ. ΥΛΙΚΩΝ",
            "ΕΝΕΡΓΕΙΑΚΕΣ ΜΕΛΕΤΕΣ",
            "ΕΡΓΑΣΙΕΣ ΔΙΑΜΟΡΦΩΣΗΣ Η ΑΝΕΓΕΡΣΗΣ ΚΤΙΣΜΑΤΟΣ Η ΑΛΛΟ",
            "ΕΡΓΑΣΙΕΣ ΔΙΑΜΟΡΦΩΣΗΣ Η ΑΝΕΓΕΡΣΗΣ ΚΤΙΣΜΑΤΟΣ Η ΑΛΛΟΥ",
            "ΕΡΓΑΣΙΕΣ ΚΑΤΑΣΚΕΥΗΣ ΣΤΕΓΩΝ",
            "ΕΡΓΑΣΙΕΣ ΚΑΤΑΣΚΕΥΗΣ ΠΛΑΙΣΙΩΝ ΣΤΕΓΩΝ",
            "ΜΕΛΕΤΗ-ΚΑΤΑΣΚΕΥΗ ΤΕΧΝΙΚΩΝ ΕΡΓΩΝ",
            "ΜΕΛΕΤΕΣ - ΚΑΤΑΣΚΕΥΕΣ ΟΙΚΟΔΟΜΙΚΩΝ ΕΡΓΩΝ",
            "ΜΕΛΕΤΕΣ-ΕΠΙΒΛΕΨΕΙ",
            "ΜΕΛΕΤΗ ΚΑΤΑΣΚΕΥΗ ΗΛΕΚ/ΚΩΝ ΕΡΓΩΝ",
            "ΜΕΛΕΤΕΣ-ΚΑΤΑΣΚΕΥΕΣ ΕΜΠΟΡΙΟ",
            "ΟΙΚΟΔΟΜΙΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΟΙΚΟΔΟΜΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΟΙΚΟΔΟΜΙΚΕΣ ΕΠΙΧ.",
            "ΤΕΧΝΙΚΗ ΕΤΑΙΡΙΑ",
            "ΤΕΧΝΙΚΗ ETAIΡΕΙΑ",
            "ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΤΕΧΝΙΚΗ ΕΜΠΟΡΙΚΗ",
            "ΤΕΧΝΙΚΟ ΓΡΑΦΕΙΟ",
            "Τεχνική εταιρεία",
            "ΤΕΧΝΙΚΗ ΚΤΗΜΑΤΙΚΗ",
            "ΣΥΝΑΡΜΟΛΟΓΗΣΗ ΚΑΙ ΕΡΓΑΣΙΕΣ ΑΝΕΓΕΡΣΗΣ ΠΡΟΚΑΤΑΣΚΕΥΑ",
            "ΤΕΧΝΙΚΗ ΕΜΠΟΡΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΤΕΧΝΙΚΗ ΚΑΤΑΣ/ΚΗ",
            "ΤΕΧΝ.ΕΤΑΙΡΕΙΑ",
            "ΤΕΧΝΙΚΗ ΥΠΟΣΤΗΡΙΞΗ-ΕΠΙΒΛΕΨΗ ΗΛΕΚ/ΚΩΝ ΕΡΓΑΣΙΩΝ",
            "ΤΕΧΝΙΚΟ ΓΡΑΦΕΙΟ ΑΝΕΛΚΥΣΤΗΡΩΝ",
            "ΤΕΧΝΙΚΗ-ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΤΕΧΝΙΚΑ ΕΡΓΑ",
            "ΤΕΧΝΙΚΗ ΚΑΤΑΣΕΥΑΣΤΙΚΗ",
            "ΤΕΧΝΙΚΗ ΕΤΑΙΡΙΑ-ΥΠΕΡΘΑΛΑΣΣΙΑΣ ΖΕΥΞΗΣ ΡΙΟΥ-ΑΝΤΙΡΡΙΟΥ",
            "ΤΕΧΝΙΚΗ ΕΤΑΙΡΕΙΑ ΑΝΕΛΚΥΣΤΗΡΩΝ",
            "ΤΕΧΝ.ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΤΕΧΝΙΚΗ ΚΑΙ ΟΙΚΟΔΟΜΙΚΗ ΕΤΑΙΡ.",
            "ΤΕΧΝΙΚΗ-ΞΕΝΟΔ/ΚΗ-ΣΥΜΒΟΥΛΕΥΤΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΣΥΝΑΡΜΟΛΟΓΗΣΗ ΚΑΙ ΕΡΓΑΣΙΕΣ ΑΝΕΓΕΡΣΗΣ ΠΡΟΚΑΤΑΣΚΕΥΑΣ",
            "ΤΕΧΝΙΚΗ ΕΜΠΟΡΙΚΗ ΑΚΙΝΗΤΩΝ",
            "ΤΕΧΝΙΚΟΣ Υ/Π",
            "ΤΕΧΝΙΚΗ ΕΤΑΙΡΙΑ ΗΛΕΚΤΡΙΚΩΝ ΕΡΓΩΝ",
            "ΤΕΧΝΙΚΗ-ΕΜΠΟΡΙΚΗ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΠΟΝΤΟΠΟΡΩΝ ΠΛΟΙΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΚΤΙΡΙΩΝ ΠΟΛΥΚΑΤΟΙΚΙΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΕΡΓΩΝ ΓΙΑ ΚΑΤΑΣΚΕΥΑΣΤΙΚΑ ΕΡΓ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΑΚΙΝΗΤΩΝ ΕΝΑΝΤΙ ΑΜΟΙΒΗΣ Η ΒΑ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΕΡΓΩΝ ΓΙΑ ΚΑΤΑΣΚΕΥΑΣΤΙΚΑ ΕΡ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΑΚΙΝΗΤΩΝ ΠΟΥ ΔΕΝ ΠΡΟΟΡΙΖΟΝΤΑ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΑΚΙΝΗΤΩΝ ΠΟΥ ΔΕΝ ΠΡΟΟΡΙΖΟΝΤ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΚΤΙΡΙΩΝ ΚΑΤΟΙΚΙΩΝ ΒΑΣΕΙ ΑΜΟ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΗΛΕΚΤΡΟΝΙΚΩΝ ΣΥΣΤΗΜΑΤΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΕΡΓΩΝ ΕΚΤΟΣ ΤΩΝ ΚΑΤΑΣΚΕΥΑΣΤ",
            "ΥΠΗΡΕΣΙΕΣ ΔΙΑΧΕΙΡΙΣΗΣ ΣΥΣΤΗΜΑΤΩΝ ΗΛΕΚΤΡΟΝΙΚΟΥ ΥΠΟΛ",
            "ΚΑΤΑΣΚΕΥΗ ΟΙΚΙΣΤΙΚΩΝ ΚΤΙΡΙΩΝ",
            "ΕΞΕΙΔΙΚΕΥΜΕΝΕΣ ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ Π.Δ.Κ.Α.",
            "ΥΠΗΡΕΣΙΕΣ ΣΧΕΔΙΑΣΜΟΥ ΔΙΑΡΘΡΩΣΗΣ Η ΑΝΑΔΙΑΡΘΡΩΣΗΣ ΕΠ",
            "ΚΤΗΜΑΤΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΥΠΗΡΕΣΙΕΣ ΓΡΑΦΕΙΩΝ ΔΙΟΡΓΑΝΩΣΗΣ ΟΡΓΑΝΩΜΕΝΩΝ ΠΕΡΙΗΓΗ",
            "ΔΡΑΣΤΗΡΙΟΤΗΤΕΣ ΜΗΧΑΝΙΚΩΝ ΚΑΙ ΣΥΝΑΦΕΙΣ ΔΡΑΣΤΗΡΙΟΤΗΤ",
            "ΔΡΑΣΤΗΡΙΟΤΗΤΕΣ ΜΗΧΑΝΙΚΩΝ ΚΑΙ ΣΥΝΑΦΕΙΣ ΔΡΑΣΤΗΡΙΟΤΗ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΟΙΚΙΣΤΙΚΑ ΚΤΙΡΙΑ (ΝΕ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΤΙΡΙΩΝ ΓΙΑ ΚΑΤΟΙΚΙΕΣ ΚΑ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΤΙΡΙΩΝ ΓΙΑ ΚΑΤΟΙΚΙΕΣ ΚΑΙ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΚΑΤΑΣΚΕΥΕΣ ΠΟΛΙΤΙΚΟΥ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΜΗ ΟΙΚΙΣΤΙΚΑ ΚΤΙΡΙΑ",
            "ΚΑΤΑΣΚΕΥΕΣ ΣΥΣΤΗΜΑΤΩΝ ΑΡΔΕΥΣΗΣ (ΤΑΦΡΩΝ)· ΚΑΤΑΣΚΕΥ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΜΗ ΟΙΚΙΣΤΙΚΑ ΚΤΙΡΙΑ (",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΟΙΚΙΣΤΙΚΑ ΚΤΙΡΙΑ (ΝΕΕ",
            "ΚΑΤΑΣΚΕΥΗ ΑΓΩΓΩΝ ΜΕΓΑΛΩΝ ΑΠΟΣΤΑΣΕΩΝ ΓΙΑ ΜΕΤΑΦΟΡΑ Υ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΕΣ ΠΟΛ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΕΤΑΙΡΙΑ",
            "ΚΑΤΑΣΚΕΥΕΣ ΑΘΛΗΤΙΣΜΟΥ ΚΑΙ ΑΝΑΨΥΧΗΣ",
            "ΚΑΤΑΣΚΕΥΕΣ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΤΙΡΙΩΝ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΓΡΑΜΜΕΣ ΜΕΤΑΦΟΡΑΣ ΗΛ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΜΟΝΑΔΕΣ ΠΑΡΑΓΩΓΗΣ ΗΛ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΤΟΠΙΚΟΥΣ ΑΓΩΓΟΥΣ, ΣΥ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΕΤΑΙΡΕΙΑ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΩΝ ΗΛΕΚΤΡΟΝΙΚΩΝ ΚΑΙ ΗΛΕΚΤΡΙΚΩΝ ΣΥΡΜΑΤ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΟΙΝΩΦΕΛΩΝ ΕΡΓΩΝ ΗΛΕΚΤΡΙΚ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΩΝ ΕΡΓΩΝ ΠΟΛΙΤΙΚΟΥ ΜΗΧΑΝΙΚΟΥ Π.Δ.Κ.Α.",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΩΝ ΕΡΓΩΝ ΠΟΛΙΤΙΚΟΥ ΜΗΧΑΝΙΚΟΥ Π.Δ.Κ.Α",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΜΕΛΕΤΗΤΙΚΗ ΕΡΓΟΛΗΠΤΙΚΗ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΠΟΥ ΠΕΡΙΛΑΜΒΑΝΟΥΝ ΞΥΛΟΥΡΓ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΜΟΝΑΔΕΣ ΠΑΡΑΓΩΓΗΣ ΗΛΕ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΑΕΡΑΓΩΓΩΝ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΤΟΠΙΚΕΣ ΓΡΑΜΜΕΣ ΜΕΤΑ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΩΝ ΗΛΕΚΤΡΟΝΙΚΩΝ ΚΑΙ ΗΛΕΚΤΡΙΚΩΝ ΣΥΡΜΑ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΥΤΟΚΙΝΗΤΟΔΡΟΜΟΥΣ, Ε",
            "ΚΑΛ.ΚΑΙ ΠΟΛ.ΕΚΔ/ΤΕΧΝ.ΟΙΚΟΔΟΜΙΚΗ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΓΩΓΟΥΣ ΜΕΓΑΛΩΝ ΑΠΟΣ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΚΟΙΝΩΦΕΛΩΝ ΕΡΓΩΝ ΗΛΕΚΤΡΙ",
            "ΚΑΤΑΣΚΕΥΕΣ ΚΤΙΡΙΩΝ",
            "ΚΑΤΑΣΚΕΥΗ & ΕΜΠΟΡΙΑ ΠΑΙΔΙΚΩΝ ΕΝΔΥΜΑΤΩΝ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ ΑΝΩΝΥΜΗ ΕΤΑΙΡΙΑ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΜΗ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΥΤΟΚΙΝΗΤΟΔΡΟΜΟΥΣ, ΕΘ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΟΥ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ ΚΑΙ ΜΕΡΩΝ ΤΟ",
            "ΚΑΤΑΣΚΕΥΕΣ ΣΥΣΤΗΜΑΤΩΝ ΑΡΔΕΥΣΗΣ (ΤΑΦΡΩΝ)· ΚΑΤΑΣΚΕΥΕ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΠΑΡΑΚΤΙΕΣ ΚΑΙ ΛΙΜΕΝΙΚ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΓΩΓΟΥΣ ΜΕΓΑΛΩΝ ΑΠΟΣΤ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΓΙΑ ΑΛΛΕΣ ΚΑΤΑΣΚΕΥΕΣ ΠΟΛΙ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ ΕΡΓΑΣΙΕΣ ΣΤΟΝ ΕΞΟΡΥΚΤΙΚΟ ΚΑΙ ΜΕΤΑΠ",
            "ΚΑΤΑΣΚΕΥΗ & ΕΜΠΟΡΙΑ ΜΗΧ/ΤΩΝ ΑΡΤΟΠΟΙΙΑΣ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ-ΞΕΝΟΔΟΧΕΙΑΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΚΑΤΑΣΚΕΥΑΣΤΙΚΕΣ & ΜΗΧΑΝΟΛΟΓΙΚΕΣ ΕΓΚΑΤΑΣΤΑΣΕΙΣ",
            "ΚΑΤΑΣΚΕΥΗ ΑΛΛΟΥ ΗΛΕΚΤΡΙΚΟΥ ΕΞΟΠΛΙΣΜΟΥ ΚΑΙ ΜΕΡΩΝ Τ",
            "ΜΗΧΑΝΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΜΗΧΑΝΟΛΟΓΟΣ- ΜΗΧΑΝΙΚΟΣ ΕΡΓΟΛΗΠΤΗΣ ΤΕΧΝ.ΕΡΓΩΝ",
            "ΜΗΧΑΝΙΚΟΣ",
            "ΜΗΧ/ΓΟΣ-ΗΛΕΚ/ΓΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΜΗΧΑΝΟΛΟΓΙΚΕΣ ΕΓΚ/ΣΕΙΣ",
            "ΜΗΧ/ΓΟΣ ΜΗΧΑΝΙΚΟΣ",
            "ΜΗΧ/ΚΕΣ ΥΠΗΡΕΣΙΕΣ",
            "ΜΗΧΑΝΟΛΟΓΟΣ ΜΗΧΑΝΙΚΟΣ-ΕΜΠΟΡΙΟ ΗΜ",
            "ANAKAIN.XΩPΩN",
            "ΑΝΑΚΑΙΝΙΣΕΙΣ ΕΣΩΤ. ΧΩΡΩΝ",
            "ΑΝΑΚΑΙΝΗΣΕΙΣ");
        $ksenodoxeioCategoryMappingArray = array("ENOIKIAZ.ΔΩMATIA",
            "TOYPIΣTΙΚΕΣ EΠIX/ΣEIΣ",
            "TOYPIΣT.EΠIX/ΣEIΣ",
            "ΑΝΩΝΥΜΟΣ ΞΕΝΟΔΟΧΕΙΑΚΗ-ΤΟΥΡΙΣΤΙΚΗ-ΟΙΚΟΔΟΜΙΚΗ-ΝΑΥΤΙΛΙΑΚΗ & ΕΜΠΟΡΙΚΗ ΕΤΑΙΡΙΑ",
            "ΒΙΟΜΗΧΑΝΙΚΕΣ-ΞΕΝΟΔΟΧΕΙΑΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΔΙΟΙΚΗΤΙΚΕΣ ΥΠΗΡΕΣΙΕΣ ΠΟΥ ΣΧΕΤΙΖΟΝΤΑΙ ΜΕ ΤΟΥΡΙΣΤΙ",
            "ΕΝΟΙΚΙΑΖ.ΔΩΜΑΤΙΑ",
            "ΕΝΟΙΚΙΑΖΟΜΕΝΑ ΔΙΑΜΕΡΙΣΜΑΤΑ",
            "ΕΝΟΙΚΙΑΖΟΜΕΝΑ ΔΩΜΑΤΙΑ",
            "ΕΜΠΟΡΙΚΗ & ΤΟΥΡΙΣΤΙΚΗ ΕΤΑΙΡΙΑ",
            "ΕΝΟΙΚΙΑΖΟΜΕΝΑ ΔΩΜΑΤΙΑ (ΥΠΟΚΑΤΑΣΤΗΜΑ: ΓΕΡΑΝΙ ΧΑΝΙΩΝ)",
            "ΕΝΟΙΚΙΑΖΟΜΕΝΑ ΔΩΜΑΤΙΑ ΚΑΙ ΔΙΑΜΕΡΙΣΜΑΤΑ",
            "ΕΜΠΟΡΙΑ-ΞΕΝΟΔ.ΕΠΙΧ/ΣΕΙΣ",
            "ΕΝΟΙΚΙΑΣΕΙΣ ΑΥΤΟΚΙΝΗΤΩΝ",
            "ΕΝΟΙΚΙΑΖΟΜΕΝΕΣ ΚΑΤΟΙΚΙΕΣ",
            "ΞΕΝΟΔΟΧ.ΕΠΙΧΕΙΡ.",
            "ΞΕΝΟΔΟΧΕΙΑΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΞΕΝΟΔΟΧΕΙΑ ΚΑΙ ΠΑΡΟΜΟΙΑ ΚΑΤΑΛΥΜΑΤΑ",
            "ΞΕΝΟΔΟΧΕΙΟ",
            "ΞΕΝΟΔ. - ΤΟΥΡ. ΕΠΙΧ/ΣΕΙΣ",
            "Ξενοδοχείο",
            "ΞΕΝΟΔΟΧΕΙΑΚΗ-ΤΟΥΡΙΣΤΙΚΗ-ΚΑΤΑΣΚΕΥΑΣΤΙΚΗ",
            "ΞENOΔOX.EΠIXEIP.",
            "ΞΕΝΟΔ.&ΤΟΥΡ.ΕΠΙΧ.",
            "ΞΕΝΩΝΑΣ",
            "ΞΕΝΟΔΟΧΕΙΑΚΕΣ ΤΟΥΡΙΣΤΙΚΕΣ ΕΜΠΟΡΙΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΞΕΝΟΔΟΧΕΙΑΚΕΣ - ΤΟΥΡΙΣΤΙΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ Α.Ε.",
            "ΞΕΝΟΔΟΧΕΙΑΚΕΣ ΕΠΕ",
            "ΞΕΝΟΔΟΧΕΙΟ-ΕΣΤΙΑΤΟΡΙΟ",
            "ΞΕΝΟΔ.ΕΠΙΧΕΙΡ.",
            "ΞΕΝΟΔΟΧΕΙΟ ΥΠΟ ΑΝΕΓΕΡΣΗ",
            "ΞΕΝΟΔ.ΥΠΗΡΕΣΙΕΣ",
            "ΤΟΥΡΙΣΤΙΚΕΣ ΕΠΙΧΕΙΡΗΣΕΙΣ",
            "ΤΟΥΡΙΣΤΙΚΟ ΓΡΑΦΕΙΟ",
            "ΤΟΥΡΙΣΤΙΚΗ ΕΠΙΧΕΙΡΗΣΗ",
            "ΤΟΥΡΙΣΤΙΚΕΣ ΕΚΜΕΤΑΛΛΕΥΣΕΙΣ",
            "ΤΟΥΡΙΣΤΙΚΕΣ ΕΠΙΠΛΩΜΕΝΕΣ ΚΑΤΟΙΚΙΕΣ",
            "ΤΟΥΡ.ΕΠΙΠΛΩΜΕΝΕΣ ΚΑΤΟΙΚΙΕΣ",
            "ΤΟΥΡΙΣΤΙΚΑ ΚΑΤΑΛΥΜΑΤΑ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, Β' ΚΑΤΗΓΟΡΙΑΣ ΚΑΙ ΚΑ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, ΠΟΛΥΤΕΛΕΙΑΣ ΚΑΙ Α' Κ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, ΠΟΛΥΤΕΛΕΙΑΣ ΚΑΙ Α' ΚΑ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, Β' ΚΑΤΗΓΟΡΙΑΣ ΚΑΙ ΚΑΤ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, Β ΚΑΤΗΓΟΡΙΑΣ ΚΑΙ ΚΑΤΩ, ΜΕ ΕΣΤΙΑΤΟΡΙΟ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, Β' ΚΑΤΗΓΟΡΙΑΣ ΚΑΙ ΚΑΤΩ, ΧΩΡΙΣ ΕΣΤΙΑΤΟΡΙΟ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, Β ΚΑΤΗΓΟΡΙΑΣ ΚΑΙ ΚΑΤΩ, ΧΩΡΙΣ ΕΣΤΙΑΤΟΡΙΟ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΠΛΩΜΕΝΩΝ ΔΩΜΑΤΙΩΝ Η ΔΙΑΜΕ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΒΑΤΗΓΩΝ ΑΥΤΟΚΙΝΗΤΩΝ, ΧΩΡΙ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΠΛΩΜΕΝΩΝ ΔΙΑΜΕΡΙΣΜΑΤΩΝ, Μ",
            "ΥΠΗΡΕΣΙΕΣ ΠΑΡΟΧΗΣ ΔΩΜΑΤΙΟΥ Η ΜΟΝΑΔΑΣ ΚΑΤΑΛΥΜΑΤΟΣ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΔΩΜΑΤΙΩΝ ΣΠΙΤΙΩΝ, ΜΑΚΡΑΣ ΔΙΑΡ",
            "ΥΠΗΡΕΣΙΕΣ ΕΚΜΙΣΘΩΣΗΣ ΚΑΙ ΔΙΑΧΕΙΡΙΣΗΣ ΙΔΙΟΚΤΗΤΩΝ Η",
            "ΥΠΗΡΕΣΙΕΣ ΠΟΥ ΠΑΡΕΧΟΝΤΑΙ ΑΠΟ ΜΠΑΡ ΞΕΝΟΔΟΧΕΙΩΝ",
            "ΥΠΗΡΕΣΙΕΣ ΞΕΝΟΔΟΧΕΙΟΥ ΥΠΝΟΥ, ΠΟΛΥΤΕΛΕΙΑΣ ΚΑΙ Α ΚΑΤΗΓΟΡΙΑΣ, ΜΕ ΕΣΤΙΑΤΟΡΙΟ",
            "ΥΠΗΡΕΣΙΕΣ ΚΑΤΑΣΚΗΝΩΣΗΣ (ΚΑΜΠΙΓΚ)",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΠΛΩΜΕΝΩΝ ΔΙΑΜΕΡΙΣΜΑΤΩΝ, ΜΕ",
            "ΥΠΗΡΕΣΙΕΣ ΚΑΤΑΛΥΜΑΤΩΝ ΔΙΑΚΟΠΩΝ ΚΑΙ ΑΛΛΩΝ ΤΥΠΩΝ ΚΑ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΑΚΙΝΗΤΩΝ, ΠΟΥ ΔΕΝ ΠΡΟΟΡΙΖΟΝΤ",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΠΛΩΜΕΝΩΝ ΔΙΑΜΕΡΙΣΜΑΤΩΝ ΜΙ",
            "ΥΠΗΡ.ΞΕΝΟΔΟΧ.-ΤΑΞΙΔ.ΠΡΑΚΤ.",
            "ΥΠΗΡΕΣΙΕΣ ΕΝΟΙΚΙΑΣΗΣ ΕΠΙΠΛΩΜΕΝΩΝ ΔΩΜΑΤΙΩΝ Η ΔΙΑΜΕΡΙΣΜΑΤΩΝ ΓΙΑ ΜΙΚΡΗ ΔΙΑΡΚΕΙΑ");

        $customerCatCode = null;
        $customerCatName = null;
        if(isset($order['am_companyactivity'])){

            if(in_array($order['am_companyactivity'], $hlektrologosCategoryMappingArray) || stripos($order['am_companyactivity'], "ΗΛΕΚΤΡΟΛ") !== false){
                $customerCatCode = $hlektrologosCode;
                $customerCatName = "ΗΛΕΚΤΡΟΛΟΓΟΣ";
            }
            else if (in_array($order['am_companyactivity'], $kataskeuasthsCategoryMappingArray) || stripos($order['am_companyactivity'], "ΤΕΧΝΙΚ") !== false || stripos($order['am_companyactivity'], "ΚΑΤΑΣΚΕΥΑΣΤΙΚ") !== false){
                $customerCatCode = $kataskeuasthsCode;
                $customerCatName = "ΚΑΤΑΣΚΕΥΑΣΤΗΣ";
            }
            else if (in_array($order['am_companyactivity'], $ksenodoxeioCategoryMappingArray) || stripos($order['am_companyactivity'], "ΞΕΝΟΔΟΧ") !== false){
                $customerCatCode = $ksenodoxeioCode;
                $customerCatName = "ΞΕΝΟΔΟΧΕΙΟ";
            }
            else{
                $customerCatName = null;
                $customerCatCode = null;
            }
        }
        //end task #11862

        $customerDataForErp['CUSTCODE'] = ($order['customer_id']) ? : 'guest-' .$order['increment_id'];
        $customerDataForErp['CUST_NAME'] = isset($order['am_companyname']) ? $order['am_companyname'] : '';
        $customerDataForErp['CUST_FNAME'] = $order['customer_firstname'] ? : $order['billing_firstname'];
        $customerDataForErp['CUST_LNAME'] = $order['customer_lastname'] ? : $order['billing_lastname'];
        $customerDataForErp['TIN'] = isset($order['am_vatid']) ? $order['am_vatid'] : null;
        $customerDataForErp['CITY'] = isset($order['am_companyregion']) ? $order['am_companyregion'] : $order['billing_city'];
        $customerDataForErp['STREET'] = isset($order['am_companyaddress']) ? removeGreekSpecialCharacters(stripChars($order['am_companyaddress'])) : removeGreekSpecialCharacters(stripChars($order['billing_street']));
        $customerDataForErp['STREETNO'] = '';
        $customerDataForErp['POSTALCODE'] = isset($order['am_companyzipcode']) ? $order['am_companyzipcode'] : $order['billing_postcode'];
        $customerDataForErp['REGION'] = '';//????
        $customerDataForErp['PREFECTURE_CODE'] = $prefectureCode ? : '';
        $customerDataForErp['PREFECTURE'] = isset($order['am_companynomos']) ? '' : $order['billing_region'];
        $customerDataForErp['PHONE1'] = $isSkroutz === false ? $order['billing_telephone'] : '';
        $customerDataForErp['PHONE2'] = '';
       // $customerDataForErp['PHONEMOB'] = $order['billing_telephone'];
        $customerDataForErp['EMAIL'] = $order['customer_email'];
        $customerDataForErp['COUNTRY_CODE'] = $order['billing_country_id'];
        $customerDataForErp['COUNTRY'] = $country;
        $customerDataForErp['TAXOCODE'] = $taxCode ? : '';
        $customerDataForErp['CUST_TAX_OFFICE'] = isset($order['am_doy']) ? str_replace("'", "", $order['am_doy']) : '';
        $customerDataForErp['PROFESSION_CODE'] = isset($order['am_companyactivitycode']) ? $order['am_companyactivitycode'] : '';
        $customerDataForErp['PROFESSION_DESCRIPTION'] = isset($order['am_companyactivity']) ? str_replace("'", "",($order['am_companyactivity'])) : '';
        //task #11862
        if($customerCatCode !== null){
            $customerDataForErp['PROF_CAT_CODE'] = $customerCatCode;
            $customerDataForErp['PROF_CAT'] = $customerCatName;
        }
        //end task #11862
        $customerDataForErp['DISZCODE'] = '0001';
        $customerDataForErp['DISZDESCR'] = '1 ΛΙΑΝΙΚΗ';
        $customerDataForErp['PAYTERM_CODE'] = '001'; //gia kainourio pelati
        $customerDataForErp['PAYTERM_DESCRIPTION'] = 'ΜΕΤΡΗΤΟΙΣ'; //gia kainourio pelati
        $customerDataForErp['XON_LIAN_CODE'] = ($isinvoice ==  $m2e->invoiceid) ? '01' : '02';
        $customerDataForErp['XON_LIAN'] = ($isinvoice ==  $m2e->invoiceid) ? 'ΧΟΝΔΡΙΚΗΣ' : 'ΛΙΑΝΙΚΗΣ';
        $customerDataForErp['VAT'] = 'Κανονικό';
        //$customerDataForErp['CREATION_DATE'] = ''; //it is autocompleted


//        if($status == 'canceled' || $status == 'closed'){
//            $m2e->finstate = '202'; //cancel order at erp
//        }
       /* if ($status == 'processing') $m2e->finstate = 101;
        elseif ($status == 'complete') $m2e->finstate = 201;
        elseif ($status == 'canceled') $m2e->finstate = 301;
        //elseif ($status == 'waiting_bank') $m2e->finstate = 5;
        else continue;*/


        //if ($m2e->finstate == 3 )  continue; //cannot insert canceled orders
       /* if ($m2e->finstate == 0 ) continue; //skip if finstate is unknown*/



        //$details = $order->getElementsByTagName("order_id"); //use real order id as code for new customers
        //$code = $details->item(0)->nodeValue;
        //if (!$code) {
        //    $code = $orderid; //else use increment id
        //}

        //$code = substr($code, -5); //read only the LAST 5 characters
        //$code = "99991" .str_pad($code,5,"0", STR_PAD_LEFT); //add extra 0 zeroes






        $email= $order["customer_email"]; //get customer_email from XML
        $customerGroupId = $order['customer_group_id'];
        $phone = $order['billing_telephone'];
        //if (strtolower($email) == "tel@" .$s1rest->appname .".gr" && $phone) $email = $phone ."@" .$s1rest->appname .".gr"; //change with {mobile}@matfashion.gr

        if ($isinvoice ==  $m2e->invoiceid) {
            $vat = $order[$m2e->vatcode];
            if ($vat)
                $erpcustomer = $s1rest->getErpCustomerByVat($vat);
        } else {
            unset($order[$m2e->vatcode]); //always remove vatid on retail
//            if($email && $phone){
//                $erpcustomer = $s1rest->getCustomerByEmailAndPhone($email, $phone);
//            }
//            if (count($erpcustomer) == 0 && $email && preg_match('/@matfashion.com$/',$email) == 0){ //check if email ends with @matfashion.com. If so then search by phone. If not search by email.
//                $erpcustomer = $s1rest->getCustomerByEmail($email);
//                if (count($erpcustomer) != 1){ //if not found by email search by phone
//                    $erpcustomer = array(); //if more than one then search by phone
//                }
//            }
//            if(count($erpcustomer) == 0 && $phone){
//                $erpcustomer = $s1rest->getCustomerByPhone($phone);
//            }

            if($email){
                $erpcustomer = $s1rest->getErpCustomerByEmail($email);
//                if(count($erpcustomer) == 0 && $phone){
//                    $erpcustomer = $s1rest->getErpCustomerByPhone($phone);
//                }
            }

        }

        if($customerGroupId == 106){ //B2B General Case
            $customerDataForErp['DISZDESCR'] = 'B2B General';
            $customerDataForErp['DISZCODE'] = '0002';
        }
        if (count($erpcustomer) == 0) {
            if($isinvoice !=  $m2e->invoiceid){
                unset($customerDataForErp['TIN']); //do not enter vat_id for retail clients
            }
            else{
                $newVatCustomer = true;
            }
            $newCustomerMSSQLID = $s1rest->getNewMSSQLUUID();
            if(!$newCustomerMSSQLID){
                $s1rest->logError("Could not get a new MSSQL UUID for customer of order #$orderid");
                return 0;
            }
            $customerDataForErp['MG_CUST_ID'] = $newCustomerMSSQLID;
            $s1rest->debug = true;
            $customer = $s1rest->insertCustomerInErp($customerDataForErp);
            // }
        } elseif(count($erpcustomer) && $isinvoice ==  $m2e->invoiceid) { //update b2b customer's data always
            unset($customerDataForErp['MG_CUST_ID']); //do not set new UUID
            unset($customerDataForErp['PAYTERM_CODE']);
            unset($customerDataForErp['PAYTERM_DESCRIPTION']);
            $customerDataForErp['UPD_STATUS'] = 1;
            $s1rest->debug = true;
            $customer = $s1rest->updateCustomerInErp($customerDataForErp);
            //$flag = 1;
        }

        if (count($erpcustomer) || ($customer)) {
            $orderID = time() ."-" .$order['increment_id'];
            $orderItemArray = array();
            date_default_timezone_set('Europe/Athens');
            $currentDatetime = date('Y-m-d H:i:s', time());

            $maxDiscountIssueExists = checkIfItemWithMaxDiscIssueExists($order);

            //task-11219 --------------------------------------------
            //When shipping method is courier
            //We use an algorithm which chooses which branch must issue the order
            //based on branch' available quantity for the order items
            $mappedShippingMethod = mapShippingMethod($order['shipping_method'], $order['payment_method']);
            if($mappedShippingMethod == '331' || $mappedShippingMethod == '330' || $mappedShippingMethod == '335' || $mappedShippingMethod == '337'){ //select branch only for speedex, courier center, metaforiki, SLM (skroutzpoint)
                //this is the function which runs the whole algorithm
                //to choose the branch that will issue the order
                $selectedBranch = getSelectedBranchForOrder($order);
                $orderData = [];

                $orderData['ORDER_ID'] = $orderid;
                //if it comes as false, we send the default branch that will issue the order which is 00
                //we will also send a new variable in the Magento Api which will declare if the bcc email will need to be included.
                //if the variable is false, then we include the bcc otherwise we don't.
                if($selectedBranch === false ){
                    $selectedBranch = '00';
                    $orderData['BR_CODE'] = "00";
                    $orderData['ORDER_IS_ISSUABLE'] = false;
                }
                else{
                    $orderData['BR_CODE'] = $selectedBranch;
                    $orderData['ORDER_IS_ISSUABLE'] = true;
                }




                //Calling the magento Api "SendEmailToBranch" with this method
                $s1rest->sendEmailToBranch($orderData);
            }
            // end task-11219 --------------------------------------------

            $isSkroutzMarketplaceOrder = isSkroutzMarketplace($order['shipping_method'], $order['payment_method']);

            foreach($order['items'] as $item){

                // Skip specific skus that contain the 'not-erp' code
                if (mb_stripos($item['item_sku'], 'not-erp') !== false) {
                    continue;
                }

                $isFreeGift = false;
                $specialPriceAndCouponCoexist = checkIfSpecialPriceAndCouponCoexist($order, $item);
                if($isinvoice ==  $m2e->invoiceid){
                    $orderItemArray['TIN'] = $order[$m2e->vatcode];
                }
                if($item['item_price'] == 0){
                    $isFreeGift = true;
                }

                $orderItemArray['MG_CUSTOMER_ID'] =  $order['customer_id'] ? : 'guest' ."-" .$order['increment_id'];
                $orderItemArray['MG_ORDER_ID'] = $orderID;
                $orderItemArray['MG_ORDER_NUMBER'] = $order['increment_id'];
                if(isset($order['am_skroutz_order_id'])){
                    $orderItemArray['SKR_ORDER_NUMBER'] = $order['am_skroutz_order_id'];
                }
                $orderItemArray['CREATED_AT'] = $currentDatetime;
                $orderItemArray['SKU'] = $item['item_sku'];
                $orderItemArray['SKU_DESCRIPTION'] = $item['item_name'];
                $orderItemArray['QTY'] = $item['item_qty_ordered'];

                if($isFreeGift){
                    $orderItemArray['BASE_AMOUNT'] = 0.01;
                    $orderItemArray['DISC_ZONE'] = 0;
                    $orderItemArray['DISC_ZONE_AMT'] = 0;
                    $orderItemArray['DISC'] = 0;
                    $orderItemArray['DISC_AMT'] = 0;
                }
                elseif($isSkroutzMarketplaceOrder){
                    $fpt = isset($item['fpt']) ? $item['fpt'] : 0;
                    $item_fpt = $fpt / $item['item_qty_ordered'];
                    $orderItemArray['BASE_AMOUNT'] = number_format(($item['item_price'] - $item_fpt) / 1.24, 4, '.', '');
                    $orderItemArray['DISC_ZONE'] = 0;
                    $orderItemArray['DISC_ZONE_AMT'] = 0;
                    $orderItemArray['DISC'] = 0;
                    $orderItemArray['DISC_AMT'] = 0;
                }
                elseif($maxDiscountIssueExists || $specialPriceAndCouponCoexist){
                    $orderItemArray['BASE_AMOUNT'] = calculateBaseAmountByRule($item);
                    $orderItemArray['DISC_ZONE'] = 0;
                    $orderItemArray['DISC_ZONE_AMT'] = 0;
                    $orderItemArray['DISC'] = 0;
                    $orderItemArray['DISC_AMT'] = 0;
                }
                else{
                    $orderItemArray['BASE_AMOUNT'] = $item['base_amount'];
                    $orderItemArray['DISC_ZONE'] = $item['item_discount_zone_prc'];
                    $orderItemArray['DISC_ZONE_AMT'] = $item['row_discount_zone_amt'];
                    $orderItemArray['DISC'] = $item['item_discount_percent'];
                    $orderItemArray['DISC_AMT'] = $item['row_discount_amount_without_tax'];
                }

                if($isFreeGift){
                    $orderItemArray['NET_PRICE'] = 0.01 ;
                    $orderItemArray['R_TAX'] = 0;
                    $orderItemArray['VAT_AMT'] = 0;
                    $orderItemArray['TOTAL_AMT'] = 0.01;
                }
                else{
                    $orderItemArray['NET_PRICE'] = $item['net_price_for_erp'] ;
                    $orderItemArray['R_TAX'] = isset($item['fpt']) ? $item['fpt'] : '0';
                    $orderItemArray['VAT_AMT'] = $item['item_row_tax_amount'];
                    $orderItemArray['TOTAL_AMT'] = $item['item_row_total'];
                }


                $s1rest->debug = true;
                $flag &= boolval($s1rest->insertOrderItemInErp($orderItemArray));
                unset($orderItemArray);
            }

            $erpOrderHeaderData = array();
            if($isinvoice ==  $m2e->invoiceid){
                $erpOrderHeaderData['TIN'] = $order[$m2e->vatcode];
            }

            $erpOrderHeaderData['MG_CUSTOMER_ID'] = ($order['customer_id']) ? : 'guest-' .$order['increment_id'];
            $erpOrderHeaderData['MG_ORDER_ID'] = $orderID;
            $erpOrderHeaderData['MG_ORDER_NUMBER'] = $order['increment_id'];
            if(isset($order['am_skroutz_order_id'])){
                $erpOrderHeaderData['SKR_ORDER_NUMBER'] = $order['am_skroutz_order_id'];
            }
            if(isset($order['am_skroutz_voucher_pdf_link'])){
                $erpOrderHeaderData['SKR_VOUCHER'] = $order['am_skroutz_voucher_pdf_link'];
            }
            $erpOrderHeaderData['MG_ORDER_FNAME'] = str_replace( array( '\'', '"', ',' , ';', '<', '>' ,'{', '}', '(', ')', '%', '/', '.'), '', mb_substr(trim($order['shipping_firstname']), 0, 99, 'UTF-8')); // Add characters limit: 99
            $erpOrderHeaderData['MG_ORDER_LNAME'] = str_replace( array( '\'', '"', ',' , ';', '<', '>' ,'{', '}', '(', ')', '%', '/', '.'), '', mb_substr(trim($order['shipping_lastname']), 0, 99, 'UTF-8')); // Add characters limit: 99
            $erpOrderHeaderData['MG_CUST_EMAIL'] = $order['customer_email'];
            $erpOrderHeaderData['STREET'] = removeGreekSpecialCharacters(stripChars($order['shipping_street']));
            $erpOrderHeaderData['CITY'] = $order['shipping_city'];
            $erpOrderHeaderData['POSTALCODE'] = $order['shipping_postcode'];
            $erpOrderHeaderData['BRANCH_CODE'] = $selectedBranch;
            $erpOrderHeaderData['BRANCH'] = mapBranch($order['shipping_method'], $selectedBranch);
            //$erpOrderHeaderData['REGION'] = '';
            $erpOrderHeaderData['ORDER_PHONE'] = $isSkroutz === false ? $order['shipping_telephone'] : '';
            $currentDatetime = date('Y-m-d H:i:s', time());
            $erpOrderHeaderData['CREATED_AT'] = $currentDatetime;
            //$erpOrderHeaderData['METAFORIKA'] = $order['shipping_amount'] ? 'ΜΕΤΑΦΟΡΙΚΑ' : '' ;
            $erpOrderHeaderData['POD_INCL_TAX'] = isset($order['cod_fee']) && $order['cod_fee'] ? $order['cod_fee'] : 0;
            $erpOrderHeaderData['BASE_POD_AMOUNT'] = isset($order['cod_fee']) && $order['cod_fee']/1.24 ? $order['cod_fee']/1.24 : 0;
            $erpOrderHeaderData['SHIPPING_METHOD'] = mapShippingMethod($order['shipping_method'], $order['payment_method']);
            if($erpOrderHeaderData['SHIPPING_METHOD'] == '332' || $erpOrderHeaderData['SHIPPING_METHOD'] == '334'){ //pickup from store or Skroutz Marketplace
                $branchData = mapBranch($order['shipping_method']);
                if($branchData){
                    $erpOrderHeaderData['BRANCH_CODE'] = $branchData['code'];
                    $erpOrderHeaderData['BRANCH'] = $branchData['branch'];
                }
            }
            $erpOrderHeaderData['METAFORIKA'] = mapMetaforikaErpField($erpOrderHeaderData['SHIPPING_METHOD']);
            $erpOrderHeaderData['SHIPPING_METHOD'] = $isSkroutz === false ? $erpOrderHeaderData['SHIPPING_METHOD'] : ''; //empty field for skroutz orders
            $erpOrderHeaderData['SHIPPING_INCL_TAX'] = $order['shipping_amount'];
            $erpOrderHeaderData['BASE_SHIPPING_AMOUNT'] = number_format($order['shipping_amount'] / 1.24, 2);
            $payment = mapPayment($order['payment_method'], $order);
            $erpOrderHeaderData['PAYTERM_CODE'] = $payment && $isSkroutz === false ? $payment['code'] : '';
            $erpOrderHeaderData['PAYTERM_DESCRIPTION'] = '';//$payment ? $payment['description'] : '';
            $erpOrderHeaderData['COUPON_CODE'] = isset($order['coupon_code']) ? $order['coupon_code'] : '';
            $erpOrderHeaderData['DOC_TYPE'] = ($isinvoice ==  $m2e->invoiceid) ? 'ΤΔΑ'  : 'ΑΠΛ-Α4' ;
            $erpOrderHeaderData['FOR_ERP'] = '1'; //προς επεξεργασία
            $erpOrderHeaderData['BNK_UID'] = $orderID;
            $erpOrderHeaderData['REMARKS'] = isset($order['am_comments']) ? $order['am_comments'] : '';

//            if(isset($newVatCustomer) && $newVatCustomer === true){
//                $erpOrderHeaderData['REMARKS'] = "GSIS" .$erpOrderHeaderData['REMARKS'];
//            }

            $erpOrderHeaderData['STATUS'] = '1';
            $erpOrderHeaderData['GRAND_TOTAL'] = $order['grand_total'];
            if(isset($order['transaction_id']) && boolval($order['transaction_id'])){
                $erpOrderHeaderData['CC_TRANS_UID'] = $order['transaction_id'];
            }
            $s1rest->debug = true;
            $flag &= boolval($s1rest->insertOrderHeaderInErp($erpOrderHeaderData));



//            $series = $isinvoice ==  $m2e->invoiceid ? $m2e->seiratimologio : $m2e->seiralianikis; //timologio 9002 , lianiki 9001
//
//
//            $tz = new DateTimeZone('Europe/Athens');
//            $date = new DateTime($xmlarray['created_at'], new DateTimeZone('UTC'));
//            $date->setTimezone($tz);
//            $date = $date->format('Y-m-d H:i:s');
//
//            $comments = str_replace( array('[',']'), ' ', $xmlarray['gift_message'] ." " .$xmlarray['comments']);
//            $comments = str_replace( "Array", '', $comments); //remove word Array if exists..
//            $comments1 = str_replace( array('[',']'), ' ', $xmlarray['shipping_description']);
//            $paymentmethod = $xmlarray['payment_method'];
//
//            //if ($paymentmethod == "paypal_express" || $paymentmethod == "paypal_standard") $payment = "1004";
//            if ($paymentmethod == "bankpayment" || $paymentmethod == "banktransfer") $payment = "1003"; //Κατάθεση σε τράπεζα=1098
//            elseif ($paymentmethod == "phoenix_cashondelivery" || $paymentmethod == "msp_cashondelivery" || $paymentmethod == "paymentfee") $payment = "1002"; //Με αντικαταβολή =1099
//            elseif ($paymentmethod == "emppay" || $paymentmethod == "emppayeb" || $paymentmethod ==  "peiraiusbankpayment") $payment = "1001"; //Πιστωτική κάρτα = 100
//            elseif ($paymentmethod == "storepayment") $payment = "1000"; //pickup from store = 1009
//            //elseif ($paymentmethod == "cashondelivery") $payment = "1005"; //pickup from store = 1005
//
//            if ($paymentmethod == "phoenix_cashondelivery" || $paymentmethod == "msp_cashondelivery" || $paymentmethod == "paymentfee") $paymentmethod = "Αντικαταβολή";
//            elseif ($paymentmethod == "emppay" || $paymentmethod ==  "peiraiusbankpayment") $paymentmethod = "Πιστωτική Κάρτα";
//
//
//            $voucherPrice = null;
//            if($payment == "1002"){ //COD
//                $voucherPrice = $xmlarray['grand_total'];
//            }
//
//            $shipment = $xmlarray['shipping_method'];
//            //$shippingid =  (int)str_replace("storepickup_", "", $shipment);
////                $shippingid =  (int)str_replace("matrixrate_matrixrate_", "", $shipment);
////                if ($shippingid == 7) $shipment = 101; //store pickup
////                elseif ($shipment == "flatrate_flatrate") $shipment = 103; //metaforiki
////                elseif ($shipment == "freeshipping_freeshipping") $shipment = 101; //paralavi apo katastima
////                elseif ($shipment == "swacshome_swacshome") $shipment = 102; //geniki taxysdromiki
////                else $shipment = 1000; ///all others courrier
//            if($shipment == "customshipping_method_code_greece"){ //courier
//                $shipment = "103";
//            }
//            elseif($shipment == "customshipping_method_code_cyprus"){
//                $shipment = "104";
//            }
//            else{
//                $shipment = "101"; //paralavi apo katastimna
//            }
//
//
//
//            $m2e->makeSaldoc($series, $date, $orderid, $trdr, $comments, $comments1, $paymentmethod, $shipment, $payment, $xmlarray['grand_total'],$xmlarray['discount_amount'], $voucherPrice);
//            //$m2e->saldoc = array_merge($m2e->saldoc, array('CCCParaliptisName'=>$xmlarray["shipping_lastname"] ." " .$xmlarray["shipping_firstname"], 'CCCShipPhone01'=>$xmlarray["shipping_telephone"])); // HACK for RDC to use saldoc instead of mtrdoc
//            // $m2e->makeVatanal($xmlarray['grand_total'], $xmlarray['tax_amount'], '1410'); //1410 = 24%VAT
//            $m2e->makeExpanal($xmlarray['shipping_amount'], $xmlarray['cod_fee']);
//            $m2e->makeMtrdoc($xmlarray['shipping_street'], $xmlarray['shipping_postcode'], $xmlarray['shipping_city'], $xmlarray[$m2e->vatcode], $xmlarray["shipping_lastname"] ." " .$xmlarray["shipping_firstname"],  $xmlarray["shipping_telephone"]);
//
//
//            $items = $xmlarray['items']['item'];
//            if ($items['item_sku']) {
//                $idata = $xmlarray['items']['item'];
//                $m2e->addItemline    ($idata['item_barcode'], intval($idata['item_qty_ordered']), ($idata['item_price'] > $idata['item_original_price'] ? floatval($idata['item_price']) : floatval($idata['item_original_price'])) , (($idata['item_original_price']-$idata['item_price'])* $idata['item_qty_ordered'])+$idata['item_discount_amount'], $idata['recID'], $idata['custom_option'], $orderid);
//                $lastitemRecId = $idata['recID']+1;
//            } elseif (count($items) > 1) {
//                foreach ($items as $item) {
//                    $idata = $s1rest->xml2array($item);
//                    $m2e->addItemline($idata['item_barcode'], intval($idata['item_qty_ordered']), ($idata['item_price'] > $idata['item_original_price'] ? floatval($idata['item_price']) : floatval($idata['item_original_price'])) , (($idata['item_original_price']-$idata['item_price'])* $idata['item_qty_ordered'])+$idata['item_discount_amount'], $idata['recID'], $idata['custom_option'], $orderid);
//                    $lastitemRecId = $idata['recID']+1;
//                }
//            }
//
//
//            // if ($xmlarray['shipping_amount'] == "20.1600") {
//            //   $m2e->addItemline('103078', '1', $xmlarray['shipping_amount'], 0, $lastitemRecId++); //metaforika ekswterikou
//            //} elseif ($xmlarray['shipping_amount'] > 0) {
//            //   $m2e->addItemline('103078', '1', $xmlarray['shipping_amount'], 0, $lastitemRecId++);
//            // }
//
//            //if ($xmlarray['cod_fee'] && $xmlarray['cod_fee'] > 0) {
//            //   $m2e->addItemline('68570', '1',$xmlarray['cod_fee'] , 0, $lastitemRecId++); //cod is lacking VAT
//            //}
//
//            $m2e->makeOrderParams();
//            /*if (isset($erporder[0]['erpId']) && $erporder[0]['CCCFINSTATES'] == '5') {
//                $m2e->makeUpdateFinstate();
//                $m2e->params['KEY'] = $erporder[0]['erpId'] ?: '';
//            }*/
//            $order = $m2e->params;
//            $s1rest->debug = true; //add json to log
//            $erporderid = $s1rest->setOrder($m2e->params);
//            if (!$erporderid) $flag=0;
        } else {
            $s1rest->logError("Could not find or create customer from erp");
            return 0;
        }

        return $flag;
    } catch( SoapFault $fault ) {
        $s1rest->logError($fault);
        return 0;
    }
}

function mapRegionCodeToErpCode($regionCode){
    $mappingArray = array(
        '002' => array( //AITOLOAKARNANIA
            'magento_code' => 'AI',
            'amasty_code' => '698',
            'region_lectical' => 'ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ'
        ),

        '003' => array( //ARGOLIDA
            'magento_code' => 'ARG',
            'amasty_code' => '699',
            'region_lectical' => 'ΑΡΓΟΛΙΔΑΣ'
        ),

        '004' => array( //ARKADIA
            'magento_code' => 'ARK',
            'amasty_code' => '700',
            'region_lectical' => 'ΑΡΚΑΔΙΑΣ'
        ),

        '005' => array( //ARTA
            'magento_code' => 'AR',
            'amasty_code' => '701',
            'region_lectical' => 'ΑΡΤΑΣ'
        ),

        '006' => array( //ATTIKI
            'magento_code' => ['AT', 'PIR'],
            'amasty_code' => '702',
            'region_lectical' => 'ΑΤΤΙΚΗΣ'
        ),

        '008' => array( //ACHAIA
            'magento_code' => 'AC',
            'amasty_code' => '1442',
            'region_lectical' => 'ΑΧΑΪΑΣ'
        ),

        '094' => array( //BOIOTIA
            'magento_code' => 'BO',
            'amasty_code' => '1443',
            'region_lectical' => 'ΒΟΙΩΤΙΑΣ'
        ),

        '010' => array( //GREVENA
            'magento_code' => 'GRV',
            'amasty_code' => '1444',
            'region_lectical' => 'ΓΡΕΒΕΝΩΝ'
        ),

        '011' => array( //DRAMA
            'magento_code' => 'DR',
            'amasty_code' => '1445',
            'region_lectical' => 'ΔΡΑΜΑΣ'
        ),

        '091' => array( //DWDEKANISA
            'magento_code' => 'DO',
            'amasty_code' => '1446',
            'region_lectical' => 'ΔΩΔΕΚΑΝΗΣΟΥ'
        ),

        '013' => array( //EVROS
            'magento_code' => 'EVR',
            'amasty_code' => '1447',
            'region_lectical' => 'ΕΒΡΟΥ'
        ),

        '014' => array( //EVOIA
            'magento_code' => 'EU',
            'amasty_code' => '1448',
            'region_lectical' => 'ΕΥΒΟΙΑΣ'
        ),

        '015' => array( //EVRYTANIA
            'magento_code' => 'EV',
            'amasty_code' => '1449',
            'region_lectical' => 'ΕΥΡΥΤΑΝΙΑΣ'
        ),

        '016' => array( //ZAKYNTHOS
            'magento_code' => 'ZA',
            'amasty_code' => '1450',
            'region_lectical' => 'ΖΑΚΥΝΘΟΥ'
        ),

        '017' => array( //HLEIA
            'magento_code' => 'EL',
            'amasty_code' => '1451',
            'region_lectical' => 'ΗΛΕΙΑΣ'
        ),

        '018' => array( //IMATHIA
            'magento_code' => 'IM',
            'amasty_code' => '1452',
            'region_lectical' => 'ΗΜΑΘΙΑΣ'
        ),

        '019' => array( //HRAKLEIO
            'magento_code' => 'HE',
            'amasty_code' => '1453',
            'region_lectical' => 'ΗΡΑΚΛΕΙΟΥ'
        ),

        '020' => array( //THESPRWTIA
            'magento_code' => 'THE',
            'amasty_code' => '1454',
            'region_lectical' => 'ΘΕΣΠΡΩΤΙΑΣ'
        ),

        '021' => array( //THESSALONIKI
            'magento_code' => 'TH',
            'amasty_code' => '1455',
            'region_lectical' => 'ΘΕΣΣΑΛΟΝΙΚΗΣ'
        ),

        '022' => array( //IOANNINA
            'magento_code' => 'IO',
            'amasty_code' => '1456',
            'region_lectical' => 'ΙΩΑΝΝΙΝΩΝ'
        ),


        '023' => array( //KAVALA
            'magento_code' => 'KA',
            'amasty_code' => '1457',
            'region_lectical' => 'ΚΑΒΑΛΑΣ'
        ),

        '024' => array( //KARDITSA
            'magento_code' => 'KAR',
            'amasty_code' => '1458',
            'region_lectical' => 'ΚΑΡΔΙΤΣΑΣ'
        ),

        '025' => array( //KASTORIA
            'magento_code' => 'KAS',
            'amasty_code' => '1459',
            'region_lectical' => 'ΚΑΣΤΟΡΙΑΣ'
        ),

        '026' => array( //KERKYRA
            'magento_code' => 'CO',
            'amasty_code' => '1460',
            'region_lectical' => 'ΚΕΡΚΥΡΑΣ'
        ),

        '027' => array( //KEFALLONIA
            'magento_code' => 'KE',
            'amasty_code' => '1461',
            'region_lectical' => 'ΚΕΦΑΛΛΗΝΙΑΣ'
        ),

        '028' => array( //KILKIS
            'magento_code' => 'KI',
            'amasty_code' => '1462',
            'region_lectical' => 'ΚΙΛΚΙΣ'
        ),

        '029' => array( //KOZANI
            'magento_code' => 'KO',
            'amasty_code' => '1463',
            'region_lectical' => 'ΚΟΖΑΝΗΣ'
        ),

        '030' => array( //KORINTHIA
            'magento_code' => 'COR',
            'amasty_code' => '1464',
            'region_lectical' => 'ΚΟΡΙΝΘΙΑΣ'
        ),

        '031' => array( //CYCLADES
            'magento_code' => 'CY',
            'amasty_code' => '1465',
            'region_lectical' => 'ΚΥΚΛΑΔΩΝ'
        ),

        '032' => array( //LAKWNIA
            'magento_code' => 'LAC',
            'amasty_code' => '1466',
            'region_lectical' => 'ΛΑΚΩΝΙΑΣ'
        ),

        '033' => array( //LARISA
            'magento_code' => 'LAR',
            'amasty_code' => '1467',
            'region_lectical' => 'ΛΑΡΙΣΑΣ'
        ),

        '034' => array( //LASITHI
            'magento_code' => 'LA',
            'amasty_code' => '1468',
            'region_lectical' => 'ΛΑΣΙΘΙΟΥ'
        ),

        '035' => array( //LESVOS
            'magento_code' => 'LES',
            'amasty_code' => '1469',
            'region_lectical' => 'ΛΕΣΒΟΥ'
        ),

        '036' => array( //LEFKADA
            'magento_code' => 'LE',
            'amasty_code' => '1470',
            'region_lectical' => 'ΛΕΥΚΑΔΑΣ'
        ),

        '054' => array( //MAGNHSIA
            'magento_code' => 'MA',
            'amasty_code' => '1471',
            'region_lectical' => 'ΜΑΓΝΗΣΙΑΣ'
        ),

        '037' => array( //MESSINIA
            'magento_code' => 'ME',
            'amasty_code' => '1472',
            'region_lectical' => 'ΜΕΣΣΗΝΙΑΣ'
        ),

        '038' => array( //XANTHI
            'magento_code' => 'XA',
            'amasty_code' => '1473',
            'region_lectical' => 'ΞΑΝΘΗΣ'
        ),

        '039' => array( //PELLA
            'magento_code' => 'PE',
            'amasty_code' => '1474',
            'region_lectical' => 'ΠΕΛΛΑΣ'
        ),

        '040' => array( //PIERIA
            'magento_code' => 'PI',
            'amasty_code' => '1475',
            'region_lectical' => 'ΠΙΕΡΙΑΣ'
        ),

        '041' => array( //PREVEZA
            'magento_code' => 'PR',
            'amasty_code' => '1476',
            'region_lectical' => 'ΠΡΕΒΕΖΑΣ'
        ),

        '042' => array( //RETHYMNO
            'magento_code' => 'RE',
            'amasty_code' => '1477',
            'region_lectical' => 'ΡΕΘΥΜΝΗΣ'
        ),

        '075' => array( //RODOPI
            'magento_code' => 'RH',
            'amasty_code' => '1478',
            'region_lectical' => 'ΡΟΔΟΠΗΣ'
        ),

        '044' => array( //SAMOS
            'magento_code' => 'SA',
            'amasty_code' => '1479',
            'region_lectical' => 'ΣΑΜΟΥ'
        ),

        '045' => array( //SERRES
            'magento_code' => 'SE',
            'amasty_code' => '1480',
            'region_lectical' => 'ΣΕΡΡΩΝ'
        ),

        '046' => array( //TRIKALA
            'magento_code' => 'TR',
            'amasty_code' => '1481',
            'region_lectical' => 'ΤΡΙΚΑΛΩΝ'
        ),

        '047' => array( //FTHIOTIDAS
            'magento_code' => 'PHT',
            'amasty_code' => '1482',
            'region_lectical' => 'ΦΘΙΩΤΙΔΑΣ'
        ),

        '048' => array( //FLWRINA
            'magento_code' => 'FL',
            'amasty_code' => '1483',
            'region_lectical' => 'ΦΛΩΡΙΝΑΣ'
        ),

        '049' => array( //FWKIDA
            'magento_code' => 'PH',
            'amasty_code' => '1484',
            'region_lectical' => 'ΦΩΚΙΔΑΣ'
        ),

        '050' => array( //CHALKIDIKI
            'magento_code' => 'CHAL',
            'amasty_code' => '1485',
            'region_lectical' => 'ΧΑΛΚΙΔΙΚΗΣ'
        ),

        '051' => array( //XANIA
            'magento_code' => 'CH',
            'amasty_code' => '1486',
            'region_lectical' => 'ΧΑΝΙΩΝ'
        ),

        '052' => array( //XIOS
            'magento_code' => 'CHI',
            'amasty_code' => '1487',
            'region_lectical' => 'ΧΙΟΥ'
        ),

    );

    foreach ($mappingArray as $erpPrefectureCode => $mapping){
        if(is_array($mapping['magento_code'])){
            foreach ($mapping['magento_code'] as $magentoRegion) {
                if($magentoRegion == $regionCode){
                    return $erpPrefectureCode;
                }
            }
        }
        if(is_array($mapping['region_lectical'])){
            foreach ($mapping['region_lectical'] as $regionLectical) {
                if($regionLectical == $regionCode){
                    return $erpPrefectureCode;
                }
            }
        }
        if($mapping['magento_code'] == $regionCode || $mapping['amasty_code'] == $regionCode || $mapping['region_lectical'] == $regionCode){
            return $erpPrefectureCode;
        }
    }

    return false;

}

function mapTaxOfficeToCode($taxOffice){

    //    $mappingArray = array(
//        "1101"=>	"ΑΘΗΝΩΝ Α'",
//        "1104"=>	"ΑΘΗΝΩΝ Δ'",
//        "1105"=>	"ΑΘΗΝΩΝ Ε'",
//        "1106"=>	"ΑΘΗΝΩΝ ΣΤ'",
//        "1110"=>	"ΑΘΗΝΩΝ Ι'",
//        "1111"=>	"ΑΘΗΝΩΝ ΙΑ'",
//        "1112"=>	"ΑΘΗΝΩΝ ΙΒ'",
//        "1113"=>	"ΑΘΗΝΩΝ ΙΓ'",
//        "1114"=>	"ΑΘΗΝΩΝ ΙΔ'",
//        "1115"=>	"ΑΘΗΝΩΝ ΙΕ'",
//        "1116"=>	"ΑΘΗΝΩΝ ΙΣΤ'",
//        "1117"=>	"ΑΘΗΝΩΝ ΙΖ'",
//        "1118"=>	"ΑΘΗΝΩΝ ΦΑΒΕ",
//        "1124"=>	"ΑΘΗΝΩΝ ΙΗ'",
//        "1125"=>	"ΚΑΤΟΙΚΩΝ ΕΞΩΤΕΡΙΚΟΥ",
//        "1126"=>	"ΑΘΗΝΩΝ ΙΘ'",
//        "1129"=>	"ΑΓ. ΔΗΜΗΤΡΙΟΥ",
//        "1130"=>	"ΚΑΛΛΙΘΕΑΣ Α'",
//        "1131"=>	"ΝΕΑΣ ΙΩΝΙΑΣ",
//        "1132"=>	"ΝΕΑΣ ΣΜΥΡΝΗΣ",
//        "1133"=>	"ΠΑΛΑΙΟΥ ΦΑΛΗΡΟΥ",
//        "1134"=>	"ΧΑΛΑΝΔΡΙΟΥ",
//        "1135"=>	"ΑΜΑΡΟΥΣΙΟΥ",
//        "1136"=>	"ΑΓΙΩΝ ΑΝΑΡΓΥΡΩΝ",
//        "1137"=>	"ΑΙΓΑΛΕΩ",
//        "1138"=>	"ΠΕΡΙΣΤΕΡΙΟΥ Α'",
//        "1139"=>	"ΓΛΥΦΑΔΑΣ",
//        "1140"=>	"ΑΘΗΝΩΝ Κ'",
//        "1141"=>	"ΑΘΗΝΩΝ ΚΑ'",
//        "1142"=>	"ΑΘΗΝΩΝ ΚΒ'",
//        "1143"=>	"ΑΘΗΝΩΝ ΚΓ'",
//        "1144"=>	"ΔΑΦΝΗΣ",
//        "1145"=>	"ΗΡΑΚΛΕΙΟΥ ΑΤΤΙΚΗΣ",
//        "1151"=>	"ΑΓΙΑΣ ΠΑΡΑΣΚΕΥΗΣ",
//        "1152"=>	"ΒΥΡΩΝΑ",
//        "1153"=>	"ΚΗΦΙΣΙΑΣ",
//        "1154"=>	"ΙΛΙΟΥ",
//        "1155"=>	"ΝΕΑΣ ΦΙΛΑΔΕΛΦΕΙΑΣ",
//        "1156"=>	"ΧΑΙΔΑΡΙΟΥ",
//        "1157"=>	"ΠΕΡΙΣΤΕΡΙΟΥ Β'",
//        "1159"=>	"ΦΑΕ ΑΘΗΝΩΝ",
//        "1172"=>	"ΖΩΓΡΑΦΟΥ",
//        "1173"=>	"ΗΛΙΟΥΠΟΛΗΣ",
//        "1174"=>	"ΚΑΛΛΙΘΕΑΣ Β'",
//        "1175"=>	"ΨΥΧΙΚΟΥ",
//        "1176"=>	"ΧΟΛΑΡΓΟΥ",
//        "1177"=>	"ΑΡΓΥΡΟΥΠΟΛΗΣ",
//        "1178"=>	"ΠΕΤΡΟΥΠΟΛΗΣ",
//        "1179"=>	"ΓΑΛΑΤΣΙΟΥ",
//        "1180"=>	"ΑΝΩ ΛΙΟΣΙΩΝ",
//        "1201"=>	"ΠΕΙΡΑΙΑ Α'",
//        "1203"=>	"ΠΕΙΡΑΙΑ Γ'",
//        "1204"=>	"ΠΕΙΡΑΙΑ Δ'",
//        "1205"=>	"ΠΕΙΡΑΙΑ Ε'",
//        "1206"=>	"ΠΕΙΡΑΙΑ ΦΑΕ",
//        "1207"=>	"ΠΕΙΡΑΙΑ ΠΛΟΙΩΝ",
//        "1209"=>	"ΠΕΙΡΑΙΑ ΣΤ'",
//        "1210"=>	"ΚΟΡΥΔΑΛΛΟΥ",
//        "1211"=>	"ΜΟΣΧΑΤΟΥ",
//        "1220"=>	"ΝΙΚΑΙΑΣ",
//        "1301"=>	"ΑΙΓΙΝΑΣ",
//        "1302"=>	"ΑΧΑΡΝΩΝ",
//        "1303"=>	"ΕΛΕΥΣΙΝΑΣ",
//        "1304"=>	"ΚΟΡΩΠΙΟΥ",
//        "1305"=>	"ΚΥΘΗΡΩΝ",
//        "1306"=>	"ΛΑΥΡΙΟΥ",
//        "1307"=>	"ΑΓΙΟΥ ΣΤΕΦΑΝΟΥ",
//        "1308"=>	"ΜΕΓΑΡΩΝ",
//        "1309"=>	"ΣΑΛΑΜΙΝΑΣ",
//        "1310"=>	"ΠΟΡΟΥ",
//        "1311"=>	"ΥΔΡΑΣ",
//        "1312"=>	"ΠΑΛΛΗΝΗΣ",
//        "1411"=>	"ΘΗΒΑΣ",
//        "1421"=>	"ΛΕΙΒΑΔΙΑΣ",
//        "1511"=>	"ΑΜΦΙΛΟΧΙΑΣ",
//        "1521"=>	"ΑΣΤΑΚΟΥ",
//        "1522"=>	"ΒΟΝΙΤΣΑΣ",
//        "1531"=>	"ΜΕΣΟΛΟΓΓΙΟΥ",
//        "1541"=>	"ΝΑΥΠΑΚΤΟΥ",
//        "1551"=>	"ΘΕΡΜΟΥ",
//        "1552"=>	"ΑΓΡΙΝΙΟΥ",
//        "1611"=>	"ΚΑΡΠΕΝΗΣΙΟΥ",
//        "1711"=>	"ΙΣΤΙΑΙΑΣ",
//        "1721"=>	"ΚΑΡΥΣΤΟΥ",
//        "1722"=>	"ΚΥΜΗΣ",
//        "1731"=>	"ΛΙΜΝΗΣ",
//        "1732"=>	"ΧΑΛΚΙΔΑΣ",
//        "1811"=>	"ΔΟΜΟΚΟΥ",
//        "1821"=>	"ΑΜΦΙΚΛΕΙΑΣ",
//        "1822"=>	"ΑΤΑΛΑΝΤΗΣ",
//        "1831"=>	"ΜΑΚΡΑΚΩΜΗΣ",
//        "1832"=>	"ΛΑΜΙΑΣ",
//        "1833"=>	"ΣΤΥΛΙΔΑΣ",
//        "1911"=>	"ΛΙΔΟΡΙΚΙΟΥ",
//        "1912"=>	"ΑΜΦΙΣΣΑΣ",
//        "2111"=>	"ΑΡΓΟΥΣ",
//        "2121"=>	"ΣΠΕΤΣΩΝ",
//        "2122"=>	"ΚΡΑΝΙΔΙΟΥ",
//        "2131"=>	"ΝΑΥΠΛΙΟΥ",
//        "2211"=>	"ΔΗΜΗΤΣΑΝΑΣ",
//        "2213"=>	"ΛΕΩΝΙΔΙΟΥ",
//        "2214"=>	"ΤΡΟΠΑΙΩΝ",
//        "2221"=>	"ΠΑΡΑΛΙΟΥ ΑΣΤΡΟΥΣ",
//        "2231"=>	"ΤΡΙΠΟΛΗΣ",
//        "2241"=>	"ΜΕΓΑΛΟΠΟΛΗΣ",
//        "2311"=>	"ΑΙΓΙΟΥ",
//        "2312"=>	"ΑΚΡΑΤΑΣ",
//        "2321"=>	"ΚΑΛΑΒΡΥΤΩΝ",
//        "2322"=>	"ΚΛΕΙΤΟΡΙΑΣ",
//        "2331"=>	"ΠΑΤΡΩΝ Α'",
//        "2332"=>	"ΠΑΤΡΩΝ Β'",
//        "2333"=>	"ΚΑΤΩ ΑΧΑΙΑΣ",
//        "2334"=>	"ΠΑΤΡΩΝ Γ'",
//        "2411"=>	"ΑΜΑΛΙΑΔΑΣ",
//        "2412"=>	"ΠΥΡΓΟΥ",
//        "2413"=>	"ΓΑΣΤΟΥΝΗΣ",
//        "2414"=>	"ΒΑΡΔΑ",
//        "2421"=>	"ΚΡΕΣΤΕΝΩΝ",
//        "2422"=>	"ΛΕΧΑΙΝΩΝ",
//        "2423"=>	"ΑΝΔΡΙΤΣΑΙΝΑΣ",
//        "2424"=>	"ΖΑΧΑΡΩΣ",
//        "2511"=>	"ΔΕΡΒΕΝΙΟΥ",
//        "2512"=>	"ΚΙΑΤΟΥ",
//        "2513"=>	"ΚΟΡΙΝΘΟΥ",
//        "2514"=>	"ΝΕΜΕΑΣ",
//        "2515"=>	"ΞΥΛΟΚΑΣΤΡΟΥ",
//        "2611"=>	"ΓΥΘΕΙΟΥ",
//        "2621"=>	"ΜΟΛΑΩΝ",
//        "2622"=>	"ΝΕΑΠΟΛΗΣ ΒΟΙΩΝ ΛΑΚΩΝΙΑΣ",
//        "2630"=>	"ΣΚΑΛΑ ΛΑΚΩΝΙΑΣ",
//        "2631"=>	"ΚΡΟΚΕΩΝ",
//        "2632"=>	"ΣΠΑΡΤΗΣ",
//        "2641"=>	"ΑΡΕΟΠΟΛΗΣ",
//        "2711"=>	"ΚΑΛΑΜΑΤΑΣ",
//        "2721"=>	"ΜΕΛΙΓΑΛΑ",
//        "2722"=>	"ΜΕΣΣΗΝΗΣ",
//        "2731"=>	"ΠΥΛΟΥ",
//        "2741"=>	"ΓΑΡΓΑΛΙΑΝΩΝ",
//        "2742"=>	"ΚΥΠΑΡΙΣΣΙΑΣ",
//        "2743"=>	"ΦΙΛΙΑΤΡΩΝ ΜΕΣΣΗΝΙΑΣ",
//        "3111"=>	"ΚΑΡΔΙΤΣΑΣ",
//        "3112"=>	"ΜΟΥΖΑΚΙΟΥ",
//        "3113"=>	"ΣΟΦΑΔΩΝ",
//        "3114"=>	"ΠΑΛΑΜΑ",
//        "3211"=>	"ΑΓΙΑΣ",
//        "3221"=>	"ΕΛΑΣΣΟΝΑΣ",
//        "3222"=>	"ΔΕΣΚΑΤΗΣ",
//        "3231"=>	"ΛΑΡΙΣΑΣ Α'",
//        "3232"=>	"ΛΑΡΙΣΑΣ Β'",
//        "3233"=>	"ΛΑΡΙΣΑΣ Γ'",
//        "3241"=>	"ΤΥΡΝΑΒΟΥ",
//        "3251"=>	"ΦΑΡΣΑΛΩΝ",
//        "3311"=>	"ΑΛΜΥΡΟΥ",
//        "3321"=>	"ΒΟΛΟΥ Α'",
//        "3322"=>	"ΒΟΛΟΥ Β'",
//        "3323"=>	"ΙΩΝΙΑΣ ΜΑΓΝΗΣΙΑΣ",
//        "3331"=>	"ΣΚΟΠΕΛΟΥ",
//        "3332"=>	"ΣΚΙΑΘΟΥ",
//        "3411"=>	"ΚΑΛΑΜΠΑΚΑΣ",
//        "3412"=>	"ΤΡΙΚΑΛΩΝ",
//        "3413"=>	"ΠΥΛΗΣ",
//        "4111"=>	"ΑΛΕΞΑΝΔΡΕΙΑΣ",
//        "4112"=>	"ΒΕΡΟΙΑΣ",
//        "4121"=>	"ΝΑΟΥΣΑΣ",
//        "4211"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Α'",
//        "4212"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Β'",
//        "4214"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Δ'",
//        "4215"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Ε'",
//        "4216"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ ΣΤ'",
//        "4217"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Ζ'",
//        "4221"=>	"ΖΑΓΚΛΙΒΕΡΙΟΥ",
//        "4222"=>	"ΛΑΓΚΑΔΑ",
//        "4223"=>	"ΣΩΧΟΥ",
//        "4224"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ ΦΑΕ",
//        "4225"=>	"ΝΕΑΠΟΛΗΣ ΘΕΣ/ΝΙΚΗΣ",
//        "4226"=>	"ΤΟΥΜΠΑΣ",
//        "4227"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Ι'",
//        "4228"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Η'",
//        "4229"=>	"ΘΕΣΣΑΛΟΝΙΚΗΣ Θ'",
//        "4231"=>	"ΑΓ. ΑΘΑΝΑΣΙΟΥ",
//        "4232"=>	"ΚΑΛΑΜΑΡΙΑΣ",
//        "4233"=>	"ΑΜΠΕΛΟΚΗΠΩΝ",
//        "4234"=>	"Ν.ΙΩΝΙΑΣ ΘΕΣ/ΚΗΣ",
//        "4311"=>	"ΚΑΣΤΟΡΙΑΣ",
//        "4312"=>	"ΝΕΣΤΟΡΙΟΥ",
//        "4313"=>	"ΑΡΓΟΥΣ ΟΡΕΣΤΙΚΟΥ",
//        "4411"=>	"ΚΙΛΚΙΣ",
//        "4421"=>	"ΓΟΥΜΕΝΙΣΣΑΣ",
//        "4521"=>	"ΓΡΕΒΕΝΩΝ",
//        "4511"=>	"ΝΕΑΠΟΛΗΣ ΒΟΙΟΥ",
//        "4531"=>	"ΠΤΟΛΕΜΑΙΔΑΣ",
//        "4541"=>	"ΚΟΖΑΝΗ",
//        "4542"=>	"ΣΕΡΒΙΩΝ",
//        "4543"=>	"ΣΙΑΤΙΣΤΑΣ",
//        "4611"=>	"ΑΡΙΔΑΙΑΣ",
//        "4621"=>	"ΓΙΑΝΝΙΤΣΩΝ",
//        "4631"=>	"ΕΔΕΣΣΑΣ",
//        "4641"=>	"ΣΚΥΔΡΑΣ",
//        "4711"=>	"ΚΑΤΕΡΙΝΗΣ Α'",
//        "4712"=>	"ΚΑΤΕΡΙΝΗΣ Β'",
//        "4714"=>	"ΑΙΓΙΝΙΟΥ",
//        "4811"=>	"ΑΜΥΝΤΑΙΟΥ",
//        "4812"=>	"ΦΛΩΡΙΝΑΣ",
//        "4911"=>	"ΑΡΝΑΙΑΣ",
//        "4921"=>	"ΚΑΣΣΑΝΔΡΑΣ",
//        "4922"=>	"ΠΟΛΥΓΥΡΟΥ",
//        "4923"=>	"ΝΕΩΝ ΜΟΥΔΑΝΙΩΝ",
//        "5111"=>	"ΔΡΑΜΑΣ",
//        "5112"=>	"ΝΕΥΡΟΚΟΠΙΟΥ",
//        "5211"=>	"ΑΛΕΞΑΝΔΡΟΥΠΟΛΗΣ",
//        "5221"=>	"ΔΙΔΥΜΟΤΕΙΧΟΥ",
//        "5231"=>	"ΟΡΕΣΤΕΙΑΔΑΣ",
//        "5241"=>	"ΣΟΥΦΛΙΟΥ",
//        "5311"=>	"ΘΑΣΟΥ",
//        "5321"=>	"ΚΑΒΑΛΑΣ Α'",
//        "5322"=>	"ΚΑΒΑΛΑΣ Β'",
//        "5331"=>	"ΧΡΥΣΟΥΠΟΛΗΣ",
//        "5341"=>	"ΕΛΕΥΘΕΡΟΥΠΟΛΗΣ",
//        "5411"=>	"ΞΑΝΘΗΣ Α'",
//        "5412"=>	"ΞΑΝΘΗΣ Β'",
//        "5511"=>	"ΚΟΜΟΤΗΝΗΣ",
//        "5521"=>	"ΣΑΠΠΩΝ",
//        "5611"=>	"ΝΙΓΡΙΤΑΣ",
//        "5621"=>	"ΣΕΡΡΩΝ Α'",
//        "5622"=>	"ΣΕΡΡΩΝ Β'",
//        "5631"=>	"ΣΙΔΗΡΟΚΑΣΤΡΟΥ",
//        "5632"=>	"ΗΡΑΚΛΕΙΑΣ",
//        "5641"=>	"ΝΕΑΣ ΖΙΧΝΗΣ",
//        "6111"=>	"ΑΡΤΑΣ",
//        "6113"=>	"ΦΙΛΙΠΠΙΑΔΑΣ",
//        "6211"=>	"ΗΓΟΥΜΕΝΙΤΣΑΣ",
//        "6231"=>	"ΠΑΡΑΜΥΘΙΑΣ",
//        "6241"=>	"ΦΙΛΙΑΤΩΝ",
//        "6221"=>	"ΠΑΡΓΑΣ",
//        "6222"=>	"ΦΑΝΑΡΙΟΥ",
//        "6411"=>	"ΠΡΕΒΕΖΑΣ",
//        "6311"=>	"ΙΩΑΝΝΙΝΩΝ Α'",
//        "6312"=>	"ΙΩΑΝΝΙΝΩΝ Β'",
//        "6313"=>	"ΔΕΛΒΙΝΑΚΙΟΥ",
//        "6315"=>	"ΜΕΤΣΟΒΟΥ",
//        "6321"=>	"ΚΟΝΙΤΣΑΣ",
//        "7111"=>	"ΑΝΔΡΟΥ",
//        "7121"=>	"ΘΗΡΑΣ",
//        "7131"=>	"ΚΕΑΣ",
//        "7141"=>	"ΜΗΛΟΥ",
//        "7151"=>	"ΝΑΞΟΥ",
//        "7161"=>	"ΠΑΡΟΥ",
//        "7171"=>	"ΣΥΡΟΥ",
//        "7172"=>	"ΜΥΚΟΝΟΥ",
//        "7181"=>	"ΤΗΝΟΥ",
//        "7211"=>	"ΛΗΜΝΟΥ",
//        "7221"=>	"ΚΑΛΛΟΝΗΣ",
//        "7222"=>	"ΜΗΘΥΜΝΑΣ",
//        "7231"=>	"ΜΥΤΙΛΗΝΗΣ",
//        "7241"=>	"ΠΛΩΜΑΡΙΟΥ",
//        "7311"=>	"ΑΓ. ΚΗΡΥΚΟΥ ΙΚΑΡΙΑΣ",
//        "7321"=>	"ΚΑΡΛΟΒΑΣΙΟΥ",
//        "7322"=>	"ΣΑΜΟΥ",
//        "7411"=>	"ΧΙΟΥ",
//        "7511"=>	"ΚΑΛΥΜΝΟΥ",
//        "7512"=>	"ΛΕΡΟΥ",
//        "7521"=>	"ΚΑΡΠΑΘΟΥ",
//        "7531"=>	"ΚΩ",
//        "7542"=>	"ΡΟΔΟΥ",
//        "8111"=>	"ΗΡΑΚΛΕΙΟΥ Α'",
//        "8112"=>	"ΜΟΙΡΩΝ",
//        "8113"=>	"ΗΡΑΚΛΕΙΟΥ Β'",
//        "8114"=>	"ΤΥΜΠΑΚΙΟΥ",
//        "8115"=>	"ΛΙΜΕΝΑ ΧΕΡΣΟΝΗΣΟΥ",
//        "8121"=>	"ΚΑΣΤΕΛΙΟΥ ΠΕΔΙΑΔΟΣ",
//        "8131"=>	"ΑΡΚΑΛΟΧΩΡΙΟΥ",
//        "8211"=>	"ΙΕΡΑΠΕΤΡΑΣ",
//        "8221"=>	"ΑΓΙΟΥ ΝΙΚΟΛΑΟΥ",
//        "8231"=>	"ΝΕΑΠΟΛΗΣ ΚΡΗΤΗΣ",
//        "8241"=>	"ΣΗΤΕΙΑΣ",
//        "8341"=>	"ΡΕΘΥΜΝΟΥ",
//        "8421"=>	"ΚΙΣΣΑΜΟΥ",
//        "8431"=>	"ΧΑΝΙΩΝ Α'",
//        "8432"=>	"ΧΑΝΙΩΝ Β'",
//        "9111"=>	"ΖΑΚΥΝΘΟΥ",
//        "9211"=>	"ΚΕΡΚΥΡΑΣ Α'",
//        "9212"=>	"ΚΕΡΚΥΡΑΣ Β'",
//        "9221"=>	"ΠΑΞΩΝ",
//        "9311"=>	"ΑΡΓΟΣΤΟΛΙΟΥ",
//        "9321"=>	"ΛΗΞΟΥΡΙΟΥ",
//        "9411"=>	"ΙΘΑΚΗΣ",
//        "9421"=>	"ΛΕΥΚΑΔΑΣ"
//    );

    $mappingArray = array(
        "100000" => "ΚΥΠΡΟΥ",
        "1101" => "Α ΑΘΗΝΩΝ",
        "1104" => "Δ ΑΘΗΝΩΝ",
        "1105" => "Ε ΑΘΗΝΩΝ",
        "1106" => "ΣΤ ΑΘΗΝΩΝ",
        "1110" => "Ι ΑΘΗΝΩΝ",
        "1111" => "ΙΑ ΑΘΗΝΩΝ",
        "1112" => "ΙΒ ΑΘΗΝΩΝ",
        "1113" => "ΙΓ ΑΘΗΝΩΝ",
        "1114" => "ΙΔ ΑΘΗΝΩΝ",
        "1115" => "ΙΕ ΑΘΗΝΩΝ",
        "1116" => "ΙΣΤΑΘΗΝΩΝ",
        "1117" => "ΙΖ ΑΘΗΝΩΝ",
        "1118" => "ΜΕΓΑΛΩΝ ΕΠΙΧΕΙΡΗΣΕΩΝ",
        "1124" => "ΙΗ ΑΘΗΝΩΝ",
        "1125" => "ΚΑΤΟΙΚΩΝ ΕΞΩΤΕΡΙΚΟΥ",
        "1126" => "ΙΘ ΑΘΗΝΩΝ",
        "1129" => "ΑΓΙΟΥ ΔΗΜΗΤΡΙΟΥ",
        "1130" => "ΚΑΛΛΙΘΕΑΣ",
        "1131" => "ΝΕΑΣ ΙΩΝΙΑΣ",
        "1132" => "ΝΕΑΣ ΣΜΥΡΝΗΣ",
        "1133" => "ΠΑΛΑΙΟΥ ΦΑΛΗΡΟΥ",
        "1134" => "ΧΑΛΑΝΔΡΙΟΥ",
        "1135" => "ΑΜΑΡΟΥΣΙΟΥ",
        "1136" => "ΑΓΙΩΝ ΑΝΑΡΓΥΡΩΝ",
        "1137" => "ΑΙΓΑΛΕΩ",
        "1138" => "ΠΕΡΙΣΤΕΡΙΟΥ",
        "1139" => "ΓΛΥΦΑΔΑΣ",
        "1140" => "Κ ΑΘΗΝΩΝ",
        "1141" => "ΚΑ ΑΘΗΝΩΝ",
        "1142" => "ΚΒ ΑΘΗΝΩΝ",
        "1143" => "ΚΓ ΑΘΗΝΩΝ",
        "1144" => "ΔΑΦΝΗΣ",
        "1145" => "Ν.ΗΡΑΚΛΕΙΟΥ",
        "1151" => "ΑΓ.ΠΑΡΑΣΚΕΥΗΣ",
        "1152" => "ΒΥΡΩΝΑ",
        "1153" => "ΚΗΦΙΣΙΑΣ",
        "1154" => "ΙΛΙΟΥ",
        "1155" => "Ν.ΦΙΛΑΔΕΛΦΕΙΑΣ",
        "1156" => "ΧΑΙΔΑΡΙΟΥ",
        "1157" => "Β ΠΕΡΙΣΤΕΡΙΟΥ",
        "1158" => "ΠΕΡΙΣΤΕΡΙΟΥ",
        "1159" => "ΦΑΕ ΑΘΗΝΩΝ",
        "1172" => "ΖΩΓΡΑΦΟΥ",
        "1173" => "ΗΛΙΟΥΠΟΛΗΣ",
        "1174" => "ΒΑ ΚΑΛΛΙΘΕΑΣ",
        "1175" => "ΨΥΧΙΚΟΥ",
        "1176" => "ΧΟΛΑΡΓΟΥ",
        "1177" => "ΑΡΓΥΡΟΥΠΟΛΗΣ",
        "1178" => "ΠΕΤΡΟΥΠΟΛΗΣ",
        "1179" => "ΓΑΛΑΤΣΙΟΥ",
        "1180" => "ΑΝΩ ΛΙΟΣΙΩΝ",
        "1201" => "Α ΠΕΙΡΑΙΑ",
        "1203" => "Γ ΠΕΙΡΑΙΩΣ",
        "1204" => "Δ ΠΕΙΡΑΙΩΣ",
        "1205" => "Ε ΠΕΙΡΑΙΑ",
        "1206" => "ΦΑΕ ΠΕΙΡΑΙΑ",
        "1207" => "ΠΛΟΙΩΝ ΠΕΙΡΑΙΑ",
        "1209" => "ΣΤ ΠΕΙΡΑΙΩΣ",
        "1210" => "ΚΟΡΥΔΑΛΛΟΥ",
        "1211" => "ΜΟΣΧΑΤΟΥ",
        "1220" => "ΝΙΚΑΙΑΣ",
        "1301" => "ΑΙΓΙΝΑΣ",
        "1302" => "ΑΧΑΡΝΩΝ",
        "1303" => "ΕΛΕΥΣΙΝΑΣ",
        "1304" => "ΚΟΡΩΠΙΟΥ",
        "1305" => "ΚΥΘΗΡΩΝ",
        "1306" => "ΛΑΥΡΙΟΥ",
        "1307" => "ΑΓ.ΣΤΕΦΑΝΟΥ",
        "1308" => "ΜΕΓΑΡΩΝ-ΕΛΕΥΣΙΝΑΣ",
        "1309" => "ΣΑΛΑΜΙΝΑΣ",
        "1310" => "ΠΟΡΟΥ",
        "1311" => "ΥΔΡΑΣ",
        "1312" => "ΠΑΛΛΗΝΗΣ",
        "1411" => "ΘΗΒΩΝ",
        "1421" => "ΛΙΒΑΔΕΙΑΣ",
        "1511" => "ΑΜΦΙΛΟΧΙΑΣ",
        "1521" => "ΑΣΤΑΚΟΥ",
        "1522" => "ΒΟΝΙΤΣΑΣ",
        "1531" => "ΜΕΣΟΛΟΓΓΙΟΥ",
        "1540" => "ΝΑΥΠΑΚΤΟΥ",
        "1541" => "ΝΑΥΠΑΚΤΟΥ",
        "1551" => "ΘΕΡΜΟΥ",
        "1552" => "ΑΓΡΙΝΙΟΥ",
        "1611" => "ΚΑΡΠΕΝΗΣΙΟΥ",
        "1711" => "ΙΣΤΙΑΙΑΣ",
        "1721" => "ΚΑΡΥΣΤΟΥ",
        "1722" => "ΚΥΜΗΣ",
        "1731" => "ΛΙΜΝΗΣ",
        "1732" => "ΧΑΛΚΙΔΑΣ",
        "1811" => "ΔΟΜΟΚΟΥ",
        "1821" => "ΑΜΦΙΚΛΕΙΑΣ",
        "1822" => "ΑΤΑΛΑΝΤΗΣ",
        "1831" => "ΜΑΚΡΑΚΩΜΗΣ",
        "1832" => "ΛΑΜΙΑΣ",
        "1833" => "ΣΤΥΛΙΔΑΣ",
        "1911" => "ΛΙΔΩΡΙΚΙΟΥ",
        "1912" => "ΑΜΦΙΣΣΑΣ",
        "2111" => "ΑΡΓΟΥΣ",
        "2121" => "ΣΠΕΤΣΩΝ",
        "2122" => "ΚΡΑΝΙΔΙΟΥ",
        "2131" => "ΝΑΥΠΛΙΟΥ",
        "2211" => "ΔΗΜΗΤΣΑΝΑΣ",
        "2213" => "ΛΕΩΝΙΔΙΟΥ",
        "2214" => "ΤΡΟΠΑΙΩΝ",
        "2221" => "ΠΑΡΑΛΙΟΥ ΑΣΤΡΟΥΣ",
        "2231" => "ΤΡΙΠΟΛΗΣ",
        "2241" => "ΜΕΓΑΛΟΠΟΛΗΣ",
        "2311" => "ΑΙΓΙΟΥ",
        "2312" => "ΑΚΡΑΤΑΣ",
        "2321" => "ΚΑΛΑΒΡΥΤΩΝ",
        "2322" => "ΚΛΕΙΤΟΡΙΑΣ",
        "2331" => "ΠΑΤΡΩΝ",
        "2332" => "Β ΠΑΤΡΩΝ",
        "2333" => "ΚΑΤΩ ΑΧΑΙΑΣ",
        "2334" => "Γ ΠΑΤΡΩΝ",
        "2411" => "ΑΜΑΛΙΑΔΑΣ",
        "2412" => "ΠΥΡΓΟΥ",
        "2413" => "ΓΑΣΤΟΥΝΗΣ",
        "2414" => "ΒΑΡΔΑΣ",
        "2421" => "ΚΡΕΣΤΕΝΩΝ",
        "2422" => "ΛΕΧΑΙΝΩΝ",
        "2423" => "ΑΝΔΡΙΤΣΑΙΝΑΣ",
        "2424" => "ΖΑΧΑΡΩΣ",
        "2511" => "ΔΕΡΒΕΝΙΟΥ",
        "2512" => "ΚΙΑΤΟΥ",
        "2513" => "ΚΟΡΙΝΘΟΥ",
        "2514" => "ΝΕΜΕΑΣ",
        "2515" => "ΞΥΛΟΚΑΣΤΡΟΥ",
        "2611" => "ΓΥΘΕΙΟΥ",
        "2621" => "ΜΟΛΑΩΝ",
        "2622" => "ΝΕΑΠΟΛΕΩΣ ΒΟΙΩΝ",
        "2630" => "ΣΚΑΛΑΣ ΛΑΚΩΝΙΑΣ",
        "2631" => "ΚΡΟΚΕΩΝ",
        "2632" => "ΣΠΑΡΤΗΣ",
        "2641" => "ΑΡΕΟΠΟΛΗΣ",
        "2711" => "ΚΑΛΑΜΑΤΑΣ",
        "2721" => "ΜΕΛΙΓΑΛΑ",
        "2722" => "ΜΕΣΣΗΝΗΣ",
        "2731" => "ΠΥΛΟΥ",
        "2741" => "ΓΑΡΓΑΛΙΑΝΩΝ",
        "2742" => "ΚΥΠΑΡΙΣΣΙΑΣ",
        "2743" => "ΦΙΛΙΑΤΡΩΝ",
        "3111" => "ΚΑΡΔΙΤΣΑΣ",
        "3112" => "ΜΟΥΖΑΚΙΟΥ",
        "3113" => "ΣΟΦΑΔΩΝ",
        "3114" => "ΚΑΡΔΙΤΣΑΣ(ΠΑΛΑΜΑ,ΚΑΡΔΙΤΣΑΣ,ΜΟΥΖΑΚΙΟΥ,ΣΟΦΑΔΩΝ)",
        "3211" => "ΑΓΙΑΣ",
        "3221" => "ΕΛΑΣΣΟΝΑΣ",
        "3222" => "ΔΕΣΚΑΤΗΣ",
        "3231" => "ΛΑΡΙΣΑΣ",
        "3232" => "Β ΛΑΡΙΣΑΣ",
        "3233" => "Γ ΛΑΡΙΣΑΣ",
        "3241" => "Α ΛΑΡΙΣΑΣ",
        "3251" => "ΦΑΡΣΑΛΩΝ",
        "3311" => "ΑΛΜΥΡΟΥ",
        "3321" => "ΒΟΛΟΥ",
        "3322" => "ΒΑ ΒΟΛΟΥ",
        "3323" => "Ν. ΙΩΝΙΑΣ ΒΟΛΟΥ",
        "3331" => "ΣΚΟΠΕΛΟΥ",
        "3332" => "ΣΚΙΑΘΟΥ",
        "3411" => "ΚΑΛΑΜΠΑΚΑΣ",
        "3412" => "ΤΡΙΚΑΛΩΝ",
        "3413" => "ΠΥΛΗΣ",
        "3414" => "ΦΑΡΚΑΔΟΝΑΣ",
        "4111" => "ΑΛΕΞΑΝΔΡΕΙΑΣ",
        "4112" => "ΒΕΡΟΙΑΣ",
        "4121" => "ΝΑΟΥΣΑΣ",
        "4211" => "Α ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4212" => "Β ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4214" => "Δ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4215" => "Ε ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4216" => "ΣΤ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4217" => "Ζ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4221" => "ΖΑΓΚΛΙΒΕΡΙΟΥ",
        "4222" => "ΛΑΓΚΑΔΑ",
        "4223" => "ΣΟΧΟΥ",
        "4224" => "ΦΑΕ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4225" => "ΝΕΑΠΟΛΕΩΣ",
        "4226" => "ΤΟΥΜΠΑΣ",
        "4227" => "Ι ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4228" => "Η ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4229" => "Θ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4231" => "ΑΓ. ΑΘΑΝΑΣΙΟΥ",
        "4232" => "ΚΑΛΑΜΑΡΙΑΣ",
        "4233" => "ΑΜΠΕΛΟΚΗΠΩΝ",
        "4234" => "ΙΩΝΙΑΣ ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "4311" => "ΚΑΣΤΟΡΙΑΣ",
        "4312" => "ΝΕΣΤΟΡΙΟΥ",
        "4313" => "ΑΡΓΟΣ ΟΡΕΣΤΙΚΟΥ",
        "4411" => "ΚΙΛΚΙΣ",
        "4421" => "ΓΟΥΜΕΝΙΤΣΑΣ",
        "4511" => "ΝΕΑΠΟΛΗΣ ΒΟΙΟΥ",
        "4521" => "ΓΡΕΒΕΝΩΝ",
        "4531" => "ΠΤΟΛΕΜΑΪΔΑΣ",
        "4541" => "ΚΟΖΑΝΗΣ",
        "4542" => "ΣΕΡΒΙΩΝ",
        "4543" => "ΣΙΑΤΙΣΤΑΣ",
        "4611" => "ΑΡΙΔΑΙΑΣ",
        "4621" => "ΓΙΑΝΝΙΤΣΩΝ",
        "4631" => "ΕΔΕΣΣΑΣ",
        "4641" => "ΣΚΥΔΡΑΣ",
        "4711" => "ΚΑΤΕΡΙΝΗΣ",
        "4712" => "Β ΚΑΤΕΡΙΝΗΣ",
        "4714" => "ΑΙΓΙΝΙΟΥ",
        "4811" => "ΑΜΥΝΤΑΙΟΥ",
        "4812" => "ΦΛΩΡΙΝΑΣ",
        "4911" => "ΑΡΝΑΙΑΣ",
        "4921" => "ΚΑΣΣΑΝΔΡΑΣ",
        "4922" => "ΠΟΛΥΓΥΡΟΥ",
        "4923" => "ΝΕΩΝ ΜΟΥΔΑΝΙΩΝ",
        "5111" => "ΔΡΑΜΑΣ",
        "5112" => "ΝΕΥΡΟΚΟΠΙΟΥ",
        "5211" => "ΑΛΕΞΑΝΔΡΟΥΠΟΛΗΣ",
        "5221" => "ΔΙΔΥΜΟΤΕΙΧΟΥ",
        "5231" => "ΟΡΕΣΤΙΑΔΑΣ",
        "5241" => "ΣΟΥΦΛΙΟΥ",
        "5311" => "ΘΑΣΟΥ",
        "5321" => "ΚΑΒΑΛΑΣ",
        "5322" => "Β ΚΑΒΑΛΑΣ",
        "5331" => "ΧΡΥΣΟΥΠΟΛΗΣ",
        "5341" => "ΕΛΕΥΘΕΡΟΥΠΟΛΗΣ",
        "5411" => "ΞΑΝΘΗΣ",
        "5412" => "Β ΞΑΝΘΗΣ",
        "5511" => "ΚΟΜΟΤΗΝΗΣ",
        "5521" => "ΣΑΠΠΩΝ",
        "5611" => "ΝΙΓΡΙΤΑΣ",
        "5621" => "Α ΣΕΡΡΩΝ",
        "5622" => "Β ΣΕΡΡΩΝ",
        "5631" => "ΣΙΔΗΡΟΚΑΣΤΡΟΥ",
        "5632" => "ΗΡΑΚΛΕΙΑΣ",
        "5641" => "ΝΕΑΣ ΖΙΧΝΗΣ",
        "6111" => "ΑΡΤΑΣ",
        "6113" => "ΦΙΛΙΠΠΙΑΔΟΣ",
        "6211" => "ΗΓΟΥΜΕΝΙΤΣΑΣ",
        "6221" => "ΠΑΡΓΑΣ",
        "6231" => "ΠΑΡΑΜΥΘΙΑΣ",
        "6241" => "ΦΙΛΙΑΤΩΝ",
        "6311" => "ΙΩΑΝΝΙΝΩΝ",
        "6312" => "Β ΙΩΑΝΝΙΝΩΝ",
        "6313" => "ΔΕΛΒΙΝΑΚΙΟΥ",
        "6315" => "ΜΕΤΣΟΒΟΥ",
        "6321" => "ΚΟΝΙΤΣΑΣ",
        "6411" => "ΠΡΕΒΕΖΑΣ",
        "7111" => "ΑΝΔΡΟΥ",
        "7121" => "ΘΗΡΑΣ",
        "7131" => "ΚΕΑΣ",
        "7141" => "ΜΗΛΟΥ",
        "7151" => "ΝΑΞΟΥ",
        "7161" => "ΠΑΡΟΥ",
        "7171" => "ΣΥΡΟΥ",
        "7172" => "ΜΥΚΟΝΟΥ",
        "7181" => "ΤΗΝΟΥ",
        "7211" => "ΛΗΜΝΟΥ",
        "7221" => "ΚΑΛΛΟΝΗΣ",
        "7222" => "ΜΗΘΥΜΝΑΣ",
        "7231" => "ΜΥΤΙΛΗΝΗΣ",
        "7241" => "ΠΛΩΜΑΡΙΟΥ",
        "7311" => "ΑΓΙΟΣ ΚΗΡΥΚΑΣ ΙΚΑΡΙΑΣ",
        "7321" => "ΚΑΡΛΟΒΑΣΙΟΥ",
        "7322" => "ΣΑΜΟΥ",
        "7411" => "ΧΙΟΥ",
        "7511" => "ΚΑΛΥΜΝΟΥ",
        "7512" => "ΛΕΡΟΥ",
        "7521" => "ΚΑΡΠΑΘΟΥ",
        "7531" => "ΚΩ",
        "7542" => "ΡΟΔΟΥ",
        "8110" => "ΗΡΑΚΛΕΙΟΥ",
        "8111" => "Α ΗΡΑΚΛΕΙΟΥ",
        "8112" => "ΜΟΙΡΩΝ",
        "8113" => "Β ΗΡΑΚΛΕΙΟΥ",
        "8114" => "ΤΥΜΠΑΚΙΟΥ",
        "8115" => "ΛΙΜΕΝΟΣ ΧΕΡΣΟΝΗΣΟΣ",
        "8121" => "ΚΑΣΤΕΛΙΟΥ ΠΕΔΙΑΔΑ",
        "8131" => "ΑΡΚΑΛΟΧΩΡΙΟΥ",
        "8211" => "ΙΕΡΑΠΕΤΡΑΣ",
        "8221" => "ΑΓΙΟΥ ΝΙΚΟΛΑΟΥ",
        "8231" => "ΝΕΑΠΟΛΕΩΣ ΚΡΗΤΗΣ",
        "8241" => "ΣΗΤΕΙΑΣ",
        "8341" => "ΡΕΘΥΜΝΟΥ",
        "8421" => "ΚΑΣΤΕΛΙΟΥ ΚΙΣΣΑΜΟΥ",
        "8431" => "ΧΑΝΙΩΝ",
        "8432" => "Β ΧΑΝΙΩΝ",
        "9111" => "ΖΑΚΥΝΘΟΥ",
        "9211" => "Α ΚΕΡΚΥΡΑΣ",
        "9212" => "Β ΚΕΡΚΥΡΑΣ",
        "9221" => "ΠΑΞΩΝ",
        "9311" => "ΑΡΓΟΣΤΟΛΙΟΥ",
        "9321" => "ΛΗΞΟΥΡΙΟΥ",
        "9411" => "ΙΘΑΚΗΣ",
        "9421" => "ΛΕΥΚΑΔΑΣ",
        "9423" => "ΝΕΑΠΟΛΕΩΣ (ΘΕΣΣΑΛΟΝΙΚΗΣ)",
        "9424" => "ΙΩΑΝΝΙΝΩΝ ",
        "9425" => "ΚΕΡΚΥΡΑΣ",
        "9426" => "ΣΕΡΡΩΝ",
        "AL" => "ALBANIA",
        "AU" => "AUSTRALIA",
        "BG" => "BULGARY",
        "CY" => "CYPRUS",
        "ES" => "ESPANIA",
        "EU" => "EUROPE",
        "NO" => "NO",
        "SB" => "SERBIA",
        "SK" => "SKOPJE",
        "TR" => "TURKEY"
    );

    $taxCode = array_search($taxOffice, $mappingArray);

    return $taxCode;
}

function mapShippingMethod($magentoShippingMethod, $paymentMethod){
    $matrixrateCodes = array(
        '637',
        '638',
        '639',
        '640',
        '641',
        '642',
        '643',
        '644',
        '645',
        '647',
        '648',
        '649',
        '650',
        '651',
        '652',
        '653',
        '654',
        '659',
        '660'
    );
    if(stripos($magentoShippingMethod,"matrixrate") !== false){
        $explodedMethod = explode('matrixrate_matrixrate_', $magentoShippingMethod);
        $code = $explodedMethod[1];

        if(in_array($code, $matrixrateCodes)){
            return '331'; //courier - Speedex
        }
        elseif($code == '663' || $code == '664'){
            return '337'; //courier center
        }
        elseif($code == '646' || $code == '658'){
            return '330'; //metaforiki
        }
    }
    elseif(stripos($paymentMethod, 'skroutz') !== false && stripos($magentoShippingMethod, 'pickupfromstore') !== false){
        return '334'; // Skroutz Marketplace
    }
    elseif(stripos($magentoShippingMethod, 'pickupfromstore') !== false){
        return '332'; // store pickup
    }
    elseif(stripos($magentoShippingMethod, 'skroutzpoint') !== false){
        return '335'; // skroutz lockers (SLM)
    }
    elseif(stripos($magentoShippingMethod, 'boxnow') !== false){
        return '336'; // BOXNOW
    }


    return '';
}

function isSkroutzMarketplace($magentoShippingMethod, $paymentMethod){
    if(stripos($paymentMethod, 'skroutz') !== false && stripos($magentoShippingMethod, 'pickupfromstore') !== false){
        return true; // Skroutz Marketplace
    }
    return false;
}

function mapMetaforikaErpField($erpShippingMethod){
    $mappingArray = array(
        '331' => 'SPEEDEX',
        '335' => 'SLM',
        '336' => 'BOXNOW',
        '337' => 'COURIER_CENTER'
    );

    return (!empty($erpShippingMethod) && isset($mappingArray[$erpShippingMethod])) ? $mappingArray[$erpShippingMethod] : '';
}


function mapPayment($magentoPaymentCode, $orderData){
    $mappingArray = array(
        'cashondelivery' => array(
            'code' => '025',
            'description' => 'ΑΝΤΙΚΑΤΑΒΟΛΗ'
        ),
        'msp_cashondelivery' => array(
            'code' => '025',
            'description' => 'ΑΝΤΙΚΑΤΑΒΟΛΗ'
        ),
        'paypal_express' => array(
            'code' => '023',
            'description' => 'PAYPAL'
        ),
        'eurobank' => array(
            'code' => '008',
            'description' => 'ΠΙΣΤΩΤΙΚΗ ΚΑΡΤΑ'
        ),
        'banktransfer' => array(
            'code' => '030',
            'description' => 'ΚΑΤΑΘΕΣΗ ΣΕ ΤΡΑΠΕΖΑ'
        ),
        'payatstore' => array(
                'code' => '001',
                'description' => 'ΜΕΤΡΗΤΟΙΣ'
        ),
        'irispay' => array(
            'code' => '028',
            'description' => 'IRIS'
        ),
    );

    if(isset($mappingArray[$magentoPaymentCode])){
        return array(
            'code' => $mappingArray[$magentoPaymentCode]['code'],
            'description' => $mappingArray[$magentoPaymentCode]['description']
        );
    }

    elseif($magentoPaymentCode == 'credit'){
        $exploded = explode("-", $orderData['customer_payterm_code']);
        $paytermCode = $exploded[0];
        $paytermDescription = isset($exploded[1]) ? $exploded[1] : ' ';
        return array(
            'code' => $paytermCode,
            'description' => $paytermDescription
        );
    }

    return '';
}

function mapBranch($shippingMethod, $brCode = null){


    $mappingArray = array(
        'vari' => array(
            'code' =>  '04',
            'branch' => 'Βάρη Αποθήκη'
        ),
        'kallithea' => array(
            'code' =>  '06',
            'branch' => 'Καλλιθέα Αποθήκη'
        ),
        'kentriki' => array(
            'code' =>  '00',
            'branch' => 'Κεντρική Αποθήκη'
        ),
        'nea_ionia' => array(
            'code' =>  '02',
            'branch' => 'Νέα Ιωνία Αποθήκη'
        ),
        'nea_smirni' => array(
            'code' =>  '05',
            'branch' => 'Ν. Σμύρνη Αποθήκη'
        ),
        'chania' => array(
            'code' =>  '26',
            'branch' => 'Χανιά Αποθήκη'
        ),
        'argyroupoli' => array(
            'code' =>  '27',
            'branch' => 'Αργυρούπολη Αποθήκη'
        ),
        'larisa' => array(
            'code' =>  '11',
            'branch' => 'Λάρισα Αποθήκη'
        ),
        'peiraias' => array(
            'code' =>  '21',
            'branch' => 'Πειραιάς Αποθήκη'
        ),
        'lykovrysi' => array(
            'code' =>  '19',
            'branch' => 'Λυκόβρυση Αποθήκη'
        ),
        'korinthos' => array(
            'code' =>  '08',
            'branch' => 'Κόρινθος Αποθήκη'
        ),
        'patra' => array(
            'code' =>  '15',
            'branch' => 'Πάτρα Αποθήκη'
        ),
        'agia_paraskeui' => array(
            'code' =>  '13',
            'branch' => 'Αγ.Παρασκευή Αποθήκη'
        ),
        'thessaloniki' => array(
            'code' =>  '20',
            'branch' => 'Θεσ/νίκη Αποθήκη'
        ),
        'xalkida' => array(
            'code' =>  '24',
            'branch' => 'Χαλκίδα Αποθήκη'
        ),
        'ilion' => array(
            'code' =>  '17',
            'branch' => 'Ίλιον Αποθήκη'
        ),
        'aigaleo' => array(
            'code' =>  '23',
            'branch' => 'Αιγάλεω Αποθήκη'
        ),
        'glyka_nera' => array(
            'code' =>  '14',
            'branch' => 'Γλυκά Νερά Αποθήκη'
        ),
        'volos' => array(
            'code' =>  '12',
            'branch' => 'Βόλος Αποθήκη'
        ),
        'moschato_peiraios' => array(
            'code' =>  '25',
            'branch' => 'Πειραιώς Αποθήκη'
        ),
        'agios_dimitrios' => array(
            'code' =>  '07',
            'branch' => 'Αγ. Δημήτριος Αποθήκη'
        ),
        'menemeni' => array(
            'code' =>  '09',
            'branch' => 'Θεσ/νίκη Αποθήκη'
        ),
        'nea_erythraia' => array(
            'code' =>  '22',
            'branch' => 'Νέα Ερυθραία Αποθήκη'
        ),
        'hrakleio_kritis' => array(
            'code' =>  '16',
            'branch' => 'Ηράκλειο Αποθήκη'
        ),
        'moschato_thessalonikis' => array(
            'code' =>  '01',
            'branch' => 'Μοσχάτο Αποθήκη'
        ),
        'chalandri' => array(
            'code' =>  '03',
            'branch' => 'Χαλάνδρι Αποθήκη'
        ),
        'agrinio' => array(
            'code' =>  '18',
            'branch' => 'Αγρίνιο Αποθήκη'
        ),
        'kerkyra' => array(
            'code' =>  '28',
            'branch' => 'Κέρκυρα Αποθήκη'
        ),
        'lamia' => array(
            'code' =>  '29',
            'branch' => 'Λαμία Αποθήκη'
        ),
        'karditsa' => array(
            'code' =>  '30',
            'branch' => 'Καρδίτσα Αποθήκη'
        ),
        'trikala' => array(
            'code' =>  '31',
            'branch' => 'Τρίκαλα Αποθήκη'
        ),
        'kalamata' => array(
            'code' =>  '33',
            'branch' => 'Καλαμάτα Αποθήκη'
        ),
        'sparti' => array(
            'code' =>  '34',
            'branch' => 'Σπάρτη Αποθήκη'
        ),
        'elefsina' => array(
            'code' =>  '36',
            'branch' => 'Ελευσίνα Αποθήκη'
        )
    );

    if($brCode !== null){
        foreach($mappingArray as $branch){
            if($branch['code'] == $brCode){
                return $branch['branch'];
            }
        }
        return '';
    }
    else{
        $branchMethod = explode('pickupfromstore_pickup_', $shippingMethod)[1];
        if(isset($mappingArray[$branchMethod])){
            return $mappingArray[$branchMethod];
        }
        return '';
    }

}

function calculateBaseAmountByRule($item){
    //to avoid bugs like in ticket https://erp.elegento.com/admin/tickets/ticket/5171
    return number_format($item['net_price_for_erp'] / $item['item_qty_ordered'], 4, '.', '');

//    $itemBaseAmount = floatval($item['base_amount']);
//    $discountZonePrc = floatval($item['item_discount_zone_prc']);
//    //$couponZonePrc = floatval($item['item_discount_percent']);
//
//    $discountAmount = floatval($item['row_discount_amount_without_tax']);
//
//   // if($couponZonePrc == 0 && $discountAmount != 0){ //bundle discount case
//        $qtyOrdered = $item['item_qty_ordered'];
//        $rowPricesWithoutDiscount = $itemBaseAmount * $qtyOrdered;
//        $couponZonePrc = ($discountAmount / $rowPricesWithoutDiscount)*100;
//        $couponZonePrc = floatval(number_format($couponZonePrc, 2, ".", ""));
//    //}
//
//    $baseAmount = $itemBaseAmount - ($itemBaseAmount * ($discountZonePrc + $couponZonePrc - ($discountZonePrc * $couponZonePrc) /100)/100);
//
//    return number_format($baseAmount, 4, '.', '');
}

function isPriceAllowed($item, $order){
    $couponDiscount = $order['discount_amount'];
    if(!isset($item['max_discount'])){
        return true;
    }
    elseif($item['max_discount'] >= 0 && floatval($couponDiscount) != 0){
        return false;
    }

    $maxDiscount = floatval($item['max_discount']);
    $originalPrice = floatval($item['base_amount']);
    $minPriceAllowed = $originalPrice - ($originalPrice * ($maxDiscount/100));

    $itemFinalPrice = floatval(number_format($item['net_price_for_erp'] / $item['item_qty_ordered'], 4, '.', ''));

    if($minPriceAllowed > $itemFinalPrice){
        return false;
    }
    return true;
}

function checkIfItemWithMaxDiscIssueExists($order){
    foreach ($order['items'] as $item){
        if(isPriceAllowed($item, $order) === false){
            return true;
        }
    }
    return false;
}

function mapZipToNomos($zip_code){
    $map_array = ["10431" => "ΑΤΤΙΚΗΣ",
        "10432" => "ΑΤΤΙΚΗΣ",
        "10433" => "ΑΤΤΙΚΗΣ",
        "10434" => "ΑΤΤΙΚΗΣ",
        "10435" => "ΑΤΤΙΚΗΣ",
        "10436" => "ΑΤΤΙΚΗΣ",
        "10437" => "ΑΤΤΙΚΗΣ",
        "10438" => "ΑΤΤΙΚΗΣ",
        "10439" => "ΑΤΤΙΚΗΣ",
        "10440" => "ΑΤΤΙΚΗΣ",
        "10441" => "ΑΤΤΙΚΗΣ",
        "10442" => "ΑΤΤΙΚΗΣ",
        "10443" => "ΑΤΤΙΚΗΣ",
        "10444" => "ΑΤΤΙΚΗΣ",
        "10445" => "ΑΤΤΙΚΗΣ",
        "10446" => "ΑΤΤΙΚΗΣ",
        "10447" => "ΑΤΤΙΚΗΣ",
        "10551" => "ΑΤΤΙΚΗΣ",
        "10552" => "ΑΤΤΙΚΗΣ",
        "10553" => "ΑΤΤΙΚΗΣ",
        "10554" => "ΑΤΤΙΚΗΣ",
        "10555" => "ΑΤΤΙΚΗΣ",
        "10556" => "ΑΤΤΙΚΗΣ",
        "10557" => "ΑΤΤΙΚΗΣ",
        "10558" => "ΑΤΤΙΚΗΣ",
        "10559" => "ΑΤΤΙΚΗΣ",
        "10560" => "ΑΤΤΙΚΗΣ",
        "10561" => "ΑΤΤΙΚΗΣ",
        "10562" => "ΑΤΤΙΚΗΣ",
        "10563" => "ΑΤΤΙΚΗΣ",
        "10564" => "ΑΤΤΙΚΗΣ",
        "10671" => "ΑΤΤΙΚΗΣ",
        "10672" => "ΑΤΤΙΚΗΣ",
        "10673" => "ΑΤΤΙΚΗΣ",
        "10674" => "ΑΤΤΙΚΗΣ",
        "10675" => "ΑΤΤΙΚΗΣ",
        "10676" => "ΑΤΤΙΚΗΣ",
        "10677" => "ΑΤΤΙΚΗΣ",
        "10678" => "ΑΤΤΙΚΗΣ",
        "10679" => "ΑΤΤΙΚΗΣ",
        "10680" => "ΑΤΤΙΚΗΣ",
        "10681" => "ΑΤΤΙΚΗΣ",
        "10682" => "ΑΤΤΙΚΗΣ",
        "10683" => "ΑΤΤΙΚΗΣ",
        "11141" => "ΑΤΤΙΚΗΣ",
        "11142" => "ΑΤΤΙΚΗΣ",
        "11143" => "ΑΤΤΙΚΗΣ",
        "11144" => "ΑΤΤΙΚΗΣ",
        "11145" => "ΑΤΤΙΚΗΣ",
        "11146" => "ΑΤΤΙΚΗΣ",
        "11147" => "ΑΤΤΙΚΗΣ",
        "11251" => "ΑΤΤΙΚΗΣ",
        "11252" => "ΑΤΤΙΚΗΣ",
        "11253" => "ΑΤΤΙΚΗΣ",
        "11254" => "ΑΤΤΙΚΗΣ",
        "11255" => "ΑΤΤΙΚΗΣ",
        "11256" => "ΑΤΤΙΚΗΣ",
        "11257" => "ΑΤΤΙΚΗΣ",
        "11361" => "ΑΤΤΙΚΗΣ",
        "11362" => "ΑΤΤΙΚΗΣ",
        "11363" => "ΑΤΤΙΚΗΣ",
        "11364" => "ΑΤΤΙΚΗΣ",
        "11454" => "ΑΤΤΙΚΗΣ",
        "11471" => "ΑΤΤΙΚΗΣ",
        "11472" => "ΑΤΤΙΚΗΣ",
        "11473" => "ΑΤΤΙΚΗΣ",
        "11474" => "ΑΤΤΙΚΗΣ",
        "11475" => "ΑΤΤΙΚΗΣ",
        "11476" => "ΑΤΤΙΚΗΣ",
        "11521" => "ΑΤΤΙΚΗΣ",
        "11522" => "ΑΤΤΙΚΗΣ",
        "11523" => "ΑΤΤΙΚΗΣ",
        "11524" => "ΑΤΤΙΚΗΣ",
        "11525" => "ΑΤΤΙΚΗΣ",
        "11526" => "ΑΤΤΙΚΗΣ",
        "11527" => "ΑΤΤΙΚΗΣ",
        "11528" => "ΑΤΤΙΚΗΣ",
        "11533" => "ΑΤΤΙΚΗΣ",
        "11551" => "ΑΤΤΙΚΗΣ",
        "11631" => "ΑΤΤΙΚΗΣ",
        "11632" => "ΑΤΤΙΚΗΣ",
        "11633" => "ΑΤΤΙΚΗΣ",
        "11634" => "ΑΤΤΙΚΗΣ",
        "11635" => "ΑΤΤΙΚΗΣ",
        "11636" => "ΑΤΤΙΚΗΣ",
        "11741" => "ΑΤΤΙΚΗΣ",
        "11742" => "ΑΤΤΙΚΗΣ",
        "11743" => "ΑΤΤΙΚΗΣ",
        "11744" => "ΑΤΤΙΚΗΣ",
        "11745" => "ΑΤΤΙΚΗΣ",
        "11835" => "ΑΤΤΙΚΗΣ",
        "11851" => "ΑΤΤΙΚΗΣ",
        "11852" => "ΑΤΤΙΚΗΣ",
        "11853" => "ΑΤΤΙΚΗΣ",
        "11854" => "ΑΤΤΙΚΗΣ",
        "11855" => "ΑΤΤΙΚΗΣ",
        "12131" => "ΑΤΤΙΚΗΣ",
        "12132" => "ΑΤΤΙΚΗΣ",
        "12133" => "ΑΤΤΙΚΗΣ",
        "12134" => "ΑΤΤΙΚΗΣ",
        "12135" => "ΑΤΤΙΚΗΣ",
        "12136" => "ΑΤΤΙΚΗΣ",
        "12137" => "ΑΤΤΙΚΗΣ",
        "12141" => "ΑΤΤΙΚΗΣ",
        "12241" => "ΑΤΤΙΚΗΣ",
        "12242" => "ΑΤΤΙΚΗΣ",
        "12243" => "ΑΤΤΙΚΗΣ",
        "12244" => "ΑΤΤΙΚΗΣ",
        "12351" => "ΑΤΤΙΚΗΣ",
        "12461" => "ΑΤΤΙΚΗΣ",
        "12462" => "ΑΤΤΙΚΗΣ",
        "1376" => "ΑΤΤΙΚΗΣ",
        "13121" => "ΑΤΤΙΚΗΣ",
        "13122" => "ΑΤΤΙΚΗΣ",
        "13123" => "ΑΤΤΙΚΗΣ",
        "13231" => "ΑΤΤΙΚΗΣ",
        "13341" => "ΑΤΤΙΚΗΣ",
        "13451" => "ΑΤΤΙΚΗΣ",
        "13501" => "ΑΤΤΙΚΗΣ",
        "13561" => "ΑΤΤΙΚΗΣ",
        "13562" => "ΑΤΤΙΚΗΣ",
        "13671" => "ΑΤΤΙΚΗΣ",
        "13672" => "ΑΤΤΙΚΗΣ",
        "13673" => "ΑΤΤΙΚΗΣ",
        "13674" => "ΑΤΤΙΚΗΣ",
        "13675" => "ΑΤΤΙΚΗΣ",
        "13676" => "ΑΤΤΙΚΗΣ",
        "13677" => "ΑΤΤΙΚΗΣ",
        "14121" => "ΑΤΤΙΚΗΣ",
        "14122" => "ΑΤΤΙΚΗΣ",
        "14123" => "ΑΤΤΙΚΗΣ",
        "14231" => "ΑΤΤΙΚΗΣ",
        "14232" => "ΑΤΤΙΚΗΣ",
        "14233" => "ΑΤΤΙΚΗΣ",
        "14234" => "ΑΤΤΙΚΗΣ",
        "14235" => "ΑΤΤΙΚΗΣ",
        "14341" => "ΑΤΤΙΚΗΣ",
        "14342" => "ΑΤΤΙΚΗΣ",
        "14343" => "ΑΤΤΙΚΗΣ",
        "14451" => "ΑΤΤΙΚΗΣ",
        "14452" => "ΑΤΤΙΚΗΣ",
        "14561" => "ΑΤΤΙΚΗΣ",
        "14562" => "ΑΤΤΙΚΗΣ",
        "14563" => "ΑΤΤΙΚΗΣ",
        "14564" => "ΑΤΤΙΚΗΣ",
        "14565" => "ΑΤΤΙΚΗΣ",
        "14568" => "ΑΤΤΙΚΗΣ",
        "14569" => "ΑΤΤΙΚΗΣ",
        "14572" => "ΑΤΤΙΚΗΣ",
        "14574" => "ΑΤΤΙΚΗΣ",
        "14575" => "ΑΤΤΙΚΗΣ",
        "14576" => "ΑΤΤΙΚΗΣ",
        "14578" => "ΑΤΤΙΚΗΣ",
        "14671" => "ΑΤΤΙΚΗΣ",
        "15121" => "ΑΤΤΙΚΗΣ",
        "15122" => "ΑΤΤΙΚΗΣ",
        "15123" => "ΑΤΤΙΚΗΣ",
        "15124" => "ΑΤΤΙΚΗΣ",
        "15125" => "ΑΤΤΙΚΗΣ",
        "15126" => "ΑΤΤΙΚΗΣ",
        "15127" => "ΑΤΤΙΚΗΣ",
        "15231" => "ΑΤΤΙΚΗΣ",
        "15232" => "ΑΤΤΙΚΗΣ",
        "15233" => "ΑΤΤΙΚΗΣ",
        "15234" => "ΑΤΤΙΚΗΣ",
        "15235" => "ΑΤΤΙΚΗΣ",
        "15236" => "ΑΤΤΙΚΗΣ",
        "15237" => "ΑΤΤΙΚΗΣ",
        "15238" => "ΑΤΤΙΚΗΣ",
        "15341" => "ΑΤΤΙΚΗΣ",
        "15342" => "ΑΤΤΙΚΗΣ",
        "15343" => "ΑΤΤΙΚΗΣ",
        "15344" => "ΑΤΤΙΚΗΣ",
        "15349" => "ΑΤΤΙΚΗΣ",
        "15351" => "ΑΤΤΙΚΗΣ",
        "15354" => "ΑΤΤΙΚΗΣ",
        "15451" => "ΑΤΤΙΚΗΣ",
        "15452" => "ΑΤΤΙΚΗΣ",
        "15561" => "ΑΤΤΙΚΗΣ",
        "15562" => "ΑΤΤΙΚΗΣ",
        "15669" => "ΑΤΤΙΚΗΣ",
        "15771" => "ΑΤΤΙΚΗΣ",
        "15772" => "ΑΤΤΙΚΗΣ",
        "15773" => "ΑΤΤΙΚΗΣ",
        "16121" => "ΑΤΤΙΚΗΣ",
        "16122" => "ΑΤΤΙΚΗΣ",
        "16231" => "ΑΤΤΙΚΗΣ",
        "16232" => "ΑΤΤΙΚΗΣ",
        "16233" => "ΑΤΤΙΚΗΣ",
        "16341" => "ΑΤΤΙΚΗΣ",
        "16342" => "ΑΤΤΙΚΗΣ",
        "16343" => "ΑΤΤΙΚΗΣ",
        "16344" => "ΑΤΤΙΚΗΣ",
        "16345" => "ΑΤΤΙΚΗΣ",
        "16346" => "ΑΤΤΙΚΗΣ",
        "16450" => "ΑΤΤΙΚΗΣ",
        "16451" => "ΑΤΤΙΚΗΣ",
        "16452" => "ΑΤΤΙΚΗΣ",
        "16552" => "ΑΤΤΙΚΗΣ",
        "16561" => "ΑΤΤΙΚΗΣ",
        "16562" => "ΑΤΤΙΚΗΣ",
        "16671" => "ΑΤΤΙΚΗΣ",
        "16672" => "ΑΤΤΙΚΗΣ",
        "16673" => "ΑΤΤΙΚΗΣ",
        "16674" => "ΑΤΤΙΚΗΣ",
        "16675" => "ΑΤΤΙΚΗΣ",
        "16777" => "ΑΤΤΙΚΗΣ",
        "17121" => "ΑΤΤΙΚΗΣ",
        "17122" => "ΑΤΤΙΚΗΣ",
        "17123" => "ΑΤΤΙΚΗΣ",
        "17124" => "ΑΤΤΙΚΗΣ",
        "17232" => "ΑΤΤΙΚΗΣ",
        "17234" => "ΑΤΤΙΚΗΣ",
        "17235" => "ΑΤΤΙΚΗΣ",
        "17236" => "ΑΤΤΙΚΗΣ",
        "17237" => "ΑΤΤΙΚΗΣ",
        "17341" => "ΑΤΤΙΚΗΣ",
        "17342" => "ΑΤΤΙΚΗΣ",
        "17343" => "ΑΤΤΙΚΗΣ",
        "17455" => "ΑΤΤΙΚΗΣ",
        "17456" => "ΑΤΤΙΚΗΣ",
        "17561" => "ΑΤΤΙΚΗΣ",
        "17562" => "ΑΤΤΙΚΗΣ",
        "17563" => "ΑΤΤΙΚΗΣ",
        "17564" => "ΑΤΤΙΚΗΣ",
        "17671" => "ΑΤΤΙΚΗΣ",
        "17672" => "ΑΤΤΙΚΗΣ",
        "17673" => "ΑΤΤΙΚΗΣ",
        "17674" => "ΑΤΤΙΚΗΣ",
        "17675" => "ΑΤΤΙΚΗΣ",
        "17676" => "ΑΤΤΙΚΗΣ",
        "17778" => "ΑΤΤΙΚΗΣ",
        "18010" => "ΑΤΤΙΚΗΣ",
        "18020" => "ΑΤΤΙΚΗΣ",
        "18030" => "ΑΤΤΙΚΗΣ",
        "18040" => "ΑΤΤΙΚΗΣ",
        "18050" => "ΑΤΤΙΚΗΣ",
        "18120" => "ΑΤΤΙΚΗΣ",
        "18121" => "ΑΤΤΙΚΗΣ",
        "18122" => "ΑΤΤΙΚΗΣ",
        "18233" => "ΑΤΤΙΚΗΣ",
        "18344" => "ΑΤΤΙΚΗΣ",
        "18345" => "ΑΤΤΙΚΗΣ",
        "18346" => "ΑΤΤΙΚΗΣ",
        "18450" => "ΑΤΤΙΚΗΣ",
        "18451" => "ΑΤΤΙΚΗΣ",
        "18452" => "ΑΤΤΙΚΗΣ",
        "18453" => "ΑΤΤΙΚΗΣ",
        "18454" => "ΑΤΤΙΚΗΣ",
        "18531" => "ΑΤΤΙΚΗΣ",
        "18532" => "ΑΤΤΙΚΗΣ",
        "18533" => "ΑΤΤΙΚΗΣ",
        "18534" => "ΑΤΤΙΚΗΣ",
        "18535" => "ΑΤΤΙΚΗΣ",
        "18536" => "ΑΤΤΙΚΗΣ",
        "18537" => "ΑΤΤΙΚΗΣ",
        "18538" => "ΑΤΤΙΚΗΣ",
        "18539" => "ΑΤΤΙΚΗΣ",
        "18540" => "ΑΤΤΙΚΗΣ",
        "18541" => "ΑΤΤΙΚΗΣ",
        "18542" => "ΑΤΤΙΚΗΣ",
        "18543" => "ΑΤΤΙΚΗΣ",
        "18544" => "ΑΤΤΙΚΗΣ",
        "18545" => "ΑΤΤΙΚΗΣ",
        "18546" => "ΑΤΤΙΚΗΣ",
        "18547" => "ΑΤΤΙΚΗΣ",
        "18648" => "ΑΤΤΙΚΗΣ",
        "18755" => "ΑΤΤΙΚΗΣ",
        "18756" => "ΑΤΤΙΚΗΣ",
        "18757" => "ΑΤΤΙΚΗΣ",
        "18758" => "ΑΤΤΙΚΗΣ",
        "18863" => "ΑΤΤΙΚΗΣ",
        "18900" => "ΑΤΤΙΚΗΣ",
        "18901" => "ΑΤΤΙΚΗΣ",
        "18902" => "ΑΤΤΙΚΗΣ",
        "18903" => "ΑΤΤΙΚΗΣ",
        "19001" => "ΑΤΤΙΚΗΣ",
        "19002" => "ΑΤΤΙΚΗΣ",
        "19003" => "ΑΤΤΙΚΗΣ",
        "19004" => "ΑΤΤΙΚΗΣ",
        "19005" => "ΑΤΤΙΚΗΣ",
        "19006" => "ΑΤΤΙΚΗΣ",
        "19007" => "ΑΤΤΙΚΗΣ",
        "19008" => "ΑΤΤΙΚΗΣ",
        "19009" => "ΑΤΤΙΚΗΣ",
        "19010" => "ΑΤΤΙΚΗΣ",
        "19011" => "ΑΤΤΙΚΗΣ",
        "19012" => "ΑΤΤΙΚΗΣ",
        "19013" => "ΑΤΤΙΚΗΣ",
        "19014" => "ΑΤΤΙΚΗΣ",
        "19015" => "ΑΤΤΙΚΗΣ",
        "19016" => "ΑΤΤΙΚΗΣ",
        "19100" => "ΑΤΤΙΚΗΣ",
        "19200" => "ΑΤΤΙΚΗΣ",
        "19300" => "ΑΤΤΙΚΗΣ",
        "19400" => "ΑΤΤΙΚΗΣ",
        "19500" => "ΑΤΤΙΚΗΣ",
        "19600" => "ΑΤΤΙΚΗΣ",
        "80100" => "ΑΤΤΙΚΗΣ",
        "80200" => "ΑΤΤΙΚΗΣ",
        "53438" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54248" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54249" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54250" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54351" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54352" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54453" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54454" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54500" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54621" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54622" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54623" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54624" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54625" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54626" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54627" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54628" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54629" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54630" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54631" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54632" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54633" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54634" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54635" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54636" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54638" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54639" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54640" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54641" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54642" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54643" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54644" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54645" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54646" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "54655" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55131" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55132" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55133" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55134" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55236" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55337" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55438" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "55535" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56121" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56122" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56123" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56224" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56334" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56429" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56430" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56431" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56532" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56533" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56625" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56626" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56727" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "56728" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57001" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57002" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57003" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57004" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57006" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57007" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57008" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57009" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57010" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57011" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57012" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57013" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57014" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57015" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57016" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57017" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57018" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57019" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57020" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57021" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57022" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57100" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57200" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57300" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57400" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "57500" => "ΘΕΣΣΑΛΟΝΙΚΗΣ",
        "20001" => "ΚΟΡΙΝΘΙΑΣ",
        "20002" => "ΚΟΡΙΝΘΙΑΣ",
        "20003" => "ΚΟΡΙΝΘΙΑΣ",
        "20004" => "ΚΟΡΙΝΘΙΑΣ",
        "20005" => "ΚΟΡΙΝΘΙΑΣ",
        "20006" => "ΚΟΡΙΝΘΙΑΣ",
        "20007" => "ΚΟΡΙΝΘΙΑΣ",
        "20008" => "ΚΟΡΙΝΘΙΑΣ",
        "20009" => "ΚΟΡΙΝΘΙΑΣ",
        "20010" => "ΚΟΡΙΝΘΙΑΣ",
        "20011" => "ΚΟΡΙΝΘΙΑΣ",
        "20012" => "ΚΟΡΙΝΘΙΑΣ",
        "20013" => "ΚΟΡΙΝΘΙΑΣ",
        "20014" => "ΚΟΡΙΝΘΙΑΣ",
        "20015" => "ΚΟΡΙΝΘΙΑΣ",
        "20016" => "ΚΟΡΙΝΘΙΑΣ",
        "20017" => "ΚΟΡΙΝΘΙΑΣ",
        "20100" => "ΚΟΡΙΝΘΙΑΣ",
        "20200" => "ΚΟΡΙΝΘΙΑΣ",
        "20300" => "ΚΟΡΙΝΘΙΑΣ",
        "20400" => "ΚΟΡΙΝΘΙΑΣ",
        "20500" => "ΚΟΡΙΝΘΙΑΣ",
        "21051" => "ΑΡΓΟΛΙΔΟΣ",
        "21052" => "ΑΡΓΟΛΙΔΟΣ",
        "21053" => "ΑΡΓΟΛΙΔΑΣ",
        "21054" => "ΑΡΓΟΛΙΔΑΣ",
        "21055" => "ΑΡΓΟΛΙΔΑΣ",
        "21056" => "ΑΡΓΟΛΙΔΑΣ",
        "21057" => "ΑΡΓΟΛΙΔΑΣ",
        "21058" => "ΑΡΓΟΛΙΔΑΣ",
        "21059" => "ΑΡΓΟΛΙΔΑΣ",
        "21060" => "ΑΡΓΟΛΙΔΑΣ",
        "21061" => "ΑΡΓΟΛΙΔΑΣ",
        "21100" => "ΑΡΓΟΛΙΔΑΣ",
        "21200" => "ΑΡΓΟΛΙΔΑΣ",
        "21300" => "ΑΡΓΟΛΙΔΑΣ",
        "22001" => "ΑΡΚΑΔΙΑΣ",
        "22002" => "ΑΡΚΑΔΙΑΣ",
        "22003" => "ΑΡΚΑΔΙΑΣ",
        "22004" => "ΑΡΚΑΔΙΑΣ",
        "22005" => "ΑΡΚΑΔΙΑΣ",
        "22006" => "ΑΡΚΑΔΙΑΣ",
        "22007" => "ΑΡΚΑΔΙΑΣ",
        "22008" => "ΑΡΚΑΔΙΑΣ",
        "22009" => "ΑΡΚΑΔΙΑΣ",
        "22010" => "ΑΡΚΑΔΙΑΣ",
        "22011" => "ΑΡΚΑΔΙΑΣ",
        "22012" => "ΑΡΚΑΔΙΑΣ",
        "22013" => "ΑΡΚΑΔΙΑΣ",
        "22014" => "ΑΡΚΑΔΙΑΣ",
        "22015" => "ΑΡΚΑΔΙΑΣ",
        "22016" => "ΑΡΚΑΔΙΑΣ",
        "22017" => "ΑΡΚΑΔΙΑΣ",
        "22018" => "ΑΡΚΑΔΙΑΣ",
        "22019" => "ΑΡΚΑΔΙΑΣ",
        "22020" => "ΑΡΚΑΔΙΑΣ",
        "22021" => "ΑΡΚΑΔΙΑΣ",
        "22022" => "ΑΡΚΑΔΙΑΣ",
        "22024" => "ΑΡΚΑΔΙΑΣ",
        "22025" => "ΑΡΚΑΔΙΑΣ",
        "22026" => "ΑΡΚΑΔΙΑΣ",
        "22027" => "ΑΡΚΑΔΙΑΣ",
        "22028" => "ΑΡΚΑΔΙΑΣ",
        "22100" => "ΑΡΚΑΔΙΑΣ",
        "22200" => "ΑΡΚΑΔΙΑΣ",
        "22300" => "ΑΡΚΑΔΙΑΣ",
        "23051" => "ΛΑΚΩΝΙΑΣ",
        "23052" => "ΛΑΚΩΝΙΑΣ",
        "23053" => "ΛΑΚΩΝΙΑΣ",
        "23054" => "ΛΑΚΩΝΙΑΣ",
        "23055" => "ΛΑΚΩΝΙΑΣ",
        "23056" => "ΛΑΚΩΝΙΑΣ",
        "23057" => "ΛΑΚΩΝΙΑΣ",
        "23058" => "ΛΑΚΩΝΙΑΣ",
        "23059" => "ΛΑΚΩΝΙΑΣ",
        "23060" => "ΛΑΚΩΝΙΑΣ",
        "23061" => "ΛΑΚΩΝΙΑΣ",
        "23062" => "ΛΑΚΩΝΙΑΣ",
        "23063" => "ΛΑΚΩΝΙΑΣ",
        "23064" => "ΛΑΚΩΝΙΑΣ",
        "23065" => "ΛΑΚΩΝΙΑΣ",
        "23066" => "ΛΑΚΩΝΙΑΣ",
        "23067" => "ΛΑΚΩΝΙΑΣ",
        "23068" => "ΛΑΚΩΝΙΑΣ",
        "23069" => "ΛΑΚΩΝΙΑΣ",
        "23070" => "ΛΑΚΩΝΙΑΣ",
        "23071" => "ΛΑΚΩΝΙΑΣ",
        "23100" => "ΛΑΚΩΝΙΑΣ",
        "23200" => "ΛΑΚΩΝΙΑΣ",
        "24001" => "ΜΕΣΣΗΝΙΑΣ",
        "24002" => "ΜΕΣΣΗΝΙΑΣ",
        "24003" => "ΜΕΣΣΗΝΙΑΣ",
        "24004" => "ΜΕΣΣΗΝΙΑΣ",
        "24005" => "ΜΕΣΣΗΝΙΑΣ",
        "24006" => "ΜΕΣΣΗΝΙΑΣ",
        "24007" => "ΜΕΣΣΗΝΙΑΣ",
        "24008" => "ΜΕΣΣΗΝΙΑΣ",
        "24009" => "ΜΕΣΣΗΝΙΑΣ",
        "24010" => "ΜΕΣΣΗΝΙΑΣ",
        "24011" => "ΜΕΣΣΗΝΙΑΣ",
        "24012" => "ΜΕΣΣΗΝΙΑΣ",
        "24013" => "ΜΕΣΣΗΝΙΑΣ",
        "24014" => "ΜΕΣΣΗΝΙΑΣ",
        "24015" => "ΜΕΣΣΗΝΙΑΣ",
        "24016" => "ΜΕΣΣΗΝΙΑΣ",
        "24017" => "ΜΕΣΣΗΝΙΑΣ",
        "24018" => "ΜΕΣΣΗΝΙΑΣ",
        "24019" => "ΜΕΣΣΗΝΙΑΣ",
        "24020" => "ΜΕΣΣΗΝΙΑΣ",
        "24021" => "ΜΕΣΣΗΝΙΑΣ",
        "24022" => "ΜΕΣΣΗΝΙΑΣ",
        "24023" => "ΜΕΣΣΗΝΙΑΣ",
        "24024" => "ΜΕΣΣΗΝΙΑΣ",
        "24100" => "ΜΕΣΣΗΝΙΑΣ",
        "24101" => "ΜΕΣΣΗΝΙΑΣ",
        "24200" => "ΜΕΣΣΗΝΙΑΣ",
        "24300" => "ΜΕΣΣΗΝΙΑΣ",
        "24400" => "ΜΕΣΣΗΝΙΑΣ",
        "24500" => "ΜΕΣΣΗΝΙΑΣ",
        "24600" => "ΜΕΣΣΗΝΙΑΣ",
        "25001" => "ΑΧΑΪΑΣ",
        "25002" => "ΑΧΑΪΑΣ",
        "25003" => "ΑΧΑΪΑΣ",
        "25004" => "ΑΧΑΪΑΣ",
        "25005" => "ΑΧΑΪΑΣ",
        "25006" => "ΑΧΑΪΑΣ",
        "25007" => "ΑΧΑΪΑΣ",
        "25008" => "ΑΧΑΪΑΣ",
        "25009" => "ΑΧΑΪΑΣ",
        "25010" => "ΑΧΑΪΑΣ",
        "25011" => "ΑΧΑΪΑΣ",
        "25012" => "ΑΧΑΪΑΣ",
        "25013" => "ΑΧΑΪΑΣ",
        "25014" => "ΑΧΑΪΑΣ",
        "25015" => "ΑΧΑΪΑΣ",
        "25016" => "ΑΧΑΪΑΣ",
        "25017" => "ΑΧΑΪΑΣ",
        "25018" => "ΑΧΑΪΑΣ",
        "25100" => "ΑΧΑΪΑΣ",
        "25200" => "ΑΧΑΪΑΣ",
        "26221" => "ΑΧΑΪΑΣ",
        "26222" => "ΑΧΑΪΑΣ",
        "26223" => "ΑΧΑΪΑΣ",
        "26224" => "ΑΧΑΪΑΣ",
        "26225" => "ΑΧΑΪΑΣ",
        "26226" => "ΑΧΑΪΑΣ",
        "26234" => "ΑΧΑΪΑΣ",
        "26331" => "ΑΧΑΪΑΣ",
        "26332" => "ΑΧΑΪΑΣ",
        "26333" => "ΑΧΑΪΑΣ",
        "26334" => "ΑΧΑΪΑΣ",
        "26335" => "ΑΧΑΪΑΣ",
        "26441" => "ΑΧΑΪΑΣ",
        "26442" => "ΑΧΑΪΑΣ",
        "26443" => "ΑΧΑΪΑΣ",
        "26444" => "ΑΧΑΪΑΣ",
        "26500" => "ΑΧΑΪΑΣ",
        "26504" => "ΑΧΑΪΑΣ",
        "27050" => "ΗΛΕΙΑΣ",
        "27051" => "ΗΛΕΙΑΣ",
        "27052" => "ΗΛΕΙΑΣ",
        "27053" => "ΗΛΕΙΑΣ",
        "27054" => "ΗΛΕΙΑΣ",
        "27055" => "ΗΛΕΙΑΣ",
        "27056" => "ΗΛΕΙΑΣ",
        "27057" => "ΗΛΕΙΑΣ",
        "27058" => "ΗΛΕΙΑΣ",
        "27059" => "ΗΛΕΙΑΣ",
        "27060" => "ΗΛΕΙΑΣ",
        "27061" => "ΗΛΕΙΑΣ",
        "27062" => "ΗΛΕΙΑΣ",
        "27063" => "ΗΛΕΙΑΣ",
        "27064" => "ΗΛΕΙΑΣ",
        "27065" => "ΗΛΕΙΑΣ",
        "27066" => "ΗΛΕΙΑΣ",
        "27067" => "ΗΛΕΙΑΣ",
        "27068" => "ΗΛΕΙΑΣ",
        "27069" => "ΗΛΕΙΑΣ",
        "27100" => "ΗΛΕΙΑΣ",
        "27200" => "ΗΛΕΙΑΣ",
        "27300" => "ΗΛΕΙΑΣ",
        "28080" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28081" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28082" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28083" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28084" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28085" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28086" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28100" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28200" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28300" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "28301" => "ΚΕΦΑΛΛΗΝΙΑΣ",
        "29090" => "ΖΑΚΥΝΘΟΥ",
        "29091" => "ΖΑΚΥΝΘΟΥ",
        "29092" => "ΖΑΚΥΝΘΟΥ",
        "29100" => "ΖΑΚΥΝΘΟΥ",
        "30001" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30002" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30003" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30004" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30005" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30006" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30007" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30008" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30009" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30010" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30011" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30012" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30013" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30014" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30015" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30016" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30017" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30018" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30019" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30020" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30021" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30022" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30023" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30024" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30025" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30026" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30027" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30100" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30200" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30214" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30300" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30400" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "30500" => "ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
        "31080" => "ΛΕΥΚΑΔΑΣ",
        "31081" => "ΛΕΥΚΑΔΑΣ",
        "31082" => "ΛΕΥΚΑΔΑΣ",
        "31083" => "ΛΕΥΚΑΔΑΣ",
        "31100" => "ΛΕΥΚΑΔΑΣ",
        "32001" => "ΒΟΙΩΤΙΑΣ",
        "32002" => "ΒΟΙΩΤΙΑΣ",
        "32003" => "ΒΟΙΩΤΙΑΣ",
        "32004" => "ΒΟΙΩΤΙΑΣ",
        "32005" => "ΒΟΙΩΤΙΑΣ",
        "32006" => "ΒΟΙΩΤΙΑΣ",
        "32007" => "ΒΟΙΩΤΙΑΣ",
        "32008" => "ΒΟΙΩΤΙΑΣ",
        "32009" => "ΒΟΙΩΤΙΑΣ",
        "32010" => "ΒΟΙΩΤΙΑΣ",
        "32011" => "ΒΟΙΩΤΙΑΣ",
        "32012" => "ΒΟΙΩΤΙΑΣ",
        "32100" => "ΒΟΙΩΤΙΑΣ",
        "32131" => "ΒΟΙΩΤΙΑΣ",
        "32200" => "ΒΟΙΩΤΙΑΣ",
        "32300" => "ΒΟΙΩΤΙΑΣ",
        "33050" => "ΦΩΚΙΔΑΣ",
        "33051" => "ΦΩΚΙΔΑΣ",
        "33052" => "ΦΩΚΙΔΑΣ",
        "33053" => "ΦΩΚΙΔΑΣ",
        "33054" => "ΦΩΚΙΔΑΣ",
        "33055" => "ΦΩΚΙΔΑΣ",
        "33056" => "ΦΩΚΙΔΑΣ",
        "33057" => "ΦΩΚΙΔΑΣ",
        "33058" => "ΦΩΚΙΔΑΣ",
        "33059" => "ΦΩΚΙΔΑΣ",
        "33060" => "ΦΩΚΙΔΑΣ",
        "33061" => "ΦΩΚΙΔΑΣ",
        "33062" => "ΦΩΚΙΔΑΣ",
        "33063" => "ΦΩΚΙΔΑΣ",
        "33100" => "ΦΩΚΙΔΑΣ",
        "33200" => "ΦΩΚΙΔΑΣ",
        "34001" => "ΕΥΒΟΙΑΣ",
        "34002" => "ΕΥΒΟΙΑΣ",
        "34003" => "ΕΥΒΟΙΑΣ",
        "34004" => "ΕΥΒΟΙΑΣ",
        "34005" => "ΕΥΒΟΙΑΣ",
        "34006" => "ΕΥΒΟΙΑΣ",
        "34007" => "ΕΥΒΟΙΑΣ",
        "34008" => "ΕΥΒΟΙΑΣ",
        "34009" => "ΕΥΒΟΙΑΣ",
        "34010" => "ΕΥΒΟΙΑΣ",
        "34011" => "ΕΥΒΟΙΑΣ",
        "34012" => "ΕΥΒΟΙΑΣ",
        "34013" => "ΕΥΒΟΙΑΣ",
        "34014" => "ΕΥΒΟΙΑΣ",
        "34015" => "ΕΥΒΟΙΑΣ",
        "34016" => "ΕΥΒΟΙΑΣ",
        "34017" => "ΕΥΒΟΙΑΣ",
        "34018" => "ΕΥΒΟΙΑΣ",
        "34019" => "ΕΥΒΟΙΑΣ",
        "34100" => "ΕΥΒΟΙΑΣ",
        "34200" => "ΕΥΒΟΙΑΣ",
        "34300" => "ΕΥΒΟΙΑΣ",
        "34400" => "ΕΥΒΟΙΑΣ",
        "34500" => "ΕΥΒΟΙΑΣ",
        "34600" => "ΕΥΒΟΙΑΣ",
        "35001" => "ΦΘΙΩΤΙΔΑΣ",
        "35002" => "ΦΘΙΩΤΙΔΑΣ",
        "35003" => "ΦΘΙΩΤΙΔΑΣ",
        "35004" => "ΦΘΙΩΤΙΔΑΣ",
        "35005" => "ΦΘΙΩΤΙΔΑΣ",
        "35006" => "ΦΘΙΩΤΙΔΑΣ",
        "35007" => "ΦΘΙΩΤΙΔΑΣ",
        "35008" => "ΦΘΙΩΤΙΔΑΣ",
        "35009" => "ΦΘΙΩΤΙΔΑΣ",
        "35010" => "ΦΘΙΩΤΙΔΑΣ",
        "35011" => "ΦΘΙΩΤΙΔΑΣ",
        "35012" => "ΦΘΙΩΤΙΔΑΣ",
        "35013" => "ΦΘΙΩΤΙΔΑΣ",
        "35014" => "ΦΘΙΩΤΙΔΑΣ",
        "35015" => "ΦΘΙΩΤΙΔΑΣ",
        "35016" => "ΦΘΙΩΤΙΔΑΣ",
        "35017" => "ΦΘΙΩΤΙΔΑΣ",
        "35018" => "ΦΘΙΩΤΙΔΑΣ",
        "35100" => "ΦΘΙΩΤΙΔΑΣ",
        "35132" => "ΦΘΙΩΤΙΔΑΣ",
        "35200" => "ΦΘΙΩΤΙΔΑΣ",
        "35300" => "ΦΘΙΩΤΙΔΑΣ",
        "36070" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36071" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36072" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36073" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36074" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36075" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36076" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36080" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36081" => "ΕΥΡΥΤΑΝΙΑΣ",
        "36100" => "ΕΥΡΥΤΑΝΙΑΣ",
        "37001" => "ΜΑΓΝΗΣΙΑΣ",
        "37002" => "ΜΑΓΝΗΣΙΑΣ",
        "37003" => "ΜΑΓΝΗΣΙΑΣ",
        "37004" => "ΜΑΓΝΗΣΙΑΣ",
        "37005" => "ΜΑΓΝΗΣΙΑΣ",
        "37006" => "ΜΑΓΝΗΣΙΑΣ",
        "37007" => "ΜΑΓΝΗΣΙΑΣ",
        "37008" => "ΜΑΓΝΗΣΙΑΣ",
        "37009" => "ΜΑΓΝΗΣΙΑΣ",
        "37010" => "ΜΑΓΝΗΣΙΑΣ",
        "37011" => "ΜΑΓΝΗΣΙΑΣ",
        "37012" => "ΜΑΓΝΗΣΙΑΣ",
        "37013" => "ΜΑΓΝΗΣΙΑΣ",
        "37100" => "ΜΑΓΝΗΣΙΑΣ",
        "37200" => "ΜΑΓΝΗΣΙΑΣ",
        "37300" => "ΜΑΓΝΗΣΙΑΣ",
        "37400" => "ΜΑΓΝΗΣΙΑΣ",
        "37500" => "ΜΑΓΝΗΣΙΑΣ",
        "38221" => "ΜΑΓΝΗΣΙΑΣ",
        "38222" => "ΜΑΓΝΗΣΙΑΣ",
        "38300" => "ΜΑΓΝΗΣΙΑΣ",
        "38322" => "ΜΑΓΝΗΣΙΑΣ",
        "38331" => "ΜΑΓΝΗΣΙΑΣ",
        "38333" => "ΜΑΓΝΗΣΙΑΣ",
        "38334" => "ΜΑΓΝΗΣΙΑΣ",
        "38445" => "ΜΑΓΝΗΣΙΑΣ",
        "38446" => "ΜΑΓΝΗΣΙΑΣ",
        "38500" => "ΜΑΓΝΗΣΙΑΣ",
        "40001" => "ΛΑΡΙΣΑΣ",
        "40002" => "ΛΑΡΙΣΑΣ",
        "40003" => "ΛΑΡΙΣΑΣ",
        "40004" => "ΛΑΡΙΣΑΣ",
        "40005" => "ΛΑΡΙΣΑΣ",
        "40006" => "ΛΑΡΙΣΑΣ",
        "40007" => "ΛΑΡΙΣΑΣ",
        "40008" => "ΛΑΡΙΣΑΣ",
        "40009" => "ΛΑΡΙΣΑΣ",
        "40010" => "ΛΑΡΙΣΑΣ",
        "40011" => "ΛΑΡΙΣΑΣ",
        "40100" => "ΛΑΡΙΣΑΣ",
        "40200" => "ΛΑΡΙΣΑΣ",
        "40300" => "ΛΑΡΙΣΑΣ",
        "40400" => "ΛΑΡΙΣΑΣ",
        "41001" => "ΛΑΡΙΣΑΣ",
        "41221" => "ΛΑΡΙΣΑΣ",
        "41222" => "ΛΑΡΙΣΑΣ",
        "41223" => "ΛΑΡΙΣΑΣ",
        "41334" => "ΛΑΡΙΣΑΣ",
        "41335" => "ΛΑΡΙΣΑΣ",
        "41336" => "ΛΑΡΙΣΑΣ",
        "41447" => "ΛΑΡΙΣΑΣ",
        "41500" => "ΛΑΡΙΣΑΣ",
        "42030" => "ΤΡΙΚΑΛΩΝ",
        "42031" => "ΤΡΙΚΑΛΩΝ",
        "42032" => "ΤΡΙΚΑΛΩΝ",
        "42033" => "ΤΡΙΚΑΛΩΝ",
        "42034" => "ΤΡΙΚΑΛΩΝ",
        "42035" => "ΤΡΙΚΑΛΩΝ",
        "42036" => "ΤΡΙΚΑΛΩΝ",
        "42037" => "ΤΡΙΚΑΛΩΝ",
        "42100" => "ΤΡΙΚΑΛΩΝ",
        "42200" => "ΤΡΙΚΑΛΩΝ",
        "42300" => "ΤΡΙΚΑΛΩΝ",
        "43060" => "ΚΑΡΔΙΤΣΑΣ",
        "43061" => "ΚΑΡΔΙΤΣΑΣ",
        "43062" => "ΚΑΡΔΙΤΣΑΣ",
        "43063" => "ΚΑΡΔΙΤΣΑΣ",
        "43064" => "ΚΑΡΔΙΤΣΑΣ",
        "43065" => "ΚΑΡΔΙΤΣΑΣ",
        "43066" => "ΚΑΡΔΙΤΣΑΣ",
        "43067" => "ΚΑΡΔΙΤΣΑΣ",
        "43068" => "ΚΑΡΔΙΤΣΑΣ",
        "43069" => "ΚΑΡΔΙΤΣΑΣ",
        "43070" => "ΚΑΡΔΙΤΣΑΣ",
        "43100" => "ΚΑΡΔΙΤΣΑΣ",
        "43200" => "ΚΑΡΔΙΤΣΑΣ",
        "43300" => "ΚΑΡΔΙΤΣΑΣ",
        "44001" => "ΙΩΑΝΝΙΝΩΝ",
        "44002" => "ΙΩΑΝΝΙΝΩΝ",
        "44003" => "ΙΩΑΝΝΙΝΩΝ",
        "44004" => "ΙΩΑΝΝΙΝΩΝ",
        "44005" => "ΙΩΑΝΝΙΝΩΝ",
        "44006" => "ΙΩΑΝΝΙΝΩΝ",
        "44007" => "ΙΩΑΝΝΙΝΩΝ",
        "44008" => "ΙΩΑΝΝΙΝΩΝ",
        "44009" => "ΙΩΑΝΝΙΝΩΝ",
        "44010" => "ΙΩΑΝΝΙΝΩΝ",
        "44012" => "ΙΩΑΝΝΙΝΩΝ",
        "44013" => "ΙΩΑΝΝΙΝΩΝ",
        "44014" => "ΙΩΑΝΝΙΝΩΝ",
        "44015" => "ΙΩΑΝΝΙΝΩΝ",
        "44016" => "ΙΩΑΝΝΙΝΩΝ",
        "44017" => "ΙΩΑΝΝΙΝΩΝ",
        "44018" => "ΙΩΑΝΝΙΝΩΝ",
        "44019" => "ΙΩΑΝΝΙΝΩΝ",
        "44020" => "ΙΩΑΝΝΙΝΩΝ",
        "44100" => "ΙΩΑΝΝΙΝΩΝ",
        "44200" => "ΙΩΑΝΝΙΝΩΝ",
        "45221" => "ΙΩΑΝΝΙΝΩΝ",
        "45232" => "ΙΩΑΝΝΙΝΩΝ",
        "45332" => "ΙΩΑΝΝΙΝΩΝ",
        "45333" => "ΙΩΑΝΝΙΝΩΝ",
        "45442" => "ΙΩΑΝΝΙΝΩΝ",
        "45444" => "ΙΩΑΝΝΙΝΩΝ",
        "45445" => "ΙΩΑΝΝΙΝΩΝ",
        "45500" => "ΙΩΑΝΝΙΝΩΝ",
        "46030" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46031" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46032" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46033" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46100" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46200" => "ΘΕΣΠΡΩΤΙΑΣ",
        "46300" => "ΘΕΣΠΡΩΤΙΑΣ",
        "47040" => "ΑΡΤΑΣ",
        "47041" => "ΑΡΤΑΣ",
        "47042" => "ΑΡΤΑΣ",
        "47043" => "ΑΡΤΑΣ",
        "47044" => "ΑΡΤΑΣ",
        "47045" => "ΑΡΤΑΣ",
        "47046" => "ΑΡΤΑΣ",
        "47047" => "ΑΡΤΑΣ",
        "47048" => "ΑΡΤΑΣ",
        "47100" => "ΑΡΤΑΣ",
        "47200" => "ΑΡΤΑΣ",
        "48060" => "ΠΡΕΒΕΖΑΣ",
        "48061" => "ΠΡΕΒΕΖΑΣ",
        "48062" => "ΠΡΕΒΕΖΑΣ",
        "48100" => "ΠΡΕΒΕΖΑΣ",
        "48200" => "ΠΡΕΒΕΖΑΣ",
        "48300" => "ΠΡΕΒΕΖΑΣ",
        "49080" => "ΚΕΡΚΥΡΑΣ",
        "49081" => "ΚΕΡΚΥΡΑΣ",
        "49082" => "ΚΕΡΚΥΡΑΣ",
        "49083" => "ΚΕΡΚΥΡΑΣ",
        "49084" => "ΚΕΡΚΥΡΑΣ",
        "49100" => "ΚΕΡΚΥΡΑΣ",
        "50001" => "ΚΟΖΑΝΗΣ",
        "50002" => "ΚΟΖΑΝΗΣ",
        "50003" => "ΚΟΖΑΝΗΣ",
        "50004" => "ΚΟΖΑΝΗΣ",
        "50005" => "ΚΟΖΑΝΗΣ",
        "50006" => "ΚΟΖΑΝΗΣ",
        "50007" => "ΚΟΖΑΝΗΣ",
        "50008" => "ΚΟΖΑΝΗΣ",
        "50009" => "ΚΟΖΑΝΗΣ",
        "50010" => "ΚΟΖΑΝΗΣ",
        "50100" => "ΚΟΖΑΝΗΣ",
        "50200" => "ΚΟΖΑΝΗΣ",
        "50300" => "ΚΟΖΑΝΗΣ",
        "50400" => "ΚΟΖΑΝΗΣ",
        "50500" => "ΚΟΖΑΝΗΣ",
        "51030" => "ΓΡΕΒΕΝΩΝ",
        "51031" => "ΓΡΕΒΕΝΩΝ",
        "51032" => "ΓΡΕΒΕΝΩΝ",
        "51100" => "ΓΡΕΒΕΝΩΝ",
        "51200" => "ΓΡΕΒΕΝΩΝ",
        "52050" => "ΚΑΣΤΟΡΙΑΣ",
        "52051" => "ΚΑΣΤΟΡΙΑΣ",
        "52052" => "ΚΑΣΤΟΡΙΑΣ",
        "52053" => "ΚΑΣΤΟΡΙΑΣ",
        "52054" => "ΚΑΣΤΟΡΙΑΣ",
        "52055" => "ΚΑΣΤΟΡΙΑΣ",
        "52056" => "ΚΑΣΤΟΡΙΑΣ",
        "52057" => "ΚΑΣΤΟΡΙΑΣ",
        "52058" => "ΚΑΣΤΟΡΙΑΣ",
        "52059" => "ΚΑΣΤΟΡΙΑΣ",
        "52100" => "ΚΑΣΤΟΡΙΑΣ",
        "52200" => "ΚΑΣΤΟΡΙΑΣ",
        "53070" => "ΦΛΩΡΙΝΑΣ",
        "53071" => "ΦΛΩΡΙΝΑΣ",
        "53072" => "ΦΛΩΡΙΝΑΣ",
        "53073" => "ΦΛΩΡΙΝΑΣ",
        "53074" => "ΦΛΩΡΙΝΑΣ",
        "53075" => "ΦΛΩΡΙΝΑΣ",
        "53076" => "ΦΛΩΡΙΝΑΣ",
        "53077" => "ΦΛΩΡΙΝΑΣ",
        "53078" => "ΦΛΩΡΙΝΑΣ",
        "53100" => "ΦΛΩΡΙΝΑΣ",
        "53200" => "ΦΛΩΡΙΝΑΣ",
        "58001" => "ΠΕΛΛΑΣ",
        "58002" => "ΠΕΛΛΑΣ",
        "58003" => "ΠΕΛΛΑΣ",
        "58004" => "ΠΕΛΛΑΣ",
        "58005" => "ΠΕΛΛΑΣ",
        "58100" => "ΠΕΛΛΑΣ",
        "58200" => "ΠΕΛΛΑΣ",
        "58300" => "ΠΕΛΛΑΣ",
        "58400" => "ΠΕΛΛΑΣ",
        "58500" => "ΠΕΛΛΑΣ",
        "59031" => "ΗΜΑΘΙΑΣ",
        "59032" => "ΗΜΑΘΙΑΣ",
        "59033" => "ΗΜΑΘΙΑΣ",
        "59034" => "ΗΜΑΘΙΑΣ",
        "59035" => "ΗΜΑΘΙΑΣ",
        "59100" => "ΗΜΑΘΙΑΣ",
        "59200" => "ΗΜΑΘΙΑΣ",
        "59300" => "ΗΜΑΘΙΑΣ",
        "60061" => "ΠΙΕΡΙΑΣ",
        "60062" => "ΠΙΕΡΙΑΣ",
        "60063" => "ΠΙΕΡΙΑΣ",
        "60064" => "ΠΙΕΡΙΑΣ",
        "60065" => "ΠΙΕΡΙΑΣ",
        "60066" => "ΠΙΕΡΙΑΣ",
        "60100" => "ΠΙΕΡΙΑΣ",
        "60200" => "ΠΙΕΡΙΑΣ",
        "60300" => "ΠΙΕΡΙΑΣ",
        "61001" => "ΚΙΛΚΙΣ",
        "61002" => "ΚΙΛΚΙΣ",
        "61003" => "ΚΙΛΚΙΣ",
        "61005" => "ΚΙΛΚΙΣ",
        "61006" => "ΚΙΛΚΙΣ",
        "61007" => "ΚΙΛΚΙΣ",
        "61100" => "ΚΙΛΚΙΣ",
        "61200" => "ΚΙΛΚΙΣ",
        "61300" => "ΚΙΛΚΙΣ",
        "61400" => "ΚΙΛΚΙΣ",
        "62041" => "ΣΕΡΡΩΝ",
        "62042" => "ΣΕΡΡΩΝ",
        "62043" => "ΣΕΡΡΩΝ",
        "62044" => "ΣΕΡΡΩΝ",
        "62045" => "ΣΕΡΡΩΝ",
        "62046" => "ΣΕΡΡΩΝ",
        "62047" => "ΣΕΡΡΩΝ",
        "62048" => "ΣΕΡΡΩΝ",
        "62049" => "ΣΕΡΡΩΝ",
        "62050" => "ΣΕΡΡΩΝ",
        "62051" => "ΣΕΡΡΩΝ",
        "62052" => "ΣΕΡΡΩΝ",
        "62053" => "ΣΕΡΡΩΝ",
        "62054" => "ΣΕΡΡΩΝ",
        "62055" => "ΣΕΡΡΩΝ",
        "62056" => "ΣΕΡΡΩΝ",
        "62100" => "ΣΕΡΡΩΝ",
        "62121" => "ΣΕΡΡΩΝ",
        "62122" => "ΣΕΡΡΩΝ",
        "62123" => "ΣΕΡΡΩΝ",
        "62124" => "ΣΕΡΡΩΝ",
        "62125" => "ΣΕΡΡΩΝ",
        "62200" => "ΣΕΡΡΩΝ",
        "62300" => "ΣΕΡΡΩΝ",
        "62400" => "ΣΕΡΡΩΝ",
        "63071" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63072" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63073" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63074" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63075" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63076" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63077" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63078" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63079" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63080" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63081" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63082" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63083" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63084" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63085" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63086" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63087" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63088" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63100" => "ΧΑΛΚΙΔΙΚΗΣ",
        "63200" => "ΧΑΛΚΙΔΙΚΗΣ",
        "64001" => "ΚΑΒΑΛΑΣ",
        "64002" => "ΚΑΒΑΛΑΣ",
        "64003" => "ΚΑΒΑΛΑΣ",
        "64004" => "ΚΑΒΑΛΑΣ",
        "64005" => "ΚΑΒΑΛΑΣ",
        "64006" => "ΚΑΒΑΛΑΣ",
        "64007" => "ΚΑΒΑΛΑΣ",
        "64008" => "ΚΑΒΑΛΑΣ",
        "64009" => "ΚΑΒΑΛΑΣ",
        "64010" => "ΚΑΒΑΛΑΣ",
        "64011" => "ΚΑΒΑΛΑΣ",
        "64012" => "ΚΑΒΑΛΑΣ",
        "64100" => "ΚΑΒΑΛΑΣ",
        "64200" => "ΚΑΒΑΛΑΣ",
        "65201" => "ΚΑΒΑΛΑΣ",
        "65302" => "ΚΑΒΑΛΑΣ",
        "65403" => "ΚΑΒΑΛΑΣ",
        "65404" => "ΚΑΒΑΛΑΣ",
        "65500" => "ΚΑΒΑΛΑΣ",
        "66031" => "ΔΡΑΜΑΣ",
        "66032" => "ΔΡΑΜΑΣ",
        "66033" => "ΔΡΑΜΑΣ",
        "66034" => "ΔΡΑΜΑΣ",
        "66035" => "ΔΡΑΜΑΣ",
        "66036" => "ΔΡΑΜΑΣ",
        "66037" => "ΔΡΑΜΑΣ",
        "66038" => "ΔΡΑΜΑΣ",
        "66100" => "ΔΡΑΜΑΣ",
        "66200" => "ΔΡΑΜΑΣ",
        "66300" => "ΔΡΑΜΑΣ",
        "67061" => "ΞΑΝΘΗΣ",
        "67062" => "ΞΑΝΘΗΣ",
        "67063" => "ΞΑΝΘΗΣ",
        "67064" => "ΞΑΝΘΗΣ",
        "67100" => "ΞΑΝΘΗΣ",
        "67200" => "ΞΑΝΘΗΣ",
        "67300" => "ΞΑΝΘΗΣ",
        "68001" => "ΕΒΡΟΥ",
        "68002" => "ΕΒΡΟΥ",
        "68003" => "ΕΒΡΟΥ",
        "68004" => "ΕΒΡΟΥ",
        "68005" => "ΕΒΡΟΥ",
        "68006" => "ΕΒΡΟΥ",
        "68007" => "ΕΒΡΟΥ",
        "68008" => "ΕΒΡΟΥ",
        "68009" => "ΕΒΡΟΥ",
        "68010" => "ΕΒΡΟΥ",
        "68011" => "ΕΒΡΟΥ",
        "68012" => "ΕΒΡΟΥ",
        "68013" => "ΕΒΡΟΥ",
        "68014" => "ΕΒΡΟΥ",
        "68100" => "ΕΒΡΟΥ",
        "68200" => "ΕΒΡΟΥ",
        "68300" => "ΕΒΡΟΥ",
        "68400" => "ΕΒΡΟΥ",
        "68500" => "ΕΒΡΟΥ",
        "69100" => "ΡΟΔΟΠΗΣ",
        "69200" => "ΡΟΔΟΠΗΣ",
        "69300" => "ΡΟΔΟΠΗΣ",
        "69400" => "ΡΟΔΟΠΗΣ",
        "69500" => "ΡΟΔΟΠΗΣ",
        "70001" => "ΗΡΑΚΛΕΙΟΥ",
        "70002" => "ΗΡΑΚΛΕΙΟΥ",
        "70003" => "ΗΡΑΚΛΕΙΟΥ",
        "70004" => "ΗΡΑΚΛΕΙΟΥ",
        "70005" => "ΗΡΑΚΛΕΙΟΥ",
        "70006" => "ΗΡΑΚΛΕΙΟΥ",
        "70007" => "ΗΡΑΚΛΕΙΟΥ",
        "70008" => "ΗΡΑΚΛΕΙΟΥ",
        "70009" => "ΗΡΑΚΛΕΙΟΥ",
        "70010" => "ΗΡΑΚΛΕΙΟΥ",
        "70011" => "ΗΡΑΚΛΕΙΟΥ",
        "70012" => "ΗΡΑΚΛΕΙΟΥ",
        "70013" => "ΗΡΑΚΛΕΙΟΥ",
        "70014" => "ΗΡΑΚΛΕΙΟΥ",
        "70015" => "ΗΡΑΚΛΕΙΟΥ",
        "70016" => "ΗΡΑΚΛΕΙΟΥ",
        "70017" => "ΗΡΑΚΛΕΙΟΥ",
        "70100" => "ΗΡΑΚΛΕΙΟΥ",
        "70200" => "ΗΡΑΚΛΕΙΟΥ",
        "70300" => "ΗΡΑΚΛΕΙΟΥ",
        "70400" => "ΗΡΑΚΛΕΙΟΥ",
        "71201" => "ΗΡΑΚΛΕΙΟΥ",
        "71202" => "ΗΡΑΚΛΕΙΟΥ",
        "71303" => "ΗΡΑΚΛΕΙΟΥ",
        "71304" => "ΗΡΑΚΛΕΙΟΥ",
        "71305" => "ΗΡΑΚΛΕΙΟΥ",
        "71306" => "ΗΡΑΚΛΕΙΟΥ",
        "71307" => "ΗΡΑΚΛΕΙΟΥ",
        "71309" => "ΗΡΑΚΛΕΙΟΥ",
        "71334" => "ΧΑΝΙΩΝ",
        "71409" => "ΗΡΑΚΛΕΙΟΥ",
        "71410" => "ΗΡΑΚΛΕΙΟΥ",
        "71414" => "ΗΡΑΚΛΕΙΟΥ",
        "71500" => "ΗΡΑΚΛΕΙΟΥ",
        "71601" => "ΗΡΑΚΛΕΙΟΥ",
        "72051" => "ΛΑΣΙΘΙΟΥ",
        "72052" => "ΛΑΣΙΘΙΟΥ",
        "72053" => "ΛΑΣΙΘΙΟΥ",
        "72054" => "ΛΑΣΙΘΙΟΥ",
        "72055" => "ΛΑΣΙΘΙΟΥ",
        "72056" => "ΛΑΣΙΘΙΟΥ",
        "72057" => "ΛΑΣΙΘΙΟΥ",
        "72058" => "ΛΑΣΙΘΙΟΥ",
        "72059" => "ΛΑΣΙΘΙΟΥ",
        "72100" => "ΛΑΣΙΘΙΟΥ",
        "72200" => "ΛΑΣΙΘΙΟΥ",
        "72300" => "ΛΑΣΙΘΙΟΥ",
        "72400" => "ΛΑΣΙΘΙΟΥ",
        "73001" => "ΧΑΝΙΩΝ",
        "73002" => "ΧΑΝΙΩΝ",
        "73003" => "ΧΑΝΙΩΝ",
        "73004" => "ΧΑΝΙΩΝ",
        "73005" => "ΧΑΝΙΩΝ",
        "73006" => "ΧΑΝΙΩΝ",
        "73007" => "ΧΑΝΙΩΝ",
        "73008" => "ΧΑΝΙΩΝ",
        "73009" => "ΧΑΝΙΩΝ",
        "73010" => "ΧΑΝΙΩΝ",
        "73011" => "ΧΑΝΙΩΝ",
        "73012" => "ΧΑΝΙΩΝ",
        "73013" => "ΧΑΝΙΩΝ",
        "73014" => "ΧΑΝΙΩΝ",
        "73100" => "ΧΑΝΙΩΝ",
        "73131" => "ΧΑΝΙΩΝ",
        "73132" => "ΧΑΝΙΩΝ",
        "73133" => "ΧΑΝΙΩΝ",
        "73134" => "ΧΑΝΙΩΝ",
        "73135" => "ΧΑΝΙΩΝ",
        "73136" => "ΧΑΝΙΩΝ",
        "73200" => "ΧΑΝΙΩΝ",
        "73300" => "ΧΑΝΙΩΝ",
        "73400" => "ΧΑΝΙΩΝ",
        "74000" => "ΡΕΘΥΜΝΗΣ",
        "74051" => "ΡΕΘΥΜΝΗΣ",
        "74052" => "ΡΕΘΥΜΝΗΣ",
        "74053" => "ΡΕΘΥΜΝΗΣ",
        "74054" => "ΡΕΘΥΜΝΗΣ",
        "74055" => "ΡΕΘΥΜΝΗΣ",
        "74056" => "ΡΕΘΥΜΝΗΣ",
        "74057" => "ΡΕΘΥΜΝΗΣ",
        "74058" => "ΡΕΘΥΜΝΗΣ",
        "74059" => "ΡΕΘΥΜΝΗΣ",
        "74060" => "ΡΕΘΥΜΝΗΣ",
        "74061" => "ΡΕΘΥΜΝΗΣ",
        "74062" => "ΡΕΘΥΜΝΗΣ",
        "74100" => "ΡΕΘΥΜΝΗΣ",
        "81100" => "ΛΕΣΒΟΥ",
        "81101" => "ΛΕΣΒΟΥ",
        "81102" => "ΛΕΣΒΟΥ",
        "81103" => "ΛΕΣΒΟΥ",
        "81104" => "ΛΕΣΒΟΥ",
        "81105" => "ΛΕΣΒΟΥ",
        "81106" => "ΛΕΣΒΟΥ",
        "81107" => "ΛΕΣΒΟΥ",
        "81108" => "ΛΕΣΒΟΥ",
        "81109" => "ΛΕΣΒΟΥ",
        "81110" => "ΛΕΣΒΟΥ",
        "81111" => "ΛΕΣΒΟΥ",
        "81112" => "ΛΕΣΒΟΥ",
        "81113" => "ΛΕΣΒΟΥ",
        "81200" => "ΛΕΣΒΟΥ",
        "81300" => "ΛΕΣΒΟΥ",
        "81400" => "ΛΕΣΒΟΥ",
        "81401" => "ΛΕΣΒΟΥ",
        "81500" => "ΛΕΣΒΟΥ",
        "82100" => "ΧΙΟΥ",
        "82101" => "ΧΙΟΥ",
        "82102" => "ΧΙΟΥ",
        "82103" => "ΧΙΟΥ",
        "82104" => "ΧΙΟΥ",
        "82200" => "ΧΙΟΥ",
        "82300" => "ΧΙΟΥ",
        "83100" => "ΣΑΜΟΥ",
        "83101" => "ΣΑΜΟΥ",
        "83102" => "ΣΑΜΟΥ",
        "83103" => "ΣΑΜΟΥ",
        "83104" => "ΣΑΜΟΥ",
        "83200" => "ΣΑΜΟΥ",
        "83300" => "ΣΑΜΟΥ",
        "83301" => "ΣΑΜΟΥ",
        "83302" => "ΣΑΜΟΥ",
        "83400" => "ΣΑΜΟΥ",
        "8446" => "ΜΑΓΝΗΣΙΑΣ",
        "84001" => "ΚΥΚΛΑΔΩΝ",
        "84002" => "ΚΥΚΛΑΔΩΝ",
        "84003" => "ΚΥΚΛΑΔΩΝ",
        "84004" => "ΚΥΚΛΑΔΩΝ",
        "84005" => "ΚΥΚΛΑΔΩΝ",
        "84006" => "ΚΥΚΛΑΔΩΝ",
        "84007" => "ΚΥΚΛΑΔΩΝ",
        "84008" => "ΚΥΚΛΑΔΩΝ",
        "84009" => "ΚΥΚΛΑΔΩΝ",
        "84010" => "ΚΥΚΛΑΔΩΝ",
        "84011" => "ΚΥΚΛΑΔΩΝ",
        "84100" => "ΚΥΚΛΑΔΩΝ",
        "84200" => "ΚΥΚΛΑΔΩΝ",
        "84201" => "ΚΥΚΛΑΔΩΝ",
        "84300" => "ΚΥΚΛΑΔΩΝ",
        "84301" => "ΚΥΚΛΑΔΩΝ",
        "84302" => "ΚΥΚΛΑΔΩΝ",
        "84400" => "ΚΥΚΛΑΔΩΝ",
        "84401" => "ΚΥΚΛΑΔΩΝ",
        "84500" => "ΚΥΚΛΑΔΩΝ",
        "84501" => "ΚΥΚΛΑΔΩΝ",
        "84502" => "ΚΥΚΛΑΔΩΝ",
        "84600" => "ΚΥΚΛΑΔΩΝ",
        "84700" => "ΚΥΚΛΑΔΩΝ",
        "84701" => "ΚΥΚΛΑΔΩΝ",
        "84702" => "ΚΥΚΛΑΔΩΝ",
        "84703" => "ΚΥΚΛΑΔΩΝ",
        "84800" => "ΚΥΚΛΑΔΩΝ",
        "84801" => "ΚΥΚΛΑΔΩΝ",
        "85001" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85002" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85100" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85101" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85102" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85103" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85104" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85105" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85106" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85107" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85108" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85109" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85110" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85111" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85200" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85300" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85301" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85302" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85303" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85400" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85401" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85500" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85600" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85700" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85800" => "ΔΩΔΕΚΑΝΗΣΟΥ",
        "85900" => "ΔΩΔΕΚΑΝΗΣΟΥ"];
    if(array_key_exists($zip_code, $map_array)){
        return $map_array[$zip_code];
    }
}

function isInvoice($order, $m2e){
    if($order[$m2e->invoicecode] ==  $m2e->invoiceid){
        return true;
    }
    else{
        return false;
    }
}

function checkIfSpecialPriceAndCouponCoexist($order, $orderItem){
    $specialPrice = $orderItem['special_price'];
    $originalPrice = $orderItem['item_original_price'];
    $couponDiscount = $order['discount_amount'];

    if(floatval($couponDiscount) != 0 && (floatval($specialPrice) < floatval($originalPrice))){
        return true;
    }
    else{
        return false;
    }
}

function stripChars($str){
    $replaced = preg_replace('/[\x{0300}-\x{036f}]+/u', '', $str);
    return $replaced;
}

//Removes all special characters from greek letters
//Special characters bug the ERP of the customer
function removeGreekSpecialCharacters($str){
    $replace_chars = array(
        'ἀ' => 'α', 'ἁ' => 'α', 'ἄ' => 'α', 'ἅ' => 'α', 'ἂ' => 'α', 'ἃ' => 'α',
        'ἐ' => 'ε', 'ἑ' => 'ε', 'ἔ' => 'ε', 'ἕ' => 'ε',
        'ἠ' => 'η', 'ἡ' => 'η', 'ἤ' => 'η', 'ἥ' => 'η', 'ἢ' => 'η', 'ἣ' => 'η', 'ῆ' => 'η',
        'ἰ' => 'ι', 'ἱ' => 'ι', 'ἴ' => 'ι', 'ἵ' => 'ι', 'ἲ' => 'ι', 'ἳ' => 'ι', 'ϊ' => 'ι',
        'ὀ' => 'ο', 'ὁ' => 'ο', 'ὄ' => 'ο', 'ὅ' => 'ο',
        'ὑ' => 'υ', 'ὕ' => 'υ', 'ὗ' => 'υ', 'ὐ' => 'υ', 'ὒ' => 'υ', 'ὓ' => 'υ',
        'ὠ' => 'ω', 'ὡ' => 'ω', 'ὤ' => 'ω', 'ὥ' => 'ω', 'ὢ' => 'ω', 'ὣ' => 'ω',
        'Ἀ' => 'Α', 'Ἁ' => 'Α', 'Ἄ' => 'Α', 'Ἅ' => 'Α', 'Ἂ' => 'Α', 'Ἃ' => 'Α',
        'Ἐ' => 'Ε', 'Ἑ' => 'Ε', 'Ἔ' => 'Ε', 'Ἕ' => 'Ε',
        'Ἠ' => 'Η', 'Ἡ' => 'Η', 'Ἤ' => 'Η', 'Ἥ' => 'Η', 'Ἢ' => 'Η', 'Ἣ' => 'Η',
        'Ἰ' => 'Ι', 'Ἱ' => 'Ι', 'Ἴ' => 'Ι', 'Ἵ' => 'Ι', 'Ἲ' => 'Ι', 'Ἳ' => 'Ι',
        'Ὀ' => 'Ο', 'Ὁ' => 'Ο', 'Ὄ' => 'Ο', 'Ὅ' => 'Ο',
        'Ὑ' => 'Υ', 'Ὕ' => 'Υ', 'Ὗ' => 'Υ', 'ὐ' => 'Υ', 'ὒ' => 'Υ', 'ὓ' => 'Υ',
        'Ὠ' => 'Ω', 'Έ' => 'Ε', 'Ί' => 'Ι', 'Ό' => 'Ο', 'Ώ' => 'Ω', 'Ή' => 'Η', 'Ύ' => 'Υ', 'Ά' => 'Α',
        'Ἐ' => 'Ε', 'Ἀ' => 'Α', 'ΐ' => 'ι', 'ΰ' => 'υ'
    );

    $normalized_str = strtr($str, $replace_chars);

    return $normalized_str;
}

function checkRestItemsForBranchAvailability($order, $brCode, $stockDataArray){
    foreach($order['items'] as $key => $item){
        if($key == 0){
            continue;
        }
        $brQty = $stockDataArray[$item['item_sku']][$brCode];
        $qtyOrdered = $item['item_qty_ordered'];

        if($qtyOrdered <= $brQty){
            continue;
        }
        else{
            return false;
        }
    }
    return true;
}

function getSelectedBranchForOrder($order){
    global $s1rest;
    foreach($order['items'] as $item){
        $SKUs[] = $item['item_sku'];
    }

    $erpResponse = $s1rest->getProductQuantityFromBranches($SKUs, $order['shipping_method']);
    $stockDataArray = [];
    foreach($erpResponse as $branchData){
        $stockDataArray[$branchData['SKU']][$branchData['BR_CODE']] = $branchData['FREE_QTY'];
    }

    $firstItemSku = $order['items'][0]['item_sku'];
    $firstItemQtyOrdered = $order['items'][0]['item_qty_ordered'];
    $firstItem00Qty = $stockDataArray[$firstItemSku]['00'];

    if($firstItem00Qty >= $firstItemQtyOrdered){
        $is00Ok = checkRestItemsForBranchAvailability($order, '00' ,$stockDataArray);
    }
    else{
        $is00Ok = false;
    }

    if($is00Ok){
        return '00';
    }
    else{

        foreach($stockDataArray[$firstItemSku] as $brCode => $branchQty){
            if($branchQty >= $firstItemQtyOrdered){
                if(checkRestItemsForBranchAvailability($order, $brCode, $stockDataArray)){
                    return $brCode;
                }
            }
        }
        //Changing the following from branch to false
        //If it returns false the branch is 00 but will also send a bcc email in a different email.
        //We do this to understand if the 00 branch can issue the order, or no branch have the required stock to issue the order to 00 comes as default.
        return false;
    }
}

