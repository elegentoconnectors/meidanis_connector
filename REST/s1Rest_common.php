<?php

class s1rest {

    const GET_ITEMS_URL = "JS/SiteData.Items/getItems";
    const GET_DOCS_URL = "JS/SiteData.Items/getDocs";
    const GET_ITEMS_BALANCE_URL = "JS/SiteData.Items/getItemsBalance2";
    // const GET_CUSTOMERS_URL = "JS/SiteData.Items/getCustomers";
    const GET_CUSTOMERS_URL = "js/RDCJS/GetCustomers";
    const GET_ITEMS_PRICES_URL = "JS/SiteData.Items/getItemsPrices";
    //const GET_RDC_ITEMS_URL = "js/RDCJS/GetProducts";
    const GET_RDC_ITEMS_URL = "js/RDCJS/GetProductsNew";
    const GET_RDC_ITEMS_BALANCE_URL = "js/RDCJS/GetProductStock";
    const GET_RDC_ITEMS_BALANCE_URL_BY_PARAMS = "js/RDCJS/GetProductStockByParams";
    const GET_RDC_ITEMS_PHOTOS_BY_PARAMS = "js/RDCJS/GetPhotosByParam";
    const GET_RDC_SIMPLE_ITEMS_BALANCE_URL = "js/RDCJS/GetSimpleStock";
    const GET_RDC_OPTIONS_ITEMS_BALANCE_URL = "js/RDCJS/GetCombinationStock";
    const GET_RDC_OPTIONS_ITEMS_BALANCE_URL_BY_PARAMS = "js/RDCJS/GetCombinationStockByParam";
    const SET_RDC_ITEMS_UPDATE_SUCCESS = "js/RDCJS/UpdateProducts";
    const SET_RDC_STOCK_UPDATE_SUCCESS = "js/RDCJS/UpdateProductStock";
    const GET_RDC_ORDER_ID = "js/RDCJS/GetOrderId";
    const GET_RDC_ORDER_STATUS = "js/RDCJS/GetOrderStatus";
    const SET_RDC_ORDER_STATUS_UPDATE_SUCCESS = "js/RDCJS/UpdateOrderStatus";
    const GET_DATA = "/";
    const SET_DATA = "/";

    const SERVER_7_URL = 'https://connector7.elegento.com/meidanis';
    const SERVER_7_GET_URI = 'get.php';
    const SERVER_7_GET_ITEMS_URI = 'get-items.php';
    const SERVER_7_INSERT_URI = 'insert.php';
    const SERVER_7_UPDATE_URI = 'update.php';
    const SERVER_7_GET_SCRIPT_PASS = 'ZBn2RtnA%y$P';

    public $debugfile = "debug/DebugCommon.txt";
    public $errorlogfile = "errors/CommonErrors.log";
    public $sendto = "p.manolas@elegento.com";
    public $sendtoadmin = "p.manolas@elegento.com";
    public $sitename = "Connector App Elegento";
    public $mailsubject = " Connector Error";
    public $fileruntime = null;

    public $filestop = "stop";

    public $difftable = null;

    public $clientid = null;
    public $datatable = null;
    public $sqllink = null;
    public $runId = null;
    public $errors = null;
    public $latesterror = null;
    public $maxtimewait = "600"; //seconds
    public $blockingentities = null;
    public $retries = 2;
    public $appid = "1001";
    public $service_url = ''; //dev-matfashion for demo
    public $appname = '';
    public $erpuser = array("user"=>'', "pass"=>''); //@Rk9r25U9bS3 password for demo
    public $dbconf = array("host"=> "localhost" ,"user"=> "meidanisconn" ,"pass"=> "8TuyG_gzYTmN" ,"db"=> "meidanisconn");
    public $debug = false; //make this true to add json responses to log
    public $debugmode = false;

    public $entity = null;

    public $customerGroups = array(
        'CAB8BF95-CFF6-4147-9F41-815F0D4E06B5' => array(
            'magento_id' => '4',
            'name' => 'ΜΟΥΤΣΙΟΣ ΙΩΑΝ.'
        ),
        'BDF6155D-DAE2-4302-8962-B5BD39628D84' => array(
            'magento_id' => '5',
            'name' => 'ΜΕΤ/ΣΗ 2'
        ),
        '3F0A4CF7-FE71-4CA2-BDDD-E4C67603E3A5' => array(
            'magento_id' => '6',
            'name' => 'ΗΛ. ΡΕΘΥΜΝΟΥ'
        ),
        '58AD49A4-4150-4DC7-B72C-EF4FD6755183' => array(
            'magento_id' => '7',
            'name' => 'ΚΑΤΩΠΟΔΗ ΥΙΟΙ'
        ),
        'A3181D8A-23C6-4EFE-9FB2-31977CB39836' => array(
            'magento_id' => '8',
            'name' => 'ZARA'
        ),
        'E759005C-EE28-4911-AC22-7F4C8A76DAF1' => array(
            'magento_id' => '9',
            'name' => 'ΓΙΑΝΝΑΚΑΚΗΣ'
        ),
        'A2EDB700-A479-48D4-9DB7-0DB67BEF77F8' => array(
            'magento_id' => '10',
            'name' => 'MET 4 NEW'
        ),
        '11D0354C-2FB9-4568-9AF2-3C3535B1CD38' => array(
            'magento_id' => '11',
            'name' => '3 ΖΩΝΗ Α'
        ),
        'EE06CF28-C8ED-4492-B013-C7674653B825' => array(
            'magento_id' => '12',
            'name' => 'ΤΑΜΠΙ 1'
        ),
        '6D2710B8-47C7-4B38-826C-4A3B0AD5E5E5' => array(
            'magento_id' => '13',
            'name' => 'B2B 2 NEW'
        ),
        '81933A71-FE12-4ACE-81C2-2933E85FEA82' => array(
            'magento_id' => '14',
            'name' => 'ΕΛΕΥΘΕΡΙΑΔΗΣ ΟΕ'
        ),
        '261DE783-17CA-4A58-8C88-F953C64B7B64' => array(
            'magento_id' => '15',
            'name' => 'ΦΛΩΡΟΣ ΙΩΑΝ.'
        ),
        '6B5860DF-E144-47F9-BD20-403D78AFD7E9' => array(
            'magento_id' => '16',
            'name' => 'ΔΟΡΟΒΙΤΣΑΣ'
        ),
        '4D03D954-33BB-4035-9ACA-C4495525E3BA' => array(
            'magento_id' => '17',
            'name' => 'ΚΑΡΑΜΠΟΤΑΣ'
        ),
        '21390105-32B5-452B-82E9-AC0B698781F5' => array(
            'magento_id' => '18',
            'name' => 'ΜΕΤ/ΣΗ 4.1'
        ),
        '225BCDB2-8A2B-4371-91D1-75C4AA9B358F' => array(
            'magento_id' => '19',
            'name' => 'ΠΑΤΡΑ Α'
        ),
        '3BBD4862-FE41-42E4-865E-AFA1DDC7846C' => array(
            'magento_id' => '20',
            'name' => 'ΚΑΛΛΙΘΕΑ 3'
        ),
        '0A2A08B8-0197-4183-96D7-79F5A2CFCDC4' => array(
            'magento_id' => '21',
            'name' => 'MET 2 NEW'
        ),
        '8F5B2A0D-5630-49B9-B719-16E66050459F' => array(
            'magento_id' => '22',
            'name' => 'ΠΑΝΤΟΚΡΑΤΟΡΑ'
        ),
        '0529CDE1-395E-495A-A439-B0A9C676E48E' => array(
            'magento_id' => '23',
            'name' => 'ΙΛΙΟΝ Β'
        ),
        '0611A26D-B5EC-4E4C-A5A2-BDB386FA7681' => array(
            'magento_id' => '24',
            'name' => 'ΒΑΓΓΟΣ'
        ),
        '56A6EA66-A710-4EBC-9EB9-EDD8366128C6' => array(
            'magento_id' => '25',
            'name' => 'ΔΗΜ-ΚΩΣΤ-ΤΖΙΒ'
        ),
        '5B797F90-7B76-472F-AB4B-6F5172BC02CE' => array(
            'magento_id' => '26',
            'name' => 'ΤΑΜΠΙ 0'
        ),
        'A5F46AFC-0E72-4D06-9BC0-A43205C98B11' => array(
            'magento_id' => '27',
            'name' => 'ΚΑΛΕΝΤΕΡΙΔΗΣ'
        ),
        'D81925E3-4947-4C24-B44C-8A4BF25C1AFA' => array(
            'magento_id' => '28',
            'name' => 'ΚΑΤΣΟΥΛΗΣ'
        ),
        'AE01CD68-6DB2-4F33-BEB8-563015BBE662' => array(
            'magento_id' => '29',
            'name' => 'B2B 1'
        ),
        'EEEFF779-668E-43F1-BA10-983D39EE6FA4' => array(
            'magento_id' => '30',
            'name' => 'GARCIA'
        ),
        'EA812978-47AB-4493-A87E-29BF05F3559E' => array(
            'magento_id' => '31',
            'name' => 'ΠΡΟΣΩΠΙΚΟ ΜΕΙΔΑΝΗ'
        ),
        '1A59C3B6-D621-4109-8F97-7AB48389C4E3' => array(
            'magento_id' => '32',
            'name' => 'LUMIN ART'
        ),
        'F1376602-ED79-4665-A7E3-36A598304F2E' => array(
            'magento_id' => '33',
            'name' => 'SALES 2'
        ),
        '8A87BEB6-6EDE-4135-A909-EB7CB38D2B8C' => array(
            'magento_id' => '34',
            'name' => 'WEB'
        ),
        '5C23698F-DEEF-4D79-9906-2F8956260D04' => array(
            'magento_id' => '35',
            'name' => 'ΜΑΥΡΗΣ'
        ),
        'A84E145E-F304-43C0-BF68-EA443195A069' => array(
            'magento_id' => '36',
            'name' => 'ΡΟΥΣΣΟΥ'
        ),
        'D7CC7316-E36E-439A-A19A-CE098FE81989' => array(
            'magento_id' => '37',
            'name' => 'ΓΑΚΗΣ'
        ),
        'F4953CEF-71A2-4827-B904-4AC2AA07ECD1' => array(
            'magento_id' => '38',
            'name' => 'ΓΕΡΟΠΟΥΛΟΣ'
        ),
        '6C4799C6-D06F-4E72-8C43-007348835C7E' => array(
            'magento_id' => '39',
            'name' => 'B2B 2'
        ),
        '5DC7C14D-65B7-4CA0-BD75-A5A214ADD302' => array(
            'magento_id' => '40',
            'name' => '2 ΖΩΝΗ Β'
        ),
        '0AB086CA-5CDA-40DB-93A9-D287B85F80D9' => array(
            'magento_id' => '41',
            'name' => 'IMPOR ELEC'
        ),
        '50611684-4183-47FF-A73A-48A0B259C2B3' => array(
            'magento_id' => '42',
            'name' => 'ΧΑΡΜΠΗΣ'
        ),
        '205FA8F0-34ED-4654-A8DD-C0BCD10EC5F2' => array(
            'magento_id' => '43',
            'name' => 'ΣΤΑΜΑΤΗΣ'
        ),
        '772936A7-90A4-4335-926E-290558CB7136' => array(
            'magento_id' => '44',
            'name' => 'ΚΡΗΤΗ 1'
        ),
        'C6C0236C-7FF2-4C75-AEDC-EFDB55DB82F0' => array(
            'magento_id' => '45',
            'name' => 'ΣΥΝ/ΜΟΣ ΗΜΑΘΙΑΣ'
        ),
        '5C5A472F-7F9A-4B4F-801D-A9371F3FAB33' => array(
            'magento_id' => '46',
            'name' => 'ΙΛΙΟΝ ΧΟΝΔ.'
        ),
        'D546B8F9-EC16-48A9-B74E-423CE1038AA2' => array(
            'magento_id' => '47',
            'name' => '1 ΛΙΑΝΙΚΗ'
        ),
        'B0110312-A129-46FC-991B-94B6F7759C25' => array(
            'magento_id' => '48',
            'name' => 'ΙΛΙΟΝ Β2Β 2'
        ),
        '34237A85-AD44-4BB6-95A9-4215F28C6DEA' => array(
            'magento_id' => '49',
            'name' => 'ΤΑΜΠΙ 3'
        ),
        '2E6E21DD-01FF-417B-BB50-29230205645D' => array(
            'magento_id' => '50',
            'name' => 'ΣΤΑΜΑΤΟΠΟΥΛΟΣ'
        ),
        'C8CBE255-4F27-48E3-9D26-B8D7DE8DACA5' => array(
            'magento_id' => '51',
            'name' => 'ΚΑΛΛΟΝΙΑΤΗΣ'
        ),
        '836ED3F5-316F-400F-A2BD-2DBBDBC57BA0' => array(
            'magento_id' => '52',
            'name' => 'ΚΟΛΛΙΑΣ ΔΗΜ.'
        ),
        '0D91DDBC-FCC6-4A58-8496-CA30DB3B1A5D' => array(
            'magento_id' => '53',
            'name' => 'ΜΑΓΓΛΑΡΗΣ'
        ),
        '474653F1-24AD-4608-9B4D-2FBA7DA733EB' => array(
            'magento_id' => '54',
            'name' => 'B2B 5'
        ),
        '614095CC-C3F7-4F8D-B77F-1C037C7EF84B' => array(
            'magento_id' => '55',
            'name' => 'ΜΟΣΧΑΣ'
        ),
        'AD40B461-CE4C-40A7-BE7D-34829A09906E' => array(
            'magento_id' => '56',
            'name' => 'ΑΒΡΑΜΟΠΟΥΛΟΣ'
        ),
        'EC60DA1E-5B6B-4C50-9206-8CFA6506D5FD' => array(
            'magento_id' => '57',
            'name' => 'ΚΡΗΤΗ'
        ),
        'A6F31579-A69F-47F3-B77F-E40AE0D93DEB' => array(
            'magento_id' => '58',
            'name' => 'ΜΕΤ/ΣΗ 1'
        ),
        '84E0B049-9DA3-45E1-90C5-54CD3701C813' => array(
            'magento_id' => '59',
            'name' => 'ΚΑΛΛΙΘΕΑ 5'
        ),
        '09070C90-DB51-420D-B539-84616A8CFF7D' => array(
            'magento_id' => '60',
            'name' => 'ΒΟΛΟΣ VIP'
        ),
        '8DE477BA-4D48-4C95-A699-6CBAC0D216E4' => array(
            'magento_id' => '61',
            'name' => 'NEGOWATT'
        ),
        '85726FDF-4726-4822-A977-B109C30F8AC6' => array(
            'magento_id' => '62',
            'name' => 'ΜΟΥΤΣΟΣ'
        ),
        '37DE2E04-EFEA-47E6-AFEB-AEEAE9A1EF32' => array(
            'magento_id' => '63',
            'name' => 'ΒΟΓΙΑΤΖΗΣ'
        ),
        'C4382D37-C764-4D69-BDD4-C54EC800482F' => array(
            'magento_id' => '64',
            'name' => 'ΗΛΕΚΤΡ/ΚΗ ΑΘ.'
        ),
        'BC863F34-C768-4924-8181-200EB36ED06B' => array(
            'magento_id' => '65',
            'name' => 'ΒΟΛΕΛΗΣ'
        ),
        'AEE83184-492A-4781-A157-4DAB6E659133' => array(
            'magento_id' => '66',
            'name' => 'MET CLUB NEW'
        ),
        'FC4C443A-8B47-48E3-8FE6-51FCD18D1913' => array(
            'magento_id' => '67',
            'name' => 'ΙΛΙΟΝ Β2Β 1'
        ),
        '14535E69-F19E-417A-A92B-9EB253F23E7D' => array(
            'magento_id' => '68',
            'name' => 'ΚΟΡΚΟΤΖΗΛΑΣ'
        ),
        'D12B339C-3817-4B7B-851C-9DF51C3EF413' => array(
            'magento_id' => '69',
            'name' => 'ΤΥΡΟΒΟΛΑΣ'
        ),
        '7301E8A7-5D30-4D75-AA2D-A03308721521' => array(
            'magento_id' => '70',
            'name' => 'ΓΚΙΝΗΣ'
        ),
        '09B3CB29-9903-42AF-B3AE-40D086C02FCA' => array(
            'magento_id' => '71',
            'name' => '4 ΜΕΤ/ΣΗ-Β2Β'
        ),
        '8589EF01-1154-45CE-844A-AFB3C02CD8B9' => array(
            'magento_id' => '72',
            'name' => 'ΧΡΗΣΤΙΔΗΣ'
        ),
        '80A07219-D327-440B-A6C8-EBAE1EC71800' => array(
            'magento_id' => '73',
            'name' => 'ΠΙΝΑΚΕΣ'
        ),
        'E96EF579-8F48-4CA9-852E-958C84FF8E14' => array(
            'magento_id' => '74',
            'name' => 'ΠΡΙΟΒΟΛΟΥ'
        ),
        '9B3F691F-2836-4E71-ABA9-8A39B8F3A5EA' => array(
            'magento_id' => '75',
            'name' => 'ΜΕΤ/ΣΗ 3'
        ),
        'ACF63778-BD6E-4AAE-8F1C-56A3E9F35567' => array(
            'magento_id' => '76',
            'name' => 'SALES 1'
        ),
        '0575B354-D829-4CD8-95A5-29B1ABBFD7FE' => array(
            'magento_id' => '77',
            'name' => 'MEGAWATT ΝΑΖΟΣ'
        ),
        '6C95BB3A-969D-4BEB-BA51-FEB24F482FDC' => array(
            'magento_id' => '78',
            'name' => 'ΜΑΡΚΑΝΤΩΝΑΤΟΣ'
        ),
        '9A87F7D4-6B15-4B98-9053-FF85B9E361AB' => array(
            'magento_id' => '79',
            'name' => 'ΣΕΗΛ ΧΑΝΙΩΝ'
        ),
        '1BD83B77-E881-4C44-8890-560039B9B4F9' => array(
            'magento_id' => '80',
            'name' => 'ΜΗΧΑΝΙΚΗ Ι&Κ'
        ),
        '041A17ED-96AA-4C5F-979C-10BF3F264D6C' => array(
            'magento_id' => '81',
            'name' => 'ΚΟΥΜΠΑΤΗΣ'
        ),
        '039C7A96-EDE4-4D97-8CFB-4550AC017D65' => array(
            'magento_id' => '82',
            'name' => 'ΠΩΓΩΝΙΔΗΣ'
        ),
        '45BCED7C-A972-441D-BFDB-15571EBE0FAA' => array(
            'magento_id' => '83',
            'name' => 'ΣΥΝΠΕ ΖΑΚΥΝΘΟΥ'
        ),
        '6C896EF6-9146-4960-9363-1013EE6DF81A' => array(
            'magento_id' => '84',
            'name' => 'ΛΑΤ. ΤΥΡΝΑΒΟΥ'
        ),
        'F271FBB4-E6A5-4FB4-BA71-034D85E30186' => array(
            'magento_id' => '85',
            'name' => 'ΒΟΛΟΣ ΗΛ/ΓΟΣ'
        ),
        '4C4C1132-E519-4E30-869F-EBDC6A56D24E' => array(
            'magento_id' => '86',
            'name' => 'ΧΑΛΚΙΑ-ΠΑΤΑΓΙΑ'
        ),
        '5FB943C7-E360-6D75-7532-016DF8856951' => array(
            'magento_id' => '87',
            'name' => 'B2B 4 CLUB'
        ),
        'AC7BBC74-AADD-4C41-BD18-D0621CC4768E' => array(
            'magento_id' => '88',
            'name' => 'ΒΙΟΜΗΧ.'
        ),
        'E9DFBEA3-66FB-4F4B-BC61-D2FC7296B6D4' => array(
            'magento_id' => '89',
            'name' => 'ΠΑΝΤΟΥΛΑΣ'
        ),
        'F381966D-8065-45DE-A15E-F747944B5FE1' => array(
            'magento_id' => '90',
            'name' => 'B2B 3'
        ),
        '9B21CA0D-2E23-4DBD-8D86-064E914DC7C3' => array(
            'magento_id' => '91',
            'name' => 'B2B 3 NEW'
        ),
        '2194A771-794D-4F6D-ABBB-BAB5B4B70249' => array(
            'magento_id' => '92',
            'name' => 'MET 3 NEW'
        ),
        '2CE85D73-D49F-49DC-A8DF-59624CA85454' => array(
            'magento_id' => '93',
            'name' => 'ΧΑΛΚΙΑΔΑΚΗΣ'
        ),
        '22F1EE3E-BC20-4150-A6C6-8C598AB3F573' => array(
            'magento_id' => '94',
            'name' => 'ΠΕΛΑΡΓΟΣ/ΡΟΜΒΟΣ'
        ),
        '42C2E6AA-D3F4-413D-9084-F8D73C6F0367' => array(
            'magento_id' => '95',
            'name' => 'ΝΕΟΤΕΚ'
        ),
        '63DA876E-422D-481C-BD31-F664E5957941' => array(
            'magento_id' => '96',
            'name' => 'ΚΡΗΤΗ 2'
        ),
        '8AFAD0BA-751B-428A-A68B-CB9BCC88223D' => array(
            'magento_id' => '97',
            'name' => 'ΜΑΝΤΗ'
        ),
        'C0D4213A-274B-4D6A-897F-610823E53909' => array(
            'magento_id' => '98',
            'name' => 'ΙΛΙΟΝ Α'
        ),
        '22309CC6-868E-44E6-A107-29A2317ABB84' => array(
            'magento_id' => '99',
            'name' => 'ΠΑΤΡΑ Β'
        ),
        'CB9ECB29-6559-4001-B733-F78F48D5BFBA' => array(
            'magento_id' => '100',
            'name' => 'ΗΛΕΚΤΡΟΤΕΧΝ.ΗΠΕΙΡΟΥ'
        ),
        '0C823437-28C4-4C6D-8A3B-3F5772F61339' => array(
            'magento_id' => '101',
            'name' => 'ΠΑΝΑΓΟΠΟΥΛΟΣ'
        ),
        '328B0CCE-5367-41EE-98CA-6ADD3C964A36' => array(
            'magento_id' => '102',
            'name' => 'DEMCO GROUP'
        ),
        '82C5EC26-D6BD-44A3-9CF9-F9049A6E6C00' => array(
            'magento_id' => '103',
            'name' => 'ΝΤΙΝΟΥΔΗΣ'
        ),
        '02362698-A56A-4961-BD74-D5DDA708CA75' => array(
            'magento_id' => '104',
            'name' => 'ΗΛΕΚΤΡΟΡΑΜΑ'
        ),
        '0ED616EE-0E74-9DA9-5AF7-018607B0A1FB' => array(
            'magento_id' => '107',
            'name' => 'ELECTRIC SYSTEMS'
        ),
        '3794C922-D72F-331C-3030-0177FC62345F' => array(
            'magento_id' => '108',
            'name' => 'ΝΙΚΟΣ TEST'
        ),
        'CC88C20F-E325-724D-D5A8-017ED8B7AF5A' => array(
            'magento_id' => '109',
            'name' => 'ΚΟΡΙΝΘΟΣ 1'
        ),
        '0EDC78CC-C820-1B70-E71D-017F209D0426' => array(
            'magento_id' => '110',
            'name' => 'ΤΖΙΒΕΛΕΚΑΣ'
        ),
        '3CCA0989-1790-8F4F-1639-0181DDDD2C3D' => array(
            'magento_id' => '111',
            'name' => 'GERO4 ΕΜΠΟΡΟΙ'
        ),
        '1072445C-9742-C4F5-D2B0-0181DDDCF4D3' => array(
            'magento_id' => '112',
            'name' => 'GERO3 ΒΙΟΜΗΧΑΝΙΑ'
        ),
        '918295A0-EFB0-076A-D57F-017E9F6F499C' => array(
            'magento_id' => '113',
            'name' => 'ΣΤΟΥΠΑΣ'
        ),
        '4805B270-3B8B-58CD-5CF5-0183EF5A3664' => array(
            'magento_id' => '114',
            'name' => 'ΣΑΡΡΗΣ'
        ),
        '86D20225-7E63-3316-AB8F-0181DDDC8577' => array(
            'magento_id' => '115',
            'name' => 'GERO2 ΕΠΑΓΓΕΛΜΑΤΙΕΣ'
        ),
        '3B79AB7C-B2E8-C786-9ABF-01836E3F8FE0' => array(
            'magento_id' => '116',
            'name' => 'Κ ΒΕΡΥΚΙΟΣ-Α ΜΠΑΛΤΣΑΣ ΟΕ'
        ),
        '7E232283-D41A-2554-73CF-01752C4D461F' => array(
            'magento_id' => '117',
            'name' => 'ΔΗΜΗΤΡΙΑΔΗΣ'
        ),
        'DE037036-086A-444E-BDEC-0181DDDC312E' => array(
            'magento_id' => '118',
            'name' => 'GERO1 ΛΙΑΝΙΚΗ'
        ),
        '6403B356-E9FD-4642-AB99-017ED8B7E01E' => array(
            'magento_id' => '119',
            'name' => 'ΚΟΡΙΝΘΟΣ 2'
        ),
        'C311C29D-73A9-D4E1-6C10-01899C5FE6D1' => array(
            'magento_id' => '120',
            'name' => '6 TEST'
        ),
        '6A9834AA-9F8E-8813-5675-01899C60581E' => array(
            'magento_id' => '122',
            'name' => '9 TEST'
        ),
        'DA73869D-5E4F-0351-18F2-01899C601565' => array(
            'magento_id' => '124',
            'name' => '7 TEST'
        ),
        '2687C72F-DBBC-DE62-2B62-018A88724E53' => array(
            'magento_id' => '125',
            'name' => 'ΔΕ 2'
        ),
        '7ED83B89-6666-45BD-083D-01894F36E3FA' => array(
            'magento_id' => '127',
            'name' => 'B2B 1 NEW'
        ),
        'ADCB3D5D-8B86-276E-C6D8-01894F37156C' => array(
            'magento_id' => '128',
            'name' => 'MET 1 NEW'
        ),
        'B9FE3836-F7BB-7FD9-4315-01899C604159' => array(
            'magento_id' => '129',
            'name' => '8 TEST'
        ),
        'E0554EB0-F745-F168-0F3E-018A8870CE13' => array(
            'magento_id' => '130',
            'name' => 'ΔΕ 1'
        ),
        '626D811C-2A9D-984B-9EC9-018A8872A32A' => array(
            'magento_id' => '133',
            'name' => 'ΔΕ 3'
        ),
        '04956F26-8648-AE57-86E3-01899C5F5B74' => array(
            'magento_id' => '134',
            'name' => '5 TEST'
        ),
        'CD1BD07D-0E2F-5E0B-F29B-018DC10BCCBE' => array(
            'magento_id' => '137',
            'name' => 'B2B E-COMMERCE'
        )
    );

    public $attributes = [
        'ELG_NAME' => [
            'magento_attribute_code' => 'name',
            'type' => 'text'
        ],
        'ELG_SKU' => [
            'magento_attribute_code' => 'sku',
            'type' => 'text'
        ],
        'Barcode' => [
            'magento_attribute_code' => 'barcode',
            'type' => 'text'
        ],
        'RETAIL_PRICE' => [
            'magento_attribute_code' => 'price',
            'type' => 'price'
        ],
        'FINAL_RETAIL' => [
            'magento_attribute_code' => 'final_retail',
            'type' => 'price'
        ],
        'ELG_ITEM_PACKAGE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'package',
            'type' => 'text'
        ],
        'MEASURMENT_UNIT' => [
            'magento_attribute_code' => '',
            'fake_code' => 'measurement',
            'type' => 'text'
        ],
        'X001' => [
            'magento_attribute_code' => 'manufacturer',
            'type' => 'dropdown'
        ],
        'X002' => [
            'magento_attribute_code' => 'eidos',
            'type' => 'dropdown'
        ],
        'X003' => [
            'magento_attribute_code' => 'leitourgia',
            'type' => 'dropdown'
        ],
        'X004' => [
            'magento_attribute_code' => 'typos',
            'type' => 'dropdown'
        ],
        'X005' => [
            'magento_attribute_code' => 'fcolor',
            'type' => 'dropdown'
        ],
        'X006' => [
            'magento_attribute_code' => 'xrhsh',
            'type' => 'dropdown'
        ],
        'X008' => [
            'magento_attribute_code' => 'oikogeneia',
            'type' => 'dropdown'
        ],
        'X010' => [
            'magento_attribute_code' => 'manufacturer_color',
            'type' => 'text'
        ],
        'X011' => [
            'magento_attribute_code' => 'yliko_kataskeyhs',
            'type' => 'dropdown'
        ],
        'X012' => [
            'magento_attribute_code' => 'technologia',
            'type' => 'dropdown'
        ],
        'X013' => [
            'magento_attribute_code' => 'topothethsh',
            'type' => 'dropdown'
        ],
        'X014' => [
            'magento_attribute_code' => 'typos_mixanismou',
            'type' => 'dropdown'
        ],
        'X016' => [
            'magento_attribute_code' => 'onomastiki_entasi',
            'type' => 'price'
        ],
        'X021' => [
            'magento_attribute_code' => 'ikanothta_diakophs',
            'type' => 'dropdown'
        ],
        'X022' => [
            'magento_attribute_code' => 'eidos_kataskeuasti',
            'type' => 'dropdown'
        ],
        'X025' => [
            'magento_attribute_code' => 'tasi_leitourgias',
            'type' => 'dropdown'
        ],
        'X026' => [
            'magento_attribute_code' => 'onomastikh_tash',
            'type' => 'dropdown'
        ],
        'X027' => [
            'magento_attribute_code' => 'euros_tasis',
            'type' => 'text'
        ],
        'X029' => [
            'magento_attribute_code' => 'typos_tashs',
            'type' => 'dropdown'
        ],
        'X032' => [
            'magento_attribute_code' => 'onomastiki_isxis',
            'type' => 'price'
        ],
        'X033' => [
            'magento_attribute_code' => 'onomastiki_isxis_watt_m',
            'type' => 'price'
        ],
        'X034' => [
            'magento_attribute_code' => 'megisth_epitrepomenh_isxys',
            'type' => 'dropdown'
        ],
        'X036' => [
            'magento_attribute_code' => 'synolikh_isxys',
            'type' => 'dropdown'
        ],
        'X043' => [
            'magento_attribute_code' => 'poloi',
            'type' => 'dropdown'
        ],
        'X044' => [
            'magento_attribute_code' => 'faseis',
            'type' => 'dropdown'
        ],
        'X045' => [
            'magento_attribute_code' => 'prostasia_ypertashs',
            'type' => 'dropdown'
        ],
        'X047' => [
            'magento_attribute_code' => 'reuma_diarrohs',
            'type' => 'dropdown'
        ],
        'X048' => [
            'magento_attribute_code' => 'megisto_vraxykyklwma',
            'type' => 'dropdown'
        ],
        'X050' => [
            'magento_attribute_code' => 'isxys_metasximatisti',
            'type' => 'price'
        ],
        'X053' => [
            'magento_attribute_code' => 'rythmish_xronou',
            'type' => 'dropdown'
        ],
        'X058' => [
            'magento_attribute_code' => 'boithitikes_epafes',
            'type' => 'dropdown'
        ],
        'X059' => [
            'magento_attribute_code' => 'kampylh',
            'type' => 'dropdown'
        ],
        'X060' => [
            'magento_attribute_code' => 'gwnia_metagwghs',
            'type' => 'dropdown'
        ],
        'X066' => [
            'magento_attribute_code' => 'diaforikh_prostasia',
            'type' => 'dropdown'
        ],
        'X067' => [
            'magento_attribute_code' => 'endeiktiko',
            'type' => 'dropdown'
        ],
        'X070' => [
            'magento_attribute_code' => 'epafes',
            'type' => 'dropdown'
        ],
        'X071' => [
            'magento_attribute_code' => 'epektasimothta',
            'type' => 'dropdown'
        ],
        'X073' => [
            'magento_attribute_code' => 'euaisthisia',
            'type' => 'dropdown'
        ],
        'X075' => [
            'magento_attribute_code' => 'autonomia',
            'type' => 'dropdown'
        ],
        'X076' => [
            'magento_attribute_code' => 'emveleia',
            'type' => 'dropdown'
        ],
        'X077' => [
            'magento_attribute_code' => 'theseis',
            'type' => 'dropdown'
        ],
        'X078' => [
            'magento_attribute_code' => 'theseis_ana_seira',
            'type' => 'dropdown'
        ],
        'X079' => [
            'magento_attribute_code' => 'platos_se_stoixeia',
            'type' => 'dropdown'
        ],
        'X081' => [
            'magento_attribute_code' => 'seires',
            'type' => 'price'
        ],
        'X082' => [
            'magento_attribute_code' => 'vash_topothethshs',
            'type' => 'text'
        ],
        'X083' => [
            'magento_attribute_code' => 'sthles',
            'type' => 'price'
        ],
        'X084' => [
            'magento_attribute_code' => 'grammes',
            'type' => 'price'
        ],
        'X086' => [
            'magento_attribute_code' => 'stoixeia',
            'type' => 'text'
        ],
        'X090' => [
            'magento_attribute_code' => 'kleidaria',
            'type' => 'text'
        ],
        'X093' => [
            'magento_attribute_code' => 'porta',
            'type' => 'dropdown'
        ],
        'X094' => [
            'magento_attribute_code' => 'typos_portas',
            'type' => 'dropdown'
        ],
        'X096' => [
            'magento_attribute_code' => 'diastaseis',
            'type' => 'dropdown'
        ],
        'X097' => [
            'magento_attribute_code' => 'vathos',
            'type' => 'dropdown'
        ],
        'X098' => [
            'magento_attribute_code' => 'c_weight',
            'type' => 'dropdown'
        ],
        'X100' => [
            'magento_attribute_code' => 'mhkos',
            'type' => 'price'
        ],
        'X101' => [
            'magento_attribute_code' => 'mhkos_kalwdiou',
            'type' => 'dropdown'
        ],
        'X105' => [
            'magento_attribute_code' => 'paxos',
            'type' => 'dropdown'
        ],
        'X106' => [
            'magento_attribute_code' => 'width',
            'type' => 'price'
        ],
        'X107' => [
            'magento_attribute_code' => 'ypsos',
            'type' => 'price'
        ],
        'X109' => [
            'magento_attribute_code' => 'diameter',
            'type' => 'price'
        ],
        'X111' => [
            'magento_attribute_code' => 'ekswterikh_diametros',
            'type' => 'text'
        ],
        'X112' => [
            'magento_attribute_code' => 'eswterikh_diametros',
            'type' => 'text'
        ],
        'X113' => [
            'magento_attribute_code' => 'diametros_ophs_topothethshs',
            'type' => 'dropdown'
        ],
        'X114' => [
            'magento_attribute_code' => 'diatomh',
            'type' => 'dropdown'
        ],
        'X116' => [
            'magento_attribute_code' => 'yliko_agwgou',
            'type' => 'dropdown'
        ],
        'X118' => [
            'magento_attribute_code' => 'typos_monwshs',
            'type' => 'dropdown'
        ],
        'X119' => [
            'magento_attribute_code' => 'ekswterikos_mandyas',
            'type' => 'dropdown'
        ],
        'X122' => [
            'magento_attribute_code' => 'typos_agwgou',
            'type' => 'dropdown'
        ],
        'X123' => [
            'magento_attribute_code' => 'xrwma_kalwdiou',
            'type' => 'dropdown'
        ],
        'X126' => [
            'magento_attribute_code' => 'arithmos_agwgwn_kalwdiou',
            'type' => 'dropdown'
        ],
        'X128' => [
            'magento_attribute_code' => 'kalwdio',
            'type' => 'text'
        ],
        'X129' => [
            'magento_attribute_code' => 'kalwdiwsi',
            'type' => 'text'
        ],
        'X130' => [
            'magento_attribute_code' => 'karouli',
            'type' => 'dropdown'
        ],
        'X131' => [
            'magento_attribute_code' => 'kathgoria_kalwdiwn',
            'type' => 'dropdown'
        ],
        'X134' => [
            'magento_attribute_code' => 'typos_thermanshs',
            'type' => 'dropdown'
        ],
        'X135' => [
            'magento_attribute_code' => 'typos_lampthrwn_thermansis',
            'type' => 'text'
        ],
        'X136' => [
            'magento_attribute_code' => 'klimakes_thermanshs',
            'type' => 'price'
        ],
        'X141' => [
            'magento_attribute_code' => 'me_boiler',
            'type' => 'dropdown'
        ],
        'X142' => [
            'magento_attribute_code' => 'me_ionisth',
            'type' => 'dropdown'
        ],
        'X143' => [
            'magento_attribute_code' => 'kalypsh_xwrou',
            'type' => 'dropdown'
        ],
        'X144' => [
            'magento_attribute_code' => 'litra',
            'type' => 'dropdown'
        ],
        'X145' => [
            'magento_attribute_code' => 'thlexeirismos',
            'type' => 'dropdown'
        ],
        'X146' => [
            'magento_attribute_code' => 'tilecheiristirio',
            'type' => 'dropdown'
        ],
        'X147' => [
            'magento_attribute_code' => 'programma',
            'type' => 'dropdown'
        ],
        'X149' => [
            'magento_attribute_code' => 'arithmos_pterygiwn',
            'type' => 'dropdown'
        ],
        'X150' => [
            'magento_attribute_code' => 'wifi',
            'type' => 'dropdown'
        ],
        'X152' => [
            'magento_attribute_code' => 'taxythta_diktyou',
            'type' => 'dropdown'
        ],
        'X154' => [
            'magento_attribute_code' => 'thyres',
            'type' => 'dropdown'
        ],
        'X157' => [
            'magento_attribute_code' => 'katagrafh',
            'type' => 'dropdown'
        ],
        'X158' => [
            'magento_attribute_code' => 'kit',
            'type' => 'text'
        ],
        'X159' => [
            'magento_attribute_code' => 'zwnes',
            'type' => 'dropdown'
        ],
        'X160' => [
            'magento_attribute_code' => 'megala_plhktra',
            'type' => 'dropdown'
        ],
        'X163' => [
            'magento_attribute_code' => 'thlefwnhths',
            'type' => 'dropdown'
        ],
        'X166' => [
            'magento_attribute_code' => 'gwnia_anyxneushs',
            'type' => 'dropdown'
        ],
        'X167' => [
            'magento_attribute_code' => 'beam_angle',
            'type' => 'dropdown'
        ],
        'X171' => [
            'magento_attribute_code' => 'othoni',
            'type' => 'dropdown'
        ],
        'X173' => [
            'magento_attribute_code' => 'arithmos_diamerismatwn',
            'type' => 'price'
        ],
        'X175' => [
            'magento_attribute_code' => 'yliko_kataskeyis_mpoytonieras',
            'type' => 'text'
        ],
        'X176' => [
            'magento_attribute_code' => 'topothethsh_mpoytonieras',
            'type' => 'dropdown'
        ],
        'X177' => [
            'magento_attribute_code' => 'fwtizomenh_othonh',
            'type' => 'text'
        ],
        'X180' => [
            'magento_attribute_code' => 'xronodiakopths',
            'type' => 'dropdown'
        ],
        'X181' => [
            'magento_attribute_code' => 'trofodosia',
            'type' => 'dropdown'
        ],
        'X182' => [
            'magento_attribute_code' => 'xronoprogramma',
            'type' => 'dropdown'
        ],
        'X183' => [
            'magento_attribute_code' => 'elaxistos_xronos_programm',
            'type' => 'dropdown'
        ],
        'X190' => [
            'magento_attribute_code' => 'metatroph_se',
            'type' => 'dropdown'
        ],
        'X193' => [
            'magento_attribute_code' => 'typos_lampthra',
            'type' => 'dropdown'
        ],
        'X194' => [
            'magento_attribute_code' => 'kalykas',
            'type' => 'dropdown'
        ],
        'X196' => [
            'magento_attribute_code' => 'arithmos_lampthrwn',
            'type' => 'dropdown'
        ],
        'X200' => [
            'magento_attribute_code' => 'xrwma_fwtos',
            'type' => 'dropdown'
        ],
        'X201' => [
            'magento_attribute_code' => 'color_temperature',
            'type' => 'price'
        ],
        'X202' => [
            'magento_attribute_code' => 'lumen',
            'type' => 'price'
        ],
        'X204' => [
            'magento_attribute_code' => 'xrwma_lampthrwn',
            'type' => 'dropdown'
        ],
        'X206' => [
            'magento_attribute_code' => 'dimmable',
            'type' => 'dropdown'
        ],
        'X210' => [
            'magento_attribute_code' => 'energeiakh_klash',
            'type' => 'text'
        ],
        'X211' => [
            'magento_attribute_code' => 'enswmatwmeno_fwtistiko',
            'type' => 'dropdown'
        ],
        'X212' => [
            'magento_attribute_code' => 'diakopths',
            'type' => 'dropdown'
        ],
        'X219' => [
            'magento_attribute_code' => 'typos_mpatarias',
            'type' => 'dropdown'
        ],
        'X220' => [
            'magento_attribute_code' => 'tash_mpatarias',
            'type' => 'dropdown'
        ],
        'X224' => [
            'magento_attribute_code' => 'autokollhto',
            'type' => 'dropdown'
        ],
        'X226' => [
            'magento_attribute_code' => 'syndesh',
            'type' => 'dropdown'
        ],
        'X228' => [
            'magento_attribute_code' => 'typos_myths',
            'type' => 'dropdown'
        ],
        'X231' => [
            'magento_attribute_code' => 'eggyhsh',
            'type' => 'text'
        ],
        'X232' => [
            'magento_attribute_code' => 'metroumena_megethi',
            'type' => 'dropdown'
        ],
        'X233' => [
            'magento_attribute_code' => 'megethos',
            'type' => 'dropdown'
        ],
        'X241' => [
            'magento_attribute_code' => 'logos_syrriknwshs',
            'type' => 'dropdown'
        ],
        'X244' => [
            'magento_attribute_code' => 'antoxh_sth_sympiesh',
            'type' => 'dropdown'
        ],
        'X246' => [
            'magento_attribute_code' => 'antoxh_sth_ghransh',
            'type' => 'text'
        ],
        'X247' => [
            'magento_attribute_code' => 'megistos_arithmos_krousewn',
            'type' => 'text'
        ],
        'X248' => [
            'magento_attribute_code' => 'vathmos_steganothtas',
            'type' => 'price'
        ],
        'X249' => [
            'magento_attribute_code' => 'antimikroviakh_prostasia',
            'type' => 'text'
        ],
        'X250' => [
            'magento_attribute_code' => 'den_diadidei_floga',
            'type' => 'text'
        ],
        'X251' => [
            'magento_attribute_code' => 'antoxh_sth_ghransh',
            'type' => 'text'
        ],
        'X252' => [
            'magento_attribute_code' => 'meiwmenes_trives',
            'type' => 'text'
        ],
        'X253' => [
            'magento_attribute_code' => 'typos_ergaleiou',
            'type' => 'dropdown'
        ],
        'X265' => [
            'magento_attribute_code' => 'ischus_fotoboltaikou',
            'type' => 'price'
        ],
        'X266' => [
            'magento_attribute_code' => 'diastaseis_fotoboltaikou',
            'type' => 'text'
        ],
        'ELG_ITEM_ID' => [
            'magento_attribute_code' => 'erp_id',
            'type' => 'text'
        ],
        'FREE_QTY' => [
            'magento_attribute_code' => 'qty',
            'type' => 'text'
        ],

        'RECYCLE_TAX' => [
            'magento_attribute_code' => '',
            'fake_code' => 'fpt',
            'type' => 'fpt'
        ],
        'ELG_ITEM_DSCNT_ZONE_ID' => [
            'magento_attribute_code' => '',
            'fake_code' => 'discount_zone_id',
            'type' => 'text'
        ],
        'CAT_TREE_CODE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'erp_tree_code',
            'type' => 'text'
        ],
        'PURCHASE_BLOCK_STATUS' => [
            'magento_attribute_code' => '',
            'fake_code' => 'purchase_block_status',
            'type' => 'text'
        ],
        'SALES_BLOCK_STATUS' => [
            'magento_attribute_code' => '',
            'fake_code' => 'sales_block_status',
            'type' => 'text'
        ],
        'WEBAVAILTYPE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'webavailtype',
            'type' => 'text'
        ],
        'Is_Web' => [
            'magento_attribute_code' => '',
            'fake_code' => 'is_web',
            'type' => 'text'
        ],
        'Is_Web_Active' => [
            'magento_attribute_code' => '',
            'fake_code' => 'is_web_active',
            'type' => 'text'
        ],
        'ELG_UPD_DATE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'erp_upd_date',
            'type' => 'text'
        ],
        'Supplier_Item_code' => [
            'magento_attribute_code' => 'mpn',
            'type' => 'text'
        ],
        'ITEM_WEIGTH_KGR' => [
            'magento_attribute_code' => 'weight',
            'type' => 'text'
        ],
        'tier_prices' => [
            'magento_attribute_code' => '',
            'fake_code' => 'tier_prices',
            'type' => 'text'
        ],
        'ITEM_WEB_DESCRIPTION' => [
            'magento_attribute_code' => '',
            'fake_code' => 'item_web_description',
            'type' => 'text'
        ],
        'ITEM_MAX_DISC' => [
            'magento_attribute_code' => 'max_discount',
            'type' => 'price'
        ],
        'special_price_to_insert' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_to_insert',
            'type' => 'price'
        ],
        'SPECIAL_PRICE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_erp',
            'type' => 'price'
        ],
        'ELG_SP_PRICE_FROM' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_from',
            'type' => 'price'
        ],
        'ELG_SP_PRICE_TO' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_to',
            'type' => 'price'
        ],
        'LEAD_TIME' => [
            'magento_attribute_code' => 'lead_time',
            'type' => 'text'
        ]
    ];


    public function remapCustomer($data) {

        $out = array();

        $out['customer_id'] = $data['CODE'];
        $out['erpid'] = $data['TRDR'];
        $out['vat'] = $data['AFM'];
        $out['email'] = $data['EMAIL'];

        return $out;
    }

    public function remapStock($data) {

        $out = array();

        $minSaleQty = 1;

        if(isset($data['PACKAGE']) && $data['PACKAGE']){
            $minSaleQty = floatval($data['PACKAGE']) > 1 ? floatval($data['PACKAGE']) : 1;
        }

        $out['erpid'] = $data['ELG_GXID'];
        $out['qty'] = (int)($data['FREE_QTY'] / $minSaleQty);
        $out['sku'] = $data['SKU'];
        $out['erp_upd_date'] = $data['ELG_UPD_DATE'];

        return $out;
    }

    public function remapOrderStatus($data) {

        $out = $data;


        $out['orderId'] = $data['WEB_ORDER_NUMBER'];
        unset($data['WEB_ORDER_NUMBER']);
        $out['erp_upd_date'] = $data['ELG_UPD_DATE'];
        unset($data['ELG_UPD_DATE']);

        return $out;
    }

    public function remapProduct($data, $groupPrices) {

        $out = array();
        foreach ($data as $key => $value) {
//            unset($valueID);
//            if(isset($value['id'])){
//                $valueID = $value['id'];
//            }
//            elseif($value['erpId']){
//                $valueID = $value['erpId'];
//            }
            $value = trim($value);
            if(trim($value) == ''){ //some attributes have spaces as values
                continue;
            }

            $magentoAttributeType = $this->attributes[$key]['type'];
            $magentoAttributeCode = ($this->attributes[$key]['magento_attribute_code']) ? : $this->attributes[$key]['fake_code'];

            if($magentoAttributeType == 'dropdown'){
                $valueID = $this->makeCodeFromString($value);
                $out['dropdown'][$key] = array('realcode'=>$magentoAttributeCode, 'id'=>$valueID, 'value'=>$value);
            }
            elseif($key == 'ELG_ITEM_DSCNT_ZONE_ID'){
                $discountZoneId = $value;
//                $erpDiscountsData = $this->getItemCustomerGroupPrices($discountZoneId);
                $out['tier_price'] = array();
                foreach($groupPrices as $discount){
                    if($discount['ELG_GXITEMDSZNID'] == $discountZoneId){
                        $customerGxId = $discount['ELG_GXCUSTDSZNID'];
                        $discountPercentage = $discount['gxDiscount'];
                        $magentoCustomerGroupId = (isset($this->customerGroups[$customerGxId])) ? $this->customerGroups[$customerGxId]['magento_id'] : false;
                        if($magentoCustomerGroupId){
                            $out['tier_price'][] = array(
                                'website_id' => '0',
                                'cust_group' => $magentoCustomerGroupId,
                                'price_qty' => '1',
                                'value_type' => 'percent',
                                'price' => $discountPercentage
                            );
                        }
                    }
                }
            }
//            elseif($magentoAttributeType == 'multiselect'){
//
//            }

//            if (is_array($value) && (isset($valueID)) && isset($value['value'])) {
//                if ($valueID == 0) continue;
//                $out['dropdown']['s'.$key] = array('realcode'=>$key, 'id'=>$valueID, 'value'=>$value['value'],'comment'=>$value['title'], 'translations'=>$value['translations'] ); //use "s" as prefix for noumeric codes
//            } elseif (is_array($value) && is_array($value[0]) && isset($value[0]['id']) && isset($value[0]['label'])) { //if it is sub-array is multiselect
//                foreach ($value as $k=>$v) {
//                    if (is_array($v) && isset($v['id']) && isset($v['label'])) {
//                        if ($v['id'] == 0) continue;
//                        $out['multiselect']['s'.$key][$key ."_" .$v['id']] = array('realcode'=>$key, 'id'=>$v['id'], 'value'=>$v['value'],'comment'=>$v['title'], 'translations'=>$value['translations']); //use "s" as prefix for noumeric codes
//                    }
//                }
//            }
            else {
                $out[$magentoAttributeCode] = $value;
            }

        }

        // $out['lastupdate'] = date("Y-m-d H:i:s");
        // $out['type_id'] = 'simple';


        return $out;
    }

    public function init_db($sqlhost, $username, $password, $database ) {
        ob_implicit_flush(TRUE);
        try {
            //$sqlhost = $config['sqlhost'];
            //$username = $config['username'];
            //$password = $config['password'] ?  $config['password'] : null;
            $this->sqllink = mysqli_connect($sqlhost,$username, $password, $database) or $this->logError( "Unable to connect to database");


            if (mysqli_connect_errno()) {
                $this->logError("Failed to connect to MAGE MySQL: " . mysqli_connect_error()); return 0;
            }

            mysqli_query($this->sqllink, "SET NAMES 'utf8'");

            return $this->sqllink;
        } catch(SoapFault $fault) {
            $this->logError($fault);
            return 0;
        }

    }
    public function canRun() {

        if ($this->debugmode === true) return true;

        $oldtime = @file_get_contents($this->fileruntime);

        //$load = sys_getloadavg();
        //debug($load[0]);
        //if ($load[0] > 6) return false;
        foreach ($this->blockingentities as $entity) {
            if (file_exists('running/' .$entity .'Running.txt')){
                return false; //another instance of connector is running
            }
        }

        if ($oldtime !== false) { //if file exist
            $datetime = time();
            $diff = $datetime - $oldtime ; //diff in seconds

            if ($diff > 3600) $this->logError("connector is not having HEARTBEAT for one hour. Take ACTION!!");

            if($diff > $this->maxtimewait) {
                return true; //too much time has passed, FORCE run again
            } else {
                return false; //wait, probably running other instance
            }
        } else {
            return true; //file not exist. run
        }

    }

    public function allowRun() {
        unlink($this->fileruntime);
    }

    public function heartbeat() {
        file_put_contents($this->fileruntime, time());
    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
        {
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        }

        return $out;
    }

    public function array_to_xml(array $arr, SimpleXMLElement $xml, $parent = null)
    {
        foreach ($arr as $k => $v) {
            if (is_int($k)) {
                 $k=substr($parent, 0, -1); //remove s (last digit) eg items => item
            }

            is_array($v)
                ? $this->array_to_xml($v, $xml->addChild($k), $k)
                : $xml->addChild($k, htmlspecialchars($v,ENT_QUOTES,'UTF-8'));
        }
        return $xml;
    }

    public function aasort (&$array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }

    public function saveDatatoDB($jsonstring, $increment_id, $tempTable = null) {

        //$date = date('Y-m-d H:i:s');
        if ($this->datatable && $this->sqllink) {
            $tableToSave = $this->datatable;
            if($tempTable !== null){
                $tableToSave = $tempTable;
            }
            $jsonstring = mysqli_real_escape_string($this->sqllink, $jsonstring);
            $query = "INSERT INTO `$tableToSave` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `retries`, `status`) VALUES (0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '$increment_id' , '$jsonstring','0','0');";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }
            return true;
        }

        logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;
    }

    public function getUnproccessedXmlData($status = 0) {
        $date = date('Y-m-d H:i:s');
//  if ($table) {
        if ($this->datatable && $this->sqllink) {
            $query = "SELECT * FROM `{$this->datatable}` WHERE `status`= $status ORDER BY id";
            //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[]=$row;
            }
            return $rows;
        }
        logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;
    }

    function getReproccessedXmlData($status = 0) {
        //$date = date('Y-m-d H:i:s');
//  if ($table) {
        if ($this->datatable && $this->sqllink) {
            $query = "SELECT * FROM `{$this->datatable}` WHERE `status`>1 AND  `status` <= $status  ORDER BY id";
            //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[]=$row;
            }
            return $rows;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;
    }

    public function getUnproccessedJsonData($status = 0) {

        $date = date('Y-m-d H:i:s');
//  if ($table) {
        if ($this->datatable && $this->sqllink) {
            $query = "SELECT * FROM `{$this->datatable}` WHERE `status`= $status ORDER BY id";
            //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[]=$row;
            }
            return $rows;
        }
        logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;

    }

    public function getReproccessedJsonData($status = 0) {

        //$date = date('Y-m-d H:i:s');
//  if ($table) {
        if ($this->datatable && $this->sqllink) {
            $query = "SELECT * FROM `{$this->datatable}` WHERE `status`>1 AND  `status` <= $status  ORDER BY id";
            //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[]=$row;
            }
            return $rows;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;

    }

    public function setXmlDataStatus($id ,$status) {

        //$date = date('Y-m-d H:i:s');
        if ($this->datatable && $this->sqllink) {

            $query = "UPDATE `{$this->datatable}` SET `date_updated`=CURRENT_TIMESTAMP, `status` = '$status' WHERE `id` = $id;";
            $sql = mysqli_query($this->sqllink,$query);//reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }

            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;

    }

    public function setXmlDataError($id ,$error) {

        $error = mysqli_real_escape_string($this->sqllink, $error);
        $date = date('Y-m-d H:i:s');
        if ($this->datatable && $this->sqllink) {

            $query = "UPDATE `{$this->datatable}` SET `errorlog` = '$error' WHERE `id` = $id;";
            $sql = mysqli_query($this->sqllink,$query);//reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }

            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;

    }

    public function close_mysql($message = null){
        if ($this->sqllink)
            $sql = mysqli_close($this->sqllink);
    }

    public function close($message = null) {
        if (!is_null($message))
            $this->logError($message);
        //unlink($fileruntime); // do not delete here. Delete should be done only if actually running
        if (!is_null($this->errors))
            $this->senderrormail(); //send (if any) error to mail
        $this->close_mysql();
    }

    public function logError ($message) {

        //adds a message error allong with date
        $date = gmdate(DATE_RFC822);
        file_put_contents($this->errorlogfile, $date .' ' .$message ."\n", FILE_APPEND | LOCK_EX);
        $this->debug($date .' ' .$message);
        echo $message ."<br>";
        $this->latesterror = $message;
        $this->errors = $this->errors .$date .' ' .$message .'\n'; //log error
    }

    public function senderrormail() {

        $date = gmdate(DATE_RFC822);
        // In case any of our lines are larger than 70 characters, we should use wordwrap()
        //$message = wordwrap($errors, 70);
        $sendto = $this->sendto; //default users

        $errors = $this->errors;
        if (strpos($errors,'Bad Gateway') !== false || strpos($errors,'Time-out') !== false || strpos($errors,'Fetching') !== false || strpos($errors,'Deadlock') !== false || strpos($errors,'Internal') !== false || strpos($errors,'Parsing') !== false || strpos($errors,'previous') !== false || strpos($errors,'unrecoverable') !== false ) {
            $sendto = $this->sendtoadmin; //send this error only to admin
        }
        mail($sendto, $this->sitename .$this->mailsubject, $this->errors ."\n" .$date .'\n ');
        $this->$errors = null; //flush error variable
    }

    public function notifyAdminAboutProductQueueIfNeeded($totalProducts){
        if($totalProducts >= 500){
            $date = gmdate(DATE_RFC822);
            mail("filippos.gkoufas@meidanis.gr", $this->sitename ." Product Updates Queue Notification", $date .'\n ' . "Total of $totalProducts products added into connector's queue for updates.");
        }
    }

    public function debug($msg, $init = false)
    {
        $msg = gmdate(DATE_RFC822, time()) .' ' .$msg ."\n";
        file_put_contents($this->debugfile, $msg , $init ? null : FILE_APPEND | LOCK_EX);
        echo $msg ."<br>";
    }


    public function getRest($uri, $params = null, $url = null) {

        $service_url = $this->service_url;
        $url = $service_url ."/" .$uri;

        if (is_array($params)) {
            $getparams = http_build_query($params);
            $url = $url ."?" .$getparams;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        //      curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_VERBOSE, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_USERAGENT,"Elegento Connector");

        $curl_response = curl_exec($ch);

        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->errors .= 'error occured during curl exec. Additioanl info: ' . var_export($info) ."/n/r";
            return false;
        }
        curl_close($ch);
        $r = iconv('windows-1253', "utf-8//IGNORE", $curl_response);
		$r = preg_replace('/[\x00-\x1F\x7F]/u', '', $r); //clean invalid characters
        $decoded = json_decode($r, true);
        if ($decoded['success'] != true) {
			$this->debug($curl_response);
            $this->errors .= 'error occured code [' .$decoded['errorcode'] .']: ' . $decoded['error'] .$curl_response ."/n/r";
            return FALSE;
        }

        return $decoded;
    }

    public function postRest($params, $uri = null) {
        //next example will insert new conversation
        $service_url = $this->service_url;
        if ($uri){
            $service_url = $service_url ."/" .$uri;
        }

        $ch = curl_init($service_url);
        //$curl_post_data = array();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additioanl info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
        $r = iconv('windows-1253', "utf-8//IGNORE", $curl_response);
		$r = preg_replace('/[\x00-\x1F\x7F]/u', '', $r); //clean invalid characters
        $decoded = json_decode($r, true);
        if ($decoded['success'] != true) {
			$this->debug($r);
            $this->logError('error occured code [' .$decoded['errorcode'] .']: ' . $decoded['error'] ." JSON: " .$params);
            return FALSE;
        }

        return $decoded;
    }


    public function M2post($theurl, $thedata)
    {

        //NA ALLAKSEI PRIN VGEI LIVE SE .GR
        $url = 'https://www.meidanis.gr/rest/all/V1/';
        $token = 'm2jcx0fvh591qob4ifeos894vwng3pkn';

        $productData = json_encode($thedata);
        $ch = curl_init($url.$theurl);

        $setHeaders = array('Content-Type:application/json','Authorization: Bearer '.$token);

        curl_setopt($ch,CURLOPT_POSTFIELDS, $productData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $setHeaders);

        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additional info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
        // $r = iconv('windows-1253', "utf-8", );
        $decoded = json_decode($curl_response, true);
        if (is_array($decoded) && array_key_exists('message', $decoded)) {
            $this->logError('error occured: ' . $decoded['message']);
            return FALSE;
        }

        return $decoded;
    }

    public function sendEmailToBranch($orderData){
        $orderData = array('orderData' => $orderData);
        $data = $this->M2post('elegento-notificationsystem/sendemailtobranch', $orderData);
    }

    public function postServer7($params, $uri = null){
        $service_url = self::SERVER_7_URL;
        if ($uri){
            $service_url = $service_url ."/" .$uri;
        }

        $ch = curl_init($service_url);
        //$curl_post_data = array();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additioanl info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
        $r = iconv('windows-1253', "utf-8//IGNORE", $curl_response);
        $r = preg_replace('/[\x00-\x1F\x7F]/u', '', $r); //clean invalid characters
        $r = str_replace("resource(3) of type (mssql result)", "", $r); //this is part of return result when Select NEWID() is executed
        if(stripos($r, "INSERT INTO") !== false){ //in case of insert query then the whole query is returned as response, so keep only the json data
            $exploded = explode('{', $r);
            $r = '{' . $exploded[1];
        }

        $decoded = json_decode($r, true);
        if (isset($decoded['success']) && $decoded['success'] != true && stripos($r, '"success":true') === false) {
            $this->debug($r);
            $this->logError('error occured code [' .$decoded['errorcode'] .']: ' . $decoded['error'] ." JSON: " .$params);
            return FALSE;
        }
        elseif(!isset($decoded['success']) && stripos($r, '"success":true') === false){
            $this->debug($r);
            $this->logError("error occured at json_decode." );
            return false;
        }

        return $decoded;
    }

    public function loginAndAuthenticate($username, $password) {


        $req = '{"service": "login","username": "' .$username .'","password":"' .$password .'","appId": "' .$this->appid .'"}';
        $result = $this->postRest($req);

        if ($result && $result['clientID']) {
            $req = json_encode(array("service" => "authenticate", "clientID"=>$result['clientID'], "COMPANY"=>$result['objs'][0]['COMPANY'] ,"BRANCH"=>$result['objs'][0]['BRANCH'], "MODULE"=>$result['objs'][0]['MODULE'], "REFID"=>$result['objs'][0]['REFID'], "USERID"=>$result['objs'][0]['USERID']));
            $result = $this->postRest($req);
            if ($result['success'] == true && $result['clientID']) {
                $this->clientid = $result['clientID'];
                return $result['clientID'];
            }
        } else {
            $this->errors .="Cannot login.";
            return false;
        }
        return false;
    }

    public function servicepost($params, $uri = null) {


        if ($this->clientid) {
            $req = json_encode(array_merge($params, array("clientID"=>$this->clientid, "appId"=>$this->appid)));
            $req = str_replace(":[]",':""',$req); //replace empty array to empty string
            if ($this->debug) $this->debug($req); //add json request to log
            $result = $this->postRest($req , $uri);
            if ($this->debug) $this->debug(json_encode($result)); //add json result to log
            if ($result['success'] == true) {
                return $result;
            }
            else{
                $this->emergencyExit();
            }
        } else {
            $this->errors .="Client id in not set. Login first";
            return false;
        }
        return false;
    }

    public function customServicePost($data, $uri = null, $dataTable = null, $fieldName = null, $fieldValue = null){
        if($dataTable && $fieldName){
            $params = (['pass' => self::SERVER_7_GET_SCRIPT_PASS, 'data' => $data, 'data_table' => $dataTable, 'key_name' => $fieldName, 'key_value' => $fieldValue]);
        }
        elseif($dataTable){
            $params = (['pass' => self::SERVER_7_GET_SCRIPT_PASS, 'data' => $data, 'data_table' => $dataTable]);
        }
        else{
            $params = (['pass' => self::SERVER_7_GET_SCRIPT_PASS, 'query' => $data]);
        }
        if ($this->debug){
            $this->debug($data);
        }
        $result = $this->postServer7($params, $uri);
        if ($this->debug) $this->debug(json_encode($result)); //add json result to log
        $retries = 0;
        while($result == false && $retries < 2){
            echo('retrying... \n\r');
            $result = $this->postServer7($params, $uri);
            if ($this->debug) $this->debug(json_encode($result)); //add json result to log
            $retries++;
        }
        if ($result === false) {
            $this->emergencyExit();
        }
        else{
            return $result;
        }
    }

    public function getIndexedTempTableData($tempTableName){
        if ($this->sqllink) {
            $query = "SELECT data, entity_id FROM `{$tempTableName}`";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to read from MySQL: " . mysqli_error($this->sqllink)); return null;
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[$row['entity_id']]=$row['data'];
            }

            return $rows;
        }
        else{
            $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
            return null;
        }

    }

    public function filterCustomers($data) {

        $out = array();

        $columnsToInclude = array(
            'CUST_NAME',
            'TIN',
            'SALESNAME',
            'DISZDESCR',
            'ACTIVE_DATE',
            'PROF_CAT',
            'PROFESSION_DESCRIPTION',
            'TURVOVER_LY',
            'TURVOVER_LTY',
            'TURVOVER_CY',
            'WATT_CY',
            'LY_TYPE_XON',
            'LY_TYPE_WEB',
            'CY_TYPE_XON',
            'CY_TYPE_WEB'
        );

        foreach ($data as $key => $value) {
            if(!is_array($value) && ($value === null || trim($value) == "" || !in_array($key, $columnsToInclude))){
                continue;
            }
            $out[$key] = $value;
        }

        return $out;
    }

    /*
     * checks if a row has changed
     * returns true if changed, false if not changed, 0 if row does not exist and null if error occurs
     */
    public function isRowChanged($tableData, $idname, $newData) {

        if (isset($tableData[$newData[$idname]])) { //if a single row is found

            $oldData = json_decode($tableData[$newData[$idname]], true);

            $rowChanged = $this->checkIfRowHasChanged($oldData, $newData);

            if ($rowChanged){
                return true; //something changed
            }

        } elseif(!isset($tableData[$newData[$idname]])) {
            return 0;
        }
        return false; //array is the same
    }

    public function updateDatatoDB($table, $data, $id_field, $id_value, $additionalwhere = null) {

        $error = 0; //reset error
        $data = mysqli_real_escape_string ($this->sqllink, $data);
        //if ($field == "erpid") continue; //never update erpid. erpid is only for the erp
//        $dateTime = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone('UTC'));
//        $dateTime->setTimezone(new \DateTimeZone('Europe/Athens'));
//        $updDate = $dateTime->format('Y-m-d H:i:s');
        $query = sprintf("UPDATE `%s` SET `data` = '%s', `date_updated` = CURRENT_TIMESTAMP WHERE `%s` = '%s'", $table, $data, $id_field, $id_value);
        if ($additionalwhere) $query = $query ." AND (" .$additionalwhere .")";

        $sql = mysqli_query($this->sqllink, $query);
        if (!$sql)
            $error = mysqli_errno($this->sqllink); //use the mySQL error number for refference

        //if (isset($error) && strstr($error, "Unknown column") === false) { //ignore error if column is missing
        if (isset($error) && $error != 1054 && $error > 0) { //ignore error if column is missing  "Unknown column"
            $this->logError(mysqli_error($this->sqllink));
            return 0;
        }

        return 1;
    }

    public function checkIfRowHasChanged($oldData, $newData){
        return $oldData != $newData;
    }

    public function createTempTableCopy() {

        if ($this->datatable && $this->sqllink) {
            $tempTableName = $this->datatable. "_temp";
            $oldTableName = $this->datatable ."_old";

            $query = "DROP TABLE IF EXISTS `$oldTableName`, `$tempTableName`;";
            $runQuery = $this->runSqlQuery($query);

            if($runQuery === false){
                return false;
            }


            $query = "CREATE TABLE $tempTableName LIKE {$this->datatable};";
            $runQuery = $this->runSqlQuery($query);

            if($runQuery === false){
                return false;
            }

            $query = "INSERT $tempTableName SELECT * FROM {$this->datatable};";
            $runQuery = $this->runSqlQuery($query);

            if($runQuery === false){
                return false;
            }


            return $tempTableName;
        }

        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;
    }

    public function runSqlQuery($query){
        $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

        if (!$sql) {
            $error = mysqli_error($this->sqllink);
            $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink));
            return false;
        }
        return true;
    }

    public function copyTempToOrig(){
        if ($this->datatable && $this->sqllink) {
            try{
                $tempTableName = $this->datatable. "_temp";
                $oldTableName = $this->datatable ."_old";
                $query = "RENAME TABLE `{$this->datatable}` TO `$oldTableName`, `$tempTableName` TO `{$this->datatable}`;";
                $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

                if (!$sql) {
                    $error = mysqli_error($this->sqllink);
                    $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink));
                    return false;
                }
                return true;
            }
            catch (\Throwable $e) {
                return false;
            }
        }

        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return false;
    }

    public function getDiffCustomers($lastupddate) {
        $query = "SELECT * FROM `{$this->difftable}` where date_updated > '$lastupddate' order by date_updated asc";
        $sql = mysqli_query($this->sqllink,$query); //select previous succesful entry

        $rows=array();
        while ($row = mysqli_fetch_assoc($sql)) {
            $jsonDecoded = json_decode($row['data'], true);
            $data = $jsonDecoded;
            $data['lastupdate'] = $row['date_updated']; //convert XML to array
            $rows[$row['entity_id']] = $data;

        }
        return $rows;
    }

    public function getItems($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_ITEMS_URL);
        return $result['data'];
    }

    public function getRdcItems($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_RDC_ITEMS_URL);
        $this->runId = $result['runId']; //used later to report to erp what is saved in DB
        return $result['data'];
    }

    public function getItemsFromErpDb($lastUpdate, $uri = null){
        //$selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), b.ATTR_ID) as ELG_ATTR_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.*, b.* FROM dbo.erp_items a LEFT JOIN dbo.erp_item_attributes b ON a.ITEM_CODE = b.ITEM_CODE WHERE a.CAT_TREE_CODE LIKE '2.7%' OR a.CAT_TREE_CODE LIKE '2.8%' OR a.CAT_TREE_CODE LIKE '2.9%' OR a.CAT_TREE_CODE LIKE '2.10%' OR a.CAT_TREE_CODE LIKE '2.11%' OR a.CAT_TREE_CODE LIKE '2.12%' OR a.CAT_TREE_CODE LIKE '2.13%' OR a.CAT_TREE_CODE LIKE '2.14%' OR a.CAT_TREE_CODE LIKE '2.15%' OR a.CAT_TREE_CODE LIKE '2.16%' OR a.CAT_TREE_CODE LIKE '2.17%' OR a.CAT_TREE_CODE LIKE '2.18%' OR a.CAT_TREE_CODE LIKE '2.19%' OR a.CAT_TREE_CODE LIKE '2.20%'";
        $selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE, convert(varchar, b.UPD_DATE, 25) as ELG_ATTR_UPD_DATE, convert(varchar, a.SP_PRICE_FROM, 25) as ELG_SP_PRICE_FROM, convert(varchar, a.SP_PRICE_TO, 25) as ELG_SP_PRICE_TO, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), b.ATTR_ID) as ELG_ATTR_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.PACKAGE as ELG_ITEM_PACKAGE, a.*, b.*, c.* FROM dbo.erp_items a LEFT JOIN dbo.erp_item_attributes b ON a.ITEM_CODE = b.ITEM_CODE LEFT JOIN dbo.erp_item_comp_bal_vi c ON a.ITEM_CODE = c.SKU WHERE a.UPD_DATE > '".$lastUpdate ."'";
//        $selectUpdatedItemsQuery = "SELECT *
//FROM    (SELECT ROW_NUMBER() OVER ( ORDER BY UPD_DATE ) AS RowNum, *
//          FROM  (SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME,
//			  convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE,
//			   convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID,
//				convert(nvarchar(36), b.ATTR_ID) as ELG_ATTR_ID,
//				 convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID,
//				  convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.*, b.X001,b.X002,b.X003,b.X004,b.X005,b.X006,b.X007,b.X008,b.X009,b.X010,b.X011,b.X012,b.X013,b.X014,b.X015,b.X016,b.X017,b.X018,b.X019,b.X020,b.X021,b.X022,b.X023,b.X024,b.X025,b.X026,b.X027,b.X028,b.X029,b.X030,b.X031,b.X032,b.X033,b.X034,b.X035,b.X036,b.X037,b.X038,b.X039,b.X040,b.X041,b.X042,b.X043,b.X044,b.X045,b.X046,b.X047,b.X048,b.X049,b.X050,b.X051,b.X052,b.X053,b.X054,b.X055,b.X056,b.X057,b.X058,b.X059,b.X060,b.X061,b.X062,b.X063,b.X064,b.X065,b.X066,b.X067,b.X068,b.X069,b.X070,b.X071,b.X072,b.X073,b.X074,b.X075,b.X076,b.X077,b.X078,b.X079,b.X080,b.X081,b.X082,b.X083,b.X084,b.X085,b.X086,b.X087,b.X088,b.X089,b.X090,b.X091,b.X092,b.X093,b.X094,b.X095,b.X096,b.X097,b.X098,b.X099,b.X100,b.X101,b.X102,b.X103,b.X104,b.X105,b.X106,b.X107,b.X108,b.X109,b.X110,b.X111,b.X112,b.X113,b.X114,b.X115,b.X116,b.X117,b.X118,b.X119,b.X120,b.X121,b.X122,b.X123,b.X124,b.X125,b.X126,b.X127,b.X128,b.X129,b.X130,b.X131,b.X132,b.X133,b.X134,b.X135,b.X136,b.X137,b.X138,b.X139,b.X140,b.X141,b.X142,b.X143,b.X144,b.X145,b.X146,b.X147,b.X148,b.X149,b.X150,b.X151,b.X152,b.X153,b.X154,b.X155,b.X156,b.X157,b.X158,b.X159,b.X160,b.X161,b.X162,b.X163,b.X164,b.X165,b.X166,b.X167,b.X168,b.X169,b.X170,b.X171,b.X172,b.X173,b.X174,b.X175,b.X176,b.X177,b.X178,b.X179,b.X180,b.X181,b.X182,b.X183,b.X184,b.X185,b.X186,b.X187,b.X188,b.X189,b.X190,b.X191,b.X192,b.X193,b.X194,b.X195,b.X196,b.X197,b.X198,b.X199,b.X200,b.X201,b.X202,b.X203,b.X204,b.X205,b.X206,b.X207,b.X208,b.X209,b.X210,b.X211,b.X212,b.X213,b.X214,b.X215,b.X216,b.X217,b.X218,b.X219,b.X220,b.X221,b.X222,b.X223,b.X224,b.X225,b.X226,b.X227,b.X228,b.X229,b.X230,b.X231,b.X232,b.X233,b.X234,b.X235,b.X236,b.X237,b.X238,b.X239,b.X240,b.X241,b.X242,b.X243,b.X244,b.X245,b.X246,b.X247,b.X248,b.X249,b.X250,b.X251,b.X252,b.X253,b.X254,b.X255,b.X256,b.X257,b.X258,b.X259,b.X260,b.X261,b.X262,b.X263,b.X264,b.X265,b.X266,b.X267,b.X268,b.X269,b.X270,b.X271,b.X272,b.X273,b.X274,b.X275,b.X276,b.X277,b.X278,b.X279,b.X280
//				  FROM dbo.erp_items a
//				  LEFT JOIN dbo.erp_item_attributes b
//				  ON a.ITEM_CODE = b.ITEM_CODE) AS joinedTable)
//				  AS RowConstrainedResult WHERE RowNum >=3000 AND RowNum < 6000";
        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getCustomersDataFromErpDb($uri = null){
        //$selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), b.ATTR_ID) as ELG_ATTR_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.*, b.* FROM dbo.erp_items a LEFT JOIN dbo.erp_item_attributes b ON a.ITEM_CODE = b.ITEM_CODE WHERE a.CAT_TREE_CODE LIKE '2.7%' OR a.CAT_TREE_CODE LIKE '2.8%' OR a.CAT_TREE_CODE LIKE '2.9%' OR a.CAT_TREE_CODE LIKE '2.10%' OR a.CAT_TREE_CODE LIKE '2.11%' OR a.CAT_TREE_CODE LIKE '2.12%' OR a.CAT_TREE_CODE LIKE '2.13%' OR a.CAT_TREE_CODE LIKE '2.14%' OR a.CAT_TREE_CODE LIKE '2.15%' OR a.CAT_TREE_CODE LIKE '2.16%' OR a.CAT_TREE_CODE LIKE '2.17%' OR a.CAT_TREE_CODE LIKE '2.18%' OR a.CAT_TREE_CODE LIKE '2.19%' OR a.CAT_TREE_CODE LIKE '2.20%'";
        $selectUpdatedItemsQuery = "SELECT CUST_NAME, TIN, SALESNAME, DISZDESCR, ACTIVE_DATE, PROF_CAT, PROFESSION_DESCRIPTION, TURVOVER_LY, TURVOVER_LTY, TURVOVER_CY, WATT_CY,
LY_TYPE_XON, LY_TYPE_WEB, CY_TYPE_XON, CY_TYPE_WEB
FROM WEB_ERP_CUST_TURNOVER_VI";
//        $selectUpdatedItemsQuery = "SELECT *
//FROM    (SELECT ROW_NUMBER() OVER ( ORDER BY UPD_DATE ) AS RowNum, *
//          FROM  (SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME,
//			  convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE,
//			   convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID,
//				convert(nvarchar(36), b.ATTR_ID) as ELG_ATTR_ID,
//				 convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID,
//				  convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.*, b.X001,b.X002,b.X003,b.X004,b.X005,b.X006,b.X007,b.X008,b.X009,b.X010,b.X011,b.X012,b.X013,b.X014,b.X015,b.X016,b.X017,b.X018,b.X019,b.X020,b.X021,b.X022,b.X023,b.X024,b.X025,b.X026,b.X027,b.X028,b.X029,b.X030,b.X031,b.X032,b.X033,b.X034,b.X035,b.X036,b.X037,b.X038,b.X039,b.X040,b.X041,b.X042,b.X043,b.X044,b.X045,b.X046,b.X047,b.X048,b.X049,b.X050,b.X051,b.X052,b.X053,b.X054,b.X055,b.X056,b.X057,b.X058,b.X059,b.X060,b.X061,b.X062,b.X063,b.X064,b.X065,b.X066,b.X067,b.X068,b.X069,b.X070,b.X071,b.X072,b.X073,b.X074,b.X075,b.X076,b.X077,b.X078,b.X079,b.X080,b.X081,b.X082,b.X083,b.X084,b.X085,b.X086,b.X087,b.X088,b.X089,b.X090,b.X091,b.X092,b.X093,b.X094,b.X095,b.X096,b.X097,b.X098,b.X099,b.X100,b.X101,b.X102,b.X103,b.X104,b.X105,b.X106,b.X107,b.X108,b.X109,b.X110,b.X111,b.X112,b.X113,b.X114,b.X115,b.X116,b.X117,b.X118,b.X119,b.X120,b.X121,b.X122,b.X123,b.X124,b.X125,b.X126,b.X127,b.X128,b.X129,b.X130,b.X131,b.X132,b.X133,b.X134,b.X135,b.X136,b.X137,b.X138,b.X139,b.X140,b.X141,b.X142,b.X143,b.X144,b.X145,b.X146,b.X147,b.X148,b.X149,b.X150,b.X151,b.X152,b.X153,b.X154,b.X155,b.X156,b.X157,b.X158,b.X159,b.X160,b.X161,b.X162,b.X163,b.X164,b.X165,b.X166,b.X167,b.X168,b.X169,b.X170,b.X171,b.X172,b.X173,b.X174,b.X175,b.X176,b.X177,b.X178,b.X179,b.X180,b.X181,b.X182,b.X183,b.X184,b.X185,b.X186,b.X187,b.X188,b.X189,b.X190,b.X191,b.X192,b.X193,b.X194,b.X195,b.X196,b.X197,b.X198,b.X199,b.X200,b.X201,b.X202,b.X203,b.X204,b.X205,b.X206,b.X207,b.X208,b.X209,b.X210,b.X211,b.X212,b.X213,b.X214,b.X215,b.X216,b.X217,b.X218,b.X219,b.X220,b.X221,b.X222,b.X223,b.X224,b.X225,b.X226,b.X227,b.X228,b.X229,b.X230,b.X231,b.X232,b.X233,b.X234,b.X235,b.X236,b.X237,b.X238,b.X239,b.X240,b.X241,b.X242,b.X243,b.X244,b.X245,b.X246,b.X247,b.X248,b.X249,b.X250,b.X251,b.X252,b.X253,b.X254,b.X255,b.X256,b.X257,b.X258,b.X259,b.X260,b.X261,b.X262,b.X263,b.X264,b.X265,b.X266,b.X267,b.X268,b.X269,b.X270,b.X271,b.X272,b.X273,b.X274,b.X275,b.X276,b.X277,b.X278,b.X279,b.X280
//				  FROM dbo.erp_items a
//				  LEFT JOIN dbo.erp_item_attributes b
//				  ON a.ITEM_CODE = b.ITEM_CODE) AS joinedTable)
//				  AS RowConstrainedResult WHERE RowNum >=3000 AND RowNum < 6000";
        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getItemPricesFromErpDb($lastUpdate, $uri = null){
        $selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE_PRICE, 25) as ELG_UPD_DATE_PRICE, convert(varchar, a.SP_PRICE_FROM, 25) as ELG_SP_PRICE_FROM, convert(varchar, a.SP_PRICE_TO, 25) as ELG_SP_PRICE_TO, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.PACKAGE as ELG_ITEM_PACKAGE, a.* FROM dbo.erp_items a WHERE a.UPD_DATE_PRICE > '".$lastUpdate ."'";

        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getCurrentTimestampFromErp($uri = null){
        $selectUpdatedItemsQuery = "SELECT convert(varchar, CURRENT_TIMESTAMP, 25) as now";

        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getItemPricesBySpecialFromErpDb($lastUpdate, $now, $uri = null){
        $selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE_PRICE, 25) as ELG_UPD_DATE_PRICE, convert(varchar, a.SP_PRICE_FROM, 25) as ELG_SP_PRICE_FROM, convert(varchar, a.SP_PRICE_TO, 25) as ELG_SP_PRICE_TO, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.* FROM dbo.erp_items a WHERE a.SP_PRICE_FROM >= '".$lastUpdate ."' and a.SP_PRICE_FROM <= '".$now ."' and a.SP_PRICE_TO >= '".$now ."' ";

        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getItemPricesBySpecialToErpDb($lastUpdate, $now, $uri = null){
        $selectUpdatedItemsQuery = "SELECT a.ITEM_CODE AS ELG_SKU, a.ITEM_DESCRIPTION as ELG_NAME, convert(varchar, a.UPD_DATE_PRICE, 25) as ELG_UPD_DATE_PRICE, convert(varchar, a.SP_PRICE_FROM, 25) as ELG_SP_PRICE_FROM, convert(varchar, a.SP_PRICE_TO, 25) as ELG_SP_PRICE_TO, convert(nvarchar(36), a.ITEM_DSCNT_ZONE_ID) as ELG_ITEM_DSCNT_ZONE_ID, convert(nvarchar(36), a.ITEM_ID) as ELG_ITEM_ID, convert(nvarchar(36), a.CAT_GXID) as ELG_CAT_GXID , a.* FROM dbo.erp_items a WHERE a.SP_PRICE_TO >= '".$lastUpdate ."' and a.SP_PRICE_TO <= '".$now ."'";

        return $this->customServicePost($selectUpdatedItemsQuery, self::SERVER_7_GET_URI)['data'];
    }

//    public function getItemsFromErpDb($lastUpdate, $uri = null){
//        $selectUpdatedItemsQuery = "SELECT * FROM dbo.erp_items WHERE ITEM_ID='B48D0CE3-4506-BF48-7A16-016FD81D4AD6'";
//        return $this->customServicePost($selectUpdatedItemsQuery, $uri)['data'];
//    }

    public function getNewMSSQLUUID(){
        return $this->customServicePost('SELECT NEWID()',self::SERVER_7_GET_URI)['data'];
    }

    public function getItemCustomerGroupPrices($itemDiscountZoneGxId, $uri = null){
        $selectItemCustomerGroupPricesQuery = "SELECT convert(nvarchar(36), GXCUSTDSZNID) as ELG_GXCUSTDSZNID,* FROM dbo.erp_disc_matrix WHERE GXITEMDSZNID = '" .$itemDiscountZoneGxId ."' ";
        return $this->customServicePost($selectItemCustomerGroupPricesQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getAllItemCustomerGroupPrices($uri = null){
        $selectItemCustomerGroupPricesQuery = "SELECT convert(nvarchar(36), GXCUSTDSZNID) as ELG_GXCUSTDSZNID, convert(nvarchar(36), GXITEMDSZNID) as ELG_GXITEMDSZNID, * FROM dbo.erp_disc_matrix ";
        return $this->customServicePost($selectItemCustomerGroupPricesQuery, self::SERVER_7_GET_URI)['data'];
    }

//    public function getItemFromErpDb($itemId, $uri = null){
//        $getItemQuery = "SELECT * FROM dbo.erp_items WHERE ITEM_ID = " .$itemId;
//        return $this->customServicePost($getItemQuery, $uri);
//    }

    public function getRdcPhotosByParam($params){
        if ($params) {
            $result = $this->servicepost($params, s1rest::GET_RDC_ITEMS_PHOTOS_BY_PARAMS);
        } else {
            $result = false;
        }

        return $result['data'];
    }
    public function getRdcOrderStatus($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_RDC_ORDER_STATUS);
        $this->runId = $result['runId']; //used later to report to erp what is saved in DB
        return $result['data'];
    }

    public function getOrderStatusFromErp($date, $uri = null){
        $selectOrderStatusesUpdatesQuery = "SELECT convert(varchar, ORDER_STATUS_DATE_TIME, 25) as ELG_UPD_DATE, * FROM dbo.erp_order_status_vi WHERE ORDER_STATUS_DATE_TIME > '$date ' ";
        $result =  $this->customServicePost($selectOrderStatusesUpdatesQuery, self::SERVER_7_GET_URI)['data'];
        if(empty($result)){
            return array();
        }
        return $result;
    }

    public function getDocs($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_DOCS_URL);
        if ($result['success'] == true) {
            return $result['data'] ? $result['data'] : 0;
        }
        return false;
    }
    public function getItemsBalance($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_ITEMS_BALANCE_URL);
        return $result['data'];
    }
    public function getRdcItemsBalance($params = null, $uri = null) {

        if ($params) {
            $result = $this->servicepost($params, s1rest::GET_RDC_ITEMS_BALANCE_URL_BY_PARAMS);
            //### $result = $this->servicepost($params, s1rest::GET_RDC_SIMPLE_ITEMS_BALANCE_URL);
            //### $optionsresult = $this->servicepost($params, s1rest::GET_RDC_OPTIONS_ITEMS_BALANCE_URL_BY_PARAMS);
            //$result = array_merge($result, $optionsresult); //merge options with simple products
            //$this->runId = $result['runId']; //used later to report to erp what is saved in DB
        }
        else {
            $result = $this->servicepost(array(), s1rest::GET_RDC_ITEMS_BALANCE_URL);
            //$optionsresult['data'] = array(); //use empty array to merge
            //$optionsresult = $this->servicepost(array(), s1rest::GET_RDC_OPTIONS_ITEMS_BALANCE_URL);
            //$result = array_merge($result, $optionsresult); //merge options with simple products

            $this->runId = $result['runId']; //used later to report to erp what is saved in DB
        }

        //$this->runId = $result['runId']; //do NOT run here because it breakes the getProducts connector
        //### return array_merge($result['quantities'], $optionsresult['quantities']) ;  //merge options with simple products
        return $result['data'];
    }

    public function getErpItemsTotalBalance($gxId, $uri = null){
        $selectProductStockQuery = "SELECT * FROM dbo.erp_item_comp_bal_vi WHERE GXITEMID='" .$gxId ."' ";
        $result =  $this->customServicePost($selectProductStockQuery, self::SERVER_7_GET_URI)['data'];
        if(empty($result)){
            return 0;
        }
        return $result;
    }

    public function getErpItemsStockUpdates($date, $uri = null){
        $selectProductsStockUpdatesQuery = "SELECT convert(varchar, a.UPD_DATE, 25) as ELG_UPD_DATE, convert(nvarchar(36), a.GXITEMID) as ELG_GXID, a.*, b.PACKAGE FROM dbo.erp_item_comp_bal_vi a LEFT JOIN dbo.erp_items b ON a.SKU = b.ITEM_CODE WHERE a.UPD_DATE > '$date ' ";
        $result =  $this->customServicePost($selectProductsStockUpdatesQuery, self::SERVER_7_GET_URI)['data'];
        if(empty($result)){
            return 0;
        }
        return $result;
    }

    public function getErpCustomers($date, $uri = null){
        $selectCustomersQuery = "SELECT convert(varchar, UPD_DATE, 25) as ELG_UPD_DATE, * FROM dbo.erp_customers WHERE UPD_DATE > '$date'";
        $result =  $this->customServicePost($selectCustomersQuery, self::SERVER_7_GET_URI)['data'];
        if(empty($result)){
            return 0;
        }
        return $result;
    }



    public function getCustomers($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_CUSTOMERS_URL);
        return $result['data'];
    }
    public function getItemsPrices($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::GET_ITEMS_PRICES_URL);
        return $result['data'];
    }

    public function getCustomerByEmailAndPhone($email, $phone, $uri = null){
        return $this->getCustomers(array("email"=>$email, "phone" => $phone, "customerType"=>"receipt"));
    }

    public function getCustomerByEmail($email, $uri = null) {

        return $this->getCustomers(array("email"=>$email, "customerType"=>"receipt"));
    }

    public function getErpCustomerByEmail($email, $uri = null){
        $selectCustomerQuery = "SELECT * FROM erp_customers_new where EMAIL='$email' AND XON_LIAN_CODE='02'"; //02 -> ΛΙΑΝΙΚΗΣ
        return $this->customServicePost($selectCustomerQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getCustomerByPhone($phone, $uri = null) {

        return $this->getCustomers(array("phone"=>$phone, "customerType"=>"receipt"));
    }

    public function getErpCustomerByPhone($phone, $uri = null){
        $selectCustomerQuery = "SELECT * FROM erp_customers_new where PHONE1='$phone'";
        return $this->customServicePost($selectCustomerQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function getCustomerByVat($vat, $uri = null) {

        return $this->getCustomers(array("afm"=>$vat, "customerType"=>"invoice"));
    }

    public function getErpCustomerByVat($vatId, $uri = null){
        $selectCustomerQuery = "SELECT MG_CUST_ID FROM erp_customers_new where TIN='$vatId' AND XON_LIAN_CODE='01'"; // 01 -> ΧΟΝΔΡΙΚΗΣ
        return $this->customServicePost($selectCustomerQuery, self::SERVER_7_GET_URI)['data'];
    }

    //Created for task 11219
    public function getProductQuantityFromBranches($SKUS, $shippingMethod = null){
        $skuquery = '';
        foreach($SKUS as $sku) {
            if ($skuquery == '') {
                $skuquery = "SKU = '$sku'";
            } else {
                $skuquery .= " OR SKU = '$sku'";
            }
        }
        // TASK #12247
        // Customer wanted to include only selected branch codes in this select, to dodge issues with new branches which are not about to be included in this mechanism
        // Changed The Select to include only wanted stores. Customer will inform us when a new branch is about to be included in this list.
        if($shippingMethod !== null && stripos($shippingMethod, 'skroutzpoint') !== false){// skroutz lockers (SLM). exclude Chania.
            $selectSKUQTY = "SELECT * FROM erp_item_br_bal_vi where ($skuquery) AND (BR_CODE = '00' OR BR_CODE = '02' OR BR_CODE = '03' OR BR_CODE = '04' OR BR_CODE = '05' OR BR_CODE = '06' OR BR_CODE = '08' OR BR_CODE = '11' OR BR_CODE = '12' OR BR_CODE = '13' OR BR_CODE = '14' OR BR_CODE = '15' OR BR_CODE = '16' OR BR_CODE = '17' OR BR_CODE = '18' OR BR_CODE = '19' OR BR_CODE = '20' OR BR_CODE = '21' OR BR_CODE = '22' OR BR_CODE = '23' OR BR_CODE = '24' OR BR_CODE = '25' OR BR_CODE = '27') ORDER BY SKU, BR_CODE ASC";
        }
        else{
            $selectSKUQTY = "SELECT * FROM erp_item_br_bal_vi where ($skuquery) AND (BR_CODE = '00' OR BR_CODE = '02' OR BR_CODE = '03' OR BR_CODE = '04' OR BR_CODE = '05' OR BR_CODE = '06' OR BR_CODE = '08' OR BR_CODE = '11' OR BR_CODE = '12' OR BR_CODE = '13' OR BR_CODE = '14' OR BR_CODE = '15' OR BR_CODE = '16' OR BR_CODE = '17' OR BR_CODE = '18' OR BR_CODE = '19' OR BR_CODE = '20' OR BR_CODE = '21' OR BR_CODE = '22' OR BR_CODE = '23' OR BR_CODE = '24' OR BR_CODE = '25' OR BR_CODE = '26' OR BR_CODE = '27' OR BR_CODE = '29') ORDER BY SKU, BR_CODE ASC";
        }

        return $this->customServicePost($selectSKUQTY, self::SERVER_7_GET_URI)['data'];
    }
    //Created for task 11219

    public function insertCustomerInErp($customerData){
        $jsonCustomerData = json_encode($customerData);
        return $this->customServicePost($jsonCustomerData, self::SERVER_7_INSERT_URI, 'erp_customers_new');
    }

    public function updateCustomerInErp($customerData){
        $jsonCustomerData = json_encode($customerData);
        return $this->customServicePost($jsonCustomerData, self::SERVER_7_UPDATE_URI, 'erp_customers_new', 'TIN', $customerData['TIN']);
    }

    public function setCustomer($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::SET_DATA);
        return $result['id'];
    }
    public function setOrder($params, $uri = null) {

        $result = $this->servicepost($params, s1rest::SET_DATA);
        return $result['id'];
    }
    public function getOrderById($orderid, $uri = null) {

        return $this->getDocs(array("FINCODE"=>$orderid));
    }
    public function getRdcOrderById($orderid, $uri = null) {

        $result = $this->servicepost(array("orderId"=>$orderid), s1rest::GET_RDC_ORDER_ID);
        return (isset($result['data'])) ? $result['data'] : $result;
    }

    public function insertOrderItemInErp($itemData){
        $jsonItemData = json_encode($itemData);
        return $this->customServicePost($jsonItemData, self::SERVER_7_INSERT_URI, 'web_order_detail');
    }

    public function insertOrderHeaderInErp($orderData){
        $jsonOrderData = json_encode($orderData);
        return $this->customServicePost($jsonOrderData, self::SERVER_7_INSERT_URI, 'web_order_header');
    }

    public function getErpOrder($orderIncrementId, $uri = null){
        $selectOrderQuery = "SELECT MG_ORDER_ID FROM web_order_header where MG_ORDER_NUMBER='$orderIncrementId'";
        return $this->customServicePost($selectOrderQuery, self::SERVER_7_GET_URI)['data'];
    }

    public function isSkroutzOrder($orderData){
        if(stripos($orderData['payment_method'], 'skroutz') !== false){
            return true;
        }
        else{
            return false;
        }
    }

    public function isSLMOrder($orderData){
        if(stripos($orderData['shipping_method'], 'skroutzpoint') !== false){
            return true;
        }
        else{
            return false;
        }
    }

    public function checkAndUpdateSkroutzVoucher($order){
        if(!$this->isSkroutzOrder($order) && !$this->isSLMOrder($order)){
            return;
        }
        $orderIncrementId = $order['increment_id'];
        $selectOrderQuery = "SELECT SKR_VOUCHER FROM web_order_header where MG_ORDER_NUMBER='$orderIncrementId'";
        $erpSkroutzVoucher = $this->customServicePost($selectOrderQuery, self::SERVER_7_GET_URI)['data'];

        if((empty($erpSkroutzVoucher) || empty($erpSkroutzVoucher[0]['SKR_VOUCHER'])) && isset($order['am_skroutz_voucher_pdf_link']) && !empty($order['am_skroutz_voucher_pdf_link'])){
            $dataToUpdate = json_encode(['SKR_VOUCHER' => $order['am_skroutz_voucher_pdf_link']]);
            return $this->customServicePost($dataToUpdate, self::SERVER_7_UPDATE_URI, 'web_order_header', 'MG_ORDER_NUMBER', $orderIncrementId);
        }
    }

    public function getAssociatedProducts($code, $uri = null) {

        return array_column($this->getItems(array("RINGITEM"=>$code)), 'CODE');
    }
    public function getCustomerByTrdr($trdr, $uri = null) {

        return $this->getCustomers(array("TRDR"=>$trdr));
    }
    public function setProductAsUpdated($erpId) {

        //$params = array("runId"=>$this->runId, "products" => array(array("sku"=>$sku, "success" => "1")));
		$params = array("runId"=>$this->runId, "data" => array(array("erpId"=>$erpId, "success" => "1")));
        $result = $this->servicepost($params, s1rest::SET_RDC_ITEMS_UPDATE_SUCCESS);
        return true;
    }
    public function setStockAsUpdated($code) {

        $params = array("runId"=>$this->runId,"data" => array(array("code"=>$code, "success" => "1")));
        $result = $this->servicepost($params, s1rest::SET_RDC_STOCK_UPDATE_SUCCESS);
        return true;
    }
    public function setOrderStatusAsUpdated($CCCWEBORDERID) {

        $params = array("runId"=>$this->runId, "data" => array(array("orderId"=>$CCCWEBORDERID, "success" => "1")));
        $result = $this->servicepost($params, s1rest::SET_RDC_ORDER_STATUS_UPDATE_SUCCESS);
        return true;
    }
    public function getBrowserData($object, $filterid, $filtervalue) {

        $filters =  "{$object}.{$filterid}={$filtervalue}&{$object}.{$filterid}_to={$filtervalue}";
        $params = array("service"=>'getBrowserInfo', "OBJECT" => $object, "FILTERS"=>$filters); //CUSTOMER.EMAIL=eleni27101971@gmail.com&CUSTOMER.EMAIL_to=eleni27101971@gmail.com
        $result = $this->servicepost($params, s1rest::GET_DATA);
        if ($result['reqID']) {
            $params = array("service"=>'getBrowserData', 'reqID'=>$result['reqID']);
            $result = $this->servicepost($params, s1rest::GET_DATA);
            return $result['rows'];
        }
        return false;
    }
    public function disableCustomer($erpid){
        //"implement here a method to SET data and return true or ERROR"
        if ($erpid) {
            $erpcustomer = $this->getCustomerByTrdr($erpid);

            $customerdata = array("ISACTIVE"=>"0", "PHONE01"=>"", "PHONE02" => "", "EMAIL" => "", "CCCFXCUSAPPRV"=>"-2");

            $params = array("service"=>"setData", "OBJECT"=>"CUSTOMER", "KEY"=> $erpid);
            $params['DATA']['CUSTOMER'] = array($customerdata);

            $trdr = $this->setCustomer($params);
            return $trdr;
        } else {
            $this->logError('no customer found ' .$erpid);
        }
    }

    /**
     * split products from configurable to simple products
     *
     * @param $products
     * @return array
     */

    public function filterProduct($data){
        $filteredData = array_intersect_key($data, $this->attributes);
        return $filteredData;
    }

    public function getLatestDuplicateAndDiscardOthers($line){
        if ($this->datatable && $this->sqllink) {
            $entity_id = $line['entity_id'];
            $query = "SELECT * FROM `{$this->datatable}` WHERE entity_id = '$entity_id' and (`status`= 0 OR status = 2) ORDER BY id desc limit 1";
            $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
            }

            $row = mysqli_fetch_assoc($sql);

            if (count($row) and $row['id']) {
                $line = $row; //update with latest row
                $query = "UPDATE `{$this->datatable}` SET `status`= 4 WHERE entity_id = '$entity_id' and (status = 0 OR status = 2) and id < {$row['id']};";
                $sql = mysqli_query($this->sqllink,$query); //reconnects if server is down

                if (!$sql) {
                    $this->logError("Failed to save to MySQL: " . mysqli_error($this->sqllink)); return false;
                }
            }

            return $line;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sqllink) . "<br>\n");
        return 0;
    }

    public function areDataChangedFromLastUpdate($line){

        $entity_id = $line['entity_id'];
        $query = "SELECT * FROM `{$this->datatable}` WHERE entity_id = '$entity_id' and status = 1 and id < {$line['id']} ORDER BY id desc limit 1";
        $sql = mysqli_query($this->sqllink,$query); //select previous succesful entry

        $row = mysqli_fetch_assoc($sql);

        $ignorefromcompare = array('updated_at');

        $newdata = json_decode($line['data'], true);
        foreach ($ignorefromcompare as $ignore) {unset($newdata[$ignore]);}

        $olddata = json_decode($row['data'], true);
        foreach ($ignorefromcompare as $ignore) {unset($olddata[$ignore]);}

        if (json_encode($olddata) == json_encode($newdata)) return false;
        else return true;


    }

    public function optimizeTable($table, $days = 20) {

        $queries[] = "DELETE FROM $table WHERE date_created < NOW() - INTERVAL $days DAY; ";
        $queries[] = "OPTIMIZE TABLE $table; ";

        foreach ($queries as $query) {
            $sql = mysqli_query($this->sqllink, $query);//reconnects if server is down
            if (!$sql) {
                //check if query returned false
                $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
                $message .= 'Whole query: ' . $query;
                $this->logError($message);
                return 0; //return error
            }
        }

        return 1;

    }

    public function greeklish($string) {
        return strtr($string, array(
            'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
            'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'X', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'U', 'Φ' => 'F',
            'Χ' => 'H', 'Ψ' => 'PS', 'Ω' => 'O',
            'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
            'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'x', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
            'σ' => 's', 'τ' => 't', 'υ' => 'u', 'φ' => 'f', 'χ' => 'h', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
            'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'u', 'ώ' => 'o',
            'ϊ' => 'i', 'ϋ' => 'u',
            'ΐ' => 'i', 'ΰ' => 'u',
            'Ά' => 'A', 'Έ' => 'E', 'Ή' => 'I', 'Ό' => 'O', 'Ύ' => 'U', 'Ϋ' => 'U', 'Ώ' => 'O'
        ));
    }

    public function makeCodeFromString($code) {
        $code = $this->greeklish($code);
        $code = preg_replace("/[^A-Za-z0-9 ]/", ' ', $code); //convert to spaces all non-alpha and spaces
        //$brand = ucwords(strtolower($brand)); //make it camel case
        $code = str_replace(" ", "-", $code); //replace spaces with dash
        $code = strtolower($code);
        return $code ;
    }

    public function mapProductCodeToCustomProductType($productCode){
        $parsedProductCode = explode(".", $productCode);
        $productTypeDigit = intval(substr_replace($parsedProductCode[1], "", 1));//keep only the first digit after the first dot which indicates the product type (blouse, shirt etc.)
        $mappingArray = [
          1 => 5553,    //blouse
          2 => 5554,    //trousers
          3 => 5555,    //vest-shirt
          4 => 5556,    //jacket
          5 => 5557,    //knitted
          6 => 5558,    //skirt
          7 => 5559,    //dress
          9 => 5560     //accessories
        ];
        return $mappingArray[$productTypeDigit];
    }

    public function emergencyExit(){
        $this->logError('erp BAD CONNECTION. Exiting and retrying later. ');
        $this->allowRun(); //make function emergencyExit
        $this->close();
        exit;
    }

    public function getSpecialPrice($productData, $now){
        if(isset($productData['ELG_SP_PRICE_FROM']) && isset($productData['ELG_SP_PRICE_TO']) && $now > $productData['ELG_SP_PRICE_FROM'] && $now < $productData['ELG_SP_PRICE_TO']){
            return isset($productData['SPECIAL_PRICE']) ? $productData['SPECIAL_PRICE'] : $productData['FINAL_RETAIL'];
        }
        else{
            return $productData['FINAL_RETAIL'];
        }
    }

    public function getMaxUpddates($products){ //function in order to avoid duplicate products
        $productArrayWithMaxUpddate = array();
        foreach ($products as $index => $product){
            if(isset($productArrayWithMaxUpddate[$product['ELG_SKU']])){
                if($productArrayWithMaxUpddate[$product['ELG_SKU']]['max_upddate'] < $product['ELG_ATTR_UPD_DATE']){
                    $productArrayWithMaxUpddate[$product['ELG_SKU']] = array('index' => $index, 'max_upddate' => $product['ELG_ATTR_UPD_DATE']);
                }
            }
            else{
                $productArrayWithMaxUpddate[$product['ELG_SKU']] = array('index' => $index, 'max_upddate' => $product['ELG_ATTR_UPD_DATE']);
            }
        }
        return $productArrayWithMaxUpddate;
    }
}
