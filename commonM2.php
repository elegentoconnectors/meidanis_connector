<?php

class m2 {
    const STATUSUNPROSSESSED = 0;
    const STATUSSUCCESFULL = 1;
    const STATUSRETRY = 2;
    const STATUSERROR = 3;
    
    public $config = null;
    public $proxy = null;
    public $sessionId = null;
    public $sql_link = null;
    public $items = null;
    public $item = null;
    public $attributesets;
    public $atttibutesmapping = null;
    public $languages;
	public $stores;
    public $error_obj_id = null;
    public $errors = null;
    public $errorlogfile = null;
    public $fileruntime = null;
    public $debugmailsubject = null;
    public $debugfile = null;
    public $productConfigFile = null;
    public $jsondefaultdatafile = null;
    public $debugmode = false;
    public $indexeddata = array();
    public $categoriesdata = array();
    public $filterSizeAttributeCode = 'fsize';
    public $categoriesMapping = array();
    public $categoriesWithNoErpId = array();

    
    public $entity = null; //i.e ProductImport
    public $table = null; //i.e connector_products
    public $blockingentities = array(); //entities that should block running of this connector. .ie stock and products import 
    
    //maRest
    public  $url;
    public  $baseurl;
    public  $token;
    public  $user;
    public  $password;
    public  $headers = array('Content-Type:application/json');
    public  $storecode = 'all';  
    
    
    public $filestop = "stop";
    public $maxtimewait = "600"; //seconds
    public $retries = 2;

    public $attributes = [
        'ELG_NAME' => [
            'magento_attribute_code' => 'name',
            'type' => 'text'
        ],
        'ELG_SKU' => [
            'magento_attribute_code' => 'sku',
            'type' => 'text'
        ],
        'Barcode' => [
            'magento_attribute_code' => 'barcode',
            'type' => 'text'
        ],
        'RETAIL_PRICE' => [
            'magento_attribute_code' => 'price',
            'type' => 'price'
        ],
        'FINAL_RETAIL' => [
            'magento_attribute_code' => 'final_retail',
            'type' => 'price'
        ],
        'PACKAGE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'package',
            'type' => 'text'
        ],
        'MEASURMENT_UNIT' => [
            'magento_attribute_code' => '',
            'fake_code' => 'measurement',
            'type' => 'text'
        ],
        'X001' => [
            'magento_attribute_code' => 'manufacturer',
            'type' => 'dropdown'
        ],
        'X002' => [
            'magento_attribute_code' => 'eidos',
            'type' => 'dropdown'
        ],
        'X003' => [
            'magento_attribute_code' => 'leitourgia',
            'type' => 'dropdown'
        ],
        'X004' => [
            'magento_attribute_code' => 'typos',
            'type' => 'dropdown'
        ],
        'X005' => [
            'magento_attribute_code' => 'fcolor',
            'type' => 'dropdown'
        ],
        'X006' => [
            'magento_attribute_code' => 'xrhsh',
            'type' => 'dropdown'
        ],
        'X008' => [
            'magento_attribute_code' => 'oikogeneia',
            'type' => 'dropdown'
        ],
        'X010' => [
            'magento_attribute_code' => 'manufacturer_color',
            'type' => 'text'
        ],
        'X011' => [
            'magento_attribute_code' => 'yliko_kataskeyhs',
            'type' => 'dropdown'
        ],
        'X012' => [
            'magento_attribute_code' => 'technologia',
            'type' => 'dropdown'
        ],
        'X013' => [
            'magento_attribute_code' => 'topothethsh',
            'type' => 'dropdown'
        ],
        'X014' => [
            'magento_attribute_code' => 'typos_mixanismou',
            'type' => 'dropdown'
        ],
        'X016' => [
            'magento_attribute_code' => 'onomastiki_entasi',
            'type' => 'price'
        ],
        'X021' => [
            'magento_attribute_code' => 'ikanothta_diakophs',
            'type' => 'dropdown'
        ],
        'X022' => [
            'magento_attribute_code' => 'eidos_kataskeuasti',
            'type' => 'dropdown'
        ],
        'X025' => [
            'magento_attribute_code' => 'tasi_leitourgias',
            'type' => 'dropdown'
        ],
        'X026' => [
            'magento_attribute_code' => 'onomastikh_tash',
            'type' => 'dropdown'
        ],
        'X027' => [
            'magento_attribute_code' => 'euros_tasis',
            'type' => 'text'
        ],
        'X029' => [
            'magento_attribute_code' => 'typos_tashs',
            'type' => 'dropdown'
        ],
        'X032' => [
            'magento_attribute_code' => 'onomastiki_isxis',
            'type' => 'price'
        ],
        'X033' => [
            'magento_attribute_code' => 'onomastiki_isxis_watt_m',
            'type' => 'price'
        ],
        'X034' => [
            'magento_attribute_code' => 'megisth_epitrepomenh_isxys',
            'type' => 'dropdown'
        ],
        'X036' => [
            'magento_attribute_code' => 'synolikh_isxys',
            'type' => 'dropdown'
        ],
        'X043' => [
            'magento_attribute_code' => 'poloi',
            'type' => 'dropdown'
        ],
        'X044' => [
            'magento_attribute_code' => 'faseis',
            'type' => 'dropdown'
        ],
        'X045' => [
            'magento_attribute_code' => 'prostasia_ypertashs',
            'type' => 'dropdown'
        ],
        'X047' => [
            'magento_attribute_code' => 'reuma_diarrohs',
            'type' => 'dropdown'
        ],
        'X048' => [
            'magento_attribute_code' => 'megisto_vraxykyklwma',
            'type' => 'dropdown'
        ],
        'X050' => [
            'magento_attribute_code' => 'isxys_metasximatisti',
            'type' => 'price'
        ],
        'X053' => [
            'magento_attribute_code' => 'rythmish_xronou',
            'type' => 'dropdown'
        ],
        'X058' => [
            'magento_attribute_code' => 'boithitikes_epafes',
            'type' => 'dropdown'
        ],
        'X059' => [
            'magento_attribute_code' => 'kampylh',
            'type' => 'dropdown'
        ],
        'X060' => [
            'magento_attribute_code' => 'gwnia_metagwghs',
            'type' => 'dropdown'
        ],
        'X066' => [
            'magento_attribute_code' => 'diaforikh_prostasia',
            'type' => 'dropdown'
        ],
        'X067' => [
            'magento_attribute_code' => 'endeiktiko',
            'type' => 'dropdown'
        ],
        'X070' => [
            'magento_attribute_code' => 'epafes',
            'type' => 'dropdown'
        ],
        'X071' => [
            'magento_attribute_code' => 'epektasimothta',
            'type' => 'dropdown'
        ],
        'X073' => [
            'magento_attribute_code' => 'euaisthisia',
            'type' => 'dropdown'
        ],
        'X075' => [
            'magento_attribute_code' => 'autonomia',
            'type' => 'dropdown'
        ],
        'X076' => [
            'magento_attribute_code' => 'emveleia',
            'type' => 'dropdown'
        ],
        'X077' => [
            'magento_attribute_code' => 'theseis',
            'type' => 'dropdown'
        ],
        'X078' => [
            'magento_attribute_code' => 'theseis_ana_seira',
            'type' => 'dropdown'
        ],
        'X079' => [
            'magento_attribute_code' => 'platos_se_stoixeia',
            'type' => 'dropdown'
        ],
        'X081' => [
            'magento_attribute_code' => 'seires',
            'type' => 'price'
        ],
        'X082' => [
            'magento_attribute_code' => 'vash_topothethshs',
            'type' => 'text'
        ],
        'X083' => [
            'magento_attribute_code' => 'sthles',
            'type' => 'price'
        ],
        'X084' => [
            'magento_attribute_code' => 'grammes',
            'type' => 'price'
        ],
        'X086' => [
            'magento_attribute_code' => 'stoixeia',
            'type' => 'text'
        ],
        'X090' => [
            'magento_attribute_code' => 'kleidaria',
            'type' => 'text'
        ],
        'X093' => [
            'magento_attribute_code' => 'porta',
            'type' => 'dropdown'
        ],
        'X094' => [
            'magento_attribute_code' => 'typos_portas',
            'type' => 'dropdown'
        ],
        'X096' => [
            'magento_attribute_code' => 'diastaseis',
            'type' => 'dropdown'
        ],
        'X097' => [
            'magento_attribute_code' => 'vathos',
            'type' => 'dropdown'
        ],
        'X098' => [
            'magento_attribute_code' => 'c_weight',
            'type' => 'dropdown'
        ],
        'X100' => [
            'magento_attribute_code' => 'mhkos',
            'type' => 'price'
        ],
        'X101' => [
            'magento_attribute_code' => 'mhkos_kalwdiou',
            'type' => 'dropdown'
        ],
        'X105' => [
            'magento_attribute_code' => 'paxos',
            'type' => 'dropdown'
        ],
        'X106' => [
            'magento_attribute_code' => 'width',
            'type' => 'price'
        ],
        'X107' => [
            'magento_attribute_code' => 'ypsos',
            'type' => 'price'
        ],
        'X109' => [
            'magento_attribute_code' => 'diameter',
            'type' => 'price'
        ],
        'X111' => [
            'magento_attribute_code' => 'ekswterikh_diametros',
            'type' => 'text'
        ],
        'X112' => [
            'magento_attribute_code' => 'eswterikh_diametros',
            'type' => 'text'
        ],
        'X113' => [
            'magento_attribute_code' => 'diametros_ophs_topothethshs',
            'type' => 'dropdown'
        ],
        'X114' => [
            'magento_attribute_code' => 'diatomh',
            'type' => 'dropdown'
        ],
        'X116' => [
            'magento_attribute_code' => 'yliko_agwgou',
            'type' => 'dropdown'
        ],
        'X118' => [
            'magento_attribute_code' => 'typos_monwshs',
            'type' => 'dropdown'
        ],
        'X119' => [
            'magento_attribute_code' => 'ekswterikos_mandyas',
            'type' => 'dropdown'
        ],
        'X122' => [
            'magento_attribute_code' => 'typos_agwgou',
            'type' => 'dropdown'
        ],
        'X123' => [
            'magento_attribute_code' => 'xrwma_kalwdiou',
            'type' => 'dropdown'
        ],
        'X126' => [
            'magento_attribute_code' => 'arithmos_agwgwn_kalwdiou',
            'type' => 'dropdown'
        ],
        'X128' => [
            'magento_attribute_code' => 'kalwdio',
            'type' => 'text'
        ],
        'X129' => [
            'magento_attribute_code' => 'kalwdiwsi',
            'type' => 'text'
        ],
        'X130' => [
            'magento_attribute_code' => 'karouli',
            'type' => 'dropdown'
        ],
        'X131' => [
            'magento_attribute_code' => 'kathgoria_kalwdiwn',
            'type' => 'dropdown'
        ],
        'X134' => [
            'magento_attribute_code' => 'typos_thermanshs',
            'type' => 'dropdown'
        ],
        'X135' => [
            'magento_attribute_code' => 'typos_lampthrwn_thermansis',
            'type' => 'text'
        ],
        'X136' => [
            'magento_attribute_code' => 'klimakes_thermanshs',
            'type' => 'price'
        ],
        'X141' => [
            'magento_attribute_code' => 'me_boiler',
            'type' => 'dropdown'
        ],
        'X142' => [
            'magento_attribute_code' => 'me_ionisth',
            'type' => 'dropdown'
        ],
        'X143' => [
            'magento_attribute_code' => 'kalypsh_xwrou',
            'type' => 'dropdown'
        ],
        'X144' => [
            'magento_attribute_code' => 'litra',
            'type' => 'dropdown'
        ],
        'X145' => [
            'magento_attribute_code' => 'thlexeirismos',
            'type' => 'dropdown'
        ],
        'X146' => [
            'magento_attribute_code' => 'tilecheiristirio',
            'type' => 'dropdown'
        ],
        'X147' => [
            'magento_attribute_code' => 'programma',
            'type' => 'dropdown'
        ],
        'X149' => [
            'magento_attribute_code' => 'arithmos_pterygiwn',
            'type' => 'dropdown'
        ],
        'X150' => [
            'magento_attribute_code' => 'wifi',
            'type' => 'dropdown'
        ],
        'X152' => [
            'magento_attribute_code' => 'taxythta_diktyou',
            'type' => 'dropdown'
        ],
        'X154' => [
            'magento_attribute_code' => 'thyres',
            'type' => 'dropdown'
        ],
        'X157' => [
            'magento_attribute_code' => 'katagrafh',
            'type' => 'dropdown'
        ],
        'X158' => [
            'magento_attribute_code' => 'kit',
            'type' => 'text'
        ],
        'X159' => [
            'magento_attribute_code' => 'zwnes',
            'type' => 'dropdown'
        ],
        'X160' => [
            'magento_attribute_code' => 'megala_plhktra',
            'type' => 'dropdown'
        ],
        'X163' => [
            'magento_attribute_code' => 'thlefwnhths',
            'type' => 'dropdown'
        ],
        'X166' => [
            'magento_attribute_code' => 'gwnia_anyxneushs',
            'type' => 'dropdown'
        ],
        'X167' => [
            'magento_attribute_code' => 'beam_angle',
            'type' => 'dropdown'
        ],
        'X171' => [
            'magento_attribute_code' => 'othoni',
            'type' => 'dropdown'
        ],
        'X173' => [
            'magento_attribute_code' => 'arithmos_diamerismatwn',
            'type' => 'price'
        ],
        'X175' => [
            'magento_attribute_code' => 'yliko_kataskeyis_mpoytonieras',
            'type' => 'text'
        ],
        'X176' => [
            'magento_attribute_code' => 'topothethsh_mpoytonieras',
            'type' => 'dropdown'
        ],
        'X177' => [
            'magento_attribute_code' => 'fwtizomenh_othonh',
            'type' => 'text'
        ],
        'X180' => [
            'magento_attribute_code' => 'xronodiakopths',
            'type' => 'dropdown'
        ],
        'X181' => [
            'magento_attribute_code' => 'trofodosia',
            'type' => 'dropdown'
        ],
        'X182' => [
            'magento_attribute_code' => 'xronoprogramma',
            'type' => 'dropdown'
        ],
        'X183' => [
            'magento_attribute_code' => 'elaxistos_xronos_programm',
            'type' => 'dropdown'
        ],
        'X190' => [
            'magento_attribute_code' => 'metatroph_se',
            'type' => 'dropdown'
        ],
        'X193' => [
            'magento_attribute_code' => 'typos_lampthra',
            'type' => 'dropdown'
        ],
        'X194' => [
            'magento_attribute_code' => 'kalykas',
            'type' => 'dropdown'
        ],
        'X196' => [
            'magento_attribute_code' => 'arithmos_lampthrwn',
            'type' => 'dropdown'
        ],
        'X200' => [
            'magento_attribute_code' => 'xrwma_fwtos',
            'type' => 'dropdown'
        ],
        'X201' => [
            'magento_attribute_code' => 'color_temperature',
            'type' => 'price'
        ],
        'X202' => [
            'magento_attribute_code' => 'lumen',
            'type' => 'price'
        ],
        'X204' => [
            'magento_attribute_code' => 'xrwma_lampthrwn',
            'type' => 'dropdown'
        ],
        'X206' => [
            'magento_attribute_code' => 'dimmable',
            'type' => 'dropdown'
        ],
        'X210' => [
            'magento_attribute_code' => 'energeiakh_klash',
            'type' => 'text'
        ],
        'X211' => [
            'magento_attribute_code' => 'enswmatwmeno_fwtistiko',
            'type' => 'dropdown'
        ],
        'X212' => [
            'magento_attribute_code' => 'diakopths',
            'type' => 'dropdown'
        ],
        'X219' => [
            'magento_attribute_code' => 'typos_mpatarias',
            'type' => 'dropdown'
        ],
        'X220' => [
            'magento_attribute_code' => 'tash_mpatarias',
            'type' => 'dropdown'
        ],
        'X224' => [
            'magento_attribute_code' => 'autokollhto',
            'type' => 'dropdown'
        ],
        'X226' => [
            'magento_attribute_code' => 'syndesh',
            'type' => 'dropdown'
        ],
        'X228' => [
            'magento_attribute_code' => 'typos_myths',
            'type' => 'dropdown'
        ],
        'X231' => [
            'magento_attribute_code' => 'eggyhsh',
            'type' => 'text'
        ],
        'X232' => [
            'magento_attribute_code' => 'metroumena_megethi',
            'type' => 'dropdown'
        ],
        'X233' => [
            'magento_attribute_code' => 'megethos',
            'type' => 'dropdown'
        ],
        'X241' => [
            'magento_attribute_code' => 'logos_syrriknwshs',
            'type' => 'dropdown'
        ],
        'X244' => [
            'magento_attribute_code' => 'antoxh_sth_sympiesh',
            'type' => 'dropdown'
        ],
        'X246' => [
            'magento_attribute_code' => 'antoxh_sth_ghransh',
            'type' => 'text'
        ],
        'X247' => [
            'magento_attribute_code' => 'megistos_arithmos_krousewn',
            'type' => 'text'
        ],
        'X248' => [
            'magento_attribute_code' => 'vathmos_steganothtas',
            'type' => 'price'
        ],
        'X249' => [
            'magento_attribute_code' => 'antimikroviakh_prostasia',
            'type' => 'text'
        ],
        'X250' => [
            'magento_attribute_code' => 'den_diadidei_floga',
            'type' => 'text'
        ],
        'X251' => [
            'magento_attribute_code' => 'antoxh_sth_ghransh',
            'type' => 'text'
        ],
        'X252' => [
            'magento_attribute_code' => 'meiwmenes_trives',
            'type' => 'text'
        ],
        'X253' => [
            'magento_attribute_code' => 'typos_ergaleiou',
            'type' => 'dropdown'
        ],
        'X265' => [
            'magento_attribute_code' => 'ischus_fotoboltaikou',
            'type' => 'price'
        ],
        'X266' => [
            'magento_attribute_code' => 'diastaseis_fotoboltaikou',
            'type' => 'text'
        ],
        'ELG_ITEM_ID' => [
            'magento_attribute_code' => 'erp_id',
            'type' => 'text'
        ],
        'FREE_QTY' => [
            'magento_attribute_code' => 'qty',
            'type' => 'text'
        ],

        'RECYCLE_TAX' => [
            'magento_attribute_code' => '',
            'fake_code' => 'fpt',
            'type' => 'fpt'
        ],
        'ELG_ITEM_DSCNT_ZONE_ID' => [
            'magento_attribute_code' => '',
            'fake_code' => 'discount_zone_id',
            'type' => 'text'
        ],
        'CAT_TREE_CODE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'erp_tree_code',
            'type' => 'text'
        ],
        'PURCHASE_BLOCK_STATUS' => [
            'magento_attribute_code' => '',
            'fake_code' => 'purchase_block_status',
            'type' => 'text'
        ],
        'SALES_BLOCK_STATUS' => [
            'magento_attribute_code' => '',
            'fake_code' => 'sales_block_status',
            'type' => 'text'
        ],
        'Is_Web' => [
            'magento_attribute_code' => '',
            'fake_code' => 'is_web',
            'type' => 'text'
        ],
        'Is_Web_Active' => [
            'magento_attribute_code' => '',
            'fake_code' => 'is_web_active',
            'type' => 'text'
        ],
        'ELG_UPD_DATE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'erp_upd_date',
            'type' => 'text'
        ],
        'Supplier_Item_code' => [
            'magento_attribute_code' => 'mpn',
            'type' => 'text'
        ],
        'ITEM_WEIGTH_KGR' => [
            'magento_attribute_code' => 'weight',
            'type' => 'text'
        ],
        'tier_prices' => [
            'magento_attribute_code' => '',
            'fake_code' => 'tier_prices',
            'type' => 'text'
        ],
        'ITEM_WEB_DESCRIPTION' => [
            'magento_attribute_code' => '',
            'fake_code' => 'item_web_description',
            'type' => 'text'
        ],
        'ITEM_MAX_DISC' => [
            'magento_attribute_code' => 'max_discount',
            'type' => 'price'
        ],
        'special_price_to_insert' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_to_insert',
            'type' => 'price'
        ],
        'SPECIAL_PRICE' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_erp',
            'type' => 'price'
        ],
        'ELG_SP_PRICE_FROM' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_from',
            'type' => 'price'
        ],
        'ELG_SP_PRICE_TO' => [
            'magento_attribute_code' => '',
            'fake_code' => 'special_price_to',
            'type' => 'price'
        ],
        'LEAD_TIME' => [
            'magento_attribute_code' => 'lead_time',
            'type' => 'text'
        ]
    ];

    public $customAttributes;
    
    
    public function initConfig(){
        $this->errorlogfile = 'errors/' .$this->entity .'Errors.log';
        $this->fileruntime = 'running/' .$this->entity .'Running.txt';
        $this->debugmailsubject = ' Connector Error - ' .$this->entity;
        $this->debugfile = 'debug/' .$this->entity .'.txt';
        $this->jsondefaultdatafile = 'confs/' .$this->entity .'.conf';
    } 
    
    public function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
        {
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        }
    
        return $out;
    }

    public function array_to_xml(array $arr, SimpleXMLElement $xml, $parent)
    {
        foreach ($arr as $k => $v) {
            if (is_int($k)) {
                $k=substr($parent, 0, -1); //remove s (last digit) eg items => item
            }

            is_array($v)
                ? $this->array_to_xml($v, $xml->addChild($k), $k)
                : $xml->addChild($k, htmlspecialchars($v,ENT_QUOTES,'UTF-8'));
        }
        return $xml;
    }
    
    public function readConf($filename) {
        $sxml=simplexml_load_file($filename);
        if (!$sxml)
        {
            $this->logError('Not Valid XML document');
            return 0;
        }
        $xmlarray=$this->xml2array($sxml); //convert to array
        $this->config = $xmlarray;
        $this->baseurl = rtrim($xmlarray['mage_url'], '/');
        $this->setRestUrlByStoreCode('all');
        $this->user = $xmlarray['mage_user'];
        $this->password = $xmlarray['token'];
        $this->debugmode = (bool)$xmlarray['debugmode'];
        $this->token = $xmlarray['token'];
        return $xmlarray;
    }

    public function  setRestUrlByStoreCode($storecode) {
        $this->url =  $this->baseurl ."/rest/" .$storecode ."/V1/";
    }

    public function getRestUrlByStoreCode(){
        $urlExploded = explode("rest/", $this->url);
        $storeCodePart = explode("/", $urlExploded[1]);
        $storeCode = $storeCodePart[0];
        return $storeCode;
    }
    
    public function getJsonDefaultCategoryData() {
    
        $json = file_get_contents($this->jsondefaultdatafile);
        $data = json_decode($json, true);
        return $data;
    }
    
    public function init_connector() {
     return $this->connect();      
    }
    
    public function init_db() {
        ob_implicit_flush(TRUE);
        try {
            $sqlhost = $this->config['sqlhost'];
            $username = $this->config['username'];
            $password = $this->config['password'] ?  $this->config['password'] : null;
            $database = $this->config['sqltable'];
            
            $this->sql_link = mysqli_connect($sqlhost,$username, $password,$database) or $this->close( "Unable to connect to database");
             mysqli_query($this->sql_link,"SET NAMES 'utf8'");
            //@mysql_select_db($config['magesqltable']) or close( "Unable to select database"); //just test if table of magento is selectable
            //@mysql_select_db($config['sqltable']) or close( "Unable to select database"); //this is the default table
           // select_mage_sql();
           // select_connector_sql(); //this should be last
            
    
            return 1;
        } catch( SoapFault $fault ) {
            $this->logError($fault);
            return 0;
        }
    
    }

    public function close($message = null) {

       // unlink($this->fileruntime); // do not delete here. Delete should be done only if actually running

        if (!is_null($message))
            $this->logError($message);
        //unlink($fileruntime); //delete runtime file
        if (!is_null($this->errors))
            $this->senderrormail(); //send (if any) error to mail
        die($message);
    }
    
    public function close_connector($message = null){

        if ($this->sql_link)
           $sql = mysqli_close($this->sql_link);  
        
        $this->close($message);
    }
    
    public function logError ($message) {
        
        //adds a message error allong with date
        $date = gmdate(DATE_RFC822);
        @file_put_contents($this->errorlogfile, $date .' ' .$message ."\n", FILE_APPEND | LOCK_EX);
        $this->debug($date .' ' .$message);
        echo $message ."<br>";
        $this->errors = $this->errors .$date .' ' .$message ."\n obj_id = " .$this->error_obj_id .'\n'; //log error
    }
    
    public function senderrormail() {
        
        $date = gmdate(DATE_RFC822);
        // In case any of our lines are larger than 70 characters, we should use wordwrap()
        //$message = wordwrap($errors, 70);
        $sendto = $this->config['mail']; //default users
        if (strpos($this->errors,'Link product does not exist') === false && strpos($this->errors,'The Product with the "%1" SKU doesn\'t exist.') === false && strpos($this->errors,'previous synchronization is running.') === false && strpos($this->errors,'Product has is_web = 0 and does not already exist in magento. Skip it.') === false) { //ignore if these errors
            if (strpos($this->errors,'Bad Gateway') !== false || strpos($this->errors,'Time-out') !== false || strpos($this->errors,'Fetching') !== false || strpos($this->errors,'Deadlock') !== false || strpos($this->errors,'Internal') !== false || strpos($this->errors,'Parsing') !== false || strpos($this->errors,'previous') !== false || strpos($this->errors,'unrecoverable') !== false ) {
                    $sendto = $this->config['debugmail']; //send this error only to admin
                }
            mail($sendto, $this->config['site'] .$this->debugmailsubject, $this->errors ."\n" .$date .'\n ');
        }
        $this->errors = null; //flush error variable
    }

    public function debug($msg, $init = false) {
        $msg = gmdate(DATE_RFC822, time()) .' ' .$msg ."\n";
        file_put_contents($this->debugfile, $msg , $init ? null : FILE_APPEND | LOCK_EX);
        echo $msg ."<br>";
    }
    
    public function check_mysql($sqltable = null, $sqlhost = null, $username = null, $password = null) {
        
        $sqlhost = $sqlhost ? $sqlhost : $this->config['sqlhost'];
        $username = $username ? $username : $this->config['username'];
        $password = $password ? $password : ($this->config['password'] ?  $this->config['password'] : null);
        $sqltable = $sqltable ? $sqltable : $this->config['sqltable'];
        
        
            $this->sql_link = mysqli_connect($sqlhost,$username,$password, $sqltable); //if connection is allready open. it just returns the same identifier
            mysqli_query($this->sql_link,"SET NAMES 'utf8'");
            //@mysql_select_db($sqltable);
        
        return $this->sql_link;
    }
    
    public function saveCustomerstoDB($xmlstring, $increment_id) {
        
        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
    
        $query = "INSERT INTO `connector_customers` (`id`, `date_created`, `entity_id`, `data`, `retries`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$increment_id' , '$xmlstring','0','0');";    
        
        $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
        if (!$sql) {
            //check if query returned false
            $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
            $message .= 'Whole query: ' . $query;
            $this->logError($message);
            return 0; //return error
        }
    
        return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    
    
    }
    
    public function saveOrdertoDB($jsonString, $increment_id) {
        
        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
            $jsonString = mysqli_real_escape_string($this->sql_link, $jsonString);
        $query = "INSERT INTO `connector_orders` (`id`, `date_created`, `entity_id`, `data`, `retries`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$increment_id' , '$jsonString','0','0');";
        $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
        if (!$sql) {
            //check if query returned false
            $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
            $message .= 'Whole query: ' . $query;
            $this->logError($message);
            return 0; //return error
        }
    
        return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    
    }
    
    public function setXmlDataStatus($id ,$status) {
        
        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
    
            $query = "UPDATE `{$this->table}` SET `date_updated`=CURRENT_TIMESTAMP, `status` = '$status' WHERE `id` = $id;";        
            $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
            if (!$sql) {
                //check if query returned false
                $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
                $message .= 'Whole query: ' . $query;
                $this->logError($message);
                return 0; //return error
            }
    
            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    
    }
    
    public function setXmlDataRetries($id ,$retries) {
        
        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
    
            $query = "UPDATE `{$this->table}` SET `date_updated`=CURRENT_TIMESTAMP, `retries` = '$retries' WHERE `id` = $id;";
            $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
            if (!$sql) {
                //check if query returned false
                $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
                $message .= 'Whole query: ' . $query;
                $this->logError($message);
                return 0; //return error
            }
    
            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    
    }
    
    public function setXmlDataError($id ,$error) {
    
        $error = mysqli_real_escape_string($this->sql_link, $error);
        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
    
            $query = "UPDATE `{$this->table}` SET `errorlog` = '$error' WHERE `id` = $id;";      
            $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
            if (!$sql) {
                //check if query returned false
                $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
                $message .= 'Whole query: ' . $query;
                $this->logError($message);
                return 0; //return error
            }
    
            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    }
    
    public function getUnproccessedJsonData($status = 0) {

        $date = date('Y-m-d H:i:s');
    //  if ($table) {
    
        $query = "SELECT * FROM `{$this->table}` WHERE `status`= $status ORDER BY id";
        //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
        $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
        if (!$sql) {
            //check if query returned false
            $message  = 'Invalid query: ' .mysqli_error($this->sql_link) . "<br>\n";
            $message .= 'Whole query: ' . $query;
            $this->logError($message);
            return 0; //return error
        }
        $rows=array();
        while ($row = mysqli_fetch_assoc($sql)) {
            $rows[]=$row;
        }
        return $rows;
        //}
        //logError("Please provide a table var" . "<br>\n");
        //return 0;
    
    }
    
    public function getReproccessedJsonData() {
       
        $date = date('Y-m-d H:i:s');
        //  if ($table) {
        
    
        $query = "SELECT * FROM `{$this->table}` WHERE `status` = 2 AND  `retries` <= {$this->retries}  ORDER BY id";
        //$query = "INSERT INTO `keepfred`.`connector_orders` (`id`, `date_created`, `date_updated`, `entity_id`, `data`, `status`) VALUES (NULL, CURRENT_TIMESTAMP, '$date','$increment_id' , '$xmlstring','0');";
        $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
    
        if (!$sql) {
            //check if query returned false
            $message  = 'Invalid query: ' .mysqli_error($this->sql_link) . "<br>\n";
            $message .= 'Whole query: ' . $query;
            $this->logError($message);
            return 0; //return error
        }
        $rows=array();
        while ($row = mysqli_fetch_assoc($sql)) {
            $rows[]=$row;
        }
        return $rows;
        //}
        //logError("Please provide a table var" . "<br>\n");
        //return 0;
    
    }
    
    public function aasort (&$array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }
    
    public function n2br($message) {
        $order   = array("\r\n","\n");//, "\n", "\r");
        $replace = "\r\n<br />"; //we must use \r\n
        if (is_string($message))
        // Processes \r\n's first so they aren't converted twice.
        $message = str_replace($order, $replace, $message);
        else return null;
    
        return $message;
    }

    public function greeklish($string) {
        return strtr($string, array(
            'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
            'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'X', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'U', 'Φ' => 'F',
            'Χ' => 'H', 'Ψ' => 'PS', 'Ω' => 'O',
            'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
            'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'x', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
            'σ' => 's', 'τ' => 't', 'υ' => 'u', 'φ' => 'f', 'χ' => 'h', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
            'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'u', 'ώ' => 'o',
            'ϊ' => 'i', 'ϋ' => 'u',
            'ΐ' => 'i', 'ΰ' => 'u',
            'Ά' => 'A', 'Έ' => 'E', 'Ή' => 'I', 'Ό' => 'O', 'Ύ' => 'U', 'Ϋ' => 'U', 'Ώ' => 'O'
        ));
    }

    public function makeCodeFromString($code, $regexp = "/[^A-Za-z0-9 ]/" ) {
        $code = $this->greeklish($code);
        $code = preg_replace($regexp, ' ', $code); //convert to spaces all non-alpha and spaces
        //$brand = ucwords(strtolower($brand)); //make it camel case
        $code = str_replace(" ", "-", $code); //replace spaces with dash
        $code = strtolower($code);
        return $code ;
    }

    public function optimizeTable($table, $days = 20) {

        $date = date('Y-m-d H:i:s');
        if ($this->check_mysql()) {
            
            $queries[] = "DELETE FROM $table WHERE date_created < NOW() - INTERVAL $days DAY; ";
            $queries[] = "OPTIMIZE TABLE $table; ";
            
            foreach ($queries as $query) {
                $sql = mysqli_query($this->sql_link, $query);//reconnects if server is down
                if (!$sql) {
                    //check if query returned false
                    $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
                    $message .= 'Whole query: ' . $query;
                    $this->logError($message);
                    return 0; //return error
                }
            }
    
            return 1;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    
    }
    
    public function mysqli_result($res,$row=0,$col=0){ 
        $numrows = mysqli_num_rows($res); 
        if ($numrows && $row <= ($numrows-1) && $row >=0){
            mysqli_data_seek($res,$row);
            $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
            if (isset($resrow[$col])){
                return $resrow[$col];
            }
        }
        return false;
    }
    
    public function canRun() {
    
        if ($this->debugmode === true) return true;

        $oldtime = @file_get_contents($this->fileruntime);
          
        //$load = sys_getloadavg();
        //debug($load[0]);
        //if ($load[0] > 6) return false;
        foreach ($this->blockingentities as $entity) {
            if (file_exists('running/' .$entity .'Running.txt')){
                return false; //another instance of connector is running
            }
        }
        
        if ($oldtime !== false) { //if file exist    
            $datetime = time();
            $diff = $datetime - $oldtime ; //diff in seconds
            
            if ($diff > 3600) $this->logError("connector is not having HEARTBEAT for one hour. Take ACTION!!");
            
            if($diff > $this->maxtimewait) {
                return true; //too much time has passed, FORCE run again
            } else {
                return false; //wait, probably running other instance
            }           
        } else {
            return true; //file not exist. run
        }
            
    }

    public function allowRun() {
        unlink($this->fileruntime);
    }
    
    public function heartbeat() {
        file_put_contents($this->fileruntime, time());
    }
    
    public function connect() {
    
     //$this->user =       $username;
     //$this->password =   $password;

        if (!$this->token) { //if token is not povided try to get a token
            $data_string = array("username" => $this->user, "password" => $this->password);
            $this->token = $this->post("integration/admin/token", $data_string);
        }
    
    if ($this->token) {
            $this->headers = array("Content-Type: application/json", "Authorization: Bearer " .$this->token);
            return $this->token;
    } else {
        $this->errors .="Cannot login to magento.";
        return false;
    }
    return false;  
}

    public function get($theurl, $thesearch="", $usefile = false)
    {
        if ($usefile) {
            $data = json_decode(file_get_contents($this->entity .'array.json'), true);
        } else {
            if ($thesearch != "") {
                $temp = "";
                $iter = 0;

                foreach ($thesearch as $value) {
                    $temp .= "searchCriteria[filter_groups][0][filters][$iter][field]=" . urlencode($value[0]) . "&";
                    $temp .= "searchCriteria[filter_groups][0][filters][$iter][value]=" . urlencode($value[2]) . "&";
                    $temp .= "searchCriteria[filter_groups][0][filters][$iter][condition_type]=" . urlencode($value[1]) . "&";

                    $iter++;
                }

                $temp = trim($temp, "&");
                $url = $this->url . $theurl . "?" . $temp;
                $mageurl = $this->url . $theurl . "?" . $temp;
            } else
                $mageurl = $this->url . $theurl;

            $i = 1;
            $data = array();
            $pagesize = 500;
            while (true) {
                $pageing ='';
                if ($thesearch) $pageing = "&searchCriteria[pageSize]=$pagesize&searchCriteria[currentPage]=" . $i;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $mageurl . $pageing);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
                curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
                // curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_USERAGENT, "Elegento Connector");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

                $curl_response = curl_exec($ch);
                if ($curl_response === false) {
                    $info = curl_getinfo($ch);
                    curl_close($ch);
                    $this->logError('error occured during curl exec. Additional info: ' . var_export($info));
                    return FALSE;
                }
                curl_close($ch);
                // $r = iconv('windows-1253', "utf-8", );
                $decoded = json_decode($curl_response, true);
                if (is_array($decoded) && array_key_exists('message', $decoded)) {

                    if ($decoded['message'] != "Το ζητούμενο προϊόν δεν υπάρχει" && $decoded['message'] != "Requested product doesn't exist" && $decoded['message'] != "The product that was requested doesn't exist. Verify the product and try again.")
                        $this->logError('error occured: ' . $decoded['message'] . " data: " . $this->url . $theurl);

                    return FALSE;
                }


                $data = array_merge($data, $decoded['items']);

                echo $decoded['total_count'] . " < " . count($data);
                if ($decoded['total_count'] <= count($data)) break;
                $i++;
                if($i > 1000){
                    break;
                }
            }
        }
        if ($thesearch) {$decoded['items'] = $data; file_put_contents($this->entity ."array.json",json_encode($data));} //use aggregated data to return
        return $decoded;
    }
     
    public function getValue($theurl, $thevalue1="", $thevalue2="", $thevalue3="")
    {
        $ch = curl_init($this->url.$theurl); 
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        $result = (array)json_decode(curl_exec($ch));
        if ($thevalue3 != "")
            return $result["$thevalue1"]["$thevalue2"]["$thevalue3"];
        elseif ($thevalue2 != "")
            return $result["$thevalue1"]["$thevalue2"];
        elseif ($thevalue1 != "")
            return $result["$thevalue1"];
        else
            return $result;
    }
     
     
    public function post($theurl, $thedata)
    {
        $productData = json_encode($thedata);
        $ch = curl_init($this->url.$theurl); 
         
        $setHeaders = array('Content-Type:application/json','Authorization: Bearer '.$this->token);
        
        curl_setopt($ch,CURLOPT_POSTFIELDS, $productData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);        
        
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additional info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
       // $r = iconv('windows-1253', "utf-8", );
        $decoded = json_decode($curl_response, true);
        if(!is_array($decoded)){
            $decoded = json_decode($decoded, true);
        }
        if (is_array($decoded) && array_key_exists('message', $decoded)) {
            $this->logError('error occured: ' . $decoded['message']);
            return FALSE;
        }
        
        return $decoded;
    }
     
    public function put($theurl, $thedata)
    {
        $productData = json_encode($thedata);
        $ch = curl_init($this->url.$theurl); 
         
        $setHeaders = array('Content-Type:application/json','Authorization: Bearer '.$this->token);
        
        curl_setopt($ch,CURLOPT_POSTFIELDS, $productData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        // curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);        
        
        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additional info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
       // $r = iconv('windows-1253', "utf-8", );
        $decoded = json_decode($curl_response, true);
        if (is_array($decoded) && array_key_exists('message', $decoded)) {
            $this->logError('error occured: ' . $decoded['message']);
            return FALSE;
        }
        
        return $decoded;
    }
     
    public function delete($theurl)
    {
        //change space to %20 for url
//        $theurl = str_replace(" ", "%20", $theurl);
//
//        $ch = curl_init($this->url.$theurl);
//
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        $result = json_decode(curl_exec($ch));
//        return $result;




        $ch = curl_init($this->url.$theurl);

        $setHeaders = array('Content-Type:application/json','Authorization: Bearer '.$this->token);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/cacert.pem.txt");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        // curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT,"Elegento Connector");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $this->logError('error occured during curl exec. Additional info: ' . var_export($info));
            return FALSE;
        }
        curl_close($ch);
        $decoded = json_decode($curl_response, true);
        if (is_array($decoded) && array_key_exists('message', $decoded)) {
            $this->logError('error occured: ' . $decoded['message']);
            return FALSE;
        }

        return $decoded;
    }  
    
    public function getProductBySku($sku) {
        $sku = urlencode($sku);
        return $this->get("products/" .$sku); //get product data
    } 
    
    public function getAttributeSets($name = null) {
        $data =  array();
        $items =  array();
        $search = array();
        
        $search[] = array('entity_type_id','eq','4');
        if($name) $search[] = array('attribute_set_name','eq','4');
        $data = $this->get("eav/attribute-sets/list/", $search); //get product data
        foreach ($data['items'] as $item) {
            $items[$item['attribute_set_name']]=$item['attribute_set_id'];
        }
        $this->attributesets = $items;
        return $items;
    }

    public function getOrders($fromdate = null) {

        $search = array();

        if($fromdate) $search[] = array('updated_at','gt',$fromdate); //2017-10-11 04:00:00
        $data = $this->get("orders/", $search); //get orders data

        return $data['items'];
    }

    public function getOrder($orderid) {
        $data = $this->get("orders/" .$orderid); //get order data
        return $data;
    }

    public function getCustomer($customerid) {
        $data = $this->get("customers/" .$customerid); //get order data
        return $data;
    }

    public function createOrUpdateCustomer($customerData){
        $customerData = array('customerData' => $customerData);
        $data = $this->post('elegento-createorupdatecustomer/createorupdatecustomer', $customerData);
//        $data = json_decode($data, true);
        return (isset($data['success'])) ? $data['success'] : false ;
    }

    public function updateIntercomCustomer($customerData){
        $customerData = array('b2bCustomerData' => $customerData);
        $data = $this->post('elegento-intercomconnector/updateb2buserdata', $customerData);
        return (isset($data['success']) && $data['success'] && isset($data['status'])) ? $data['status'] : false ;
    }
    
	/**
     * @return array
     */
    public function getStoreViews() {
        $items = array();
        $data = $this->get("store/storeViews"); //get data
        foreach ($data as $item) {
            if ($item['code'] != 'admin') {
                $items[$item['code']] = $item;
            }
        }
        $this->stores = $items;
        return $items;
    }

    /**
     * @param $storecode
     * @return mixed
     */
    public function getStoreIdbyCode($storecode) {
        $storeid = $this->stores[$storecode]['id'];
        return $storeid;
    }

    public function updateOrCreateProduct($sku, $productdata) {
        $data = new StdClass();
        $data->product = $productdata;
//        if(stripos($sku, "/")){
//            //$sku = str_replace("/", "%2F", $sku); //%2F is the urlencode result of '/'. Below it gets encoded again so that we overcome the bug of magento which cannot accept '/' in urls of rest api. Being double encoded, the first decodind which will take place from Apache will result in %2F and the next decoding from magento will result in '/' which is the desired result.
//        }
        $sku = urlencode($sku);
        return $this->put("products/$sku",$data); //put data
    }

    public function setStockBySku($sku, $qty, $stockid = 1) {
        $data = new StdClass();
        $data->stockItem->qty = $qty;
        $data->stockItem->is_in_stock = true;
        $sku = urlencode($sku);
        return $this->put("products/$sku/stockItems/$stockid",$data); //put data
    }

    public function getStockBySku($sku, $stockid = 1) {
        $sku = urlencode($sku);
        return $this->get("stockItems/$sku/"); //get data
    }

    public function createProduct($sku, $data) {
        //TODO update
    }

    public function updateConfigurableProduct($sku, $data) {
        //TODO update
    }

    public function createConfigurableProduct($sku, $data) {
        //TODO update
    }

    public function getProductMediaBySku($sku) {
        $sku = urlencode($sku);
        return $this->get("products/" .$sku ."/media"); //get product data
    }
    public function getProductImage($sku, $imageid) {
        $sku = urlencode($sku);
        return $this->get("products/" .$sku ."/media/" .$imageid); //get product data
    }
    public function addProductMediaBySku($sku, $image) {
        $sku = urlencode($sku);
        $imageJsonData = json_encode($image);
       // $this->debug("image data before adding it: $imageJsonData");
        return $this->post("products/" .$sku ."/media",$image); //get product data
    }
    public function updateProductMediaBySku($sku, $image) {
        $sku = urlencode($sku);
        return $this->put("products/" .$sku ."/media/" .$image['entry']['id'],$image); //get product data
    }
    public function filterImageByPosition($images, $position) {
        foreach ($images as $image) {
            if ($image['position'] == $position) return $image;
        }
        return false;
    }

    public function findAndDeleteDuplicates($images, $sku){
        $existingPositions = array();
        foreach($images as $image) {
            if(isset($existingPositions[$image['position']])){
                $this->deleteProductMediaBySku($sku, $image['id']);
            }
            else{
                $existingPositions[$image['position']] = $image['id'];
            }
        }
    }

    public function imageExists($sku, $position){
        $productImages = $this->getProductMediaBySku($sku);
        $imageexists = $this->filterImageByPosition($productImages, $position);
        return is_array($imageexists) ? true : false;
    }

    public function deleteProductMediaBySku($sku, $imageID){
        $sku = urlencode($sku);
        return $this->delete("products/" .$sku ."/media/" .$imageID);
    }

    public function checkIfBase64IsEqual($existingImage, $importedImage){
        if($existingImage === false){
            return false;
        }
        $mediaUrl = $this->config['mage_url'] . '/pub/media/catalog/product';
        $existingImageUrl = $mediaUrl. $existingImage['file'];
        $fContents = file_get_contents($existingImageUrl);
        $existingBase64 = base64_encode(file_get_contents($existingImageUrl));

        $importedImageBase64 = $importedImage['entry']['content']['base64_encoded_data'];

        if($existingBase64 === $importedImageBase64){
            return true;
        }
        else{
            return false;
        }
    }

    public function makeImageData($photo, $sku,$name) {
        $fileName = substr($photo['orig_filename'], 0, (strrpos($photo['orig_filename'], "."))); //remove extension from filename

        $imageNameEnding = "_" .$photo['position'] ."_" .time() .".jpg";
        $charsRemainingForImageName = 90 - strlen($imageNameEnding); //90 chars is image name limit of Magento
        $imageNameStart = substr($name,0,$charsRemainingForImageName);

        $imageFullName = $imageNameStart .$imageNameEnding;


        $image = array("media_type"=>"image",
            "disabled" => false,
            "label"=>'',
            "position"=>$photo['position'],
            "file"=> $imageFullName,
        );

        $imgdata = base64_encode(file_get_contents($photo['filename']));
        $f = finfo_open();
        $mime_type = finfo_buffer($f, file_get_contents($photo['filename']), FILEINFO_MIME_TYPE);

        $image['content'] = array(
            "base64_encoded_data"=>$imgdata,
            "type"=> $mime_type,
            "name"=> $imageFullName,
        );
        if ($photo['position'] == 1) { //1st image is the base image
            $image['types'] = array("image", "small_image", "thumbnail");
        }
//        elseif($photo['position'] == 2){    //2nd one is the hoverimage
//            $image['types'] = array("hoverimage");
//        }

//        elseif($photo['position'] == 3){       //3rd one is the google image which we want it hidden in frontend
//            $image['types'] = array("googleimage");
//            $image['disabled'] = true; //this makes it hidden
//        }

        return array('entry' => $image);
    }


    /**
     * @param $attributecode
     * @return array
     */
    public function getAttributeOptions($attributecode, $filterbyname = null) {
        $items =  array();
        $data = $this->get("products/attributes/$attributecode/options"); //get data
        foreach ($data as $item) {
            if ($filterbyname && $item['label'] != $filterbyname) continue; //exclude everything except one
            if($item['value']) {
                $items[$item['label']] = $item['value'];
            }
        }
        return $items;
    }

    public function getAllAttributeOptions() {
        $data = $this->get("products/attributes?searchCriteria=0"); //get data
        $items = array();
        if ($data['total_count']) {
            $attributes = $data['items'];
            foreach ($attributes as $attribute) {
                foreach ($attribute['options'] as $option) {
                    if (strlen($option['label']) && $option['label'] != " ")
                        $items[$attribute['attribute_code']][$option['value']] = $option['label'];
                }
            }
        }

        $this->indexeddata = $items; //

        return $items;
    }

    public function getCategories() {
        $data = $this->get("categories?searchCriteria=0"); //get data
        $this->categoriesdata = $data;
        return $data;
    }

    public function optionId2LabelFormat($attributecode,$id) {
        if (isset($this->indexeddata[$attributecode][$id])) {
            // return $id .":" .$this->indexeddata[$attributecode][$id];
            return array('id' => $id, 'name' => $this->indexeddata[$attributecode][$id]);
        }
        return "";
    }

    /**
     * @param $attributecode
     * @param $label
     * @param null $value
     * @param null $translations
     * @return bool|mixed
     */
    public function setAttributeOptionOriginal($attributecode, $label, $value = null, $translations = null) {

        $option = new stdClass();
        $option->label = $label;

        $value ? $option->value = $value : ""; //if value exists UPDATE else CREATE

        if ($translations) {
            $option->store_labels = array();
            foreach ($translations as $storecode => $translation) {
                $storeid = $this->getStoreIdbyCode($storecode);
                if ($storeid && $translation)
                    $option->store_labels[] = array('store_id' => $storeid, 'label' => $translation);
            }
        }

										
        $data = $this->post("products/attributes/$attributecode/options", array("option" => $option)); //get data

        return $data;
    }

    /**
     * @param $attributecode
     * @param $label
     * @param null $value
     * @param null $translations
     * @return bool|mixed
     */
    public function setAttributeOption($attributecode, $label, $value = null, $translations = null) {

        $option = new stdClass();
        $option->label = $label;

        $value ? $option->value = $value : ""; //if value exists UPDATE else CREATE

        if ($translations) {
            $option->store_labels = array();
            foreach ($translations as $storecode => $translation) {
                $storeid = $this->getStoreIdbyCode($storecode);
                if ($storeid && $translation)
                    $option->store_labels[] = array('store_id' => $storeid, 'label' => $translation);
            }
        }


        $data = $this->post("elegento-createattributeoptionapi/createattributeoption/$attributecode", array("option" => $option)); //get data

        return $data;
    }



    public function getOptionIDbyErpId($attrcode, $s_id) {

        $s_id = $attrcode ."_" .$s_id;

        if ($this->check_mysql()) {
            //reconnects if server is down
            //$query = "SELECT * FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `attr_id` = " .$attr_id;
            $query = "SELECT option_id FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `attr_id` = '" .$attrcode ."'";

            $sql = mysqli_query($this->sql_link,$query);
            $variable=mysqli_result($sql,0,"option_id");

            return $variable;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    }

    public function getOptionID($s_id, $attrcode) {

        if ($this->check_mysql()) {
            //reconnects if server is down
            //$query = "SELECT * FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `attr_id` = " .$attr_id;
            $query = "SELECT option_id FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `attr_id` = '" .$attrcode ."'";

            $sql = mysqli_query($this->sql_link,$query);
            $variable=$this->mysqli_result($sql,0,"option_id");

            return $variable;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    }

    public function isOptionChangedAtERP($s_id, $label) {

        if ($this->check_mysql()) {
            //reconnects if server is down
            //$query = "SELECT * FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `attr_id` = " .$attr_id;
            $query = "SELECT option_id FROM `connector_map_options_id` WHERE `s_id` = '" .$s_id ."' AND `label` = '" .$label ."'";

            $sql = mysqli_query($this->sql_link,$query);
            $variable=$this->mysqli_result($sql,0,"option_id");

            if ($variable) return false;
            else return true;

        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    }

    public function addOptionID ($attrcode, $optionID, $s_id, $label) {

        $signature = crc32($attrcode .$optionID .$s_id .$label);
        $query = "INSERT INTO `connector_map_options_id` (`id`, `attr_id`, `option_id`, `s_id`, `label`, `signature`,`date`) VALUES (NULL, '$attrcode', '$optionID', '$s_id', '$label', '$signature', CURRENT_TIMESTAMP);";
        $sql = mysqli_query($this->sql_link,$query); //write connection between option_id and s_id
        if (!$sql) {
            //check if query returned false
            $message  = 'Invalid query: ' . mysqli_error($this->sql_link) . "<br>\n";
            $message .= 'Whole query: ' . $query;
            $this->logError($message);
            return 0; //return error
        }


        return $optionID;
    }
     
    public function token()
    {
        return $this->token;
    }
     
    public function url()
    {
        return $this->url;
    }

    public function getOrderByIncrementId($incrementid = null) {

        $search = array();

        if($incrementid) $search[] = array('increment_id','eq',$incrementid); //2017-10-11 04:00:00
        $data = $this->get("orders/", $search); //get orders data

        return $data['items'][0];
    }

    public function addOrderInvoice($orderid, $data) {
        return $this->post("order/" .$orderid ."/invoice",$data);
    }
    public function addOrderShipping($orderid, $data) {
        return $this->post("order/" .$orderid ."/ship",$data);
    }
    public function cancelOrder($orderid) {
        return $this->post("orders/" .$orderid ."/cancel");
    }
    public function creditOrder($orderid, $data) {
        return $this->post("order/" .$orderid ."/refund", $data);
    }

    public function forceStockUpdate($skus){
        $date = date('Y-m-d H:i:s');
        //  if ($table) {

        foreach($skus as $sku){
            $query = "SELECT * FROM `connector_stock` WHERE `entity_id` = '$sku' ORDER BY `id` DESC LIMIT 1 ";
            $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down
            if (!$sql) {
                //check if query returned false
                $message  = 'Invalid query: ' .mysqli_error($this->sql_link) . "<br>\n";
                $message .= 'Whole query: ' . $query;
                $this->logError($message);
                return 0; //return error
            }
            $rows=array();
            while ($row = mysqli_fetch_assoc($sql)) {
                $rows[]=$row;
            }
            foreach($rows as $row){
                $id = $row['id'];
                $query = "UPDATE `connector_stock` SET `status` = 0, `retries` = 0 WHERE `id` = $id";

                $sql = mysqli_query($this->sql_link,$query);//reconnects if server is down

                if (!$sql) {
                    //check if query returned false
                    $message  = 'Invalid query: ' .mysqli_error($this->sql_link) . "<br>\n";
                    $message .= 'Whole query: ' . $query;
                    $this->logError($message);
                    return 0; //return error
                }
            }
        }
    }

    function UpdateProductTranslations ($productData) {
        global $languages,$proxy,$sessionId, $attrcodes;
        try {
            $ret = 1;
            $sku = $productData->sku;
            $sku = urlencode($sku);
            $dataToUpdate = array();
            //$previousStoreCode = $this->getRestUrlByStoreCode();
            foreach($languages as $storeCode => $translationData ){
                foreach($translationData as $attributeCode => $label){


                    $dataToUpdate['sku'] = $sku;
                    $dataToUpdate['attributeCode'] = $attributeCode;
                    $dataToUpdate['attributeValue'] = $label;
                    $dataToUpdate['storeCode'] = $storeCode;
                    //$putParameters = ["param" => $dataToUpdate];
                    $this->put("elegento-updateattributesapi/updateattributes",$dataToUpdate); //put data

//                    if($attributeCode == 'name'){
//                        $dataToUpdate[$attributeCode] = $label;
//                    }
//                    else{
//                        if(!isset($dataToUpdate['custom_attributes'])){
//                            $dataToUpdate['custom_attributes'] = array();
//                        }
//                        $dataToUpdate['custom_attributes'][] = ['attribute_code' => $attributeCode, 'value' => $label];
//                    }
                }
               // $this->setRestUrlByStoreCode($storeCode);
               // $this->updateOrCreateProduct($sku, $dataToUpdate);
            }
           // $this->setRestUrlByStoreCode($previousStoreCode);   //reset url store code
//            foreach ($languages as $language => $langdata) {
//                $data = $productData['languages'][$language]; //get language data
//                foreach ($data as $key => $value) {
//                    if ($value == "null") $data[$key] = false; //empty string and use default if null
//                }
//                if (count($data)) {
//                    $ret &= $proxy->call($sessionId, 'product.update', array($sku, $data, $langdata['storeviewcode'],"sku"));
//                }
//            }



        } catch( SoapFault $fault ) {
            logError($fault);
            return 0;
        }
        return $ret;
    }

    public function getAllProductAttributes(){
        $params = [[
           'attribute_id',
           'gteq',
           '1'
        ]];
        $url = 'products/attributes';
        $productAttributes = $this->get($url, $params);
        foreach ($productAttributes['items'] as $productAttribute){
            if($productAttribute['frontend_input'] == 'select' || $productAttribute['frontend_input'] == 'multiselect'){
                $options = $productAttribute['options'];
                array_shift($options);
                foreach($options as $option){
                    $this->productAttributesOptions[$productAttribute['attribute_code']][$option['label']] = $option['value'];
                }
            }
        }
    }

    public function getProductFilterSize($realSize){
        $filterSizes = array();
        if($realSize == 'XS' || $realSize == 'S' || $realSize == 'O/S' || $realSize == 'S/M'){
            $filterSizes[] = strval(array_search('S', $this->indexeddata[$this->filterSizeAttributeCode]));
        }
        if($realSize == 'M' || $realSize == 'O/S' || $realSize == 'S/M'){
            $filterSizes[] = strval(array_search('M', $this->indexeddata[$this->filterSizeAttributeCode]));
        }
        if($realSize == 'L' || $realSize == 'XL' || $realSize == 'XXL' || $realSize == 'O/S' || $realSize == 'L/XL'){
            $filterSizes[] = strval(array_search('L', $this->indexeddata[$this->filterSizeAttributeCode]));
        }

        return $filterSizes;
    }

    public function getCategoriesMapping(){
        $data = $this->get('categories/list?searchCriteria');
        foreach ($data['items'] as $catData){
            $hasErpId = false;
            if(isset($catData['custom_attributes'])){
                foreach ($catData['custom_attributes'] as $custAttr){
                    if($custAttr['attribute_code']  == 'erp_id'){
                        $hasErpId = true;
                        //$this->categoriesMapping[$catData['id']] = $custAttr['value'];
                        $explodedErpIds = explode(",", $custAttr['value']);
                        foreach ($explodedErpIds as $erpId){
                            $erpId = trim($erpId);
                            $this->categoriesMapping[$erpId][] = $catData['id'];
                        }
                        break;
                    }
                }
                if($hasErpId === false){
                    $this->categoriesWithNoErpId[] = $catData['id'];
                }
            }
        }
        //return json_decode($data,true);
    }

    public function getErpCustomAttributes(){
        foreach ($this->attributes as $customAttribute){
            if(isset($customAttribute['magento_attribute_code']) && ($customAttribute['magento_attribute_code'])){
                $this->customAttributes[] = $customAttribute['magento_attribute_code'];
            }
        }
    }

    public function addOrderComment($orderid, $data) {
        return $this->post("orders/" .$orderid ."/comments", $data);
    }

    public function getLatestDuplicateAndDiscardOthers($line){
        if ($this->table && $this->sql_link) {
            $entity_id = $line['entity_id'];
            $query = "SELECT * FROM `{$this->table}` WHERE entity_id = '$entity_id' and (`status`= 0 OR status = 2) ORDER BY id desc limit 1";
            $sql = mysqli_query($this->sql_link,$query); //reconnects if server is down

            if (!$sql) {
                $this->logError("Failed to save to MySQL: " . mysqli_error($this->sql_link)); return false;
            }

            $row = mysqli_fetch_assoc($sql);

            if (count($row) and $row['id']) {
                $line = $row; //update with latest row
                $query = "UPDATE `{$this->table}` SET `status`= 4 WHERE entity_id = '$entity_id' and (status = 0 OR status = 2) and id < {$row['id']};";
                $sql = mysqli_query($this->sql_link,$query); //reconnects if server is down

                if (!$sql) {
                    $this->logError("Failed to save to MySQL: " . mysqli_error($this->sql_link)); return false;
                }
            }

            return $line;
        }
        $this->logError("Could not establish a valid SQL connection. Message: " . mysqli_error($this->sql_link) . "<br>\n");
        return 0;
    }

    public function areDataChangedFromLastUpdate($line, $ignorefromcompare, $dateFilter = true){

        $entity_id = $line['entity_id'];

        if($dateFilter){
            $query = "SELECT * FROM `{$this->table}` WHERE entity_id = '$entity_id' and status = 1 and id < {$line['id']} and date_created > SUBDATE(NOW(), INTERVAL 1 DAY) ORDER BY id desc limit 1";
        }
        else{
            $query = "SELECT * FROM `{$this->table}` WHERE entity_id = '$entity_id' and status = 1 and id < {$line['id']} ORDER BY id desc limit 1";
        }
        $sql = mysqli_query($this->sql_link,$query); //select previous succesful entry

        $row = mysqli_fetch_assoc($sql);

        $newdata = json_decode($line['data'], true);
        foreach ($ignorefromcompare as $ignore) {unset($newdata[$ignore]);}

        $olddata = json_decode($row['data'], true);
        foreach ($ignorefromcompare as $ignore) {unset($olddata[$ignore]);}

        if (json_encode($olddata) == json_encode($newdata))
            return false;
        else
            return true;

    }
}