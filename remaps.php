<?php
function remapProductVars($ProductData, $productexist = 0)
{ global $togascollection_id,$size_id,$color_id,$en_storeviewid,$isnewver;
	//if ((int)$ProductData['visibility']==1)
	//$ProductData['visibility']=4; //product visible on category and search

	//if (is_numeric($ProductData ['sku'])) {
	//	$ProductData ['sku']= 'm' .$ProductData ['sku']; //sku is nummeric add an m. else api will read it as id
	//}
	if ($isnewver == true && is_string($ProductData ['conf_attribute_ids'])) {
		$ProductData ['conf_attribute_ids']= str_replace("526", "1005", $ProductData ['conf_attribute_ids']);
		$ProductData ['conf_attribute_ids'] = str_replace("80", "272", $ProductData ['conf_attribute_ids']);
	}
	
	if ($ProductData['websites'])
	$ProductData['websites'] = array($ProductData['websites']);
	else $ProductData['websites'] = array("1");

	if ($ProductData['category']) //if exists
	$ProductData['categories'] = explode(";", $ProductData['category']); //convert to array

	if ($ProductData['description']) //if exists
	$ProductData['description'] = ($ProductData['description'] ); //add <br>
	if ($ProductData['short_description']) //if exists
	$ProductData['short_description'] = ($ProductData['short_description']); //add <br>


	$status=$ProductData['status'];
	if ($productexist == 0 || $status) //if product doesmt exist use this feild, else only update if attribute exists
		$ProductData ['status']=is_null($status) ? null : (int)$status;

	$url_key = $ProductData['url_key'];
	//if url key not specified use sku
	if ($url_key) //new product
		$ProductData ['url_key']=$url_key;

	$weight = $ProductData['weight'];
	if ($productexist == 0 || $weight) //new product
		$ProductData ['weight']=$weight ? (float)$weight : 1;
	else if (!$weight) 
		$ProductData ['weight'] = 1;

	$taxclass_id = $ProductData['tax_class_id'];
	if ($productexist == 0 || $taxclass_id) //new product
		$ProductData ['tax_class_id']=is_numeric($taxclass_id) ? (int)$taxclass_id : null;

	$price = $ProductData['price'];
	if ($productexist == 0 || $price) //new product
		$ProductData['price'] = $price ? (float)$price : null;

	$val = $ProductData['news_from_date'];
	if ($val) {
		$date = new DateTime($val);
		$ProductData ['news_from_date']=$date->format('Y-m-d H:i:s');
	}
	
	
	$val = $ProductData['news_to_date'];
	if ($val) {
	$date = new DateTime($val);
	$ProductData ['news_to_date']=$date->format('Y-m-d H:i:s');
	}
	
	$val = $ProductData['special_from_date'];
	if ($val) {
	$date = new DateTime($val);
	$ProductData ['special_from_date']=$date->format('Y-m-d H:i:s');
	}
	
	$val = $ProductData['special_to_date'];
	if ($val) {
	$date = new DateTime($val);
	$ProductData ['special_to_date']=$date->format('Y-m-d H:i:s');
	}
	
	$qty = $ProductData['qty'];
	if ($productexist == 0) //new product
		$ProductData ['qty']=$qty ? (int)$qty : 0;

	$is_in_stock = $ProductData['is_in_stock'];
	if ($productexist == 0 || $is_in_stock) //new product
		$ProductData ['is_in_stock']=$is_in_stock ? (int)$is_in_stock : "1";

	$use_config_manage_stock = $ProductData['use_config_manage_stock']; //set this always to 
	$ProductData ['use_config_manage_stock']=$use_config_manage_stock ? (int)$use_config_manage_stock : "1";

	$manage_stock = $ProductData['manage_stock'];
	$ProductData ['manage_stock']=$manage_stock ? (int)$manage_stock : "1";
	
	$yml = $ProductData['yml'];
	if ($productexist == 0 || $yml) //new product
		$ProductData ['yml']=$yml ? (int)$yml : "1";
	
	$isfamous = $ProductData['isfamous'];
	if ($productexist == 0 || $isfamous) //new product
		$ProductData ['isfamous']=$isfamous ? (int)$isfamous : "0";
	

	//################ color ########################
	$s_id=$ProductData['color_sid']; //get s_id (from xml)
	$optionVal=$ProductData['color_label'];//get label from xml

	if (!empty($s_id) && !empty($optionVal))
	{
		$optionID=optionMake($color_id, $s_id, $optionVal); //update or create color value options (80 is color id)
		if ($optionID) {
			$ProductData['color']=(int)$optionID; //update flavor dropdown value
			updateOptionLabelPerStore($optionID,$ProductData['color_label_en'],$en_storeviewid);
		}
		else
		logError("Error with color optionID = $optionID, color_sid= $s_id. color_label= $optionVal");
	}
	else if ( !empty($s_id) && empty($optionVal))
	logError("Color option_label is missing. color_sid= $s_id. color_label= $optionVal");

	//################ size ########################
	$s_id=$ProductData['size_sid']; //get s_id (from xml)
	$optionVal=$ProductData['size_label'];//get label from xml

	if (!empty($s_id) && !empty($optionVal))
	{
		$optionID=optionMake($size_id, $s_id, $optionVal); //update or create size value options (526 is size id)
		if ($optionID){
			$ProductData['size']=(int)$optionID; //update size dropdown value
			updateOptionLabelPerStore($optionID,$ProductData['size_label_en'],$en_storeviewid);
		}

		else
		logError("Error with size optionID = $optionID, size_sid= $s_id. size_label= $optionVal");
	}
	else  if ( !empty($s_id) && empty($optionVal))
	logError("Size option is missing. size_sid= $s_id. size_label= $optionVal");
	
	
	//################ togascollection ########################
	$s_id=$ProductData['togascollection_sid']; //get s_id (from xml)
	$optionVal=$ProductData['togascollection_label'];//get label from xml
	
	if (!empty($s_id) && !empty($optionVal))
	{
		$optionID=optionMake($togascollection_id, $s_id, $optionVal); //update or create togascollection value options (527 is togascollection id)
		if ($optionID){
			$ProductData['togascollection']=(int)$optionID; //update size dropdown value
			updateOptionLabelPerStore($optionID,$ProductData['togascollection_label_en'],$en_storeviewid); //update translation
		}
	
		else
		logError("Error with togascollection optionID = $optionID, togascollection_sid= $s_id. togascollection_label= $optionVal");
	}
	else  if ( !empty($s_id) && empty($optionVal))
	logError("togascollection option is missing. togascollection_sid= $s_id. togascollection_label= $optionVal");

	return $ProductData;
}

function order_getitems($var){
	$out = array();
	$out['item_sku'] = $var['item_sku'];
	$out['item_id'] = $var['item_item_id'];
	$out['item_qty_ordered'] = $var['item_qty_ordered'];
	$out['item_base_cost'] = $var['item_base_cost'];
	$out['item_price'] = $var['item_price'];
	$out['item_row_total'] = $var['item_row_total'];
	//$out['item_discount_percent'] = $var['item_discount_percent'];
	//$out['item_discount_amount'] = $var['item_discount_amount'];
	$out['item_tax_percent'] = $var['item_tax_percent'];
	$out['item_tax_amount'] = $var['item_tax_amount'];
	$out['item_name'] = $var['item_name'];
	$out['item_cost'] = $var['item_base_cost'];
	$out['item_description'] = $var['item_description'];
	$out['item_product_type'] = $var['item_product_type'];
	$out['item_product_options'] = $var['item_product_options'];
	$gv = unserialize($var['item_product_options']);
	$gv = $gv['info_buyRequest'];
	$out['CCCVOUCHER2FRIEND'] = $gv['send_friend'];
	$out['CCCFRIENDSNAME'] = $gv['recipient_name'];
	$out['CCCFRIENDSEMAIL'] = $gv['recipient_email'];
	$out['CCCPOSTGIFTVOUCHER'] = $gv['recipient_ship'];
	$out['CCCVOUCHERMESSAGE'] = $gv['message'];

	return $out;
}

function order_getOrderAttributes($var){
	$out = array();
	$out['customer_id'] = $var['customer_id'];
	$out['created_at'] = $var['created_at'];
	$out['grand_total'] = $var['grand_total'];
	$out['tax_amount'] = $var['tax_amount'];
	$out['subtotal'] = $var['subtotal'];
	$out['discount_amount'] = (float)$var['discount_amount']*-1;
	$out['total_qty_ordered'] = $var['total_qty_ordered'];
	$out['total_item_count'] = $var['total_item_count'];
	$out['increment_id'] = $var['increment_id'];
	$out['order_id'] = $var['order_id'];
	$out['shipping_postcode'] = $var['shipping_address']['postcode'] ? $var['shipping_address']['postcode'] : $var['billing_address']['postcode'];
	$out['shipping_lastname'] = $var['shipping_address']['lastname'] ? $var['shipping_address']['lastname'] : $var['billing_address']['lastname'];
	$out['shipping_street'] = $var['shipping_address']['street'] ? $var['shipping_address']['street'] : $var['billing_address']['street'];
	$out['shipping_city'] = $var['shipping_address']['city'] ? $var['shipping_address']['city'] : $var['billing_address']['city'];
	$out['shipping_email'] = $var['shipping_address']['email'] ? $var['shipping_address']['email'] : $var['billing_address']['email'];
	$out['shipping_telephone'] = $var['shipping_address']['telephone'] ? $var['shipping_address']['telephone'] : $var['billing_address']['telephone'];
	$out['shipping_firstname'] = $var['shipping_address']['firstname'] ? $var['shipping_address']['firstname'] : $var['billing_address']['firstname'];
	$out['shipping_company'] = $var['shipping_address']['company'] ? $var['shipping_address']['company'] : $var['billing_address']['company'];
	$out['shipping_method'] = $var['shipping_method'];
	$out['shipping_amount'] = $var['shipping_amount'];
	$out['shipping_description'] = $var['shipping_description'];
	$out['payment_method'] = $var['payment']['method'];
	$out['comments'] = $var['gift_message'];
	$out['customer_note'] = $var['customer_note'];
	$out['status'] = $var['status'];
	$out['customer_lastname'] = $var['customer_lastname'];
	$out['coupon_code'] = $var['coupon_code'];


	return $out;
}

function remapOrderStatus($orderData) {
	
	return $orderData;
}

?>