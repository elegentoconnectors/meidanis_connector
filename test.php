<?php
include("commonM2.php");

$api = new m2;

$api->readConf("confs/conf.xml"); //read magento and db config

//$api->url =   "https://matfashion.elegento.com/rest/en/V1/";
$api->errorlogfile = 'errors/ProductImportErrors.log';
$api->debugfile = 'debug/DebugProductImport.txt';

$api->init_connector(); //get token
$v = $api->getProductMediaBySku('7401.7103.1-black');
$v = $api->getAllAttributeOptions();
$v = $api->getAttributeSets();
$api->setRestUrlByStoreCode('en');
$v = $api->getProductBySku('631.1283-1');
$v = $api->getAttributeOptions('color');
$v = $api->getAttributeOptions('color',"black44");
$v = $api->getStoreViews();

$images = $api->getProductMediaBySku('7301.4025.1');
$v = $api->filterImageByPosition($images, '1');
$v = $api->getProductImage('7301.4025.1', '6482');
$image = '{
  "entry": {
  "id": 6481,
    "media_type": "image",
    "label": "test",
    "position": 9,
    "disabled": false,
    "types": [
      "thumbnail"
    ],
    "file": "test.png",
    "content": {
      "base64_encoded_data": "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAQAAAAnOwc2AAAAEUlEQVR42mNkqGfAAIxDWRAAOIQFAap6xDkAAAAASUVORK5CYII=",
      "type": "image/png",
      "name": "test.png"
    }
  }
}';
$v = $api->updateProductMediaBySku('TEST-180112360014170',json_decode($image,true));
print_r($v);

exit();
//$v = $api->getProductBySku("1801123600141703");
//$v = $api->get("eav/attribute-sets/1");
$v = $api->getAttributeOptions('color');
$v = $api->getAttributeOptions('color',"black44");
$v = $api->getStoreViews();
$v = $api->getStoreIdbyCode('el2');
$v = $api->setAttributeOption("color",'test2',"20",array('el'=>'τεστ', 'en'=>'test3'));
print_r($v);
//$api->headers = array("Content-Type: application/json", "Authorization: Bearer " .$api->token);

if ($api->token) {
    $search = array(
      //  array ("id", "eq", "2047"), // this one wil NOT work
        array ("id", "eq", "1"), // this one is FINE !
    );
   // $retour = $api->get("customers/1"); //get customer data
   // $retour = $api->get("products/test"); //get product data
   $v = $api->getAttributeSets();
   $retour = $api->getProductBySku("test");
   
   $search = array(
   // array ("entity_id", "eq", "1"),
    array ("sku", "like", "%test-1%"),
);
//$retour = $api->get("products", $search);
//$data = getProductData();
//$retour = $api->put("products/11", $data);
}


var_dump($retour);

function getProductData() {
        
    $post ='{
      "product": {
        "sku": "MY_SKU11",
        "name": "test name",
        "attribute_set_id": "4",
        "price": 110,
        "status": 1,
        "visibility": 4,
        "typeId": "simple",
        "weight": 10,
        "extensionAttributes": {
          "stockItem": {
            "stockId": 1,
            "qty": 20,
            "isInStock": true,
            "isQtyDecimal": false,
            "useConfigMinQty": true,
            "minQty": 0,
            "useConfigMinSaleQty": 0,
            "minSaleQty": 0,
            "useConfigMaxSaleQty": true,
            "maxSaleQty": 0,
            "useConfigBackorders": false,
            "backorders": 0,
            "useConfigNotifyStockQty": true,
            "notifyStockQty": 20,
            "useConfigQtyIncrements": false,
            "qtyIncrements": 0,
            "useConfigEnableQtyInc": false,
            "enableQtyIncrements": false,
            "useConfigManageStock": true,
            "manageStock": true,
            "lowStockDate": "string",
            "isDecimalDivided": true,
            "stockStatusChangedAuto": 0,
            "extensionAttributes": {}
          }
        },
        "options": [],
        "tierPrices": [],
        "customAttributes": [
            {
                "attributeCode": "special_price",
                "value": "12"
            },
            {
                "attribute_code": "description",
                "value": "simple description"
            },
            {
                "attribute_code": "short_description",
                "value": "short desc desktription"
            },
            {
                "attribute_code": "category_ids",
                "value": ["4", "3"]
            }
            
        ]
      },
      "saveOptions": true
    }';
    
    return json_decode($post);
}
